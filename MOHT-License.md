MOH Office for Healthcare Transformation (MOHT) CONFIDENTIAL

This software and associated documentation files (the “Software”) are subject to copyright protection under the laws of Singapore and through international treaties, other countries.  The copyright in this Software is owned by MOHT.  However, the copyright in some contents incorporated in this Software may be owned by third parties, where so indicated.
 
No part of this Software may be used, copied, modified, merged, reproduced, licensed, sub-licensed, sold, published, transmitted, modified, adapted, publicly displayed, and/or disseminated (including storage in any medium by electronic means whether or not transiently for any purpose, except when permitted herein) without MOHT’s prior written permission. 
 
Copyright © 2018-2019 MOH Office for Healthcare Transformation
