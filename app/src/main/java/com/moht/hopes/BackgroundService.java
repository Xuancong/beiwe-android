package com.moht.hopes;

import static com.moht.hopes.survey.SurveyScheduler.deactivateSurvey;
import static com.moht.hopes.survey.SurveyScheduler.isSurveyPushType;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.AppOpsManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.app.usage.UsageStats;
import android.app.usage.UsageStatsManager;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.hardware.SensorManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.PowerManager;
import android.os.SystemClock;
import androidx.annotation.RequiresApi;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.provider.Settings;
import android.service.notification.StatusBarNotification;
import android.util.Log;
import android.view.accessibility.AccessibilityManager;

import com.moht.hopes.listeners.AccelerometerListener;
import com.moht.hopes.listeners.AmbientLightListener;
import com.moht.hopes.listeners.BluetoothListener;
import com.moht.hopes.listeners.BluetoothStateListener;
import com.moht.hopes.listeners.CallListener;
import com.moht.hopes.listeners.GPSListener;
import com.moht.hopes.listeners.GyroscopeListener;
import com.moht.hopes.listeners.MMSSentLogger;
import com.moht.hopes.listeners.MagnetoListener;
import com.moht.hopes.listeners.PowerStateListener;
import com.moht.hopes.listeners.SignificantMotionListener;
import com.moht.hopes.listeners.SmsSentLogger;
import com.moht.hopes.listeners.SociabilityCallLogger;
import com.moht.hopes.listeners.SociabilityMsgLogger;
import com.moht.hopes.listeners.PedometerListener;
import com.moht.hopes.listeners.TapsListener;
import com.moht.hopes.listeners.UsageListener;
import com.moht.hopes.listeners.WifiListener;
import com.moht.hopes.networking.PostRequest;
import com.moht.hopes.networking.SurveyDownloader;
import com.moht.hopes.storage.EncryptionEngine;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.survey.SurveyRecorder;
import com.moht.hopes.survey.SurveyScheduler;
import com.moht.hopes.ui.user.LoginActivity;
import com.moht.hopes.ui.user.MainMenuActivity;
import com.moht.hopes.ui.utils.SurveyNotifications;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.SortedMap;
import java.util.TreeMap;

@SuppressLint("StaticFieldLeak")
public class BackgroundService extends Service {
	private static String TAG = "BackgroundService";
	public static Context appContext;

	public AccelerometerListener accelerometerListener;
	public AmbientLightListener ambientLightListener;
	public BluetoothListener bluetoothListener;
	public BluetoothStateListener bluetoothStateListener;
	public CallListener callListener;
	public GPSListener gpsListener;
	public GyroscopeListener gyroscopeListener;
	public MagnetoListener magnetoListener;
	public UsageListener usageListener;
	public PedometerListener pedometerListener;
	public PowerStateListener powerStateListener;
	public WifiListener wifiListener;
	public SmsSentLogger smsSentLogger;
	public MMSSentLogger mmsSentLogger;
	public TapsListener tapsListener;
	public SignificantMotionListener significantMotionListener;
	public SociabilityCallLogger sociabilityCallLogger = null;
	public SociabilityMsgLogger sociabilityMsgLogger = null;


	public static Timer timer;

	//localHandle is how static functions access the currently instantiated background service.
	//It is to be used ONLY to register new surveys with the running background service, because
	//that code needs to be able to update the IntentFilters associated with timerReceiver.
	//This is Really Hacky and terrible style, but it is okay because the scheduling code can only ever
	//begin to run with an already fully instantiated background service.
	public static BackgroundService localHandle;
	public static boolean isTapAdded = false, finalSetupDone = false;
	public static Activity activity = null;
	public static ActivityManager activityManager = null;
	public static UsageStatsManager usageStatsManager = null;
	public static ApplicationInfo appInfo = null;
	public static AppOpsManager opsManager = null;
	public static AccessibilityManager accessibilityManager = null;
	public static SensorManager sensorManager = null;
	public static PackageManager packageManager = null;
	private static NotificationManager mNotificationManager;
	private static boolean notify = false;

	@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
	@Override
	/* onCreate is essentially the constructor for the service, initialize variables here. */
	public void onCreate() {
		mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		appContext = this.getApplicationContext();
		activityManager = (ActivityManager) getSystemService(ACTIVITY_SERVICE);
		usageStatsManager = (UsageStatsManager) getSystemService(USAGE_STATS_SERVICE);
		accessibilityManager = (AccessibilityManager)getSystemService(Context.ACCESSIBILITY_SERVICE);
		packageManager = appContext.getPackageManager();
		sensorManager = (SensorManager) appContext.getSystemService(Context.SENSOR_SERVICE);
		try {
			appInfo = getPackageManager().getApplicationInfo(getPackageName(), 0);
		} catch (Exception e) {
			appInfo = null;
		}
		opsManager = (AppOpsManager) appContext.getSystemService( Context.APP_OPS_SERVICE );

		Thread.setDefaultUncaughtExceptionHandler(new CrashHandler(appContext));
		PersistentData.initialize( appContext );
		TextFileManager.initialize( appContext, false );
		PostRequest.initialize( appContext );
		localHandle = this;  //yes yes, hacky, I know.
		registerTimers(appContext);

		doSetup();
	}

	public String getForegroundAppName() {
		String topPackageName = null;
		long time = System.currentTimeMillis();
		List<UsageStats> stats = usageStatsManager.queryUsageStats(UsageStatsManager.INTERVAL_DAILY, time - 1000*1000, time);
		if (stats != null) {
			SortedMap<Long, UsageStats> runningTask = new TreeMap<>();
			for (UsageStats usageStats : stats) {
				runningTask.put(usageStats.getLastTimeUsed(), usageStats);
			}
			topPackageName = runningTask.isEmpty()?"None": Objects.requireNonNull(runningTask.get(runningTask.lastKey())).getPackageName();
		}
		return topPackageName;
	}

	public void doSetup() {
		if ( PersistentData.getEnabled(PersistentData.POWER_STATE) && powerStateListener==null )
			powerStateListener = newPowerStateListener();
		if ( bluetoothStateListener==null )
			bluetoothStateListener = newBluetoothStateListener();
		if ( PersistentData.getEnabled(PersistentData.GPS) && gpsListener==null )
			gpsListener = new GPSListener(appContext); // Permissions are checked in the broadcast receiver
		if ( PersistentData.getEnabled(PersistentData.WIFI) && wifiListener==null )
			wifiListener = WifiListener.initialize( appContext );
		if ( PersistentData.getEnabled(PersistentData.ACCELEROMETER) && accelerometerListener==null )
			accelerometerListener = new AccelerometerListener( appContext );
		if ( PersistentData.getEnabled(PersistentData.AMBIENTLIGHT) && ambientLightListener==null )
			ambientLightListener = new AmbientLightListener( appContext );
		if ( PersistentData.getEnabled(PersistentData.GYROSCOPE) && gyroscopeListener==null )
			gyroscopeListener = new GyroscopeListener( appContext );
		if ( PersistentData.getEnabled(PersistentData.MAGNETOMETER) && magnetoListener==null )
			magnetoListener = new MagnetoListener( appContext );
		if ( PersistentData.getEnabled(PersistentData.STEPS) && pedometerListener ==null )
			pedometerListener = new PedometerListener( appContext );
		if ( PersistentData.getEnabled(PersistentData.USAGE) && usageListener==null )
			usageListener = new UsageListener( appContext );
		if ( PersistentData.getEnabled(PersistentData.TAPS) && !isTapAdded )
			tapsListener = new TapsListener( this );

		//Bluetooth, wifi, gps, calls, and texts need permissions
		if( PersistentData.getEnabled(PersistentData.BLUETOOTH) && bluetoothListener==null ) {
			if ( PermissionHandler.confirmBluetooth(appContext) )
				startBluetooth();
		}

		if( PersistentData.getEnabled(PersistentData.TEXTS) ){
			if (!PermissionHandler.confirmTexts(appContext))
				sendBroadcast(Timer.checkForSMSEnabled);
			else{
				if(smsSentLogger==null)
					smsSentLogger = startSmsSentLogger();
				if(mmsSentLogger==null)
					mmsSentLogger = startMmsSentLogger();
			}
		}

		if ( PersistentData.getEnabled(PersistentData.SOCIABILITY_CALL) )
			startSociabilityCallLogger();

		if ( PersistentData.getEnabled(PersistentData.SOCIABILITY_MSG) )
			startSociabilityMsgLogger();

		if( PersistentData.getEnabled(PersistentData.CALLS) && callListener ==null ) {
			if (PermissionHandler.confirmCalls(appContext))
				callListener = startCallLogger();
			else
				sendBroadcast(Timer.checkForCallsEnabled);
		}

		//Only do the following if the device is registered
		if ( PersistentData.isRegistered() ) {
			DeviceInfo.initialize( appContext ); //if at registration this has already been initialized. (we don't care.)			
			startTimers();
		}
	}
	
	/** Stops the BackgroundService instance. */
	public void stop() { if (BuildConfig.APP_IS_DEBUG) { this.stopSelf(); } }
	
	/*#############################################################################
	#########################         Starters              #######################
	#############################################################################*/
	
	/** Initializes the Bluetooth listener 
	 * Note: Bluetooth has several checks to make sure that it actually exists on the device with the capabilities we need.
	 * Checking for Bluetooth LE is necessary because it is an optional extension to Bluetooth 4.0. */
	@SuppressLint("LongLogTag")
	public void startBluetooth(){
		//Note: the Bluetooth listener is a BroadcastReceiver, which means it must have a 0-argument constructor in order for android can instantiate it on broadcast receipts.
		//The following check must be made, but it requires a Context that we cannot pass into the BluetoothListener, so we do the check in the BackgroundService.
		if ( appContext.getPackageManager().hasSystemFeature( PackageManager.FEATURE_BLUETOOTH_LE ) && PersistentData.getEnabled(PersistentData.BLUETOOTH) ) {
			this.bluetoothListener = new BluetoothListener();
			if ( this.bluetoothListener.isBluetoothEnabled() ) {
//				Log.i("Background Service", "success, actually doing bluetooth things.");
				registerReceiver(this.bluetoothListener, new IntentFilter("android.bluetooth.adapter.action.STATE_CHANGED") );
			} else {
				//TODO: Low priority. Eli. Track down why this error log pops up, cleanup.  -- the above check should be for the (new) doesBluetoothCapabilityExist function instead of isBluetoothEnabled
				if(BuildConfig.APP_IS_DEBUG) Log.e("Background Service", "bluetooth Failure. Should not have gotten this far.");
				TextFileManager.getDebugLogFile().write("BackgroundService::startBluetooth: Failure, should not have gotten to this line of code");
			}
		} else {
			if (PersistentData.getEnabled(PersistentData.BLUETOOTH)) {
				TextFileManager.getDebugLogFile().write(
						"BackgroundService::startBluetooth: " +
								"BackgroundService bluetooth init, " +
								"Device does not support bluetooth LE, bluetooth features disabled");
				if(BuildConfig.APP_IS_DEBUG) Log.w("BackgroundService bluetooth init", "Device does not support bluetooth LE, bluetooth features disabled."); }
			this.bluetoothListener = null;
		}
	}
	
	/** Initializes the sms logger. */
	public SmsSentLogger startSmsSentLogger() {
		SmsSentLogger smsSentLogger = new SmsSentLogger(new Handler(), appContext);
		this.getContentResolver().registerContentObserver(Uri.parse("content://sms/"), true, smsSentLogger);
		return smsSentLogger;
	}
	
	public MMSSentLogger startMmsSentLogger(){
		MMSSentLogger mmsMonitor = new MMSSentLogger(new Handler(), appContext);
		this.getContentResolver().registerContentObserver(Uri.parse("content://mms/"), true, mmsMonitor);
		return mmsMonitor;
	}

	public void startSociabilityCallLogger() {
		if (sociabilityCallLogger==null) {
			sociabilityCallLogger = new SociabilityCallLogger();
			LocalBroadcastManager.getInstance(appContext).registerReceiver(sociabilityCallLogger,
					new IntentFilter("com.moht.hopes.action.CALL_PROCESSED"));
		}
	}

	public void startSociabilityMsgLogger() {
		if (sociabilityMsgLogger==null) {
			sociabilityMsgLogger = new SociabilityMsgLogger();
			LocalBroadcastManager.getInstance(appContext).registerReceiver(sociabilityMsgLogger,
					new IntentFilter("com.moht.hopes.action.MESSAGE_PROCESSED"));
		}
	}

	/** Initializes the call logger. */
	private CallListener startCallLogger() {
		CallListener callListener = new CallListener(new Handler(), appContext);
		this.getContentResolver().registerContentObserver(Uri.parse("content://call_log/calls/"), true, callListener);
		return callListener;
	}
	
	/** Initializes the PowerStateListener. 
	 * The PowerStateListener requires the ACTION_SCREEN_OFF and ACTION_SCREEN_ON intents
	 * be registered programatically. They do not work if registered in the hopes's manifest.
	 * Same for the ACTION_POWER_SAVE_MODE_CHANGED and ACTION_DEVICE_IDLE_MODE_CHANGED filters,
	 * though they are for monitoring deeper power state changes in 5.0 and 6.0, respectively. */
	@SuppressLint("InlinedApi")
	private PowerStateListener newPowerStateListener() {
		IntentFilter filter = new IntentFilter();
		PowerStateListener ret = new PowerStateListener();
		filter.addAction(Intent.ACTION_SCREEN_ON);
		filter.addAction(Intent.ACTION_SCREEN_OFF);
		filter.addAction(Intent.ACTION_USER_PRESENT);
		filter.addAction(Intent.ACTION_SHUTDOWN);
		filter.addAction(Intent.ACTION_REBOOT);
		filter.addAction(PowerManager.ACTION_POWER_SAVE_MODE_CHANGED);
		filter.addAction(PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED);

		registerReceiver(ret, filter);
		PowerStateListener.start(appContext);
		return ret;
	}

	private BluetoothStateListener newBluetoothStateListener() {
		IntentFilter filter = new IntentFilter();
		filter.addAction(BluetoothAdapter.ACTION_STATE_CHANGED);
		BluetoothStateListener ret = new BluetoothStateListener();
		registerReceiver(ret, filter);
		BluetoothStateListener.start();
		return ret;
	}
	
	
	/** create timers that will trigger events throughout the program, and
	 * register the custom Intents with the controlMessageReceiver. */
	@SuppressWarnings("static-access")
	public static void registerTimers(Context appContext) {
		localHandle.timer = new Timer(localHandle);
		IntentFilter filter = new IntentFilter();
		filter.addAction( appContext.getString( R.string.turn_accelerometer_off ) );
		filter.addAction( appContext.getString( R.string.turn_accelerometer_on ) );
		filter.addAction( appContext.getString( R.string.turn_ambientlight_on ) );
		filter.addAction( appContext.getString( R.string.turn_bluetooth_off ) );
		filter.addAction( appContext.getString( R.string.turn_bluetooth_on ) );
		filter.addAction( appContext.getString( R.string.turn_gps_off ) );
		filter.addAction( appContext.getString( R.string.turn_gps_on ) );
		filter.addAction( appContext.getString( R.string.turn_gyroscope_off ) );
		filter.addAction( appContext.getString( R.string.turn_gyroscope_on ) );
		filter.addAction( appContext.getString( R.string.turn_magnetometer_off ) );
		filter.addAction( appContext.getString( R.string.turn_magnetometer_on ) );
		filter.addAction( appContext.getString( R.string.signout_intent ) );
		filter.addAction( appContext.getString( R.string.voice_recording ) );
		filter.addAction( appContext.getString( R.string.update_usage ) );
		filter.addAction( appContext.getString( R.string.run_wifi_log ) );
		filter.addAction( appContext.getString( R.string.upload_data_files_intent ) );
		filter.addAction( appContext.getString( R.string.create_new_data_files_intent ) );
		filter.addAction( appContext.getString( R.string.check_for_new_surveys_intent ) );
		filter.addAction( appContext.getString( R.string.check_for_sms_enabled ) );
		filter.addAction( appContext.getString( R.string.check_for_calls_enabled ) );
		filter.addAction( appContext.getString( R.string.dismiss_survey_notification ) );
		filter.addAction( appContext.getString( R.string.expire_survey_notification ) );
		filter.addAction( ConnectivityManager.CONNECTIVITY_ACTION );
		filter.addAction( "crashBeiwe" );
		filter.addAction( "enterANR" );
		List<String> surveyIds = PersistentData.getSurveyIds();
		for (String surveyId : surveyIds) { filter.addAction(surveyId); }
		appContext.registerReceiver(localHandle.timerReceiver, filter);
	}
	
	/*#############################################################################
	####################            Timer Logic             #######################
	#############################################################################*/
	
	public void startTimers() {
		if( PersistentData.isUnregisterDebugMode ) return;

		long now = System.currentTimeMillis();

		if (PersistentData.getEnabled(PersistentData.ACCELEROMETER)) {  //if accelerometer data recording is enabled and...
			if(PersistentData.getMostRecentAlarmTime( getString(R.string.turn_accelerometer_on )) < now || //the most recent accelerometer alarm time is in the past, or...
					!timer.alarmIsSet(Timer.accelerometerOnIntent) ) { //there is no scheduled accelerometer-on timer.
				sendBroadcast(Timer.accelerometerOnIntent); // start accelerometer timers (immediately runs accelerometer recording session).
				//note: when there is no accelerometer-off timer that means we are in-between scans.  This state is fine, so we don't check for it.
			} else if(timer.alarmIsSet(Timer.accelerometerOffIntent)
					&& PersistentData.getMostRecentAlarmTime(getString( R.string.turn_accelerometer_on )) - PersistentData.getAccelerometerOffDurationMilliseconds() + 1000 > now ) {
				accelerometerListener.turn_on();
			}
		}

		if (PersistentData.getEnabled(PersistentData.GYROSCOPE)) {
			if(PersistentData.getMostRecentAlarmTime( getString(R.string.turn_gyroscope_on )) < now || //the most recent accelerometer alarm time is in the past, or...
					!timer.alarmIsSet(Timer.gyroscopeOnIntent) ) {
				sendBroadcast(Timer.gyroscopeOnIntent);
			} else if(timer.alarmIsSet(Timer.gyroscopeOffIntent)
					&& PersistentData.getMostRecentAlarmTime(getString( R.string.turn_gyroscope_on )) - PersistentData.getGyroOffDurationMilliseconds() + 1000 > now ) {
				gyroscopeListener.turn_on();
			}
		}

		if (PersistentData.getEnabled(PersistentData.MAGNETOMETER)) {
			if(PersistentData.getMostRecentAlarmTime( getString(R.string.turn_magnetometer_on )) < now || //the most recent magnetometer alarm time is in the past, or...
					!timer.alarmIsSet(Timer.magnetometerOnIntent) ) {
				sendBroadcast(Timer.magnetometerOnIntent);
			} else if(timer.alarmIsSet(Timer.magnetometerOffIntent)
					&& PersistentData.getMostRecentAlarmTime(getString( R.string.turn_magnetometer_on )) - PersistentData.getMagnetometerOffDurationMilliseconds() + 1000 > now ) {
				magnetoListener.turn_on();
			}
		}

		if (PersistentData.getEnabled(PersistentData.STEPS)) {
			if(pedometerListener ==null)
				pedometerListener = new PedometerListener(getApplicationContext());
			if(!pedometerListener.check_status())
				pedometerListener.turn_on();
			// Pedometer will always run in background, no need to turn on and off
		}

		if (PersistentData.getEnabled(PersistentData.SIGNIFICANT_MOTION)) {
			if (significantMotionListener == null)
				significantMotionListener = new SignificantMotionListener(getApplicationContext());
			if (!significantMotionListener.check_status())
				significantMotionListener.turn_on();
		}

		if (PersistentData.getEnabled(PersistentData.AMBIENTLIGHT)) {  //if ambient light data recording is enabled and...
			if(PersistentData.getMostRecentAlarmTime( getString(R.string.turn_ambientlight_on )) < now || //the most recent accelerometer alarm time is in the past, or...
					!timer.alarmIsSet(Timer.ambientLightIntent) ) { //there is no scheduled accelerometer-on timer.
				sendBroadcast(Timer.ambientLightIntent); // start accelerometer timers (immediately runs accelerometer recording session).
				//note: when there is no off timer that means we are in-between scans.  This state is fine, so we don't check for it.
			}
		}

		if ( PersistentData.getEnabled(PersistentData.GPS) ) {
			if ( gpsListener == null ) gpsListener = new GPSListener( appContext );
			if(timer.alarmIsSet(Timer.gpsOnIntent)) timer.cancelAlarm(Timer.gpsOnIntent);
			if(timer.alarmIsSet(Timer.gpsOffIntent)) timer.cancelAlarm(Timer.gpsOffIntent);
			if ( PersistentData.getGpsOffDurationMilliseconds()==0 ) gpsListener.turn_on_passive();
			sendBroadcast(Timer.gpsOnIntent);
		}

		if (PersistentData.getEnabled(PersistentData.USAGE)) {  //if usage recording is enabled and...
			if(PersistentData.getMostRecentAlarmTime( getString(R.string.update_usage )) < now || //the most recent usage update time is in the past, or...
					!timer.alarmIsSet(Timer.usageIntent) ) { //there is no scheduled usage update timer.
				sendBroadcast(Timer.usageIntent); // start usage timers (immediately update usage).
				//note: when there is no off timer that means we are in-between scans.  This state is fine, so we don't check for it.
			}
		}

		if (PersistentData.getEnabled(PersistentData.WIFI)) {
			if (PersistentData.getMostRecentAlarmTime(getString(R.string.run_wifi_log)) < now || //the most recent wifi log time is in the past or
					!timer.alarmIsSet(Timer.wifiLogIntent)) {
				sendBroadcast(Timer.wifiLogIntent);
			}
		}
		
		//if Bluetooth recording is enabled and there is no scheduled next-bluetooth-enable event, set up the next Bluetooth-on alarm.
		//(Bluetooth needs to run at absolute points in time, it should not be started if a scheduled event is missed.)
		if ( PermissionHandler.confirmBluetooth(appContext) && !timer.alarmIsSet(Timer.bluetoothOnIntent)) {
			/* If the BluetoothAdapter is null, or if the BluetoothAdapter.getAddress() returns null,
			 * record an empty string for the Bluetooth MAC Address.
			 * The Bluetooth MAC Address is always empty in Android 8.0 and above, because the hopes needs
			 * the LOCAL_MAC_ADDRESS permission, which is a system permission that it's not allowed to
			 * have:
			 * https://android-developers.googleblog.com/2017/04/changes-to-device-identifiers-in.html
			 * The Bluetooth MAC Address is also sometimes empty on Android 7 and lower. */
			//This will not work on all devices: http://stackoverflow.com/questions/33377982/get-bluetooth-local-mac-address-in-marshmallow
			String bluetoothAddress = Settings.Secure.getString(appContext.getContentResolver(), "bluetooth_address");
			if (bluetoothAddress == null) bluetoothAddress = "";
			DeviceInfo.bluetoothMAC = EncryptionEngine.hashMAC(bluetoothAddress);
			timer.setupExactSingleAbsoluteTimeAlarm(PersistentData.getBluetoothTotalDurationMilliseconds(), PersistentData.getBluetoothGlobalOffsetMilliseconds(), Timer.bluetoothOnIntent); }
		
		// Functionality timers. We don't need aggressive checking for if these timers have been missed, as long as they run eventually it is fine.
		if (!timer.alarmIsSet(Timer.uploadDatafilesIntent))
			timer.setupExactSingleAlarm(PersistentData.getUploadDataFilesFrequencyMilliseconds(), Timer.uploadDatafilesIntent);
		if (!timer.alarmIsSet(Timer.createNewDataFilesIntent))
			timer.setupExactSingleAlarm(PersistentData.getCreateNewDataFilesFrequencyMilliseconds(), Timer.createNewDataFilesIntent);
		if (!timer.alarmIsSet(Timer.checkForNewSurveysIntent) && PersistentData.getCheckForNewSurveysFrequencyMilliseconds()!=0)
			timer.setupExactSingleAlarm(PersistentData.getCheckForNewSurveysFrequencyMilliseconds(), Timer.checkForNewSurveysIntent);

		//checks for the current expected state for survey notifications,
		for (String surveyId : (ArrayList<String>)PersistentData.getSurveyIds() )
			if ( isSurveyPushType(surveyId) || PersistentData.getSurveyNotificationState(surveyId) || PersistentData.getMostRecentSurveyAlarmTime(surveyId) < now )
				//if survey notification should be active or the most recent alarm time is in the past, trigger the notification.
				SurveyNotifications.displaySurveyNotification(appContext, surveyId);
		
		//checks that surveys are actually scheduled, if a survey is not scheduled, schedule it!
		for (String surveyId : (ArrayList<String>)PersistentData.getSurveyIds() )
			if ( !timer.alarmIsSet( new Intent(surveyId) ) )
				SurveyScheduler.scheduleSurvey(surveyId);

		Intent restartServiceIntent = new Intent( getApplicationContext(), BackgroundService.class);
		restartServiceIntent.setPackage( getPackageName() );
		PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, 0 );
		AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService( Context.ALARM_SERVICE );
		alarmService.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis() + 1000 * 60 * 2, 1000 * 60 * 2, restartServicePendingIntent);
	}
	
	/**Refreshes the logout timer.
	 * This function has a THEORETICAL race condition, where the BackgroundService is not fully instantiated by a session activity,
	 * in this case we log an error to the debug log, print the error, and then wait for it to crash.  In testing on a (much) older
	 * version of the hopes we would occasionally see the error message, but we have never (august 10 2015) actually seen the hopes crash
	 * inside this code. */
	public static void startAutomaticLogoutCountdownTimer(){
		long tm = PersistentData.getMillisecondsBeforeAutoLogout();
		if ( tm == 0 ) return;
		if ( timer == null ) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("backgroundService", "timer is null, BackgroundService may be about to crash, the Timer was null when the BackgroundService was supposed to be fully instantiated.");
			TextFileManager.getDebugLogFile().write(
					"BackgroundService::startAutomaticLogoutCountdownTimer: our not-quite-race-condition encountered, "
							+ "Timer was null when the BackgroundService was supposed to be fully instantiated");
			return;
		}
		timer.setupExactSingleAlarm(tm, Timer.signoutIntent);
		PersistentData.loginOrRefreshLogin();
	}
	
	/** cancels the sign out timer */
	public static void clearAutomaticLogoutCountdownTimer() { timer.cancelAlarm(Timer.signoutIntent); }
	
	/** The Timer requires the BackgroundService in order to create alarms, hook into that functionality here. */
	public static void setSurveyAlarm(String surveyId, Calendar alarmTime) { timer.setupSurveyAlarm(surveyId, alarmTime); }

	public static void cancelSurveyAlarm(String surveyId) { timer.cancelAlarm(new Intent(surveyId)); }
	
	/**The timerReceiver is an Android BroadcastReceiver that listens for our timer events to trigger,
	 * and then runs the appropriate code for that trigger. 
	 * Note: every condition has a return statement at the end; this is because the trigger survey notification
	 * action requires a fairly expensive dive into PersistantData JSON unpacking.*/
	private BroadcastReceiver timerReceiver = new BroadcastReceiver() {
		@SuppressLint("LongLogTag")
		@Override public void onReceive(Context appContext, Intent intent) {
			if( BuildConfig.APP_IS_DEBUG )
				Log.d("BackgroundService - timers","Received broadcast: " + intent.toString() );
//			TextFileManager.getDebugLogFile().write(System.currentTimeMillis() + " Received Broadcast: " + intent.toString() );
			String broadcastAction = Objects.toString(intent.getAction(), "");

			/* For GPS and Accelerometer the failure modes are:
			 * 1. If a recording event is triggered and followed by Doze being enabled then HOPES will record until the Doze period ends.
			 * 2. If, after Doze ends, the timers trigger out of order HOPES ceaces to record and triggers a new recording event in the future. */

			/* Disable active sensor */
			if (broadcastAction.equals( appContext.getString(R.string.turn_accelerometer_off) ) ) {
				accelerometerListener.turn_off();
				return; }
			if (broadcastAction.equals( appContext.getString(R.string.turn_gps_off) ) ) {
				if ( PermissionHandler.checkGpsPermissions(appContext) ) { gpsListener.turn_off(); }
				return; }
			if (broadcastAction.equals( appContext.getString(R.string.turn_gyroscope_off) ) ) {
				gyroscopeListener.turn_off();
				return; }
			if (broadcastAction.equals( appContext.getString(R.string.turn_magnetometer_off) ) ) {
				magnetoListener.turn_off();
				return; }
//			if (broadcastAction.equals( appContext.getString(R.string.turn_steps_off) ) ) {
//				stepsListener.turn_off();
//				return; }

			/* Enable active sensors, reset timers. */
			//Accelerometer. We automatically have permissions required for accelerometer.
			if (broadcastAction.equals( appContext.getString(R.string.turn_accelerometer_on) ) ) {
				if ( accelerometerListener == null ) accelerometerListener = new AccelerometerListener( appContext );
				accelerometerListener.turn_on();
				//start both the sensor-off-action timer, and the next sensor-on-timer.
				long off_duration = PersistentData.getAccelerometerOffDurationMilliseconds();
				if(off_duration>0)
					timer.setupExactSingleAlarm(PersistentData.getAccelerometerOnDurationMilliseconds(), Timer.accelerometerOffIntent);
				long alarmTime = timer.setupExactSingleAlarm(off_duration + PersistentData.getAccelerometerOnDurationMilliseconds(), Timer.accelerometerOnIntent);
				//record the system time that the next alarm is supposed to go off at, so that we can recover in the event of a reboot or crash. 
				PersistentData.setMostRecentAlarmTime(getString(R.string.turn_accelerometer_on), alarmTime );
				return; }

			//AmbientLight. We automatically have permissions required for ambient light sensor.
			if (broadcastAction.equals( appContext.getString(R.string.turn_ambientlight_on) ) ) {
				if ( ambientLightListener == null ) ambientLightListener = new AmbientLightListener( appContext );
				ambientLightListener.turn_on();
				//start the next sensor-on-timer.
				long alarmTime = timer.setupExactSingleAlarm(PersistentData.getAmbientLightIntervalMilliseconds(), Timer.ambientLightIntent);
				PersistentData.setMostRecentAlarmTime(getString(R.string.turn_ambientlight_on), alarmTime );
				return; }

			//Gyroscope. Almost identical logic to accelerometer above.
			if (broadcastAction.equals( appContext.getString(R.string.turn_gyroscope_on) ) ) {
				if ( gyroscopeListener == null ) gyroscopeListener = new GyroscopeListener( appContext );
				gyroscopeListener.turn_on();
				long off_duration = PersistentData.getGyroOffDurationMilliseconds();
				if(off_duration>0)
					timer.setupExactSingleAlarm(PersistentData.getGyroOnDurationMilliseconds(), Timer.gyroscopeOffIntent);
				long alarmTime = timer.setupExactSingleAlarm(off_duration + PersistentData.getGyroOnDurationMilliseconds(), Timer.gyroscopeOnIntent);
				PersistentData.setMostRecentAlarmTime(getString(R.string.turn_gyroscope_on), alarmTime );
				return; }

			//Magnetometer. Almost identical logic to accelerometer above.
			if (broadcastAction.equals( appContext.getString(R.string.turn_magnetometer_on) ) ) {
				if ( magnetoListener == null ) magnetoListener = new MagnetoListener( appContext );
				magnetoListener.turn_on();
				long off_duration = PersistentData.getMagnetometerOffDurationMilliseconds();
				if(off_duration>0)
					timer.setupExactSingleAlarm(PersistentData.getMagnetometerOnDurationMilliseconds(), Timer.magnetometerOffIntent);
				long alarmTime = timer.setupExactSingleAlarm(off_duration + PersistentData.getMagnetometerOnDurationMilliseconds(), Timer.magnetometerOnIntent);
				PersistentData.setMostRecentAlarmTime(getString(R.string.turn_magnetometer_on), alarmTime );
				return; }

			//GPS. Almost identical logic to accelerometer above.
			if (broadcastAction.equals( appContext.getString(R.string.turn_gps_on) ) ) {
				if ( gpsListener == null ) gpsListener = new GPSListener( appContext );
				long on_duration = PersistentData.getGpsOnDurationMilliseconds();
				long off_duration = PersistentData.getGpsOffDurationMilliseconds();
				if ( on_duration!=0 && off_duration==0 ) {        // Mode 1: only off==0, normal mode, hybrid passive
					long difference = (System.currentTimeMillis() - PersistentData.getLastGpsUpdateTime());
					if (difference >= on_duration){
						gpsListener.turn_on();
						timer.setupExactSingleAlarm(60000L, Timer.gpsOffIntent);
					} else {
						// Set up the timer with the calculated remaining time
						timer.setupExactSingleAlarm(on_duration - difference, Timer.gpsOnIntent);
					}
				} else if ( on_duration==0 && off_duration!=0 ) {  // Mode 2: only on==0, single update mode, low battery drain
					gpsListener.turn_on_single();
					timer.setupExactSingleAlarm(off_duration, Timer.gpsOnIntent);
				} else if ( on_duration!=0 && on_duration!=0 ){                        // Mode 3: both non-zero, normal update mode
					gpsListener.turn_on();
					timer.setupExactSingleAlarm(on_duration, Timer.gpsOffIntent);
					timer.setupExactSingleAlarm(on_duration+off_duration, Timer.gpsOnIntent);
				}// else both are zero, only passive
				return; }

			//run a wifi scan.  Most similar to GPS, but without an off-timer.
			if (broadcastAction.equals( appContext.getString(R.string.run_wifi_log) ) ) {
				if ( wifiListener == null ) wifiListener = WifiListener.initialize( appContext );
				if ( PermissionHandler.checkWifiPermissions(appContext) ) { wifiListener.scanWifi(); }
				else { TextFileManager.getDebugLogFile().write(System.currentTimeMillis() + " user has not provided permission for Wifi."); }
				long alarmTime = timer.setupExactSingleAlarm(PersistentData.getWifiLogFrequencyMilliseconds(), Timer.wifiLogIntent);
				PersistentData.setMostRecentAlarmTime( getString(R.string.run_wifi_log), alarmTime );
				return; }

			//Usage update. We automatically have permissions required for usage.
			if (broadcastAction.equals( appContext.getString(R.string.update_usage) ) ) {
				if ( usageListener == null ) usageListener = new UsageListener( appContext );
				usageListener.updateUsage();
				//start the next sensor-on-timer.
				long alarmTime = timer.setupExactSingleAlarm(PersistentData.getUsageUpdateIntervalMilliseconds(), Timer.usageIntent);
				PersistentData.setMostRecentAlarmTime(getString(R.string.update_usage), alarmTime );
				return; }

			/* Bluetooth timers are unlike GPS and Accelerometer because it uses an absolute-point-in-time as a trigger, and therefore we don't need to store most-recent-timer state.
			 * The Bluetooth-on action sets the corresponding Bluetooth-off timer, the Bluetooth-off action sets the next Bluetooth-on timer.*/
			if (broadcastAction.equals( appContext.getString(R.string.turn_bluetooth_on) ) ) {
				if ( !PersistentData.getEnabled(PersistentData.BLUETOOTH) ) { if(BuildConfig.APP_IS_DEBUG) Log.e("BackgroundService Listener", "invalid Bluetooth on received"); return; }
				if ( PermissionHandler.checkBluetoothPermissions(appContext) ) {
					if (bluetoothListener != null) bluetoothListener.enableBLEScan(); }
				else { TextFileManager.getDebugLogFile().write(System.currentTimeMillis() + " user has not provided permission for Bluetooth."); }
				timer.setupExactSingleAlarm(PersistentData.getBluetoothOnDurationMilliseconds(), Timer.bluetoothOffIntent);
				return;
			}
			if (broadcastAction.equals( appContext.getString(R.string.turn_bluetooth_off) ) ) {
				if ( PermissionHandler.checkBluetoothPermissions(appContext) ) {
					if ( bluetoothListener != null) bluetoothListener.disableBLEScan(); }
				timer.setupExactSingleAbsoluteTimeAlarm(PersistentData.getBluetoothTotalDurationMilliseconds(), PersistentData.getBluetoothGlobalOffsetMilliseconds(), Timer.bluetoothOnIntent);
				return;
			}
			
			// Starts a data upload attempt.
			if (broadcastAction.equals( appContext.getString(R.string.upload_data_files_intent) )) {
				if (PermissionHandler.getNextPermission(getApplicationContext(), false)!=null && notify)
					createPermissionNotification();
				if (!notify) notify = true;
				PostRequest.uploadAllFiles();
				timer.setupExactSingleAlarm(PersistentData.getUploadDataFilesFrequencyMilliseconds(), Timer.uploadDatafilesIntent);
				return;
			}
			// Creates new data files
			if (broadcastAction.equals( appContext.getString(R.string.create_new_data_files_intent) ) ) {
				TextFileManager.makeNewFilesForEverything();
				timer.setupExactSingleAlarm(PersistentData.getCreateNewDataFilesFrequencyMilliseconds(), Timer.createNewDataFilesIntent);
				return;
			}

			// Signs out the user. (does not set up a timer, that is handled in activity and sign-in logic)
			if (broadcastAction.equals( appContext.getString(R.string.signout_intent) ) ) {
				if ( PersistentData.isUnregisterDebugMode || PersistentData.getMillisecondsBeforeAutoLogout() == 0 ) return;
				PersistentData.logout();
				Intent loginPage = new Intent(appContext, LoginActivity.class);
				loginPage.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				appContext.startActivity(loginPage);
				return;
			}

			if (broadcastAction.equals( appContext.getString(R.string.check_for_sms_enabled) ) ) {
				if ( PermissionHandler.confirmTexts(appContext) ) { startSmsSentLogger(); startMmsSentLogger(); }
				else if (PersistentData.getEnabled(PersistentData.TEXTS) ) { timer.setupExactSingleAlarm(30000L, Timer.checkForSMSEnabled); }
			}
			if (broadcastAction.equals( appContext.getString(R.string.check_for_calls_enabled) ) ) {
				if ( PermissionHandler.confirmCalls(appContext) ) { startCallLogger(); }
				else if (PersistentData.getEnabled(PersistentData.CALLS) ) { timer.setupExactSingleAlarm(30000L, Timer.checkForCallsEnabled); }
			}

			//Downloads the most recent survey questions and schedules the surveys.
			if (broadcastAction.equals(appContext.getString(R.string.check_for_new_surveys_intent)) && PersistentData.getCheckForNewSurveysFrequencyMilliseconds()!=0) {
				SurveyDownloader.downloadSurveys( getApplicationContext() );
				timer.setupExactSingleAlarm(PersistentData.getCheckForNewSurveysFrequencyMilliseconds(), Timer.checkForNewSurveysIntent);
				return;
			}

			//checks if the action is the dismissal of survey notification via swipe-off
			if (broadcastAction.equals(getString(R.string.dismiss_survey_notification))){
				String surveyId = intent.getStringExtra("survey_id");
				deactivateSurvey(surveyId);
				SurveyRecorder surveyRecorder = new SurveyRecorder();
				surveyRecorder.appendLineToLogFile(surveyId, "", "", "Survey notification dismissed");
				return;
			}

			//checks if the action is the dismissal of push notification via swipe-off
			if (broadcastAction.equals(getString(R.string.dismiss_push_notification))){
				String push_hash = intent.getStringExtra("push_hash");
				SurveyRecorder surveyRecorder = new SurveyRecorder();
				surveyRecorder.appendLineToLogFile(push_hash, "", "", "Push notification dismissed");
				return;
			}

			if (PersistentData.isRegistered() && broadcastAction.equals(ConnectivityManager.CONNECTIVITY_ACTION)) {
				NetworkInfo networkInfo = intent.getParcelableExtra(ConnectivityManager.EXTRA_NETWORK_INFO);
				if(networkInfo.getType() == ConnectivityManager.TYPE_WIFI) {
					long time_diff = System.currentTimeMillis() - PersistentData.getLong(PersistentData.LASY_UPLOAD_TIME_KEY,0);
					if(time_diff > PersistentData.getUploadDataFilesFrequencyMilliseconds()*0.5)
						PostRequest.uploadAllFiles();
					return;
				}
			}

			// handle expiry of a survey notification
			if (broadcastAction.equals(getString(R.string.expire_survey_notification))) try{
				String surveyId = intent.getStringExtra("survey_id");
				mNotificationManager.cancel(surveyId.hashCode());
				deactivateSurvey(surveyId);
			} catch (Exception e) {} finally { return; }

			// handle expiry of a push notification
			if (broadcastAction.equals(getString(R.string.expire_push_notification))) try{
				int push_hash = intent.getIntExtra("push_hash", 0);
				mNotificationManager.cancel(push_hash);
			} catch (Exception e) {} finally { return; }

			//checks if the action is the id of a survey (expensive), if so pop up the notification for that survey, schedule the next alarm
			if ( PersistentData.getSurveyIds().contains( broadcastAction ) ) {
//				Log.i("BACKGROUND SERVICE", "new notification: " + broadcastAction);
				SurveyNotifications.displaySurveyNotification(appContext, broadcastAction);
				SurveyScheduler.scheduleSurvey(broadcastAction);
				return;
			}

			//this is a special action that will only run if the hopes device is in debug mode.
			if (broadcastAction.equals("crashBeiwe") && BuildConfig.APP_IS_DEBUG)
				throw new NullPointerException("beeeeeoooop.");
			//this is a special action that will only run if the hopes device is in debug mode.
			if (broadcastAction.equals("enterANR") && BuildConfig.APP_IS_DEBUG) {
				try { Thread.sleep(100000); }
				catch(InterruptedException ignored) { }
			}
		}
	};

	private static String PERMISSION_CHANNEL_ID = "permission_notification_channel";
	private void createPermissionNotification(){
		// If user has not responded to the previous permission notification, don't notify again
		StatusBarNotification[] notifications = mNotificationManager.getActiveNotifications();
		for (StatusBarNotification notification: notifications)
			if ( notification.getId()==0 ) return;

		Intent intent = new Intent(this, MainMenuActivity.class);
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);

		Notification.Builder n  = new Notification.Builder(this)
				.setSmallIcon(R.mipmap.ic_launcher)
				.setContentIntent(pIntent)
				.setContentTitle(getString(R.string.notify_permission_revoked))
				.setContentText(getString(R.string.notify_request_permission))
				.setOngoing(true)
				.setAutoCancel(true);

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O && mNotificationManager.getNotificationChannel(PERMISSION_CHANNEL_ID)==null) {
			NotificationChannel channel = new NotificationChannel(PERMISSION_CHANNEL_ID, "HOPES", NotificationManager.IMPORTANCE_DEFAULT);
			mNotificationManager.createNotificationChannel(channel);
		}

		mNotificationManager.notify(0, n.build());
	}
		
	/*##########################################################################################
	############## code related to onStartCommand and binding to an activity ###################
	##########################################################################################*/
	@Override
	public IBinder onBind(Intent arg0) { return new BackgroundServiceBinder(); }
	
	/**A public "Binder" class for Activities to access.
	 * Provides a (safe) handle to the background Service using the onStartCommand code
	 * used in every RunningBackgroundServiceActivity */
	public class BackgroundServiceBinder extends Binder {
        public BackgroundService getService() {
            return BackgroundService.this;
        }
	}
	
	/*##############################################################################
	########################## Android Service Lifecycle ###########################
	##############################################################################*/
	
	/** The BackgroundService is meant to be all the time, so we return START_STICKY */
	@Override public int onStartCommand(Intent intent, int flags, int startId){ //Log.d("BackroundService onStartCommand", "started with flag " + flags );
		if(BuildConfig.APP_IS_DEBUG)
			TextFileManager.getDebugLogFile().write("BackgroundService::onStartCommand: "
					+ System.currentTimeMillis() + " started with flag " + flags);
		return START_STICKY;
		//we are testing out this restarting behavior for the service.  It is entirely unclear that this will have any observable effect.
		//return START_REDELIVER_INTENT;
	}
	//(the rest of these are identical, so I have compactified it)
	@Override public void onTaskRemoved(Intent rootIntent) { //Log.d("BackgroundService onTaskRemoved", "onTaskRemoved called with intent: " + rootIntent.toString() );
		if(BuildConfig.APP_IS_DEBUG)
			TextFileManager.getDebugLogFile().write("BackgroundService::onTaskRemoved: "
					+ System.currentTimeMillis() + " onTaskRemoved called with intent: " + rootIntent.toString());
		restartService();
	}
	@Override public boolean onUnbind(Intent intent) { //Log.d("BackgroundService onUnbind", "onUnbind called with intent: " + intent.toString() );
		TextFileManager.getDebugLogFile().write("BackgroundService::onUnbind: " +
						System.currentTimeMillis() + " onUnbind called with intent: " + intent.toString());
		restartService();
		return super.onUnbind(intent); }
	@Override public void onDestroy() { //Log.w("BackgroundService", "BackgroundService was destroyed.");
		//note: this does not run when the service is killed in a task manager, OR when the stopService() function is called from debugActivity.
		TextFileManager.getDebugLogFile().write("BackgroundService::onDestroy: "
				+ System.currentTimeMillis() + " BackgroundService was destroyed");
		restartService();
		super.onDestroy(); }
	@Override public void onLowMemory() { //Log.w("BackgroundService onLowMemory", "Low memory conditions encountered");
		TextFileManager.getDebugLogFile().write("BackgroundService::onLowMemory: " + System.currentTimeMillis() + " onLowMemory called");
		restartService(); }
	
	/** Sets a timer that starts the service if it is not running in ten seconds. */
	private void restartService(){
		//how does this even...  Whatever, 10 seconds later the background service will start.
		Intent restartServiceIntent = new Intent( getApplicationContext(), this.getClass() );
	    restartServiceIntent.setPackage( getPackageName() );
	    PendingIntent restartServicePendingIntent = PendingIntent.getService( getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT );
	    AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService( Context.ALARM_SERVICE );
	    alarmService.set(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime() + 500, restartServicePendingIntent);
	}
}
