package com.moht.hopes;

import static com.moht.hopes.BackgroundService.*;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.Settings;

import com.moht.hopes.storage.EncryptionEngine;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.tasks.FCMService;

import java.util.Arrays;

/**This is a class that NEEDS to be instantiated in the background service. In order to get the Android ID, the class needs
 * Context. Once instantiated, the class assigns two variables for AndroidID and BluetoothMAC. Once they are instantiated,
 * they can be called from different classes to be used. They are hashed when they are called.
 * 
 * The class is used to grab unique ID data, and pass it to the server. The data is used while authenticating users
 * 
 * @author Dor Samet, Eli Jones */  

public class DeviceInfo {
	private static String last_substr(String s, int length){
		if ( s == null )
			return "";
		return s.length()>length?s.substring(s.length()-length):s;
	}
	public static String last_substr(String s){
		int phone_number_length = 8;
		return last_substr(s, phone_number_length);
	}
	/* TODO:  Ensure this number is updated whenever a version of the hopes is pushed to the website for any reason.
	 * Don't forget to update the version in the android manifest, only the version string is visible to the user.
	 * Version history:
	 * 1: add additional device data during the registration process, including this version number.
	 * 2: added sms debugging
	 * 3: added universal crash log to the hopes
	 * 4: added a lot of CrashHandler integration to all sorts of error-handled conditions the codebase.
	 * 5: Initial version of Android 6.
	 * 		Enhanced Audio UI rewrite included,raw recording code is functional but not enabled, no standard on parsing audio surveys for their type.
	 * 6: second version of Android 6.
	 * 		Enhanced audio functionality is included and functional, fixed crash that came up on a device when the hopes was uninstalled and then reinstalled?
	 * 		A functionally identical 6 was later released, it contains a new error message for a new type of iOS-related registration error.
	 * 7: audio survey files now contain the surveyid for the correct audio survey.
	 * 8: Now support a flag for data upload over cellular, fixed bug in wifi scanning.
	 * 9: Moves Record/Play buttons outside the scroll window on voice recording screen.
	 * 10: hopes version 2.0, Change to TextFileManager to potentially improve uninitialized errors, added device idle and low power mode change to power state listener.
	 * 11: development version of 12.
	 * 12: hopes version 2.1, several bug fixes including a fix for the on-opening-surveyactivity crash and a crash that could occur when when encrypting audio files. Adds support for branching serveys (conditional display logic)
	 * 13: hopes version 2.1.1, data improvements to GPS and WIFI data streams, improvements in said data streams and additional context provided in the hopes log (debug log).
	 * 14: hopes version 2.1.2, minor behavior improvement in extremely rare occurrence inside of sessionActivity, see inline documentation there for details.
	 * 15: bug fix in crash handler
	 * 16: hopes version 2.1.3, rewrite of file uploading to fix App Not Responding (ANR) errors; also BackgroundService.onStartCommand() now uses START_REDELIVER_INTENT
	 * 17: hopes version 2.1.4, fixed a bug that still showed the next survey, even if that survey time had been deleted in the backend and the updatee had propagated to the phone
	 * 18: hopes version 2.1.5, fixed bugs with recording received SMS messages and sent MMS messages
	 * 19: hopes version 2.2.0, enabled hopes to point to any server URL; improved Registration and Password Reset interfaces and networking.
	 * 20: hopes version 2.2.1, updated text on Registration screens
	 * 21: hopes version 2.2.2, updates styles, restores persistent, individual survey notifications in Android 7
	 * 22: hopes version 2.2.3, improves error messages
	 * 23: hopes version 2.2.4, OnnelaLabServer version and GooglePlayStore version (with customizable URL) have different names (Beiwe vs. Beiwe2)
	 * 24: hopes version 2.2.5, handle null Bluetooth MAC Address in Android 8.0 and above
	 * 25: hopes version 2.2.6, fix crash on opening hopes from audio survey notification when AppContext is null
	 * 26: hopes version 2.2.7, Added Sentry
	 * 27: hopes version 2.3.0, fix restart on crash
	 * 28: hopes version 2.3.1, Add ACCESS_COARSE_LOCATION to permission handler
	 * 29: hopes version 2.3.2, Add more intents to wake up the hopes in the BootListener;
	 * 		fixed crashes in SMS Sent Listener, SpannableString survey questions, and Registration
	 * 		screen orientation change
	 * 30: hopes version 2.3.3, Adds a repeating timer to restart the Background Service if crashed,
	 * 		improves file upload, fixes an occasional crash in audio recordings,
	 * 		fixes an occasional crash in registration, handles image surveys without crashing
	 * 31: hopes version 2.4.0, Updates compile target to SDK 26 (Android 8.0),
	 *		implements anoynmized phone number and MAC address hashing,
	 *		prevents creation of header-less files when the phone is out of storage space,
	 *		fixes a crash in trying to upload files before registration
	 * 32: hopes version 2.4.1, Fixes survey notification crash on Android 4,
	 * 		prevent crash in phone call logger,
	 * 		protect against crash in MMS logger
	 * 33: hopes version 2.4.2, Fuzzes/anonymizes GPS data for studies on servers that enable it	*/

	public static String androidID;
	public static String bluetoothMAC;
	@SuppressLint("StaticFieldLeak")
	private static Context context;

	/** grab the Android ID and the Bluetooth's MAC address */
	@SuppressLint("HardwareIds")
	public static void initialize(Context appContext) {
		context = appContext;

		/* Previously, the App sometimes gets cleaned up partially in the background, androidID becomes null,
		*  file upload fails with invalid device_id, now we store it in shared preferences */
		androidID = getAndroidID(appContext); // sha-256-hashed Android ID
	}
	
	public static String getBeiweVersion() {
		try {
			PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(),0);
			return info.versionCode + "-" + BuildConfig.BUILD_TYPE;
		} catch (PackageManager.NameNotFoundException e) {
			TextFileManager.getDebugLogFile().write(
					"DeviceInfo::getBeiweVersion: " + Arrays.toString(e.getStackTrace()));
			return "unknown";
		}
	}
	public static String getAndroidVersion() { return android.os.Build.VERSION.RELEASE; }
	public static String getProduct() { return android.os.Build.PRODUCT; }
	public static String getBrand() { return android.os.Build.BRAND; }
	public static String getHardwareId() { return android.os.Build.HARDWARE; }
	public static String getManufacturer() { return android.os.Build.MANUFACTURER; }
	public static String getModel() { return android.os.Build.MODEL; }
	public static String getAndroidID(Context appContext) {
		String android_id = PersistentData.getString("device_id", "");
		if(android_id.isEmpty() && appContext!=null){
			android_id = EncryptionEngine.safeHash(Settings.Secure.getString( appContext.getContentResolver(), Settings.Secure.ANDROID_ID ));
			PersistentData.setString("device_id", android_id);
		}
		return android_id;
	}
	public static String getBluetoothMAC() { return EncryptionEngine.hashMAC(bluetoothMAC); }
	public static String getFCMtoken() {
		return FCMService.getToken();
	}
}