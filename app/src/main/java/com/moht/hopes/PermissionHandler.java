package com.moht.hopes;

import android.Manifest;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;

import com.moht.hopes.listeners.AccessibilityListener;
import com.moht.hopes.storage.PersistentData;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class PermissionHandler {
	public static final int PERMISSION_GRANTED = PackageManager.PERMISSION_GRANTED;
//	public static int PERMISSION_DENIED = PackageManager.PERMISSION_DENIED;
	static String POWER_EXCEPTION_PERMISSION = "POWER_EXCEPTION_PERMISSION";
	static String USAGE_EXCEPTION_PERMISSION = "USAGE_EXCEPTION_PERMISSION";
	static String APPLICATION_OVERLAY_PERMISSION = "APPLICATION_OVERLAY_PERMISSION";
	static String ACCESSIBILITY_OVERLAY_PERMISSION = "ACCESSIBILITY_OVERLAY_PERMISSION";

	public static Map <String, String> permissionMessages = new HashMap<>();
	static {
			permissionMessages.put(Manifest.permission.ACCESS_FINE_LOCATION, "use Location Services.");
			permissionMessages.put(Manifest.permission.ACCESS_BACKGROUND_LOCATION, "use Location Services in the background.");
			permissionMessages.put(Manifest.permission.ACCESS_NETWORK_STATE, "view your Network State.");
			permissionMessages.put(Manifest.permission.ACCESS_WIFI_STATE, "view your Wifi State.");
			permissionMessages.put(Manifest.permission.READ_SMS, "view your SMS messages.");
			permissionMessages.put(Manifest.permission.BLUETOOTH, "use Bluetooth.");
			permissionMessages.put(Manifest.permission.BLUETOOTH_ADMIN, "use Bluetooth.");
			permissionMessages.put(Manifest.permission.CALL_PHONE, "access your Phone service.");
			permissionMessages.put(Manifest.permission.INTERNET, "access The Internet.");
			permissionMessages.put(Manifest.permission.READ_CALL_LOG, "access your Phone service.");
			permissionMessages.put(Manifest.permission.READ_CONTACTS, "access your Contacts.");
			permissionMessages.put(Manifest.permission.READ_PHONE_STATE, "access your Phone service.");
			permissionMessages.put(Manifest.permission.RECEIVE_BOOT_COMPLETED, "start up on Boot.");
			permissionMessages.put(Manifest.permission.RECORD_AUDIO, "access your Microphone." );
			permissionMessages.put(Manifest.permission.ACCESS_COARSE_LOCATION, "use Location Services." );
			permissionMessages.put(Manifest.permission.RECEIVE_MMS, "receive MMS messages.");
			permissionMessages.put(Manifest.permission.SYSTEM_ALERT_WINDOW, "create system alert window.");
			permissionMessages.put(Manifest.permission.BIND_ACCESSIBILITY_SERVICE,"bind accessibility service.");
			permissionMessages.put(Manifest.permission.GET_TASKS, "get task list.");
			permissionMessages.put(Manifest.permission.ACTIVITY_RECOGNITION, "measure physical activity.");
			permissionMessages.put(Manifest.permission.READ_PHONE_NUMBERS, "access your Phone service.");
			permissionMessages = Collections.unmodifiableMap(permissionMessages);
	}

	static String getNormalPermissionMessage(String permission) {
		return String.format("For this study HOPES needs permission to %s. " +
				"Please press allow on the following permissions request.",
				permissionMessages.get(permission));
	}

	static String getBumpingPermissionMessage(String permission) {
		return String.format("To be fully enrolled in this study HOPES needs permission to %s. " +
				"You appear to have denied or removed this permission. " +
				"HOPES will now bump you to its settings page so you can manually enable it.",
				permissionMessages.get(permission));
	}
	
	/*  The following are enabled by default.
	 *  AccessNetworkState
	 *  AccessWifiState
	 *  Bluetooth
	 *  BluetoothAdmin
	 *  Internet
	 *  ReceiveBootCompleted
	 * 
	 *  the following are enabled on the registration screen
	 *  ReadSms
	 *  ReceiveMms
	 *  ReceiveSms
	 * 
	 *  This leaves the following to be prompted for at session activity start
	 *  AccessFineLocation - GPS
	 *  CallPhone - Calls
	 *  ReadCallLog - Calls
	 *  ReadContacts - Calls and SMS
	 *  ReadPhoneState - Calls
	 *  WriteCallLog - Calls
	 *  
	 *  We will check for microphone recording as a
	 *  special condition on the audio recording screen. */

	// Permission check
	public static Boolean checkPermissions(Context context, String [] permissions) {
		for(String permission: permissions)
			if(context.checkSelfPermission(permission)!=PERMISSION_GRANTED)
				return false;
		return true;
	}

	public static Boolean checkAccessCoarseLocation(Context context) {
		return context.checkSelfPermission(
				Manifest.permission.ACCESS_COARSE_LOCATION)==PERMISSION_GRANTED;
	}

	public static Boolean checkAccessFineLocation(Context context) {
		return context.checkSelfPermission(
				Manifest.permission.ACCESS_FINE_LOCATION)==PERMISSION_GRANTED;
	}

	public static Boolean checkAccessBackgroundLocation(Context context) {
		return Build.VERSION.SDK_INT>=29 ? context.checkSelfPermission(
				Manifest.permission.ACCESS_BACKGROUND_LOCATION)==PERMISSION_GRANTED : true;
	}

	public static Boolean checkActivityRecognition(Context context) {
		return Build.VERSION.SDK_INT>=29? context.checkSelfPermission(
				Manifest.permission.ACTIVITY_RECOGNITION)==PERMISSION_GRANTED : true;
	}

	public static Boolean checkAccessNetworkState(Context context) {
		return context.checkSelfPermission(
				Manifest.permission.ACCESS_NETWORK_STATE)==PERMISSION_GRANTED;
	}

	public static Boolean checkAccessWifiState(Context context) {
		return context.checkSelfPermission(
				Manifest.permission.ACCESS_WIFI_STATE)==PERMISSION_GRANTED;
	}

	public static Boolean checkAccessBluetooth(Context context) {
		return context.checkSelfPermission(
				Manifest.permission.BLUETOOTH)==PERMISSION_GRANTED;
	}

	public static Boolean checkAccessBluetoothAdmin(Context context) {
		return context.checkSelfPermission(
				Manifest.permission.BLUETOOTH_ADMIN)==PERMISSION_GRANTED;
	}

	public static Boolean checkAccessCallPhone(Context context) {
		return context.checkSelfPermission(
				Manifest.permission.CALL_PHONE)==PERMISSION_GRANTED;
	}

	public static Boolean checkAccessReadCallLog(Context context) {
		return context.checkSelfPermission(
				Manifest.permission.READ_CALL_LOG)==PERMISSION_GRANTED;
	}

	public static Boolean checkAccessReadContacts(Context context) {
		return context.checkSelfPermission(
				Manifest.permission.READ_CONTACTS)==PERMISSION_GRANTED;
	}

	public static Boolean checkAccessReadPhoneState(Context context) {
		return context.checkSelfPermission(
				Manifest.permission.READ_PHONE_STATE)==PERMISSION_GRANTED;
	}

	public static Boolean checkAccessReadPhoneNumbers(Context context) {
		return context.checkSelfPermission(
				Manifest.permission.READ_PHONE_NUMBERS)==PERMISSION_GRANTED;
	}

	public static Boolean checkAccessReadSms(Context context) {
		return context.checkSelfPermission(
				Manifest.permission.READ_SMS)==PERMISSION_GRANTED;
	}

	public static Boolean checkAccessReceiveMms(Context context) {
		return context.checkSelfPermission(
				Manifest.permission.RECEIVE_MMS)==PERMISSION_GRANTED;
	}

	public static Boolean checkAccessReceiveSms(Context context) {
		return context.checkSelfPermission(
				Manifest.permission.RECEIVE_SMS)==PERMISSION_GRANTED;
	}

	public static Boolean checkAccessRecordAudio(Context context) {
		return context.checkSelfPermission(
				Manifest.permission.RECORD_AUDIO)==PERMISSION_GRANTED;
	}

	private static Boolean checkGetTasks(Context context) {
		return context.checkSelfPermission(Manifest.permission.GET_TASKS)==PERMISSION_GRANTED;
	}

	// Higher level permission check that use private method to check for permissions
	static boolean checkGpsPermissions(Context context) {
		return checkAccessFineLocation(context);
	}

	private static boolean checkCallsPermissions(Context context) {
		return checkAccessReadPhoneState(context) &&
				checkAccessCallPhone(context) &&
				checkAccessReadCallLog(context);
	}

	private static boolean checkTextsPermissions(Context context) {
		return checkAccessReadContacts(context) &&
				checkAccessReadSms(context) &&
				checkAccessReceiveMms(context) &&
				checkAccessReceiveSms(context);
	}

	public static boolean checkWifiPermissions(Context context) {
		return checkAccessWifiState(context) &&
				checkAccessNetworkState(context);
	}

	static boolean checkBluetoothPermissions(Context context) {
		return checkAccessBluetooth(context) &&
				checkAccessBluetoothAdmin(context);
	}

	static boolean confirmCalls(Context context) {
		return PersistentData.getEnabled(PersistentData.CALLS) &&
				checkCallsPermissions(context);
	}

	static boolean confirmTexts(Context context) {
		return PersistentData.getEnabled(PersistentData.TEXTS) &&
				checkTextsPermissions(context);
	}

	static boolean confirmBluetooth(Context context) {
		return PersistentData.getEnabled(PersistentData.BLUETOOTH) &&
				checkBluetoothPermissions(context);
	}

	public static Boolean checkDrawOverlayPermission(Context context) {
		return Settings.canDrawOverlays(context);
	}

	public static Boolean checkAccessUsagePermission() {
		// App usage
		try {
			if (BackgroundService.opsManager==null ||
					BackgroundService.opsManager.checkOpNoThrow(
							AppOpsManager.OPSTR_GET_USAGE_STATS,
							BackgroundService.appInfo.uid,
							BackgroundService.appInfo.packageName)!=0)
				return false;
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	public static String getNextPermission(Context context, Boolean includeRecording) {
		if (PersistentData.getEnabled(PersistentData.GPS)) {
			if (!checkAccessCoarseLocation(context))
				return Manifest.permission.ACCESS_COARSE_LOCATION;
			if (!checkAccessFineLocation(context))
				return Manifest.permission.ACCESS_FINE_LOCATION;
			if (!checkAccessBackgroundLocation(context))
				return Manifest.permission.ACCESS_BACKGROUND_LOCATION;
		}

		if (PersistentData.getEnabled(PersistentData.WIFI)) {
			if (!checkAccessWifiState(context))
				return Manifest.permission.ACCESS_WIFI_STATE;
			if (!checkAccessNetworkState(context))
				return Manifest.permission.ACCESS_NETWORK_STATE;
			if (!checkAccessCoarseLocation(context))
				return Manifest.permission.ACCESS_COARSE_LOCATION;
			if (!checkAccessFineLocation(context))
				return Manifest.permission.ACCESS_FINE_LOCATION;
			if (!checkAccessBackgroundLocation(context))
				return Manifest.permission.ACCESS_BACKGROUND_LOCATION;
		}

		if (PersistentData.getEnabled(PersistentData.BLUETOOTH)) {
			if (!checkAccessBluetooth(context))
				return Manifest.permission.BLUETOOTH;
			if (!checkAccessBluetoothAdmin(context))
				return Manifest.permission.BLUETOOTH_ADMIN;
			if (!checkAccessBackgroundLocation(context))
				return Manifest.permission.ACCESS_BACKGROUND_LOCATION;
		}

		if (PersistentData.getEnabled(PersistentData.CALLS)) {
			if (!checkAccessReadPhoneState(context))
				return Manifest.permission.READ_PHONE_STATE;
			if (!checkAccessReadCallLog(context))
				return Manifest.permission.READ_CALL_LOG;
		}

		if (PersistentData.getEnabled(PersistentData.TEXTS)) {
			if (!checkAccessReadContacts(context))
				return Manifest.permission.READ_CONTACTS;
			if (!checkAccessReadSms(context))
				return Manifest.permission.READ_SMS;
			if (!checkAccessReceiveMms(context))
				return Manifest.permission.RECEIVE_MMS;
			if (!checkAccessReceiveSms(context))
				return Manifest.permission.RECEIVE_SMS;
		}

		if (includeRecording) {
			if (!checkAccessRecordAudio(context))
				return Manifest.permission.RECORD_AUDIO;
		}

		//if (!checkSystemAlertPermission(context)) return Manifest.permission.SYSTEM_ALERT_WINDOW;
		if (!checkGetTasks(context))
			return Manifest.permission.GET_TASKS;

		// The phone call permission is invariant, it is required for all studies in order for the
		// call clinician functionality to work
		if (!checkAccessCallPhone(context))
			return Manifest.permission.CALL_PHONE;

		if (PersistentData.getEnabled(PersistentData.STEPS))
			if ( !checkActivityRecognition(context) )
				return Manifest.permission.ACTIVITY_RECOGNITION;

		PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
		if (!pm.isIgnoringBatteryOptimizations(context.getPackageName()))
			return POWER_EXCEPTION_PERMISSION;

		// App usage
		if(PersistentData.getEnabled(PersistentData.USAGE))
			try {
				ApplicationInfo localAppInfo = context.getPackageManager().getApplicationInfo(context.getPackageName(), 0);
				AppOpsManager opsManager = (AppOpsManager) context.getSystemService(Context.APP_OPS_SERVICE);
				if (opsManager==null || opsManager.checkOpNoThrow("android:get_usage_stats", localAppInfo.uid, localAppInfo.packageName)!=0)
					return USAGE_EXCEPTION_PERMISSION;
			} catch (Exception ignored){}

		// Taps uses TYPE_APPLICATION_OVERLAY
		if (PersistentData.getEnabled(PersistentData.TAPS) &&
				!checkDrawOverlayPermission(context))
			return APPLICATION_OVERLAY_PERMISSION;

		// Accessibility, sociability_call and sociability_msg, use the same ACCESSIBILITY permission
		if ((PersistentData.getEnabled(PersistentData.ACCESSIBILITY) ||
				PersistentData.getEnabled(PersistentData.SOCIABILITY_CALL) || PersistentData.getEnabled(PersistentData.SOCIABILITY_MSG)) &&
				!AccessibilityListener.isEnabled(context))
			return ACCESSIBILITY_OVERLAY_PERMISSION;

		// Do final setup: certain features need to instantiate after granting permission
		if (!BackgroundService.finalSetupDone){
			try{
				BackgroundService.localHandle.doSetup();
				BackgroundService.finalSetupDone = true;
			} catch (Exception ignored) {}
		}
		return null;
	}
}
