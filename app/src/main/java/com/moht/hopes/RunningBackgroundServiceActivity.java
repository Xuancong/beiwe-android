package com.moht.hopes;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.moht.hopes.BackgroundService.BackgroundServiceBinder;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.ui.DebugInterfaceActivity;
import com.moht.hopes.ui.user.LicenceActivity;
import com.moht.hopes.ui.user.MainMenuActivity;

import java.security.KeyStore;
import java.util.Arrays;
import java.util.Objects;

import static com.moht.hopes.storage.PersistentData.PASSWORD_RESET_NUMBER_KEY;
import static com.moht.hopes.storage.PersistentData.PCP_PHONE_NUMBER;

import androidx.appcompat.app.AppCompatActivity;

/**All Activities in the hopes extend this Activity.  It ensures that the hopes's key services (i.e.
 * BackgroundService, LoginManager, PostRequest, DeviceInfo, and WifiListener) are running before
 * the interface tries to interact with any of those.
 * 
 * Activities that require the user to be logged in (SurveyActivity, GraphActivity, 
 * AudioRecorderActivity, etc.) extend SessionActivity, which extends this.
 * Activities that do not require the user to be logged in (the login, registration, and password-
 * reset Activities) extend this activity directly.
 * Therefore all Activities have this Activity's functionality (binding the BackgroundService), but
 * the login-protected Activities have additional functionality that forces the user to log in. 
 * 
 * @author Eli Jones, Josh Zagorsky
 */
public class RunningBackgroundServiceActivity extends AppCompatActivity {
	/** The backgroundService variable is an Activity's connection to the ... BackgroundService.
	 * We ensure the BackgroundService is running in the onResume call, and functionality that
	 * relies on the BackgroundService is always tied to UI elements, reducing the chance of
	 * a null backgroundService variable to essentially zero. */
	protected BackgroundService backgroundService;
	protected static RunningBackgroundServiceActivity mSelf = null;
	protected static Menu s_menu = null;
	private NotificationManager mNotificationManager;
	public static KeyStore keyStore = null;
	private static final String AndroidKeyStore = "AndroidKeyStore";
	private static String TAG = "RunningBackgroundServiceActivity: ";

	//an unused variable for tracking whether the background Service is connected,
	// uncomment if we ever need that.
//	protected boolean isBound = false;
	
	/**The ServiceConnection Class is our trigger for events that rely on the BackgroundService */
	protected ServiceConnection backgroundServiceConnection = new ServiceConnection() {
	    @Override
	    public void onServiceConnected(ComponentName name, IBinder binder) {
	        BackgroundServiceBinder some_binder = (BackgroundServiceBinder) binder;
	        backgroundService = some_binder.getService();
			if (keyStore==null) {
				try {
					keyStore = KeyStore.getInstance(AndroidKeyStore);
					keyStore.load(null);
				} catch (Exception e) {
					TextFileManager.getDebugLogFile().write(
							"RunningBackgroundServiceActivity::onServiceConnected: " + DebugInterfaceActivity.printStack(e));
					if(BuildConfig.APP_IS_DEBUG) Log.e(TAG, "Key store initialization failed");
				}
			}
	        doBackgroundDependentTasks();
//	        isBound = true;
	    }
	    
	    @Override
	    public void onServiceDisconnected(ComponentName name) {
		    if(BuildConfig.APP_IS_DEBUG) Log.w(TAG, "Background Service Disconnected");
	        backgroundService = null;
//	        isBound = false;
	    }
	};
	
	@Override
	protected void onCreate(Bundle bundle){ 
		super.onCreate(bundle);
		mSelf = this;
		Thread.setDefaultUncaughtExceptionHandler(new CrashHandler(getApplicationContext()));
		PersistentData.initialize(getApplicationContext());

		// Initialize notification manager onCreate
		mNotificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
	}
	
	/** Override this function to do tasks on creation,
	 * but only after the background Service has been initialized. */
	protected void doBackgroundDependentTasks() { }

	/**On creation of RunningBackgroundServiceActivity we guarantee that the BackgroundService is
	 * actually running, we then bind to it so we can access program resources. */
	@Override
	protected void onResume() {
		super.onResume();
		
		Intent startingIntent = new Intent(getApplicationContext(), BackgroundService.class);
		startingIntent.addFlags(Intent.FLAG_FROM_BACKGROUND);
		startService(startingIntent);
		bindService( startingIntent, backgroundServiceConnection, Context.BIND_AUTO_CREATE);
	}

	/** disconnect BackgroundServiceConnection when the Activity closes, otherwise we have a
	 * memory leak warning (and probably an actual memory leak, too). */
	@Override
	protected void onPause() {
		super.onPause();
		activityNotVisible = true;
		unbindService(backgroundServiceConnection);
	}

	/*####################################################################
	########################## Common UI #################################
	####################################################################*/

	/** Common UI element, the menu button.*/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; adds items to the action bar if it is present.
		s_menu = menu;
		getMenuInflater().inflate(R.menu.logged_out_menu, menu);
//		menu.findItem(R.id.menu_call_clinician).setTitle(PersistentData.getCallClinicianButtonText());
		return true;
	}

	/** Common UI element, items in menu.*/
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch (item.getItemId()) {
//		case R.id.menu_about:
//			startActivity(new Intent(getApplicationContext(), AboutActivityLoggedOut.class));
//			return true;
		case R.id.menu_licence:
			startActivity(new Intent(getApplicationContext(), LicenceActivity.class));
			return true;
//		case R.id.menu_call_clinician:
//			callClinician(null);
//			return true;
//		case R.id.menu_call_research_assistant:
//			callResearchAssistant(null);
//			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	/** sends user to phone, calls the user's clinician. */
	public void callClinician( View v ) {
		startPhoneCall(PersistentData.getString(PCP_PHONE_NUMBER));
	}
	
	/** sends user to phone, calls the study's research assistant. */
	public void callResearchAssistant(View v) {
		startPhoneCall(PersistentData.getString(PASSWORD_RESET_NUMBER_KEY));
	}

	private void startPhoneCall(String phoneNumber) {
		Intent callIntent = new Intent(Intent.ACTION_CALL);
		callIntent.setData(Uri.parse("tel:" + phoneNumber));
		try {
			startActivity(callIntent);
		} catch (SecurityException e) {
			showAlertThatEnablesUserToGrantPermission(this,
					getString(R.string.cant_make_a_phone_call_permissions_alert),
					getString(R.string.cant_make_phone_call_alert_title),
					0);
		}
	}

	/*####################################################################
	###################### Permission Prompting ##########################
	####################################################################*/

	private static Boolean prePromptActive = false;
	private static Boolean postPromptActive = false;
	private static Boolean powerPromptActive = false;
	private static Boolean usagePromptActive = false;
	private static Boolean overlayPromptActive = false;
	private static Boolean accessibilityPromptActive = false;
	private static Boolean thisResumeCausedByFalseActivityReturn = false;
	private static Boolean aboutToResetFalseActivityReturn = false;
	private static Boolean activityNotVisible = false;
	public static AlertDialog alertDialog;
	public Boolean isAudioRecorderActivity() { return false; }

	private void goToSettings(Integer permissionIdentifier) {
		Intent myAppSettings = new Intent(
				Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
				Uri.parse("package:" + getPackageName()));
		myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
		myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivityForResult(myAppSettings, permissionIdentifier);
	}

	@TargetApi(23)
	private void goToPowerSettings(Integer powerCallbackIdentifier) {
		@SuppressLint("BatteryLife") Intent powerSettings = new Intent(
				Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS,
				Uri.parse("package:" + getPackageName()));
		powerSettings.addCategory(Intent.CATEGORY_DEFAULT);
		powerSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivityForResult(powerSettings, powerCallbackIdentifier);
	}

	@TargetApi(23)
	private void goToAccessibilitySettings(Integer callbackIdentifier) {
		Intent accessibilitySettings = new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS);
		accessibilitySettings.addCategory(Intent.CATEGORY_DEFAULT);
		accessibilitySettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivityForResult(accessibilitySettings, callbackIdentifier);
	}

	@TargetApi(23)
	private void goToOverlaySettings(Integer callbackIdentifier) {
		Intent overlaySettings = new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION);
		overlaySettings.addCategory(Intent.CATEGORY_DEFAULT);
		overlaySettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivityForResult(overlaySettings, callbackIdentifier);
	}

	@TargetApi(21)
	private void goToUsageSettings(Integer callbackIdentifier) {
		Intent usageSettings = new Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS);
		usageSettings.addCategory(Intent.CATEGORY_DEFAULT);
		usageSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivityForResult(usageSettings, callbackIdentifier);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		aboutToResetFalseActivityReturn = true;
	}

	public void initializePermissionAlertFlags(){
		prePromptActive = false;
		postPromptActive = false;
		powerPromptActive = false;
		usagePromptActive = false;
		overlayPromptActive = false;
		accessibilityPromptActive = false;
		thisResumeCausedByFalseActivityReturn = false;
		aboutToResetFalseActivityReturn = false;
	}

	public void createPermissionNotification(){
		Class cls = MainMenuActivity.class;
		Intent intent = new Intent(this, cls);
		PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent,
				PendingIntent.FLAG_UPDATE_CURRENT);
		Notification.Builder n  = new Notification.Builder(this)
				.setContentTitle("Permissions Revoked")
				.setContentText("Please open the app and grant permissions for the study")
				.setSmallIcon(R.mipmap.ic_launcher)
				.setContentIntent(pIntent)
				.setAutoCancel(true);

		if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
			String channelId = "hopes_1";
			NotificationChannel channel = new NotificationChannel(channelId, "HOPES",
					NotificationManager.IMPORTANCE_DEFAULT);
			mNotificationManager.createNotificationChannel(channel);
			n.setChannelId(channelId);
		}

		mNotificationManager.notify(0, n.build());
	}

	@Override
	public void onRequestPermissionsResult (int requestCode,
											String[] permissions,
											int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (!activityNotVisible) checkPermissionsLogic();
	}

	protected void checkPermissionsLogic() {
		// gets called as part of onResume,
		activityNotVisible = false;

		if (aboutToResetFalseActivityReturn) {
			aboutToResetFalseActivityReturn = false;
			thisResumeCausedByFalseActivityReturn = false;
			return;
		}

		if (!thisResumeCausedByFalseActivityReturn) {
			String permission = PermissionHandler.getNextPermission(getApplicationContext(), this.isAudioRecorderActivity());
			if (permission==null) return;
			BackgroundService.finalSetupDone = false;
			if (!prePromptActive && !postPromptActive && !powerPromptActive) {
				if (permission.equals(PermissionHandler.POWER_EXCEPTION_PERMISSION)) {
					showPowerManagementAlert(this,
							getString(R.string.power_management_exception_alert),
							1000);
					return;
				}

				if (permission.equals(PermissionHandler.USAGE_EXCEPTION_PERMISSION)) {
					showUsagePermissionAlert(this,
							getString(R.string.usage_permission_alert),
							1001);
					return;
				}

				if (permission.equals(PermissionHandler.APPLICATION_OVERLAY_PERMISSION)) {
					showOverlayPermissionAlert(this,
							getString(R.string.overlay_permission_alert),
							1002);
					return;
				}

				if (permission.equals(PermissionHandler.ACCESSIBILITY_OVERLAY_PERMISSION)) {
					showAccessibilityPermissionAlert(this,
							getString(R.string.accessibility_permission_alert),
							1003);
					return;
				}

				if (permission.equals("android.permission.ACCESS_BACKGROUND_LOCATION")) {
					showRegularPermissionAlertWithMsg(this,
							getString(R.string.regular_permission_alert)+PermissionHandler.permissionMessages.get(permission),
							permission,
							1004);
					return;
				}

				if (shouldShowRequestPermissionRationale(permission)) {
					if (!prePromptActive && !postPromptActive)
						showAlertThatForcesUserToGrantPermission(this,
								PermissionHandler.getBumpingPermissionMessage(permission), 1 );
				} else if (!prePromptActive && !postPromptActive )
					showRegularPermissionAlert(this,
							PermissionHandler.getNormalPermissionMessage(permission), permission, 1);
			}
		}
	}

	/* Message Popping */
	public static void showRegularPermissionAlert(final Activity activity,
												  final String message,
												  final String permission,
												  final Integer permissionCallback) {
		if (prePromptActive) return;
		prePromptActive = true;
		activity.requestPermissions(new String[]{ permission }, permissionCallback );
		prePromptActive = false;
	}

	public static void showRegularPermissionAlertWithMsg(final Activity activity,
	                                              final String message,
	                                              final String permission,
	                                              final Integer permissionCallback) {
		if (prePromptActive) return;
		prePromptActive = true;
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle("Permissions Requirement:");
		builder.setMessage(message);
		builder.setPositiveButton("OK", (arg0, arg1) -> {
			activity.requestPermissions(new String[]{ permission }, permissionCallback );
			prePromptActive = false;
		});
		if (alertDialog==null || !alertDialog.isShowing()) {
			alertDialog = builder.create();
			alertDialog.show();
		}
	}

	public static void showAlertThatForcesUserToGrantPermission(
			final RunningBackgroundServiceActivity activity,
			final String message,
			final Integer permissionCallback) {
		if (postPromptActive) return;
		postPromptActive = true;
		thisResumeCausedByFalseActivityReturn = true;
		activity.goToSettings(permissionCallback);
		postPromptActive = false;
	}

	public static void showAlertThatEnablesUserToGrantPermission(
			final RunningBackgroundServiceActivity activity,
			final String message,
			final String title,
			final Integer permissionCallback) {
		activity.goToSettings(permissionCallback);
	}

	public static void showPowerManagementAlert(
			final RunningBackgroundServiceActivity activity,
			final String message,
			final Integer powerCallbackIdentifier) {
		if (powerPromptActive) return;
		powerPromptActive = true;
		thisResumeCausedByFalseActivityReturn = true;
		activity.goToPowerSettings(powerCallbackIdentifier);
		powerPromptActive = false;
	}

	public static void showAccessibilityPermissionAlert(
			final RunningBackgroundServiceActivity activity,
			final String message,
			final Integer callbackIdentifier) {
		if (accessibilityPromptActive) { return; }
		accessibilityPromptActive = true;
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle("Permissions Requirement:");
		builder.setMessage(message);
		builder.setOnDismissListener( new DialogInterface.OnDismissListener() { @Override public void onDismiss(DialogInterface dialog) {
			thisResumeCausedByFalseActivityReturn = true;
			activity.goToAccessibilitySettings(callbackIdentifier);
			accessibilityPromptActive = false;
		} } );
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { @Override public void onClick(DialogInterface arg0, int arg1) {  } } ); //Okay button
		builder.create().show();
	}

	public static void showOverlayPermissionAlert(
			final RunningBackgroundServiceActivity activity,
			final String message,
			final Integer callbackIdentifier) {
		if (overlayPromptActive) { return; }
		overlayPromptActive = true;
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle("Permissions Requirement:");
		builder.setMessage(message);
		builder.setOnDismissListener( new DialogInterface.OnDismissListener() { @Override public void onDismiss(DialogInterface dialog) {
			thisResumeCausedByFalseActivityReturn = true;
			activity.goToOverlaySettings(callbackIdentifier);
			overlayPromptActive = false;
		} } );
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() { @Override public void onClick(DialogInterface arg0, int arg1) {  } } ); //Okay button
		builder.create().show();
	}

	public static void showUsagePermissionAlert(
			final RunningBackgroundServiceActivity activity,
			final String message,
			final Integer callbackIdentifier) {
		if (usagePromptActive) { return; }
		usagePromptActive = true;
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle("Permissions Requirement:");
		builder.setMessage(message);
		builder.setOnDismissListener( new DialogInterface.OnDismissListener() {
			@Override public void onDismiss(DialogInterface dialog) {
			thisResumeCausedByFalseActivityReturn = true;
			activity.goToUsageSettings(callbackIdentifier);
			usagePromptActive = false; } } );
		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			@Override public void onClick(DialogInterface arg0, int arg1) {  } } ); //Okay button
		builder.create().show();
	}

//	public static int nNeedToClick;
//	private static int menu_item_id;
//	public void onClickText(View view){
//		if(--nNeedToClick==0)
//			findViewById(R.id.resetAPP).setVisibility(View.VISIBLE);
//		if(nNeedToClick==-10) {
//			Button button = findViewById(R.id.resetAPP);
//			button.setTextColor(0xffff0000);
//			button.setOnLongClickListener(view1 -> {
//				try {
//					MenuItem mi = s_menu.add("Unlock Debug Interface");
//					menu_item_id = mi.getItemId();
//					mi.setOnMenuItemClickListener(item -> {
//						s_menu.removeItem(menu_item_id);
//						promptView("This will unlock debug interface. Are you sure?", () -> {
//							DebugInterfaceActivity.unlocked = true;
//							startActivity(new Intent(mSelf, DebugInterfaceActivity.class));
//							return null;
//						});
//						return true;
//					});
//				} catch (Exception ignored) {}
//				return true;
//			});
//		}
//	}

//	public void promptView(String msg, Callable func){
//		LayoutInflater li = LayoutInflater.from(this);
//		View promptsView = li.inflate(R.layout.password_prompt, null);
//		final EditText userInput = promptsView.findViewById(R.id.editTextDialogUserInput);
//
//		DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
//			if(which==DialogInterface.BUTTON_POSITIVE &&
//					userInput.getText().toString().equals("MOHTp@ssw0rd"))
//				try{func.call();} catch (Exception ignored){}
//		};
//
//		new AlertDialog.Builder(this)
//				.setView(promptsView)
//				.setTitle("Warning")
//				.setMessage(msg)
//				.setPositiveButton("OK", dialogClickListener)
//				.setNegativeButton("Cancel", dialogClickListener).show();
//	}

	public void resetAPP(View view) {
		// Uncomment the below lines and above method if you wanted to use secret access to debug interface on dev version
//		if (!BuildConfig.APP_IS_BETA) return;
//		promptView("This will unregister any study and reset the APP. Are you sure?", () -> {
//			DebugInterfaceActivity.RESET();
//			return null;
//		});
	}
}
