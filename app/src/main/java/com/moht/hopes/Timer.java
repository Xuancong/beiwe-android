package com.moht.hopes;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.moht.hopes.storage.PersistentData;

import java.util.Calendar;

/** The Timer class provides a meeans of setting various timers.  These are used by the BackgroundService
 * for devices that must be turned on/off, and timing the user to automatically logout after a period of time.
 * This class includes all the Intents and IntentFilters we for trigged broadcasts.
 * @author Eli, Dor */
public class Timer {
	private AlarmManager alarmManager;
	private Context appContext;
	
//	public static final long ONE_DAY_IN_MILLISECONDS = 24 * 60 * 60 * 1000L;
//	public static final long ONE_WEEK_IN_MILLISECONDS = 7 * ONE_DAY_IN_MILLISECONDS;
	
	// Control Message Intents
	public static Intent accelerometerOffIntent;
	public static Intent accelerometerOnIntent;
	public static Intent bluetoothOffIntent;
	public static Intent bluetoothOnIntent;
//	public static Intent dailySurveyIntent;
	public static Intent gyroscopeOffIntent;
	public static Intent gyroscopeOnIntent;
	public static Intent magnetometerOffIntent;
	public static Intent magnetometerOnIntent;
//	public static Intent stepsOffIntent;
//	public static Intent stepsOnIntent;
	public static Intent gpsOffIntent;
	public static Intent gpsOnIntent;
	static Intent signoutIntent;
//	public static Intent voiceRecordingIntent;
//	public static Intent weeklySurveyIntent;
	public static Intent wifiLogIntent;
	static Intent uploadDatafilesIntent;
	static Intent createNewDataFilesIntent;
	static Intent checkForNewSurveysIntent;
	static Intent checkForSMSEnabled;
	static Intent checkForCallsEnabled;
	public static Intent ambientLightIntent;
	public static Intent ambientTemperatureIntent;
	public static Intent usageIntent;

	// Constructor
	public Timer( BackgroundService backgroundService ) {
		appContext = backgroundService.getApplicationContext();
		alarmManager = (AlarmManager)( backgroundService.getSystemService( Context.ALARM_SERVICE ));
		
		// double alarm intents
		accelerometerOffIntent = setupIntent( appContext.getString(R.string.turn_accelerometer_off) );
		accelerometerOnIntent = setupIntent( appContext.getString(R.string.turn_accelerometer_on) );
		ambientLightIntent = setupIntent( appContext.getString(R.string.turn_ambientlight_on) );
		ambientTemperatureIntent = setupIntent( appContext.getString(R.string.turn_ambienttemperature_on) );
		bluetoothOffIntent = setupIntent( appContext.getString(R.string.turn_bluetooth_off) );
		bluetoothOnIntent = setupIntent( appContext.getString(R.string.turn_bluetooth_on) );
		gyroscopeOffIntent = setupIntent( appContext.getString(R.string.turn_gyroscope_off) );
		gyroscopeOnIntent = setupIntent( appContext.getString(R.string.turn_gyroscope_on) );
		gpsOffIntent = setupIntent( appContext.getString(R.string.turn_gps_off) );
		gpsOnIntent = setupIntent( appContext.getString(R.string.turn_gps_on) );
		magnetometerOffIntent = setupIntent( appContext.getString(R.string.turn_magnetometer_off) );
		magnetometerOnIntent = setupIntent( appContext.getString(R.string.turn_magnetometer_on) );
//		stepsOffIntent = setupIntent( appContext.getString(R.string.turn_steps_off) );
//		stepsOnIntent = setupIntent( appContext.getString(R.string.turn_steps_on) );
		usageIntent = setupIntent( appContext.getString(R.string.update_usage) );
		
		// Set up event triggering alarm intents
		signoutIntent = setupIntent( appContext.getString(R.string.signout_intent) );
		wifiLogIntent = setupIntent( appContext.getString(R.string.run_wifi_log) );
		uploadDatafilesIntent = setupIntent( appContext.getString(R.string.upload_data_files_intent) );
		createNewDataFilesIntent = setupIntent( appContext.getString(R.string.create_new_data_files_intent) );
		checkForNewSurveysIntent = setupIntent( appContext.getString(R.string.check_for_new_surveys_intent) );

		checkForSMSEnabled = setupIntent( appContext.getString( R.string.check_for_sms_enabled ) );
		checkForCallsEnabled = setupIntent( appContext.getString( R.string.check_for_calls_enabled ) );
	}
	
	/* ######################################################################
	 * ############################ Common Code #############################
	 * ####################################################################*/
	
	// Setup custom intents to be sent to the listeners running in the background service
	private static Intent setupIntent( String action ){
		Intent newIntent = new Intent();
		newIntent.setAction( action );
		return newIntent;
	}
	
	/* ###############################################################################################
	 * ############################ The Various Types of Alarms Creation #############################
	 * #############################################################################################*/
	
	/** Single exact alarm for an event that happens once.
	 * @return a long of the system time in milliseconds that the alarm was set for. */
	public Long setupExactSingleAlarm(Long milliseconds, Intent intentToBeBroadcast) {
		long triggerTime = System.currentTimeMillis() + milliseconds;
		PendingIntent pendingIntent = PendingIntent.getBroadcast(appContext, 0, intentToBeBroadcast, 0);
		setExactAlarm(AlarmManager.RTC_WAKEUP, triggerTime, pendingIntent);
		return triggerTime;
	}
	
	/** setupExactTimeAlarm creates an Exact Alarm that will go off at a specific time within a
	 * period, e.g. every hour (period), at 47 minutes past the hour (start time within period).
	 * setupExactTimeAlarm is used for the Bluetooth timer, so that every device that has this hopes
	 * turns on its Bluetooth at the same moment. */
	void setupExactSingleAbsoluteTimeAlarm(long period, long startTimeInPeriod, Intent intentToBeBroadcast) {
		long currentTime = System.currentTimeMillis();
		// current unix time (mod) 3,600,000 milliseconds = the next hour-boundary, to which we add the EXACT_REPEAT_TIMER_OFFSET.
		long nextTriggerTime = currentTime - ( currentTime % period ) + startTimeInPeriod;
		if (nextTriggerTime < currentTime) { nextTriggerTime += period; }
		PendingIntent pendingTimerIntent = PendingIntent.getBroadcast(appContext, 0, intentToBeBroadcast, 0);
		setExactAlarm(AlarmManager.RTC_WAKEUP, nextTriggerTime, pendingTimerIntent);
	}
	
	void setupSurveyAlarm(String surveyId, Calendar alarmTime){
		Intent intentToBeBroadcast = new Intent(surveyId);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(appContext, 0, intentToBeBroadcast, 0);
		long nextTriggerTime = alarmTime.getTimeInMillis();
		setExactAlarm(AlarmManager.RTC_WAKEUP, nextTriggerTime, pendingIntent);
		PersistentData.setMostRecentSurveyAlarmTime(surveyId, nextTriggerTime);
	}

	public void setupSurveyExpire(String surveyId, Calendar alarmTime){
		Intent intentToBeBroadcast = new Intent(appContext.getString(R.string.expire_survey_notification));
		intentToBeBroadcast.putExtra("survey_id", surveyId);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(appContext, 0, intentToBeBroadcast,0);
		setExactAlarm(AlarmManager.RTC_WAKEUP, alarmTime.getTimeInMillis(), pendingIntent);
	}

	public void setupNotificationExpire(int pushInfoHash, Calendar alarmTime){
		Intent intentToBeBroadcast = new Intent(appContext.getString(R.string.expire_push_notification));
		intentToBeBroadcast.putExtra("push_hash", pushInfoHash);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(appContext, 0, intentToBeBroadcast,0);
		setExactAlarm(AlarmManager.RTC_WAKEUP, alarmTime.getTimeInMillis(), pendingIntent);
	}

	/* ##################################################################################
	 * ############################ Other Utility Functions #############################
	 * ################################################################################*/	
	
	/** In API 19 and above, alarms are inexact (to save power).  In API 18 and
	 *  below, alarms are exact.
	 *  This function checks the phone's operating system's API version and
	 *  returns TRUE if alarms are exact in this version (i.e., if it's API 18
	 *  or below), and returns FALSE if alarms are inexact.  */
	private static Boolean alarmsAreExactInThisApiVersion() {
		int currentApiVersion = android.os.Build.VERSION.SDK_INT;
		return currentApiVersion < android.os.Build.VERSION_CODES.KITKAT;
	}
	
	/** Calls AlarmManager.set() for API < 19, and AlarmManager.setExact() for API 19+
	 * For an exact alarm, it seems you need to use .set() for API 18 and below, and
	 * .setExact() for API 19 (KitKat) and above. */
	private void setExactAlarm(int type, long triggerAtMillis, PendingIntent operation) {
		if (alarmsAreExactInThisApiVersion()) {
			alarmManager.set(type, triggerAtMillis, operation); }
		else { alarmManager.setExact(type, triggerAtMillis, operation); }
	}
	
	/**Cancels an alarm, does not return any info about whether the alarm existed.
	 * @param intentToBeBroadcast an Intent identifying the alarm to cancel. */
	void cancelAlarm(Intent intentToBeBroadcast) {
		PendingIntent pendingIntent = PendingIntent.getBroadcast(appContext, 0, intentToBeBroadcast, 0);
		alarmManager.cancel(pendingIntent);
	}
		
	/**Checks if an alarm is set.
	 * @param intent an Intent identifying the alarm to check.
	 * @return Returns TRUE if there is an alarm set matching that intent; otherwise false. */
	Boolean alarmIsSet(Intent intent) {
		PendingIntent pendingIntent = PendingIntent.getBroadcast(appContext, 0, intent, PendingIntent.FLAG_NO_CREATE);
		return pendingIntent != null;
	}
}
