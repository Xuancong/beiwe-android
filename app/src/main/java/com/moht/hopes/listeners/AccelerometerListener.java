package com.moht.hopes.listeners;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import androidx.annotation.Keep;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.storage.TextFileManager;

public class AccelerometerListener implements SensorEventListener{
	public static final String name = "accelerometer";
	public static final String short_name = "acc";
	public static final String header = "timestamp,accuracy,x,y,z";

	private SensorManager accelSensorManager;
	private Sensor accelSensor;

	private Boolean exists;
	private Boolean enabled = null;
	private String accuracy;
	private long lastTimeMillis = 0;
	
	public static int checkPermission(){ return 0; }
	
	/**Listens for accelerometer updates.  NOT activated on instantiation.
	 * Use the turn_on() function to log any accelerometer updates to the 
	 * accelerometer log.
	 * @param applicationContext a Context from an activity or service. */
	public AccelerometerListener(Context applicationContext){
		PackageManager pkgManager = applicationContext.getPackageManager();
		this.accuracy = "unknown";
		this.exists = pkgManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_ACCELEROMETER);
		
		if (this.exists) {
			enabled = false;
			this.accelSensorManager = (SensorManager) applicationContext.getSystemService(Context.SENSOR_SERVICE);
			if (this.accelSensorManager ==  null ) {
				if(BuildConfig.APP_IS_DEBUG) Log.e("Accelerometer Problems", "accelSensorManager does not exist? (1)" );
				TextFileManager.getDebugLogFile().write("accelSensorManager does not exist? (1)");
				exists = false;	}
			
			this.accelSensor = accelSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			if (this.accelSensor == null ) {
				if(BuildConfig.APP_IS_DEBUG) Log.e("Accelerometer Problems", "accelSensor does not exist? (2)" );
				TextFileManager.getDebugLogFile().write("accelSensor does not exist? (2)");
				exists = false;
			}
		}
	}

	public synchronized void turn_on() {
		if ( !this.exists ) return;
		if ( !BackgroundService.sensorManager.registerListener(this, accelSensor, SensorManager.SENSOR_DELAY_NORMAL) ) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("Accelerometer", "Accelerometer is broken");
			TextFileManager.getDebugLogFile().write("Trying to start Accelerometer session, device cannot find accelerometer."); }
		enabled = true;	}
	
	public synchronized void turn_off(){
		BackgroundService.sensorManager.unregisterListener(this);
		enabled = false; }
	
	/** Update the accuracy, synchronized so very closely timed trigger events do not overlap.
	 * (only triggered by the system.) */
	@Override
	public synchronized void onAccuracyChanged(Sensor arg0, int arg1) {	accuracy = "" + arg1; }
	
	/** On receipt of a sensor change, record it.  Include accuracy. 
	 * (only ever triggered by the system.) */
	@Override
	public synchronized void onSensorChanged(SensorEvent arg0) {
		long currentTimeMillis = System.currentTimeMillis();
		if(currentTimeMillis-lastTimeMillis<500) return;
		lastTimeMillis = currentTimeMillis;
		float[] values = arg0.values;
		String data = currentTimeMillis + TextFileManager.DELIMITER
				+ accuracy + TextFileManager.DELIMITER
				+ values[0] + TextFileManager.DELIMITER
				+ values[1] + TextFileManager.DELIMITER + values[2];
		TextFileManager.getFile(this).write(data);
	}
}