package com.moht.hopes.listeners;

/* *********************************************************************
 *
 * MOH Office of Healthcare Transformation (MOHT) CONFIDENTIAL
 *
 *  Copyright 2018-2019
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of MOH Office of Healthcare Transformation.
 * The intellectual and technical concepts contained
 * herein are proprietary to MOH Office of Healthcare Transformation
 * and may be covered by Singapore, U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from MOH Office of Healthcare Transformation.
 */

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.AccessibilityServiceInfo;
import android.accessibilityservice.GestureDescription;
import android.annotation.SuppressLint;
import android.app.Notification;
import android.content.Context;
import android.graphics.Path;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import androidx.annotation.RequiresApi;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.tasks.NotificationProcessTask;
import com.moht.hopes.tasks.SociabilityCallProcessTask;
import com.moht.hopes.tasks.SociabilityProcessTask;
import com.moht.hopes.threadManagers.NotificationManager;
import com.moht.hopes.threadManagers.SociabilityCallManager;
import com.moht.hopes.threadManagers.SociabilityManager;
import com.moht.hopes.ui.DebugInterfaceActivity;
import com.moht.hopes.utils.AccessibilityEventInformation;
import com.moht.hopes.utils.SociabilityMessageStructure;
import com.moht.hopes.utils.SociabilityUtils;
import com.moht.hopes.utils.WhatsAppResourceId;
import com.moht.hopes.utils.WhatsappUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static android.graphics.PixelFormat.TRANSLUCENT;
import static android.view.MotionEvent.ACTION_DOWN;
import static android.view.MotionEvent.ACTION_MOVE;
import static android.view.MotionEvent.ACTION_OUTSIDE;
import static android.view.MotionEvent.ACTION_UP;
import static android.view.WindowManager.LayoutParams.FLAG_FULLSCREEN;
import static android.view.WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE;
import static android.view.WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH;
import static android.view.WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN;

public class AccessibilityListener extends AccessibilityService {
	public static final String name = "accessibility";
	public static final String short_name = "acb";
	public static final String header = "timestamp,packageName,className,text,orientation";
	public static boolean listen = false;

	@SuppressLint("StaticFieldLeak")
	public static AccessibilityListener mSelf = null;
	private Context hContext;
	private AccessibilityOverlayView overlayView;
	private WindowManager localWindowManager;
	private WindowManager.LayoutParams localLayoutParams;

	public AccessibilityListener() {}

	private static GestureDescription createClick(float x, float y, int duration_ms, boolean willContinue) {
		Path clickPath = new Path();
		clickPath.moveTo(x, y);
		GestureDescription.StrokeDescription clickStroke =
				new GestureDescription.StrokeDescription(clickPath, 0, duration_ms, willContinue);
		GestureDescription.Builder clickBuilder = new GestureDescription.Builder();
		clickBuilder.addStroke(clickStroke);
		return clickBuilder.build();
	}

	private static GestureDescription createLongClick(float x, float y, int duration_ms, boolean willContinue) {
		Path clickPath = new Path();
		clickPath.moveTo(x, y);
		clickPath.rLineTo(0.1f, 0.1f );
		GestureDescription.StrokeDescription clickStroke =
				new GestureDescription.StrokeDescription(clickPath, 0, duration_ms, willContinue);
		GestureDescription.Builder clickBuilder = new GestureDescription.Builder();
		clickBuilder.addStroke(clickStroke);
		return clickBuilder.build();
	}

	private GestureDescription createStroke(int duration_ms, boolean willContinue) {
		Path clickPath = new Path();
		clickPath.moveTo(lastX, lastY);
		for(P p : XYs)
			clickPath.lineTo(p.X, p.Y);
		GestureDescription.StrokeDescription clickStroke =
				new GestureDescription.StrokeDescription(clickPath, 0, duration_ms, willContinue);
		GestureDescription.Builder clickBuilder = new GestureDescription.Builder();
		clickBuilder.addStroke(clickStroke);
		return clickBuilder.build();
	}

	class P {
		float X, Y;
		P(float x, float y){ X=x; Y=y; }
	}

	private ArrayList <P> XYs = new ArrayList<>();
	private float lastX, lastY;

	class AccessibilityOverlayView extends View {
		AccessibilityOverlayView(Context paramContext) { super(paramContext); }

		private long lastTime;

		@SuppressLint({"ClickableViewAccessibility"})
		public boolean onTouchEvent( MotionEvent e )
		{
			GestureDescription gesture;
			if ( e != null ) {
				DebugInterfaceActivity.smartLog( name, e.toString() );

				switch (e.getAction()){
					case ACTION_DOWN:
						lastTime = e.getEventTime();
						lastX = e.getRawX();
						lastY = e.getRawY();
						break;
					case ACTION_MOVE:
						XYs.add(new P(e.getRawX(), e.getRawY()));
						break;
					case ACTION_UP:
						long nowTime = e.getEventTime();
						localWindowManager.removeViewImmediate(overlayView);
						if(XYs.isEmpty()){
							long duration = nowTime -lastTime;
							if( duration>500 )
								gesture = createLongClick( e.getRawX(), e.getRawY(), (int)duration, false );
							else
								gesture = createClick( e.getRawX(), e.getRawY(), 1, false );
						} else
							gesture = createStroke( (int)(nowTime -lastTime), false );
						dispatchGesture( gesture, null, null );
						XYs.clear();
						localWindowManager.addView( overlayView, localLayoutParams );
						break;
					case ACTION_OUTSIDE:
					default:
				}
				e.getAction();
			}
			return true;
		}
	}

	@Override
	protected void onServiceConnected() {
		super.onServiceConnected();
		listen = true;
		mSelf = this;
		hContext = getApplicationContext();
		/* After reboot/power-on, the Android OS will resume all accessibility services before background services.
		 * Thus, many BackgroundService pointers (including BackgroundService.localHandle) will be null
		 * for a few seconds before the main service gets resumed by the OS. Here, we bring up the main
		 * service immediately to speed up service resumption after reboot. */
		if ( BackgroundService.localHandle == null )
			BootListener.startBackgroundService(hContext);
	}

	public static boolean isGestureMode = false;

	public void toggleGestureMode(){
		try {
			if (!isGestureMode) {
				int flags = FLAG_WATCH_OUTSIDE_TOUCH | SOFT_INPUT_ADJUST_PAN | FLAG_NOT_FOCUSABLE | FLAG_FULLSCREEN;
				localLayoutParams = new WindowManager.LayoutParams(
						ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT,
						WindowManager.LayoutParams.TYPE_ACCESSIBILITY_OVERLAY, flags, TRANSLUCENT);

				localWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);

				if (localWindowManager != null && Settings.canDrawOverlays(hContext)) {
					overlayView = new AccessibilityOverlayView(BackgroundService.localHandle.getApplicationContext());
					localWindowManager.addView(overlayView, localLayoutParams);
				}
			} else
				localWindowManager.removeViewImmediate(overlayView);
			isGestureMode = !isGestureMode;
		} catch (Exception e){
			TextFileManager.getDebugLogFile().write("AccessibilityLister::toggleGestureMode: " + Arrays.toString(e.getStackTrace()));
		}
	}

	public static boolean isEnabled(Context context){
		if ( BackgroundService.accessibilityManager == null )
			return false;
		List<AccessibilityServiceInfo> runningServices =
				BackgroundService.accessibilityManager.getEnabledAccessibilityServiceList(AccessibilityEvent.TYPES_ALL_MASK);
		for ( AccessibilityServiceInfo info : runningServices ){
			if (info.getId().startsWith(context.getPackageName()))
				return true;
		}
		return false;
	}

	public static int checkPermission(){
		return isEnabled(BackgroundService.appContext)?1:-1;
	}

	public static String convertKeyChar( String text ){
		if(text.startsWith("[") && text.endsWith("]"))
			text = text.replace("[", "").replace("]", "");
		switch (text.toLowerCase()) {
			case "done":
			case "recents":
			case "clear":
			case "close all":
			case "close":
			case "back":
			case "delete":
			case "backspace":
			case "enter":
			case "recent apps":
			case "overview":
			case "home":
			case "clear all":
			case "navigate up":
			case "close all recent apps":
				return text;
			default:
				if(text.length()==1){
					char ch = text.charAt(0);
					if(Character.isAlphabetic(ch)) return "a";
					if(Character.isDigit(ch)) return "0";
					return ".";
				}
		}
		return "[OTHER]";
	}

	private static String last_package_name = "", last_class_name = "";
	public void logi( String tag, Object object ){
		if(object instanceof String) {
			String s = (String)object;
			if(BuildConfig.APP_IS_DEBUG)
				for (int x = 0, X = s.length() / 4000; x <= X; ++x) {
					if (x == X)
						Log.i(tag, s.substring(x * 4000));
					else
						Log.i(tag, s.substring(x * 4000, (x + 1) * 4000));
				}
		} else {
			AccessibilityEvent event = (AccessibilityEvent) object;
			String msg = TextFileManager.CS2S(event.getContentDescription());
			if(msg.isEmpty())
				msg = TextFileManager.CS2S(event.getText());
			String package_name = TextFileManager.CS2S(event.getPackageName());
			String class_name = TextFileManager.CS2S(event.getClassName());
			String data = System.currentTimeMillis() + TextFileManager.DELIMITER +
					(package_name.equals(last_package_name) ? "" : package_name) +
					TextFileManager.DELIMITER +
					(class_name.equals(last_class_name) ? "" : class_name) +
					TextFileManager.DELIMITER + convertKeyChar(msg) +
					TextFileManager.DELIMITER + getResources().getConfiguration().orientation;
			last_package_name = package_name;
			last_class_name = class_name;
			TextFileManager.getFile(this).write(data);
		}
	}

	@Override
	public void onInterrupt() {}

	@Override
	protected boolean onGesture(int gestureId){
		return super.onGesture(gestureId);
	}

	@Override
	protected boolean onKeyEvent (KeyEvent event){
		return super.onKeyEvent(event);
	}

	// START OF NEW CODE
	private static boolean isPopup = false;
	private static boolean isNotificationReply = false;
	private static boolean isCall = false;
	private static boolean processCall = false;
	private static String popupCount = "";
	private static List<AccessibilityEventInformation> eventInfos = new ArrayList<>();
	private static List<AccessibilityEventInformation> callEventInfos = new ArrayList<>();
	List<String> ids = new ArrayList<>(Arrays.asList(
			WhatsAppResourceId.POPUPCONTACT.getId(),
			WhatsAppResourceId.POPUPCOUNT.getId(),
			WhatsAppResourceId.POPUPVOICENOTE.getId(),
			WhatsAppResourceId.CONTACTNAME.getId(),
			WhatsAppResourceId.MESSAGELIST.getId(),
			WhatsAppResourceId.NOTIFICATIONTEXTBOX.getId(),
			WhatsAppResourceId.CALLSCREEN.getId(),
			WhatsAppResourceId.CALLSTATUS.getId(),
			WhatsAppResourceId.CALLVIDEOCONTAINER.getId(),
			WhatsAppResourceId.CALLVIDEOSTATUS.getId(),
			WhatsAppResourceId.CALLLABEL.getId(),
			WhatsAppResourceId.TEXTBOX.getId()));
	Map<String, AccessibilityNodeInfo> nodesMap = new HashMap<>();

	private List<String> previousMessages = new ArrayList<>();
	private List<String> newMessages =  new ArrayList<>();

	@RequiresApi(api = Build.VERSION_CODES.KITKAT)
	@Override
	public void onAccessibilityEvent(final AccessibilityEvent event) {
		if(listen) {
			if (event.getPackageName() == null) return;
			long timeStamp = System.currentTimeMillis();

			switch (event.getEventType()) {
				// capture all the whatsapp notifications and process
				case AccessibilityEvent.TYPE_NOTIFICATION_STATE_CHANGED:
					// check for whatsapp app
					if (SociabilityUtils.isIMApp(event.getPackageName().toString())) return;
					if(!event.getPackageName().toString()
							.equals(SociabilityUtils.APP_PACKAGE_WHATSAPP)) return;

					// check notification for whatsapp call / message
					Notification notification = (Notification) event.getParcelableData();
					if (notification==null) return;

					// get the message / contact from the bundle and check for call
					Bundle bundle = notification.extras;
					CharSequence message = bundle.getCharSequence(Notification.EXTRA_TEXT);
					CharSequence contact = bundle.getCharSequence(Notification.EXTRA_TITLE);

					if (message!=null){
						String text = message.toString();
						// check for incoming / outgoing calls and add event to list
						if (text.contains("Incoming voice call") ||
								text.contains("Incoming video call") ||
								text.contains("Calling")) {
							isCall = true;
							callEventInfos.add(new AccessibilityEventInformation(event,
									getRootInActiveWindow(), new HashMap<>(),
									bundle, timeStamp));
						}
					}

					if (contact!=null){
						String contactText = contact.toString();
						// check for missed call and process the call
						if (contactText.contains("issed") && contactText.contains(" call")) {
							callEventInfos.add(new AccessibilityEventInformation(event,
									getRootInActiveWindow(), new HashMap<>(),
									bundle, timeStamp));
							manageSociabilityCallEvents();
							isCall = false;
							processCall = false;
							callEventInfos = new ArrayList<>();
							return;
						}
					}

					// handle notification as messages if its not call
					if (!isCall) manageNotificationMessages(bundle);
					break;

				// check for call screen end / popup start
				case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
					// get important nodes from screen to determine the type of message / call
					nodesMap = SociabilityUtils.findNodesByViewIds(getRootInActiveWindow(), ids);

					// check for whatsapp audio / video call end
					if (isCall && !processCall &&
							nodesMap.get(WhatsAppResourceId.CALLSCREEN.getId())==null)
						processCall = true;

					// check for whatsapp app
					if (SociabilityUtils.isIMApp(event.getPackageName().toString())) return;
					if(!event.getPackageName().toString()
							.equals(SociabilityUtils.APP_PACKAGE_WHATSAPP)) return;

					// whatsapp popup starts with window state change
					if (nodesMap.get(WhatsAppResourceId.POPUPCONTACT.getId())!=null){
						eventInfos.add(new AccessibilityEventInformation(
								event, getRootInActiveWindow(), nodesMap, timeStamp));
						isPopup = true;
					}
					break;

				// add text change events to event list if it is a popup / notification reply
				case AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED:
					// check for whatsapp app
					if (SociabilityUtils.isIMApp(event.getPackageName().toString())) return;
					if(!event.getPackageName().toString()
							.equals(SociabilityUtils.APP_PACKAGE_WHATSAPP)) return;

					// get important nodes from screen to determine the type of message / call
					nodesMap = SociabilityUtils.findNodesByViewIds(getRootInActiveWindow(), ids);

					// check for notification text box and set notification reply flag
					AccessibilityNodeInfo notificationTextboxNode = nodesMap
							.get(WhatsAppResourceId.NOTIFICATIONTEXTBOX.getId());
					if (notificationTextboxNode!=null) isNotificationReply = true;

					// check for popup reply / notification reply and event to list
					if (nodesMap.get(WhatsAppResourceId.POPUPCONTACT.getId())!=null ||
							(isNotificationReply && notificationTextboxNode!=null &&
									notificationTextboxNode.getText()!=null))
						eventInfos.add(new AccessibilityEventInformation(
								event, getRootInActiveWindow(), nodesMap, timeStamp));
					break;

				// handle whatsapp notification reply / popup reply / chat screen messages / calls
				case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
					// check for whatsapp app
					if (SociabilityUtils.isIMApp(event.getPackageName().toString())) return;
					if(!event.getPackageName().toString()
							.equals(SociabilityUtils.APP_PACKAGE_WHATSAPP)) return;

					// get important nodes from screen to determine the type of message / call
					nodesMap = SociabilityUtils.findNodesByViewIds(getRootInActiveWindow(), ids);

					// if whatsapp popup or notification reply add event info to list
					if (isPopup || isNotificationReply) {
						eventInfos.add(new AccessibilityEventInformation(
								event, getRootInActiveWindow(), nodesMap, timeStamp));
					}

					// if whatsapp call add event to call events info list
					if (isCall) {
						callEventInfos.add(new AccessibilityEventInformation(
								event, getRootInActiveWindow(), nodesMap, null, timeStamp));
					}

					// if its call & process call -> assume call end and process the call events
					if (isCall && processCall){
						manageSociabilityCallEvents();
						isCall = false;
						processCall = false;
						callEventInfos = new ArrayList<>();
						return;
					}

					// if notification reply and edit text box disappears process reply messages
					if (isNotificationReply &&
							nodesMap.get(WhatsAppResourceId.NOTIFICATIONTEXTBOX.getId())==null){
						manageSociabilityMessageEvents();
						eventInfos = new ArrayList<>();
						isNotificationReply = false;
						return;
					}

					// if whatsapp popup & popup count reduce from 2 to null process popup reply
					if (nodesMap.get(WhatsAppResourceId.POPUPCONTACT.getId())!=null
							&& popupCount.equals("2") &&
							nodesMap.get(WhatsAppResourceId.POPUPCOUNT.getId())==null){
						manageSociabilityMessageEvents();
						eventInfos = new ArrayList<>();
						popupCount = "";
						return;
					}

					// if whatsapp popup & popup count reduce from n to n-1 process popup reply
					if (nodesMap.get(WhatsAppResourceId.POPUPCONTACT.getId())!=null &&
							nodesMap.get(WhatsAppResourceId.POPUPCOUNT.getId())!=null &&
							Objects.requireNonNull(nodesMap
									.get(WhatsAppResourceId.POPUPCOUNT.getId()))
									.getText()!=null){
						String count = (Objects.requireNonNull(nodesMap
								.get(WhatsAppResourceId.POPUPCOUNT.getId()))
								.getText().toString()).split(" of ")[1];
						if (popupCount.equals("")) popupCount = count;
						if (!count.equals(popupCount) &&
								Integer.valueOf(count) < Integer.valueOf(popupCount)) {
							manageSociabilityMessageEvents();
							eventInfos = new ArrayList<>();
						}
						popupCount = count;
						return;
					}

					// if whatsapp popup & popup disappears process popup reply
					if ((isPopup && nodesMap.get(WhatsAppResourceId.POPUPCONTACT.getId())==null)){
						manageSociabilityMessageEvents();
						eventInfos = new ArrayList<>();
						isPopup = false;
						return;
					}

					// handle chat screen messages
					if (nodesMap.get(WhatsAppResourceId.MESSAGELIST.getId()) == null ||
							nodesMap.get(WhatsAppResourceId.TEXTBOX.getId())==null) return;

					// handle chat screen messages using thread pool executors
					// not working as the events recorded somehow becomes same so no diff
					// need to investigate
//                eventInfos.add(new AccessibilityEventInformation(
//                        event, event.getSource(), nodesMap, timeStamp));
//                if (eventInfos.size()>2) eventInfos.remove(0);
//                manageSociabilityMessageEvents();

					// log the chat screen diff messages and broadcast to logger
					List<SociabilityMessageStructure> bms =
							handleChatScreenMessages(getRootInActiveWindow());
					for (SociabilityMessageStructure b : bms) {
						WhatsappUtils.broadcastToSociabilityMessages(getApplicationContext(), b);
					}
					break;

				// when we click reply from notification, view text selection change is triggered
				case AccessibilityEvent.TYPE_VIEW_TEXT_SELECTION_CHANGED:
					// check for whatsapp app
					if (SociabilityUtils.isIMApp(event.getPackageName().toString())) return;
					if(!event.getPackageName().toString()
							.equals(SociabilityUtils.APP_PACKAGE_WHATSAPP)) return;

					// get important nodes from screen to determine the type of message / call
					nodesMap = SociabilityUtils.findNodesByViewIds(getRootInActiveWindow(), ids);

					// check for notification edit text box for reply notification and
					// set reply notification flag and add event to list
					AccessibilityNodeInfo textBoxNode = nodesMap
							.get(WhatsAppResourceId.NOTIFICATIONTEXTBOX.getId());
					if (textBoxNode!=null &&
							textBoxNode.getText()!=null &&
							textBoxNode.getText().toString().equals("Reply")){
						isNotificationReply = true;
						eventInfos.add(new AccessibilityEventInformation(
								event, getRootInActiveWindow(), nodesMap,
								System.currentTimeMillis()));
						break;
					}
					break;

				// check for notification whatsapp call decline / abort video call
				case AccessibilityEvent.TYPE_VIEW_CLICKED:
					// old code: accessibility tap recording
					logi("Gesture:onAccessibilityEvent", event);

					// handle whatsapp call notification decline click
					if (isCall && event.getText()!=null &&
							event.getText().toString().contains("Decline"))
						processCall = true;
					break;

				// old code: accessibility tap recording
				case AccessibilityEvent.TYPE_VIEW_LONG_CLICKED:
				case AccessibilityEvent.TYPE_VIEW_HOVER_ENTER:
					logi("Gesture:onAccessibilityEvent", event);
					break;
				case AccessibilityEvent.TYPE_VIEW_FOCUSED:
				case AccessibilityEvent.TYPE_ANNOUNCEMENT:
				case AccessibilityEvent.TYPE_ASSIST_READING_CONTEXT:
				case AccessibilityEvent.TYPE_GESTURE_DETECTION_END:
				case AccessibilityEvent.TYPE_GESTURE_DETECTION_START:
				case AccessibilityEvent.TYPE_TOUCH_EXPLORATION_GESTURE_END:
				case AccessibilityEvent.TYPE_TOUCH_EXPLORATION_GESTURE_START:
				case AccessibilityEvent.TYPE_TOUCH_INTERACTION_END:
				case AccessibilityEvent.TYPE_TOUCH_INTERACTION_START:
				case AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUSED:
				case AccessibilityEvent.TYPE_VIEW_ACCESSIBILITY_FOCUS_CLEARED:
				case AccessibilityEvent.TYPE_VIEW_CONTEXT_CLICKED:
				case AccessibilityEvent.TYPE_VIEW_HOVER_EXIT:
				case AccessibilityEvent.TYPE_VIEW_SCROLLED:
				case AccessibilityEvent.TYPE_VIEW_SELECTED:
				case AccessibilityEvent.TYPE_VIEW_TEXT_TRAVERSED_AT_MOVEMENT_GRANULARITY:
				case AccessibilityEvent.TYPE_WINDOWS_CHANGED:
				default:
					break;
			}
		}
	}

	private void manageNotificationMessages(Bundle bundle){
		NotificationProcessTask notificationTask =
				new NotificationProcessTask(bundle, getApplicationContext());
		NotificationManager.getNotificationManager().processNotification(notificationTask);
	}

	private void manageSociabilityMessageEvents(){
		if (eventInfos.size()<=1) return;
		SociabilityProcessTask sociabilityTask =
				new SociabilityProcessTask(eventInfos, getApplicationContext());
		SociabilityManager.getSociabilityManager().processSociabilityEvents(sociabilityTask);
	}

	private void manageSociabilityCallEvents(){
		SociabilityCallProcessTask sociabilityCallTask =
				new SociabilityCallProcessTask(callEventInfos, getApplicationContext());
		SociabilityCallManager.getSociabilityCallManager()
				.processSociabilityCallEvents(sociabilityCallTask);
	}

	private List<SociabilityMessageStructure> handleChatScreenMessages(AccessibilityNodeInfo root) {
		String groupName = "";
		String contactName = "";
		long time = System.currentTimeMillis();
		boolean isGroup = false;
		List<SociabilityMessageStructure> bms = new ArrayList<>();

		// Traverse through nodes and check if it is group and also store the contact header
		List<AccessibilityNodeInfo> nodes = SociabilityUtils.preOrderTraverse(root);
		for (AccessibilityNodeInfo n: nodes){
			if (n.getViewIdResourceName()!=null &&
					n.getViewIdResourceName().equals(WhatsAppResourceId.CONTACTNAME.getId()))
				contactName = n.getText().toString();
			if (n.getContentDescription()!=null &&
					n.getContentDescription().toString().equals("New group call")){
				isGroup = true;
				break;
			}
			if (n.getClassName()!=null && n.getClassName().toString().contains("list")) break;
		}

		// If group set group name as contact header
		if (isGroup) {
			groupName = contactName;
			contactName = "";
		}

		if (previousMessages.size()==0)
			previousMessages = WhatsappUtils.processMessagesFromNodes(nodes);
		newMessages = WhatsappUtils.processMessagesFromNodes(nodes);

		// Making the two lists equal size by grabbing the last elements
		// Uneven lists are caused by keypad close and open
		int[] lengths = {newMessages.size(), previousMessages.size()};
		Arrays.sort(lengths);
		newMessages = newMessages
				.subList(newMessages.size()-lengths[0], newMessages.size());
		previousMessages = previousMessages
				.subList(previousMessages.size()-lengths[0], previousMessages.size());

		// Get diff list and add diff to HOPES list
		List<String> diffList = WhatsappUtils.getDiffList(newMessages, previousMessages);

		for (String diffMessage: diffList){
			String messageTime = "";
			String messageType = "TEXT";

			String[] message = diffMessage.split(WhatsappUtils.DELIMITER);

			// If group get contact name and message time from message text
			if (isGroup){
				contactName = diffMessage.substring(diffMessage.indexOf("-GRP-")+5);
				messageTime = message[2].substring(0,message[2].indexOf("-GRP-"));
			}

			// set message text and send receive text
			String messageText = message[0];
			String sendReceive = message[1];

			// If not group set message time
			if (messageTime.isEmpty() && message.length>2) messageTime = message[2];

			// If it is group and message is sent then set contact to empty string
			if (isGroup && diffMessage.contains("SENT")) contactName = "";

			// Determine the type of message from the message text
			if(messageText.contains("HOPES_ATTACHMENT-")) {
				messageType = messageText.split("-")[1];
				messageText = "";
			}

			if (messageText.isEmpty() && messageType.equals("TEXT")) continue;
			bms.add(new SociabilityMessageStructure(messageText, sendReceive, contactName,
					messageTime, messageType, groupName, "FOREGROUND", time));
		}

		String[] previousArray = newMessages.toArray(new String[0]);
		previousMessages = new ArrayList<>(Arrays.asList(previousArray));
		newMessages = new ArrayList<>();
		return bms;
	}
	//END OF NEW CODE
}
