package com.moht.hopes.listeners;

/*************************************************************************
 *
 * MOH Office of Healthcare Transformation (MOHT) CONFIDENTIAL
 *
 *  Copyright 2018-2019
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of MOH Office of Healthcare Transformation.
 * The intellectual and technical concepts contained
 * herein are proprietary to MOH Office of Healthcare Transformation
 * and may be covered by Singapore, U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from MOH Office of Healthcare Transformation.
 */

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.storage.TextFileManager;

public class AmbientLightListener implements SensorEventListener{
	public static final String name = "ambientlight";
	public static final String short_name = "amb";
	public static final String header = "timestamp,accuracy,value";

	private SensorManager sensorManager;
	private Sensor sensor;
	private Context appContext;
	private Boolean enabled;
	private String accuracy = "";
	private long lastTimeMillis = 0;

	/**Listens for ambient light sensor updates.  NOT activated on instantiation.
	 * Use the turn_on() function to log any ambient light sensor updates to the
	 * ambient light sensor log.
	 * @param applicationContext a Context from an activity or service. */
	public AmbientLightListener(Context applicationContext){
		this.appContext = applicationContext;
		PackageManager pkgManager = applicationContext.getPackageManager();
		Boolean exists = pkgManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_LIGHT);

		if (exists) {
			enabled = false;
			this.sensorManager = (SensorManager) applicationContext.getSystemService(Context.SENSOR_SERVICE);
			if (this.sensorManager ==  null ) {
				if(BuildConfig.APP_IS_DEBUG) Log.e("AmbientLight Problems", "sensorManager does not exist? (1)" );
				TextFileManager.getDebugLogFile().write("sensorManager does not exist? (1)");
			}

			this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
			if (this.sensor == null ) {
				TextFileManager.getDebugLogFile().write("sensor does not exist? (2)");
            }
		}
	}

	public static int checkPermission(){ return 0; }

	public synchronized void turn_on() {
		if ( !BackgroundService.sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL) )
			TextFileManager.getDebugLogFile().write("Trying to start AmbientLightSensor session, device cannot find AmbientLightSensor.");
		enabled = true;
	}

	public synchronized void turn_off(){
		BackgroundService.sensorManager.unregisterListener(this);
		enabled = false;
	}

	/** Update the accuracy, synchronized so very closely timed trigger events do not overlap.
	 * (only triggered by the system.) */
	@Override
	public synchronized void onAccuracyChanged(Sensor arg0, int arg1) {	accuracy = "" + arg1; }

	/** On receipt of a sensor change, record it.  Include accuracy. (only ever triggered by the system.) */

	@Override
	public synchronized void onSensorChanged(SensorEvent arg0) {
		long currentTimeMillis = System.currentTimeMillis();
		if(currentTimeMillis-lastTimeMillis<500) return;
		lastTimeMillis = currentTimeMillis;
		float value = arg0.values[0];
		String data = currentTimeMillis + TextFileManager.DELIMITER + accuracy + TextFileManager.DELIMITER + value;
		TextFileManager.getFile(this).write(data);
		turn_off();
	}
}
