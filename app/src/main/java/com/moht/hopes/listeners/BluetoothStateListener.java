package com.moht.hopes.listeners;

import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.storage.TextFileManager;

import java.util.Objects;

import static com.moht.hopes.PermissionHandler.checkAccessBluetooth;
import static com.moht.hopes.PermissionHandler.checkAccessBluetoothAdmin;
import static com.moht.hopes.PermissionHandler.checkAccessBackgroundLocation;

/** Listens for bluetooth state changes.
 *  Turning On, Turning Off, On, Off */
public class BluetoothStateListener extends BroadcastReceiver {
	public static final String name = "bluetooth_state";
	public static final String short_name = "bls";
	public static final String header = "timestamp,event";

	private static Boolean started = false;

	public static void start(){
		started = true;
	}


	public static int checkPermission(){ return 0; }
	
	/** Handles the logging, includes a new line for the CSV files.
	 * This code is otherwise reused everywhere.*/
	private void makeLogStatement(String message) {
		TextFileManager.getFile(this).write(
				System.currentTimeMillis() + TextFileManager.DELIMITER + message );
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (!started) { return; }
		final String action = intent.getAction();

		if (Objects.requireNonNull(action).equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
			final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
			switch (state) {
				case BluetoothAdapter.STATE_OFF:
					makeLogStatement("Bluetooth off");
					break;
				case BluetoothAdapter.STATE_TURNING_OFF:
					makeLogStatement("Turning Bluetooth off");
					break;
				case BluetoothAdapter.STATE_ON:
					makeLogStatement("Bluetooth on");
					break;
				case BluetoothAdapter.STATE_TURNING_ON:
					makeLogStatement("Turning Bluetooth on");
					break;
			}
		}
	}
}
