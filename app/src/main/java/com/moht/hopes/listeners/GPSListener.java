package com.moht.hopes.listeners;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.sip.SipSession;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.PermissionHandler;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.ui.DebugInterfaceActivity;

import java.util.function.Consumer;

import static com.moht.hopes.PermissionHandler.*;

/* Notes/observation on Location Services:
 * We are passing in "0" as the minimum time for location updates to be pushed to us, this results in about
 * 1 update every second.  This is based on logs made using a nexus 7 tablet.
 * This makes sense, GPSs on phones do not Have that kind of granularity/resolution.
 * However, we need the resolution in milliseconds for the line-by-line encryption scheme.
 * So, we grab the system time instead.  This may add a fraction of a second to the timestamp.
 * 
 * We are NOT recording which location provider provided the update, or which location providers
 * are available on a given device. */

public class GPSListener implements LocationListener, Consumer<Location> {
	public static final String name = "gps";
	public static final String short_name = "gps";
	public static final String header = "timestamp,latitude,longitude,altitude,accuracy,locationTime,provider";
	
	private Context appContext;
	private LocationManager locationManager;
	private PassiveListener passiveListener = null;

	private Boolean enabled;
	private long lastTimeMillis = 0;
	//does not have an explicit "exists" boolean.  Use check_status() function, it will return false if there is no GPS.

	class PassiveListener implements LocationListener {
		GPSListener parent;

		PassiveListener(GPSListener _parent){parent = _parent;}

		@Override
		public void onLocationChanged(Location location) { parent.onLocationChanged(location); }
	}

	public static int checkPermission(){
		return (checkAccessCoarseLocation(BackgroundService.appContext) && checkAccessFineLocation(BackgroundService.appContext)
				&& checkAccessBackgroundLocation(BackgroundService.appContext))?1:-1;
	}

	private void makeDebugLogStatement(String message) {
		TextFileManager.getDebugLogFile().write(
				"GPSListener: " + System.currentTimeMillis() + " " + message);
	}

	/** Listens for GPS updates from the network GPS location provider and/or the true
	 * GPS provider, both if possible.  It is NOT activated upon instantiation.  Requires an
	 * application Context object be passed in in order to interface with location services.
	 * When activated using the turn_on() function it will log any location updates to the GPS log.
	 * @param appContext A Context provided an Activity or Service. */
	public GPSListener (Context appContext) {
		this.appContext = appContext;
		enabled = false;
		if(BuildConfig.APP_IS_DEBUG) Log.d("GPSListener::GPSListener()", "initializing GPS...");
		//There is a possibility (mostly in development) that this will not be instantiated all the time, so we instantiate an extra one here.
		locationManager = (LocationManager) this.appContext.getSystemService(Context.LOCATION_SERVICE);
	}

	/** Turns on passive provider, listen to GPS updates from other APPs, this incurs no battery drain */
	@SuppressWarnings("MissingPermission")
	public synchronized boolean turn_on_passive() {
		try {
			if(passiveListener==null) passiveListener = new PassiveListener(this);
			locationManager.requestLocationUpdates(LocationManager.PASSIVE_PROVIDER, 0, 0, passiveListener);
			return true;
		} catch (Exception e){
			makeDebugLogStatement(e.toString());
			return false;
		}
	}

	@Override
	public void accept(Location location) {
		onLocationChanged(location);
	}

	/** Turns on passive provider, listen to GPS updates from other APPs, this incurs no battery drain */
	@SuppressWarnings("MissingPermission")
	public synchronized void turn_on_single() {
		if (!locationManager.isLocationEnabled()) return;
		if(Build.VERSION.SDK_INT>=30) {
			try{ locationManager.getCurrentLocation(LocationManager.NETWORK_PROVIDER, null, appContext.getMainExecutor(), this); }
			catch (Exception e) { makeDebugLogStatement(e.toString()); }
			try{ locationManager.getCurrentLocation(LocationManager.GPS_PROVIDER, null, appContext.getMainExecutor(), this); }
			catch (Exception e) { makeDebugLogStatement(e.toString()); }
		} else {
			try{ locationManager.requestSingleUpdate (LocationManager.NETWORK_PROVIDER, this, null); }
			catch (Exception e) { makeDebugLogStatement(e.toString()); }
			try{ locationManager.requestSingleUpdate (LocationManager.GPS_PROVIDER, this, null); }
			catch (Exception e) { makeDebugLogStatement(e.toString()); }
		}
	}

	/** Turns on GPS providers, provided they are accessible. Handles permission errors appropriately */
	@SuppressWarnings("MissingPermission")
	public synchronized void turn_on() {
		// if already enabled return true.  We want the above logging, do not refactor to earlier in the logic.
		if ( enabled ) return;
		PersistentData.setGpsRecordCount(0);

		boolean coarsePermissible = PermissionHandler.checkAccessCoarseLocation(appContext);
		boolean finePermissible = PermissionHandler.checkAccessFineLocation(appContext);
		boolean fineExists = BackgroundService.packageManager.hasSystemFeature(PackageManager.FEATURE_LOCATION_GPS);
		boolean coarseExists = BackgroundService.packageManager.hasSystemFeature(PackageManager.FEATURE_LOCATION_NETWORK);

		if ( coarseExists && (finePermissible || coarsePermissible)) try {
			locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 0, this);
			enabled = true;
		} catch (Exception e) {
			makeDebugLogStatement(e.toString());
		}
		if ( fineExists && finePermissible ) try {
				locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
				enabled = true;
			} catch (Exception e){
				makeDebugLogStatement(e.toString());
		}
	}

	/** Disable all location updates */
	@SuppressWarnings("MissingPermission")
	public synchronized void turn_off(){
		// pretty confident this cannot fail.
		locationManager.removeUpdates(this);
		enabled = false;
	}
	
	/** pushes an update to us whenever there is a location update. */
	@Override
	public void onLocationChanged(Location location) {
		if(location==null) return;  // some Android will occasionally pass in a NULL pointer
		long currentTimeMillis = System.currentTimeMillis();
		if(currentTimeMillis-lastTimeMillis<500) return;
		lastTimeMillis = currentTimeMillis;

		// Latitude and longitude offset should be 0 unless GPS fuzzing is enabled
		double latitude = location.getLatitude() + PersistentData.getLatitudeOffset();
		double longitude = location.getLongitude() + PersistentData.getLongitudeOffset();

		String data = currentTimeMillis + TextFileManager.DELIMITER
				+ latitude + TextFileManager.DELIMITER
				+ longitude + TextFileManager.DELIMITER
				+ location.getAltitude() + TextFileManager.DELIMITER
				+ location.getAccuracy() + TextFileManager.DELIMITER
				+ location.getTime() + TextFileManager.DELIMITER
				+ location.getProvider();

		//note, altitude is notoriously inaccurate, getAccuracy only applies to latitude/longitude
		TextFileManager.getFile(this).write(data);
		PersistentData.setLastGpsUpdateTime(System.currentTimeMillis());
		int gpsCount = PersistentData.getGpsRecordCount() + 1;
		PersistentData.setGpsRecordCount(gpsCount);
		if(gpsCount == 3) turn_off();
	}
	
	/*  We do not actually need to implement any of the following overrides.
	 *  When a provider has a changed we do not need to record it, and we have
	 *  not encountered any corner cases where these are relevant. */
	
//  arg0 for Provider Enabled/Disabled is a string saying "network" or "gps".
	@Override
	public void onProviderDisabled(String arg0) {
		if(BuildConfig.APP_IS_DEBUG) DebugInterfaceActivity.smartLog(GPSListener.name,"location provider disabled: " + arg0);
	}
	@Override
	public void onProviderEnabled(String arg0) {
		if(BuildConfig.APP_IS_DEBUG) DebugInterfaceActivity.smartLog(GPSListener.name, "location provider enabled: " + arg0);
	}
	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		//Called when the provider status changes, when a provider is unable to fetch a location,
		// or if the provider has recently become available after a period of unavailability.
		// arg0 is the name of the provider that changed status.
		// arg1 is the status of the provider. 0=out of service, 1=temporarily unavailable, 2=available
		if(BuildConfig.APP_IS_DEBUG) DebugInterfaceActivity.smartLog(GPSListener.name,
				"location provider status changed: " + arg0 + "," + arg1 + "," + arg2.toString());
	}
}
