package com.moht.hopes.listeners;

/*************************************************************************
 *
 * MOH Office of Healthcare Transformation (MOHT) CONFIDENTIAL
 *
 *  Copyright 2018-2019
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of MOH Office of Healthcare Transformation.
 * The intellectual and technical concepts contained
 * herein are proprietary to MOH Office of Healthcare Transformation
 * and may be covered by Singapore, U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from MOH Office of Healthcare Transformation.
 */

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.storage.TextFileManager;

public class GyroscopeListener implements SensorEventListener{
	public static final String name = "gyro";
	public static final String short_name = "gyr";
	public static final String header = "timestamp,accuracy,x,y,z";

	private SensorManager gyroSensorManager;
	private Sensor gyroSensor;

    private Boolean exists;
	private Boolean enabled = null;
	private String accuracy;
	private long lastTimeMillis = 0;

	public Boolean check_status(){
		if (exists) return enabled;
		return false; }

	public static int checkPermission(){
		return 0;
	}

	/**Listens for Gyroscope updates.  NOT activated on instantiation.
	 * Use the turn_on() function to log any Gyroscope updates to the
	 * Gyroscope log.
	 * @param applicationContext a Context from an activity or service. */
	public GyroscopeListener(Context applicationContext){
		this.accuracy = "unknown";
		this.exists = BackgroundService.packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_GYROSCOPE);

		if (this.exists) {
			enabled = false;
			this.gyroSensorManager = (SensorManager) applicationContext.getSystemService(Context.SENSOR_SERVICE);
			if (this.gyroSensorManager ==  null ) {
				if(BuildConfig.APP_IS_DEBUG) Log.e("Gyroscope Problems", "gyroSensorManager does not exist? (1)" );
				TextFileManager.getDebugLogFile().write(
						"GyroscopeListener::GyroscopeListener: " +
								"gyroSensorManager does not exist? (1)");
				exists = false;	}

			this.gyroSensor = gyroSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
			if (this.gyroSensor == null ) {
				if(BuildConfig.APP_IS_DEBUG) Log.e("Gyroscope Problems", "gyroSensor does not exist? (2)" );
				TextFileManager.getDebugLogFile().write(
						"GyroscopeListener::GyroscopeListener: " +
						"gyroSensor does not exist? (2)");
				exists = false;	}
		} }

	public synchronized void turn_on() {
		if ( !this.exists ) return;
		if ( !BackgroundService.sensorManager.registerListener(this, gyroSensor, SensorManager.SENSOR_DELAY_NORMAL) ) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("Gyroscope", "Gyroscope is broken");
			TextFileManager.getDebugLogFile().write(
					"GyroscopeListener::GyroscopeListener: " +
					"Trying to start Gyroscope session, device cannot find Gyroscope."); }
		enabled = true;	}

	public synchronized void turn_off(){
		BackgroundService.sensorManager.unregisterListener(this);
		enabled = false; }

	/** Update the accuracy, synchronized so very closely timed trigger events do not overlap.
	 * (only triggered by the system.) */
	@Override
	public synchronized void onAccuracyChanged(Sensor arg0, int arg1) {	accuracy = "" + arg1; }

	/** On receipt of a sensor change, record it.  Include accuracy.
	 * (only ever triggered by the system.) */
	@Override
	public synchronized void onSensorChanged(SensorEvent arg0) {
//		Log.e("Gyroscope", "Gyroscope update");
		long currentTimeMillis = System.currentTimeMillis();
		if(currentTimeMillis-lastTimeMillis<500) return;
		lastTimeMillis = currentTimeMillis;
		float[] values = arg0.values;
		String data = currentTimeMillis + TextFileManager.DELIMITER
				+ accuracy + TextFileManager.DELIMITER
				+ values[0] + TextFileManager.DELIMITER
				+ values[1] + TextFileManager.DELIMITER + values[2];
		TextFileManager.getFile(this).write(data);
	}
}
