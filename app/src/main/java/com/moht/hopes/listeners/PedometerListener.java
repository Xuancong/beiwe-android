package com.moht.hopes.listeners;

/*************************************************************************
 *
 * MOH Office of Healthcare Transformation (MOHT) CONFIDENTIAL
 *
 *  Copyright 2018-2019
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of MOH Office of Healthcare Transformation.
 * The intellectual and technical concepts contained
 * herein are proprietary to MOH Office of Healthcare Transformation
 * and may be covered by Singapore, U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from MOH Office of Healthcare Transformation.
 */


import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.util.Log;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;

import static com.moht.hopes.PermissionHandler.checkActivityRecognition;


public class PedometerListener implements SensorEventListener {
	public static final String name = "pedometer";
	public static final String short_name = "ped";
	public static final String header = "timestamp,sensor_timestamp,sensor,steps";

	private Sensor stepsCounterSensor, stepsDetectorSensor;
	private boolean exists = false;
	private boolean enabled = false;
	private String accuracy = "unknown";

	public Boolean check_status() {
		if (exists) return enabled;
		return false;
	}

	public static int checkPermission(){
		if ( android.os.Build.VERSION.SDK_INT >= 29 )
			return checkActivityRecognition(BackgroundService.appContext)?1:-1;
		return 1;
	}

	/**
	 * Listens for Magnetometer updates.  NOT activated on instantiation.
	 * Use the turn_on() function to log any Magnetometer updates to the Magnetometer log.
	 *
	 * @param applicationContext a Context from an activity or service.
	 */
	public PedometerListener(Context applicationContext) {
//		exists = BackgroundService.packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_COUNTER);
//		exists = BackgroundService.packageManager.hasSystemFeature(PackageManager.FEATURE_SENSOR_STEP_DETECTOR);
		stepsCounterSensor = BackgroundService.sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER);
		stepsDetectorSensor = BackgroundService.sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR);
		exists = (stepsCounterSensor != null || stepsDetectorSensor != null);
		if (stepsCounterSensor == null) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("StepsListener", "StepsCounterSensor does not exist? (2)");
			TextFileManager.getDebugLogFile().write("StepsCounterSensor does not exist? (2)");
		}
		if (stepsDetectorSensor == null) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("StepsListener", "StepsDetectorSensor does not exist? (2)");
			TextFileManager.getDebugLogFile().write("StepsDetectorSensor does not exist? (2)");
		}
	}

	public synchronized void turn_on() {
		if ( !exists || enabled ) return;
		if ( !BackgroundService.sensorManager.registerListener(this, stepsCounterSensor, SensorManager.SENSOR_DELAY_NORMAL) ) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("StepsListener", "StepsCounterSensor is broken");
			TextFileManager.getDebugLogFile().write("Trying to start StepsCounterSensor session, device failed to register listener."); }
		if ( !BackgroundService.sensorManager.registerListener(this, stepsDetectorSensor, SensorManager.SENSOR_DELAY_NORMAL) ) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("StepsListener", "StepsDetectorSensor is broken");
			TextFileManager.getDebugLogFile().write("Trying to start StepsDetectorSensor session, device failed to register listener."); }
		enabled = true;
	}

	public synchronized void turn_off(){
		if(stepsCounterSensor!=null) BackgroundService.sensorManager.unregisterListener(this, stepsCounterSensor);
		if(stepsDetectorSensor!=null) BackgroundService.sensorManager.unregisterListener(this, stepsDetectorSensor);
		enabled = false;
	}

	/** Update the accuracy, synchronized so very closely timed trigger events do not overlap.
	 * (only triggered by the system.) */
	@Override
	public synchronized void onAccuracyChanged(Sensor arg0, int arg1) {	accuracy = "" + arg1; }

	float prev_value = Float.NEGATIVE_INFINITY;
	/** On receipt of a sensor change, record it.  Include accuracy.
	 * (only ever triggered by the system.) */
	@Override
	public synchronized void onSensorChanged(SensorEvent arg0) {
		float[] values = arg0.values;
		String sensor = "null";
		if(arg0.sensor==stepsDetectorSensor){
			sensor = "detector";
			if(values[0] == 0) return;
		} else {
			sensor = "counter";
			if(values[0] == prev_value) return;
			prev_value = values[0];
		}
		// Why put both timestamps?? Some vendors such as Huawei put sequence number (1,2,3,...) as timestamp in arg0.timestamp
		String data = System.currentTimeMillis() + TextFileManager.DELIMITER + arg0.timestamp + TextFileManager.DELIMITER + sensor;
		data += TextFileManager.DELIMITER + (values.length>0?values[0]:"");
		TextFileManager.getFile(this).write(data);
	}
}


