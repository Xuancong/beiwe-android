package com.moht.hopes.listeners;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.PowerManager;
import android.util.Log;

import com.moht.hopes.BuildConfig;
import com.moht.hopes.storage.TextFileManager;

import java.util.Objects;
import java.util.TimeZone;

import static com.moht.hopes.PermissionHandler.checkAccessUsagePermission;

/** Listens for power state changes.
 *  Screen On/Off, Power Connect/Disconnect, Airplane Mode.
 *  @author Josh Zagorsky, Eli Jones, May/June 2014 */
public class PowerStateListener extends BroadcastReceiver {
	public static final String name = "power_state";
	public static final String short_name = "pow";
	public static final String header = "timestamp,event,tz";

	// The Power State Manager can receive broadcasts before the app is even running.
	// This would cause a a crash because we need the TextFileManager to be available.
	// The started variable is set to true during the startup process for the hopes.
	private static Boolean started = false;
	
	private static PowerManager powerManager;
	public static void start(Context context){
		started = true;
		powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
	}

	public static int checkPermission(){
		return checkAccessUsagePermission()?1:-1;
	}
	
	/** Handles the logging, includes a new line for the CSV files.
	 * This code is otherwise reused everywhere.*/
	private void makeLogStatement(String message) {
		// Convert timezone to +/- minutes
		int minutes = TimeZone.getDefault().getRawOffset()/(1000*60);
		minutes = TimeZone.getDefault().getDisplayName(true, TimeZone.SHORT).contains("+") ? minutes : minutes*-1;
		TextFileManager.getFile(this.getClass()).write(System.currentTimeMillis() + TextFileManager.DELIMITER
				+ message + TextFileManager.DELIMITER + minutes);
	}
	
	
	@Override
	public void onReceive(Context externalContext, Intent intent) {
		if (!started) { return; }
		String action = intent.getAction();
		if(action==null) action = "";

		// Screen on/off
		if (action.equals(Intent.ACTION_SCREEN_OFF)) { makeLogStatement("Screen turned off"); }
		else if (action.equals(Intent.ACTION_SCREEN_ON)) { makeLogStatement("Screen turned on"); }
		else if (action.equals(Intent.ACTION_USER_PRESENT)) { makeLogStatement("Phone unlocked"); }
		
		// Power connected/disconnected
		else if (action.equals(Intent.ACTION_POWER_CONNECTED)) { makeLogStatement("Power connected"); }
		else if (action.equals(Intent.ACTION_POWER_DISCONNECTED)) { makeLogStatement("Power disconnected"); }
		
		// Shutdown/Restart
		else if (action.equals(Intent.ACTION_SHUTDOWN)) { makeLogStatement("Device shut down signal received"); }
		else if (action.equals(Intent.ACTION_REBOOT)) { makeLogStatement("Device reboot signal received"); }
		
		//android 5.0+  Power save mode is a low-battery state where android turns off battery draining features.
		else if (action.equals(PowerManager.ACTION_POWER_SAVE_MODE_CHANGED)) {
			if (powerManager.isPowerSaveMode()) {
				makeLogStatement("Power Save Mode state change signal received; device in power save state."); }
			else { makeLogStatement("Power Save Mode change signal received; device not in power save state."); }
		}
		
		//andoird 6.0+.  This indicates that Doze mode has been entered
		else if (action.equals(PowerManager.ACTION_DEVICE_IDLE_MODE_CHANGED)) {
			if (powerManager.isDeviceIdleMode()) {
				makeLogStatement("Device Idle (Doze) state change signal received; device in idle state."); }
			else { makeLogStatement("Device Idle (Doze) state change signal received; device not in idle state."); }
			if(BuildConfig.APP_IS_DEBUG) Log.d("device idle state", "" + powerManager.isDeviceIdleMode() );
		}
	}
}
