package com.moht.hopes.listeners;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.TriggerEvent;
import android.hardware.TriggerEventListener;
import android.util.Log;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.storage.TextFileManager;

public class SignificantMotionListener extends TriggerEventListener {
	public static final String name = "devicemotion";
	public static final String short_name = "dem";
	public static final String header = "timestamp,value";

	private SensorManager sensorManager;
	private Sensor sensor;

	private boolean exists = false;
	private Boolean enabled = null;

	public Boolean check_status(){
		if (exists) return enabled;
		return false;
	}

	public static int checkPermission(){
		return 0;
	}

	/**Listens for accelerometer updates.  NOT activated on instantiation.
	 * Use the turn_on() function to log any accelerometer updates to the
	 * accelerometer log.
	 * @param applicationContext a Context from an activity or service. */
	public SignificantMotionListener(Context applicationContext){
		exists = enabled = false;
		this.sensorManager = BackgroundService.sensorManager;
		if (this.sensorManager ==  null )
			return;

		this.sensor = sensorManager.getDefaultSensor(Sensor.TYPE_SIGNIFICANT_MOTION);
		if (this.sensor == null ) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("Significant Motion Listener Problems", "Significant Motion Sensor does not exist? (2)" );
			TextFileManager.getDebugLogFile().write("Significant Motion Sensor does not exist? (2)");
			return;
		}

		exists = true;
	}

	public synchronized void turn_on() {
		if ( !this.exists ) return;
		boolean res = sensorManager.requestTriggerSensor(this, sensor);
		enabled = true;	}
	
	public synchronized void turn_off(){
		sensorManager.cancelTriggerSensor(this, sensor);
		enabled = false; }
	
	/** On receipt of a sensor change, record it.  Include accuracy. (only ever triggered by the system.) */
	public void onTrigger(TriggerEvent event) {
		if(!enabled) return;
		long currentTimeMillis = System.currentTimeMillis();
		String data = currentTimeMillis+"";
		for (float val : event.values)
			data += TextFileManager.DELIMITER + val;
		TextFileManager.getFile(this).write(data);
		turn_on();
	}
}