package com.moht.hopes.listeners;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.storage.EncryptionEngine;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.ui.DebugInterfaceActivity;

import java.util.Arrays;
import java.util.Objects;

public class SociabilityCallLogger extends BroadcastReceiver {
    public static final String name = "sociability_call";
    public static final String short_name = "soc";

    public static String header = "timestamp,app,contact,eventType,callType,recordedDuration,sessionId";

    public static int checkPermission(){
        return AccessibilityListener.isEnabled(BackgroundService.appContext)?1:-1;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Objects.requireNonNull(intent.getAction())
                .equals("com.moht.hopes.action.CALL_PROCESSED")) {
            try {
                Bundle bundle = intent.getExtras();
                if (bundle != null) {
                    long receivedAt = (long) bundle.get("ReceivedAt");
                    String app = (String) Objects.requireNonNull(bundle.get("Namespace"));
                    String contact = (String) bundle.get("ContactName");
                    int callEventType = determineEventTypeFromCallEvents(
                            (String) Objects.requireNonNull(bundle.get("CallEventType")));
                    String recordedDuration = (String) bundle.get("RecordedDuration");
                    recordedDuration = recordedDuration==null ? ":" : recordedDuration;
                    int callType = determineTypeFromCallType(
                            (String) Objects.requireNonNull(bundle.get("CallType")));
                    long sessionId = Long.valueOf(
                            (String) Objects.requireNonNull(bundle.get("SessionId")));

                    writeToFile(app, contact, callEventType, callType,
                            recordedDuration, sessionId, receivedAt);
                }
            } catch (Exception e) {
                if(BuildConfig.APP_IS_DEBUG) Log.e("SociabilityCallLogger","CALL_RECEIVED Caught exception: " + e.getCause() + ", " + e.getMessage());
                TextFileManager.getDebugLogFile().write("SociabilityCallLogger::onReceive: " + DebugInterfaceActivity.printStack(e));
            }
        }
    }

    protected int determineEventTypeFromCallEvents(String callEventType) {
        switch (callEventType) {
            case "MISSED":
                return 15;
            case "BUSY":
                return 14;
            case "NOTANSWERED":
                return 13;
            case "CALLING":
                return 12;
            case "RINGING":
                return 11;
            case "REJECTED":
                return 10;
            case "ENDED":
                return 9;
            case "RECONNECTED":
                return 8;
            case "RECONNECTING":
                return 7;
            case "CONNECTED":
                return 6;
            case "CONNECTING":
                return 5;
            case "DECLINED":
                return 4;
            case "ACCEPTED":
                return 3;
            case "OUTGOING":
                return 2;
            case "INCOMING":
                return 1;
            default:
                return 0;
        }
    }

    protected int determineTypeFromCallType(String callType) {
        switch (callType) {
            case "VIDEO":
                return 2;
            case "AUDIO":
                return 1;
            default:
                return 0;
        }
    }

    private void writeToFile(String namespace, String contact, int eventType, int callType,
                             String recordedDuration, long sessionId, long receivedAt) {
        long timeStamp = receivedAt==0 ? System.currentTimeMillis() : receivedAt;
        String contactName = EncryptionEngine
                .hashSociabilityContact(Objects.requireNonNull(contact));
        String duration = recordedDuration.replace(":", "");

        String data = timeStamp + TextFileManager.DELIMITER +
                namespace + TextFileManager.DELIMITER +
                contactName + TextFileManager.DELIMITER +
                eventType + TextFileManager.DELIMITER +
                callType + TextFileManager.DELIMITER +
                duration + TextFileManager.DELIMITER +
                sessionId;
	
        if (BuildConfig.APP_IS_DEBUG) Log.d("SociabilityCallLogger", "data = " + data);

	// write encrypted line to file
        TextFileManager.getFile(this).write(data);
    }
}
