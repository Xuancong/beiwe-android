package com.moht.hopes.listeners;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.storage.EncryptionEngine;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.ui.DebugInterfaceActivity;

import java.util.Arrays;
import java.util.Objects;

public class SociabilityMsgLogger extends BroadcastReceiver {
	public static final String name = "sociability_msg";
	public static final String short_name = "som";

	private Object obj = new Object();

	public static String header = "timestamp,app,contact,orientation,length,minute,type,groupName,useCaseType,received";

	public static int checkPermission() {
		return AccessibilityListener.isEnabled(BackgroundService.appContext) ? 1 : -1;
	}

	@Override
	public void onReceive(Context context, Intent intent) {
		if (Objects.requireNonNull(intent.getAction())
				.equals("com.moht.hopes.action.MESSAGE_PROCESSED")) {
			try {
				Bundle bundle = intent.getExtras();
				if (bundle != null) {
					synchronized (obj) {

						String app = (String) Objects.requireNonNull(bundle.get("Namespace"));
						int orientation = Objects.requireNonNull(
								bundle.get("SendReceive")).equals("SENT") ? 0 : 1;
						int messageLength = (int) bundle.get("MessageLength");
						String minute = (String) bundle.get("HourMinute");
						long receivedAt = (long) bundle.get("ReceivedAt");
						String caseType = (String) bundle.get("UseCaseType");
						int useCaseType = determineUseCaseTypeFromUseCaseType(
								Objects.requireNonNull(caseType));
						minute = minute == null ? ":" : minute;
						int type = determineTypeFromMessageType(
								(String) Objects.requireNonNull(bundle.get("MessageType")));
						String groupName = (String) Objects.requireNonNull(bundle.get("GroupName"));

						// popup only if in beta mode
						if (BuildConfig.APP_IS_DEBUG) {
							String o = bundle.get("SendReceive").toString();
							String contactName = (String) bundle.get("ContactName");
							String logLine = contactName + ", " + o + "(" + messageLength + ") " +
									", " + minute.replace(":", "") + ", " +
									Objects.requireNonNull(groupName) + ", " + caseType + ", " +
									(String) Objects.requireNonNull(bundle.get("MessageType"));

							Log.d("SociabilityLogger", "data = " + logLine);
							Toast.makeText(context, logLine, Toast.LENGTH_LONG).show();
						}

						// write encrypted to local file
						writeToFile(app, (String) bundle.get("ContactName"),
								orientation, messageLength, Objects.requireNonNull(minute),
								type, groupName, useCaseType, receivedAt);

					}
				}
			} catch (Exception e) {
                if(BuildConfig.APP_IS_DEBUG) Log.e("SociabilityLogger","MESSAGE_RECEIVED Caught exception: "
                        + e.getCause() + ", " + e.getMessage());
				TextFileManager.getDebugLogFile().write("SociabilityLogger::onReceive: " + DebugInterfaceActivity.printStack(e));
			}
		}
	}

	protected int determineUseCaseTypeFromUseCaseType(String useCaseType) {
		switch (useCaseType) {
			case "POPUP":
				return 2;
			case "FOREGROUND":
				return 1;
			case "NOTIFICATION":
			default:
				return 0;
		}
	}

	protected int determineTypeFromMessageType(String messageType) {
		switch (messageType) {
			case "VOICERECORDING":
				return 7;
			case "FILE":
				return 6;
			case "LOCATION":
				return 5;
			case "CONTACT":
				return 4;
			case "AUDIO":
				return 3;
			case "VIDEO":
				return 2;
			case "IMAGE":
				return 1;
			case "TEXT":
			default:
				return 0;
		}
	}

	private void writeToFile(String namespace, String contact, int orientation,
	                         int length, String minute, int type,
	                         String groupName, int useCaseType, long receivedAt) {
		String data = System.currentTimeMillis() +
				TextFileManager.DELIMITER + namespace +
				TextFileManager.DELIMITER +
				EncryptionEngine.hashSociabilityContact(Objects.requireNonNull(contact)) +
				TextFileManager.DELIMITER + orientation +
				TextFileManager.DELIMITER + length +
				TextFileManager.DELIMITER + minute.replace(":", "") +
				TextFileManager.DELIMITER + type +
				TextFileManager.DELIMITER;

		// we only encrypt the group name if the group name is not empty
		if (Objects.requireNonNull(groupName) != "") {
			data += EncryptionEngine.hashSociabilityContact(Objects.requireNonNull(groupName));
		} else {
			// we fall through . When not a group, pass no group name
		}

		data += TextFileManager.DELIMITER + useCaseType + TextFileManager.DELIMITER + receivedAt;

		if (BuildConfig.APP_IS_DEBUG) Log.d("SociabilityLogger", "data = " + data);

		// encrypted write
		TextFileManager.getFile(this).write(data);
	}
}
