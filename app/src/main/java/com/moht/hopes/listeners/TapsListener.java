package com.moht.hopes.listeners;

import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Build;
import android.provider.Settings;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.storage.TextFileManager;

import java.util.Arrays;

import static com.moht.hopes.PermissionHandler.checkActivityRecognition;
import static com.moht.hopes.PermissionHandler.checkDrawOverlayPermission;

/*************************************************************************
 *
 * MOH Office of Healthcare Transformation (MOHT) CONFIDENTIAL
 *
 *  Copyright 2018-2019
 *  All Rights Reserved.
 *
 * NOTICE:  All information contained herein is, and remains
 * the property of MOH Office of Healthcare Transformation.
 * The intellectual and technical concepts contained
 * herein are proprietary to MOH Office of Healthcare Transformation
 * and may be covered by Singapore, U.S. and Foreign Patents,
 * patents in process, and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from MOH Office of Healthcare Transformation.
 */

public class TapsListener {
	public static final String name = "taps";
	public static final String short_name = "tap";
	public static final String header = "timestamp,in_app_name,orientation";
	private final Context context;
	private BackgroundService service;
	private InvisibleTouchView layerView;
	private WindowManager localWindowManager;

	public static int checkPermission(){
		return checkDrawOverlayPermission(BackgroundService.appContext)?1:-1;
	}

    public TapsListener(BackgroundService paramBackgroundService)
	{
		context = paramBackgroundService.getApplicationContext();
		service = paramBackgroundService;
		localWindowManager = (WindowManager)context.getSystemService(Context.WINDOW_SERVICE);
		layerView = new InvisibleTouchView(context);
		new Intent("android.intent.action.MAIN").addCategory("android.intent.category.HOME");
		addView();
	}

	public void addView()
	{
		if (BackgroundService.isTapAdded) return;
		WindowManager.LayoutParams localLayoutParams = new WindowManager.LayoutParams(
				1, 1, Build.VERSION.SDK_INT>=26?2038:2002, 262184, -3);

		if (Settings.canDrawOverlays(context))
		{
			if ( localWindowManager != null )
			{
				localWindowManager.addView(layerView, localLayoutParams);
				BackgroundService.isTapAdded = true;
			}
		}
		else
			BackgroundService.isTapAdded = false;
	}

	public void removeView()
	{
		try {
			if (this.layerView == null)
				throw new Exception("layerView is null");
			WindowManager localWindowManager =
					(WindowManager)this.context.getSystemService(Context.WINDOW_SERVICE);
			if (localWindowManager == null)
				throw new Exception("localWindowManager is null");
			localWindowManager.removeView(this.layerView);
			BackgroundService.isTapAdded = false;
        } catch (Exception e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("Reading", "Exception during removal");
			TextFileManager.getDebugLogFile().write("TapsListener::removeView: " + Arrays.toString(e.getStackTrace()));
		}
	}

	private class InvisibleTouchView extends View
	{
		InvisibleTouchView(Context paramContext)
		{
			super(paramContext);
		}

		public boolean onTouchEvent(MotionEvent paramMotionEvent )
		{
			super.onTouchEvent( paramMotionEvent );
			if ( paramMotionEvent != null ){
				String appname = TextFileManager.CS2S(service.getForegroundAppName());
				context.getResources().getConfiguration();
				String data = System.currentTimeMillis()
						+ TextFileManager.DELIMITER + appname
						+ TextFileManager.DELIMITER +
						Configuration.ORIENTATION_LANDSCAPE;
				TextFileManager.getFile(TapsListener.class).write( data );
			}
			return false;
		}
	}
}

