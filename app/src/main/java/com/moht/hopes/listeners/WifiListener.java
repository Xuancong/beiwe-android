package com.moht.hopes.listeners;

import android.content.Context;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.storage.EncryptionEngine;
import com.moht.hopes.storage.TextFileManager;

import java.util.List;

import static com.moht.hopes.PermissionHandler.checkWifiPermissions;

/**WifiListener
 * WifiListener houses a single public function, scanWifi.  This function grabs the mac
 * addresses of local wifi beacons and writes them to the wifiLog.  It only gets the data
 * if wifi is enabled.
 * @author Eli */
public class WifiListener {
	public static final String name = "wifi";
	public static final String short_name = "wif";
	public static final String header = "timestamp,hashedMAC,frequency,RSSI";
	private static WifiManager wifiManager;

	/** WifiListener requires an application context in order to access 
	 * the devices wifi info.  
	 * @param appContext requires a Context */
	private WifiListener (Context appContext) { wifiManager = (WifiManager) appContext.getSystemService(Context.WIFI_SERVICE); }
	
	public static WifiListener initialize( Context context ) { return new WifiListener( context ); }

	public static int checkPermission(){
		return checkWifiPermissions(BackgroundService.appContext)?1:-1;
	}
	
	//#######################################################################################
	//#############################  WIFI STATE #############################################
	//#######################################################################################

	/** Writes to the wifiLog file all mac addresses of local wifi beacons. */
	public void scanWifi() {
		if ( wifiManager.getWifiState() == WifiManager.WIFI_STATE_ENABLED ) {
			List<ScanResult> scanResults = wifiManager.getScanResults();
			if (scanResults != null) {
				String timeStamp = String.valueOf(System.currentTimeMillis());
				for (ScanResult result : scanResults) {
					String data = timeStamp + "," +
							EncryptionEngine.hashMAC(result.BSSID) + "," +
							result.frequency + "," +
							result.level;
					TextFileManager.getFile(this).write(data);
				}
			}
		} else
			TextFileManager.getDebugLogFile().write(
					"WifiListener::scanWifi: " + System.currentTimeMillis() +
							" wifi is not available for scanning at this time.");
	}
}
