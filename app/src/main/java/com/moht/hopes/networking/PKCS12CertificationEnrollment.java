package com.moht.hopes.networking;

import android.util.Base64;
import android.util.Log;

import com.moht.hopes.BuildConfig;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.ui.DebugInterfaceActivity;

import java.io.OutputStream;
import java.security.KeyFactory;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PKCS12CertificationEnrollment {

	private List<X509Certificate> certList;
	private String privateKey;
	private String alias;
	private OutputStream output;
	private String TAG = "PKCS12CertificationEnrollment";

	public PKCS12CertificationEnrollment(String alias,
	                                     OutputStream output,
	                                     String privateKey) {
		this.alias = alias;
		this.output = output;
		this.certList = new ArrayList<>();
		this.privateKey = privateKey;
	}

	public void addCert(X509Certificate cert) {
		certList.add(cert);
	}

	private PrivateKey getPrivateKey() {
		PrivateKey pvtKey = null;
		String key = privateKey.replaceAll("-----BEGIN RSA PRIVATE KEY-----", "")
				.replaceAll("-----END RSA PRIVATE KEY-----", "")
				.replaceAll("\\n", "")
				.replaceAll(" ", "");
		try {
			byte[] pkcs8EncodedBytes = Base64.decode(key, Base64.DEFAULT);
			PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(pkcs8EncodedBytes);
			pvtKey = KeyFactory.getInstance("RSA").generatePrivate(keySpec);
		} catch (Exception e) {
            if (BuildConfig.APP_IS_DEBUG) Log.e(TAG, "Exception while converting string to private key");
			TextFileManager.getDebugLogFile().write(TAG + "::getPrivateKey: " + DebugInterfaceActivity.printStack(e));
		}
		return pvtKey;
	}

	public void enroll(String pin) {
		try {
			PrivateKey key = getPrivateKey();
			KeyStore ks = KeyStore.getInstance("PKCS12");
			ks.load(null, null);
			ks.setKeyEntry(alias, key, pin.toCharArray(), certList.toArray(new Certificate[0]));
			ks.store(output, pin.toCharArray());
		} catch (Exception e) {
			if (BuildConfig.APP_IS_DEBUG) Log.d(TAG, "Exception while creating entry on key store");
			TextFileManager.getDebugLogFile().write(TAG + "::enroll: " + DebugInterfaceActivity.printStack(e));
		}
	}
}
