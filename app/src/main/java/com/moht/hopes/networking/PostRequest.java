package com.moht.hopes.networking;

import android.content.Context;
import android.icu.util.Output;
import android.util.Log;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.DeviceInfo;
import com.moht.hopes.R;
import com.moht.hopes.storage.EncryptionEngine;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.ui.DebugInterfaceActivity;
import com.moht.hopes.ui.user.MainMenuActivity;
import com.moht.hopes.utils.GS_JSON;
import com.moht.hopes.utils.GS_JSON.*;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Objects;
import java.util.Random;
import java.util.zip.CRC32;
import java.util.zip.CheckedOutputStream;
import java.util.zip.Checksum;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * PostRequest is our class for handling all HTTP operations we need; they are all in the form of HTTP post requests.
 * All HTTP connections are HTTPS, and automatically include a password and identifying information.
 *
 * @author Josh, Eli, Dor
 */

//TODO: Low priority. Eli. clean this up and update docs. It does not adequately state that it puts into any request automatic security parameters, and it is not obvious why some of the functions exist (minimal http thing)
public class PostRequest {
	private static Context appContext;

	/**
	 * Uploads must be initialized with an appContext before they can access the wifi state or upload a _file_.
	 */
	private PostRequest(Context applicationContext) {
		appContext = applicationContext;
	}

	/**
	 * Simply runs the constructor, using the applcationContext to grab variables.  Idempotent.
	 */
	public static void initialize(Context applicationContext) {
		new PostRequest(applicationContext);
	}

	private static final Object FILE_UPLOAD_LOCK = new Object() {
	}; //Our lock for file uploading

	/*##################################################################################
	 ##################### Publicly Accessible Functions ###############################
	 #################################################################################*/


	/**
	 * For use with Async tasks.
	 * This opens a connection with the server, sends the HTTP parameters, then receives a response code, and returns it.
	 *
	 * @param parameters HTTP parameters
	 * @return serverResponseCode
	 */
	public static int httpRegister(String parameters, String url) {
		try {
			return doRegisterRequest(parameters, new URL(url));
		} catch (MalformedURLException e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("PostRequestFileUpload", "malformed URL");
			TextFileManager.getDebugLogFile().write("PostRequest::httpRegister: " + DebugInterfaceActivity.printStack(e));
			return 0;
		} catch (Exception e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("PostRequest", "Network error");
			TextFileManager.getDebugLogFile().write("PostRequest::httpRegister: " + DebugInterfaceActivity.printStack(e));
			return 502;
		}
	}

	/**
	 * For use with Async tasks.
	 * Makes an HTTP post request with the provided URL and parameters, returns the server's response code from that request
	 *
	 * @param parameters HTTP parameters
	 * @return an int of the server's response code from the HTTP request
	 */
	public static int httpRequestcode(String parameters, String url, String newPassword) {
		try {
			return doPostRequestGetResponseCode(parameters, new URL(url), newPassword);
		} catch (MalformedURLException e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("PostRequestFileUpload", "malformed URL");
			TextFileManager.getDebugLogFile().write("PostRequest::httpRequestCode: " + DebugInterfaceActivity.printStack(e));
			return 0;
		} catch (Exception e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("PostRequest", "Unable to establish network connection");
			TextFileManager.getDebugLogFile().write("PostRequest::httpRequestCode: " + DebugInterfaceActivity.printStack(e));
			return 502;
		}
	}

	/**
	 * For use with Async tasks.
	 * Makes an HTTP post request with the provided URL and parameters, returns a string of the server's entire response.
	 *
	 * @param parameters HTTP parameters
	 * @param urlString  a string containing a url
	 * @return a string of the contents of the return from an HTML request.
	 */
	//TODO: Eli. low priority. investigate the android studio warning about making this a package local function
	static String httpRequestString(String parameters, String urlString) {
		try {
			return doPostRequestGetResponseString(parameters, urlString);
		} catch (Exception e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("PostRequest error", "Download File failed with exception");
			TextFileManager.getDebugLogFile().write("PostRequest::httpRequestString: " + DebugInterfaceActivity.printStack(e));
			throw new NullPointerException("Download File failed.");
		}
	}

	static TrustManager[] trustAllCertificates = new TrustManager[]{
			new X509TrustManager() {
				@Override
				public X509Certificate[] getAcceptedIssuers() {
					return null; // Not relevant.
				}

				@Override
				public void checkClientTrusted(X509Certificate[] certs, String authType) {
					// Do nothing. Just allow them all.
				}

				@Override
				public void checkServerTrusted(X509Certificate[] certs, String authType) {
					// Do nothing. Just allow them all.
				}
			}
	};
	
	/*##################################################################################
	 ################################ Common Code ######################################
	 #################################################################################*/

	/**
	 * Creates an HTTP connection with minimal settings.  Some network funcitonality
	 * requires this minimal object.
	 *
	 * @param url a URL object
	 * @return a new HttpsURLConnection with minimal settings applied
	 * @throws IOException This function can throw 2 kinds of IO exceptions:
	 * IOExceptions and ProtocolException
	 */
	private static HttpsURLConnection minimalHTTP(URL url) throws Exception {
		try {
			System.setProperty("jsse.enableSNIExtension", "false");
			SSLContext context = SSLContext.getInstance("SSL");
			context.init(null, trustAllCertificates, new SecureRandom());
			HostnameVerifier hostnameVerifier = (hostname, session) -> true;

			// Create a new HttpsURLConnection and set its parameters
			HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
			connection.setSSLSocketFactory(context.getSocketFactory());
			connection.setHostnameVerifier(hostnameVerifier);
			connection.setUseCaches(false);
			connection.setDoOutput(true);
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Cache-Control", "no-cache");
			connection.setConnectTimeout(60000);
			connection.setReadTimeout(100000);
			return connection;
		} catch (Exception e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("ssl","Error: SSL connection error!");
			TextFileManager.getDebugLogFile().write("PostRequest::minimalHTTP: " + DebugInterfaceActivity.printStack(e));
			throw(e);
		}
	}

	/**
	 * For use with functionality that requires additional parameters be added to an HTTP operation.
	 * @param parameters a string that has been created using the makeParameters function
	 * @param url        a URL object
	 * @return a new HttpsURLConnection with common settings
	 */
	public static HttpsURLConnection setupHTTP(String parameters, URL url, String newPassword) throws Exception {
		HttpsURLConnection connection = minimalHTTP(url);
		DataOutputStream request = new DataOutputStream(Objects.requireNonNull(connection).getOutputStream());
		request.write(securityParameters(newPassword).getBytes());
		request.write(parameters.getBytes());
		request.flush();
		request.close();

		return connection;
	}

	/**
	 * Reads in the response data from an HttpsURLConnection, returns it as a String.
	 *
	 * @param connection an HttpsURLConnection
	 * @return a String containing return data
	 * @throws IOException Exception
	 */
	private static String readResponse(HttpsURLConnection connection) throws IOException {
		int responseCode = connection.getResponseCode();
		if (responseCode == 200) {
			ByteArrayOutputStream result = new ByteArrayOutputStream();
			InputStream inputStream = connection.getInputStream();
			byte[] buffer = new byte[1024];
			for (int length; (length = inputStream.read(buffer)) != -1; ) {
				result.write(buffer, 0, length);
			}
			try {
				return result.toString("UTF-8");
			} catch (Exception e) {
				return result.toString();
			}
		}
		return Integer.toString(responseCode);
	}


	/*##################################################################################
	 ####################### Actual Post Request Functions #############################
	 #################################################################################*/

	private static String doPostRequestGetResponseString(String parameters, String urlString) throws Exception {
		HttpsURLConnection connection = setupHTTP(parameters, new URL(urlString), null);
		connection.connect();
		String data = readResponse(connection);
		connection.disconnect();
		return data;
	}


	private static int doPostRequestGetResponseCode(String parameters, URL url, String newPassword) throws Exception {
		HttpsURLConnection connection = setupHTTP(parameters, url, newPassword);
		int response = connection.getResponseCode();
		connection.disconnect();
		return response;
	}


	private static int doRegisterRequest(String parameters, URL url) throws Exception {
		HttpsURLConnection connection = setupHTTP(parameters, url, null);
		int response = connection.getResponseCode();

		if (response == 200) {
			String responseBody = readResponse(connection);
			try {
				JsonObject responseJSON = (JsonObject)GS_JSON.parse(responseBody);
				PersistentData.setLoginCredentials((String)responseJSON.get("user_id"), (String)responseJSON.get("password"));
				writeKey((String)responseJSON.get("encrypt_key"), response);
				long created_on = (long)responseJSON.get("created_on");
				Random gen = new Random(created_on);
				PersistentData.setFloat(PersistentData.LATITUDE_OFFSET_KEY, -1000+gen.nextFloat()*2000);
				PersistentData.setFloat(PersistentData.LONGITUDE_OFFSET_KEY, -1000+gen.nextFloat()*2000);
				PersistentData.setFloat(PersistentData.ROTATION_OFFSET_KEY, (float)(-Math.PI+gen.nextFloat()*Math.PI));
				JsonObject deviceSettings = (JsonObject)responseJSON.get("device_settings");
				PersistentData.writeDeviceSettings(deviceSettings);
			} catch (Exception e) {
				TextFileManager.getDebugLogFile().write(
						"PostRequest::doRegisterRequest: " + Arrays.toString(e.getStackTrace()));
			}
		}
		connection.disconnect();
		return response;
	}

	private static int doUpdateSettings() throws Exception {
		String parameters = PostRequest.makeParameter("patient_id", PersistentData.getPatientID());
		URL url = new URL(PersistentData.getServerUrl()+"/update_setting");
		HttpsURLConnection connection = setupHTTP(parameters, url, null);
		int response = connection.getResponseCode();
		if (response == 200) {
			String responseBody = readResponse(connection);
			try {
				JsonObject responseJSON = (JsonObject)GS_JSON.parse(responseBody);
				JsonObject deviceSettings = (JsonObject)responseJSON.get("device_settings");
				PersistentData.writeDeviceSettings(deviceSettings);
				BackgroundService.localHandle.doSetup();
			} catch (Exception e) {
				TextFileManager.getDebugLogFile().write("PostRequest::doUpdateSettings: " + Arrays.toString(e.getStackTrace()));
			}
		}
		connection.disconnect();
		return response;
	}

	public static int writeKey(String key, int httpResponse) {
		if (!key.startsWith("MIIBI")  && !key.toLowerCase().startsWith("0x")) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("PostRequest - register", " Received an invalid encryption key from server");
			TextFileManager.getDebugLogFile().write("PostRequest::doUpdateSettings: Received an invalid encryption key from server");
			return 2;
		}
		TextFileManager.getKeyFile().deleteSafely();
		TextFileManager.getKeyFile().write(key);
		return httpResponse;
	}


	/**
	 * Constructs and sends a multipart HTTP POST request with a file attached.
	 * This function uses minimalHTTP() directly because it needs to add a header (?) to the
	 * HttpsURLConnection object before it writes a file to it.
	 * The original doFileUpload() function does not work if a text file contains special characters like "+"
	 * @param file      the File to be uploaded
	 * @param uploadUrl the destination URL that receives the upload
	 * @return HTTP Response code as int
	 * @throws IOException Exception
	 */
	public static String CRLF = "\r\n", charset = "UTF-8";
	private static int doFileUpload(File file, URL uploadUrl, int conn_timeout, boolean is_last) throws Exception {
		if (file.length() > 1024 * 1024 * 10 && BuildConfig.APP_IS_DEBUG)
			DebugInterfaceActivity.smartLog( DebugInterfaceActivity.UploadFiles.name,
					"upload big file: name = " + file.getName() + " ; length = " + file.length());

		HttpsURLConnection connection = minimalHTTP(uploadUrl);
		connection.setConnectTimeout(conn_timeout);
		String boundary = Long.toHexString(System.currentTimeMillis());
		connection.setRequestProperty("Content-Type", "multipart/form-data; boundary=" + boundary);
		OutputStream output = connection.getOutputStream();
		PrintWriter writer = new PrintWriter(new OutputStreamWriter(output,charset),true);

		// Send normal param.
		String params = securityParameters(PersistentData.getPassword()) + makeParameter("file_name", file.getName());
		if(is_last) params += makeParameter("is_last", "1");
		params = params.substring(0, params.length()-1);
		writer.append("--" + boundary).append(CRLF);
		writer.append("Content-Disposition: form-data; name=\"params\"").append(CRLF);
		writer.append("Content-Type: text/plain; charset=" + charset).append(CRLF);
		writer.append(CRLF).append(params).append(CRLF).flush();

		// Send binary file.
		writer.append("--" + boundary).append(CRLF);
		writer.append("Content-Disposition: form-data; name=\"file\"; filename=\"" + file.getName() + "\"").append(CRLF);
		writer.append("Content-Type: text/plain").append(CRLF);
		writer.append("Content-Transfer-Encoding: binary").append(CRLF);
		writer.append(CRLF).flush();
		Checksum crc32 = new CRC32();
		CheckedOutputStream CS_output = new CheckedOutputStream(output, crc32);
		Files.copy(file.toPath(), CS_output);
		CS_output.flush(); // Important before continuing with writer!
		writer.append(CRLF).flush(); // CRLF is important! It indicates end of boundary.

		// End of multipart/form-data.
		writer.append("--" + boundary + "--").append(CRLF).flush();

		// Get HTTP Response. Pretty sure this blocks, nothing can really be done about that.
		int response = connection.getResponseCode();
		String responseBody = readResponse(connection);
		connection.disconnect();

		if(response!=200) return response;

		// Verify file checksum
		JsonObject responseJSON = (JsonObject)GS_JSON.parse(responseBody);
		if( (long)responseJSON.get("filesize") != file.length() || (long)responseJSON.get("checksum") != crc32.getValue())
			throw new Exception("File corruption during upload");

		String report_html = (String)responseJSON.getOrDefault("report_html", null);
		if(report_html!=null) {
			PersistentData.setReportContent(report_html);
			MainMenuActivity.displayGraphs();
		}

		DebugInterfaceActivity.smartLog( DebugInterfaceActivity.UploadFiles.name,
				"uploading finished: filename = " + file.getName() + " ; response = " + response);
		return response;
	}


	//#######################################################################################
	//################################## File Upload ########################################
	//#######################################################################################
	/**
	 * Uploads all available files on a separate thread.
	 */
	public static void uploadAllFiles() {
		// determine if you are allowed to upload over WiFi or cellular data, return if not.
		if (!NetworkUtility.canUpload(appContext))
			return;

		if(BuildConfig.APP_IS_DEBUG) Log.d("DOING UPLOAD STUFF", "DOING UPLOAD STUFF");

		// Run the HTTP POST on a separate thread
		Thread uploaderThread = new Thread(PostRequest::doUploadAllFiles, "uploader_thread");
		uploaderThread.start();
	}

	/**
	 * Uploads all files to the HOPES server.
	 * Files get deleted as soon as a 200 OK code in received from the server.
	 */
	private static void doUploadAllFiles() {
		synchronized (FILE_UPLOAD_LOCK) {
			File file;
			URL uploadUrl; //set up url, write a crash log and fail gracefully if this ever breaks.
			try {
				uploadUrl = new URL(addWebsitePrefix(appContext.getResources()
						.getString(R.string.data_upload_url)));
			} catch (Exception e) {
				if(BuildConfig.APP_IS_DEBUG) Log.e("PostRequest::doUploadAllFiles", DebugInterfaceActivity.printStack(e));
				return;
			}

			String [] file_list = TextFileManager.getAllUploadableFiles();
			for (int n=0; n<file_list.length; ++n) {
				String fileName = file_list[n];
				String datetime = Calendar.getInstance().getTime().toString();
				try {
					file = new File(appContext.getFilesDir() + "/" + fileName);
					int statusCode = PostRequest.doFileUpload(file, uploadUrl, 20000, (n==file_list.length-1));
					if (statusCode==200) {
						TextFileManager.delete(fileName);
						PersistentData.setMainUploadInfo("Last Successful File Upload: " + datetime + "\nStatus:" + statusCode);
					} else {
						PersistentData.setFailUploadInfo("Last Failed File Upload: " + datetime + "\nStatus:" + statusCode);

						// If phone is de-registered reset the app and kill the app
						if (statusCode==412)
							DebugInterfaceActivity.RESET(false, true);
					}
				} catch (Exception e) {
					String stack_trace = DebugInterfaceActivity.printStack(e);
					PersistentData.setFailUploadInfo("Last Failed File Upload: " + datetime + "\nError: " + e.toString() + "\nStackTrace: " + stack_trace);
					TextFileManager.getDebugLogFile().write("PostRequest::doUploadAllFiles: " + stack_trace);
					if(BuildConfig.APP_IS_DEBUG) Log.e("PostRequest.java", "Failed to upload file " + fileName);
				}
			}
			if(BuildConfig.APP_IS_DEBUG) Log.i("DOING UPLOAD STUFF", "DONE WITH UPLOAD");
		}

		/// Reporting
//		CompletionReportDownloader.downloadReport(appContext);
	}


	//#######################################################################################
	//############################### UTILITY FUNCTIONS #####################################
	//#######################################################################################

	public static String makeParameter(String key, String value) {
		return key + "=" + value + "&";
	}

	/**
	 * Create the 3 standard security parameters for POST request authentication.
	 *
	 * @param newPassword If this is a Forgot Password request, pass in a newPassword string from
	 *                    a text input field instead of from the device storage.
	 * @return a String of the securityParameters to append to the POST request
	 */
	private static String securityParameters(String newPassword) {
		String patientId = PersistentData.getPatientID();
		String studyId = PersistentData.getStudyID();
		String deviceId = DeviceInfo.getAndroidID(null);
		String password = (newPassword != null) ? newPassword : PersistentData.getPassword();

		long stmp = System.currentTimeMillis()/100000L;
		newPassword = EncryptionEngine.safeHash(password+"@"+stmp);

		return makeParameter("patient_id", patientId) +
				makeParameter("study_id", studyId) +
				makeParameter("password", newPassword) +
				makeParameter("device_id", deviceId);
	}

	public static String addWebsitePrefix(String URL) {
		String serverUrl = PersistentData.getServerUrl();
		if (serverUrl != null) {
			return serverUrl + URL;
		} else {
			// If serverUrl == null, this should be an old version of the hopes that didn't let the
			// user specify the URL during registration, so assume the URL is either
			// studies.moht.org or staging.moht.org.
			return appContext.getResources().getString(R.string.default_study_website) + URL;
		}
	}
}
