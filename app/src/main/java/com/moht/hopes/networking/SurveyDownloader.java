package com.moht.hopes.networking;

import android.content.Context;
import android.util.Log;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.R;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.survey.SurveyScheduler;
import com.moht.hopes.ui.DebugInterfaceActivity;
import com.moht.hopes.ui.utils.SurveyNotifications;
import com.moht.hopes.utils.GS_JSON;
import com.moht.hopes.utils.GS_JSON.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.moht.hopes.networking.PostRequest.addWebsitePrefix;
import static com.moht.hopes.survey.SurveyScheduler.isSurveyPushType;

public class SurveyDownloader {
	
	public static void downloadSurveys( Context appContext ) {
		if(BuildConfig.APP_IS_DEBUG) Log.d("SurveyDownloader", "downloadJSONQuestions() called");
		doDownload( addWebsitePrefix(appContext.getResources().getString(R.string.download_surveys_url)), appContext );
	}

	private static void doDownload(final String url, final Context appContext) { new HTTPAsync(url) {
		String jsonResponseString;
		@Override
		protected Void doInBackground(Void... arg0) {
			String parameters = "";
			Map<String, String> checksums = PersistentData.getSurveyChecksums();
			String checksumsStr = GS_JSON.toJSON(checksums);
			if (checksumsStr != null)
				parameters += PostRequest.makeParameter("checksums", checksumsStr);
			try { jsonResponseString = PostRequest.httpRequestString( parameters, url); }
			catch (NullPointerException ignored) {  }  //We do not care.
			return null; //hate
		}
		@Override
		protected void onPostExecute(Void arg) {
			if(jsonResponseString.equals("412")) {
				DebugInterfaceActivity.RESET(false, true);
				return;
			}
			responseCode = updateSurveys( appContext, jsonResponseString);
			super.onPostExecute(arg);
		} }.execute();
	}

	private static int updateSurveys( Context appContext, String jsonString){
		if (jsonString == null) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("Survey Downloader", "jsonString is null, probably have no network connection. squashing.");
			TextFileManager.getDebugLogFile().write("SurveyDownloader::updateSurveys: JSON string null, Probably no network connection");
			return -1; }

		JsonArray<JsonObject> surveys;
		try {
			surveys = (JsonArray<JsonObject>)GS_JSON.parse(jsonString);
		} catch (Exception e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("Survey Downloader", "JSON PARSING FAIL");
			TextFileManager.getDebugLogFile().write("SurveyDownloader::updateSurveys: " + DebugInterfaceActivity.printStack(e));
			return -1;
		}

		List<String> oldSurveyIds = PersistentData.getSurveyIds();
		ArrayList<String> newSurveyIds = new ArrayList<String>();
		String surveyId;
		Boolean surveyUpToDate;
		String surveyType;
		String jsonQuestionsString;
		String jsonChecksumString;
		String jsonTimingsString, jsonExpiryString;
		String jsonSettingsString;

		for (JsonObject surveyJSON: surveys){

			try {
				surveyId = (String)surveyJSON.get("_id");
			} catch (Exception e) {
				if (BuildConfig.APP_IS_DEBUG) Log.e("Survey Downloader", "JSON fail 2");
				TextFileManager.getDebugLogFile().write("SurveyDownloader::updateSurveys: " + DebugInterfaceActivity.printStack(e));
				return -1;
			}

			surveyUpToDate = (Boolean)surveyJSON.get("up_to_date");
			if (surveyUpToDate != null && surveyUpToDate) {
				newSurveyIds.add(surveyId);
				continue;
			}

			try {
				surveyType = (String)surveyJSON.get("survey_type");
			} catch (Exception e) {
				if(BuildConfig.APP_IS_DEBUG) Log.e("Survey Downloader", "JSON fail 2.5");
				TextFileManager.getDebugLogFile().write("SurveyDownloader::updateSurveys: " + DebugInterfaceActivity.printStack(e));
				return -1;
			}

			try {
				jsonQuestionsString = ((JsonArray)surveyJSON.get("content")).getSourceString();
			} catch (Exception e) {
				if(BuildConfig.APP_IS_DEBUG) Log.e("Survey Downloader", "JSON fail 3");
				TextFileManager.getDebugLogFile().write("SurveyDownloader::updateSurveys: " + DebugInterfaceActivity.printStack(e));
				return -1;
			}

			try {
				jsonTimingsString = ((JsonArray)surveyJSON.get("timings")).getSourceString();
			} catch (Exception e) {
				if(BuildConfig.APP_IS_DEBUG) Log.e("Survey Downloader", "JSON fail 4");
				TextFileManager.getDebugLogFile().write(
						"SurveyDownloader::updateSurveys: " + DebugInterfaceActivity.printStack(e));
				return -1;
			}

			try {
				jsonExpiryString = Objects.toString(surveyJSON.get("expiry"), "");
			} catch (Exception e) {
				jsonExpiryString = "";
			}

			try {
				jsonChecksumString = (String)surveyJSON.get("checksum");
			} catch (Exception e) {
				if(BuildConfig.APP_IS_DEBUG) Log.e("Survey Downloader", "JSON fail 5");
				TextFileManager.getDebugLogFile().write("SurveyDownloader::updateSurveys: " + DebugInterfaceActivity.printStack(e));
				return -1;
			}

			try {
				jsonSettingsString = ((JsonObject)surveyJSON.get("settings")).getSourceString();
			} catch (Exception e) {
				if(BuildConfig.APP_IS_DEBUG) Log.e("Survey Downloader", "JSON settings not present");
				TextFileManager.getDebugLogFile().write("SurveyDownloader::updateSurveys: " + DebugInterfaceActivity.printStack(e));
				return -1;
			}

			// if surveyId already exists, check for changes, add to list of new survey ids.
			if ( oldSurveyIds.contains(surveyId) ) {
				PersistentData.setSurveyContent(surveyId, jsonQuestionsString);
				PersistentData.setSurveyChecksum(surveyId, jsonChecksumString);
				PersistentData.setSurveyType(surveyId, surveyType);
				PersistentData.setSurveySettings(surveyId, jsonSettingsString);
				if ( ! PersistentData.getSurveyTimes(surveyId).equals(jsonTimingsString) ) {
					BackgroundService.cancelSurveyAlarm(surveyId);
					PersistentData.setSurveyTimes(surveyId, jsonTimingsString);
					PersistentData.setSurveyExpiry(surveyId, jsonExpiryString);
					SurveyScheduler.scheduleSurvey(surveyId);
				}
				newSurveyIds.add(surveyId);
			} else { //if survey is new, create new survey entry.
				// Log.d("debugging survey update", "CREATE A SURVEY");
				PersistentData.addSurveyId(surveyId);
				PersistentData.createSurveyData(surveyId, jsonQuestionsString, jsonChecksumString,
						jsonTimingsString, jsonExpiryString, surveyType, jsonSettingsString);
				BackgroundService.registerTimers(appContext); // We need to register the surveyId before we can schedule it
				SurveyScheduler.scheduleSurvey(surveyId);
				SurveyScheduler.checkImmediateTriggerSurvey(surveyId);
			}
		}

		for (String oldSurveyId : oldSurveyIds){ //for each old survey id
			if ( !newSurveyIds.contains( oldSurveyId ) && !isSurveyPushType(oldSurveyId) ) { //check if it is still a valid survey (in the list of new survey ids.)
				// Log.d("survey downloader", "deleting survey " + oldSurveyId);
				PersistentData.deleteSurvey(oldSurveyId);
				//It is almost definitely not worth the effort to cancel any ongoing alarms for a survey. They are one-time, and there is de minimus value to actually cancelling it.
				// also, that requires accessing the background service, which means using ugly hacks like we do with the survey scheduler (though it would be okay because this code can only actually run if the background service is already instantiated.
				SurveyNotifications.dismissNotification(appContext, oldSurveyId);
				BackgroundService.registerTimers(appContext);
			}
		}
		return 200;
	}
}
