package com.moht.hopes.storage;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.util.Base64;
import android.util.Log;

import androidx.appcompat.app.AlertDialog;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.DeviceInfo;
import com.moht.hopes.storage.models.ECPoint;
import com.moht.hopes.ui.DebugInterfaceActivity;

import org.spongycastle.crypto.PBEParametersGenerator;
import org.spongycastle.crypto.digests.SHA256Digest;
import org.spongycastle.crypto.generators.PKCS5S2ParametersGenerator;
import org.spongycastle.crypto.params.KeyParameter;

import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Objects;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**The EncryptionEngine handles all encryption and hashing duties for the hopes.
 * Per-file AES encryption keys are generated and encrypted with the provided RSA key.
 * The RSA key is provided by the administrating server.
 * Hashing uses the SHA256 hashing algorithm.
 * @author Eli Jones, Josh Zagorsky */
public class EncryptionEngine {
	private static final boolean IS_RSA = false;
	private static PublicKey RSAkey = null;

	/*############################################################################
	 * ############################### Hashing ###################################
	 * #########################################################################*/
	
	/** Takes a string as input, handles the usual thrown exceptions, and return a hash string of that input.  
	 * @param input A String to hash
	 * @return a Base64 String of the hash result. */
	public static String safeHash (String input) {
		try { return unsafeHash( input ); }
		catch (NoSuchAlgorithmException e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("Hashing function", "NoSuchAlgorithmException");
			TextFileManager.getDebugLogFile().write("EncryptionEngine::safeHash: " + DebugInterfaceActivity.printStack(e));
			throw new NullPointerException("device is too old, crashed inside safeHash 1");
		}
	}
	
	
	/** Takes a string as input, outputs a hash.
	 * @param input A String to hash.
	 * @return a Base64 String of the hash result. */
	public static String unsafeHash (String input) throws NoSuchAlgorithmException {
		if (input == null ) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("Hashing", "The hash function received a null string, it should now crash...");
			input = "";
		}
		if (input.length() == 0) { return "null_data"; } //if an empty string is provided, return a string describing this.
		MessageDigest hash;
		hash = MessageDigest.getInstance("SHA-256");
		hash.update( input.getBytes(StandardCharsets.UTF_8) );
		return toBase64String( hash.digest() );
	}

	/** Takes a string as input, outputs a PBKDF2 hash.
	 * @param input A String to hash.
	 * @return a Base64 String of the hash result. */
	private static String PBKDF2Hash(String input) {
		byte[] salt = PersistentData.getHashSalt();
		int iterations = PersistentData.getHashIterations();
		PKCS5S2ParametersGenerator generator = new PKCS5S2ParametersGenerator(new SHA256Digest());
		generator.init(PBEParametersGenerator.PKCS5PasswordToUTF8Bytes(input.toCharArray()), salt, iterations);

		// keySize is 512 because we think it is the size of the digest
		KeyParameter key = (KeyParameter)generator.generateDerivedMacParameters(512);
		return toBase64String(key.getKey());
	}
	
	/**Converts a phone number into a 64-character hexadecimal string.
	 * First standardizes the phone numbers by grabbing the last 10 digits, so
	 * that hopefully, two identical phone numbers will get identical hashes,
	 * even if one has dashes and a country code and the other doesn't.
	 * 
	 * Grabbing the last 10 numerical characters is much simpler than using something like this:
	 * https://github.com/googlei18n/libphonenumber
	 * 
	 * @param phoneNumber a string representing a phone number
	 * @return a hexadecimal string, or an error message string */
	public static String hashPhoneNumber(String phoneNumber) {

		// Strip from the string any characters that aren't digits 0-9
		String justDigits = phoneNumber.replaceAll("\\D+", "");

		// Grab the last N digits
		String lastN = DeviceInfo.last_substr(justDigits);

		// Hash the last N digits
		return PersistentData.getBoolean(PersistentData.USE_ANONYMIZED_HASHING) ? PBKDF2Hash(lastN) : safeHash(lastN);
	}

	public static String hashSociabilityContact(String contact) {
		return (PersistentData.getUseAnonymizedHashing() ? PBKDF2Hash(contact) : safeHash(contact)).substring(0, 10);
	}


	public static String hashMAC(String MAC) {
		if(MAC==null || MAC.isEmpty()) return "";
		if (PersistentData.getBoolean(PersistentData.USE_ANONYMIZED_HASHING))
			return PBKDF2Hash(MAC);
		else
			return safeHash(MAC);
	}
	
	
	/*############################################################################
	 * ############################ Encryption ###################################
	 * #########################################################################*/

	/**Encrypts data using the RSA cipher and the public half of an RSA key pairing provided by the server.
	 * @param data to be encrypted
	 * @return a hex string of the encrypted data. */
	@SuppressLint("TrulyRandom")
	public static String encryptRSA(byte[] data) throws InvalidKeySpecException {
		if (RSAkey == null) readKey();
		
		// unfortunately we have problems encrypting this data, it occasionally loses a character,
		// so we need to base64 encode it first.
		data = toBase64Array(data);
		
		byte[] encryptedText;
		Cipher rsaCipher;
		
		try { rsaCipher = Cipher.getInstance("RSA/NONE/OAEPPadding"); }
		catch (NoSuchAlgorithmException e) {
			TextFileManager.getDebugLogFile().write("EncryptionEngine::encryptRSA: " + DebugInterfaceActivity.printStack(e));
			if(BuildConfig.APP_IS_DEBUG) Log.e("Encryption Engine", "THIS DEVICE DOES NOT SUPPORT RSA");
			throw new NullPointerException("device is too old");}
		catch (NoSuchPaddingException e) {
			TextFileManager.getDebugLogFile().write("EncryptionEngine::encryptRSA: " + DebugInterfaceActivity.printStack(e));
			if(BuildConfig.APP_IS_DEBUG) Log.e("Encryption Engine", "Device does not recognize padding format.  this is interesting because there ISN'T ONE (instance 1)");
			throw new NullPointerException("device is too old");}

		try { rsaCipher.init(Cipher.ENCRYPT_MODE, RSAkey);	}
		catch (InvalidKeyException e) {
			TextFileManager.getDebugLogFile().write("EncryptionEngine::encryptRSA: " + DebugInterfaceActivity.printStack(e));
			if(BuildConfig.APP_IS_DEBUG) Log.e("Encryption Engine", "The key is not a valid public RSA key.");
			throw new NullPointerException("the RSA key was not valid");
		} //will crash soon
		
		try { encryptedText = rsaCipher.doFinal( data ); }
		catch (IllegalBlockSizeException e1) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("Encryption Engine", "The key is malformed.");
			TextFileManager.getDebugLogFile().write("EncryptionEngine::encryptRSA: " + DebugInterfaceActivity.printStack(e1));
			// NOTE FOR FUTURE UPGRADES TO THIS SOFTWARE.
			// The only solution to this error is to uninstall and reregister the device,
			// or build entirely new functionality (no.) to redownload the key.
			// This seems like it would be a security headache, so let's not attempt that.
			throw new NullPointerException("RSA Key is invalid, the user needs to reregister their device."); } 
		catch (BadPaddingException e2) {
			TextFileManager.getDebugLogFile().write("EncryptionEngine::encryptRSA: " + DebugInterfaceActivity.printStack(e2));
			if(BuildConfig.APP_IS_DEBUG) Log.e("Encryption Engine", "Device does not reconize padding format.  this is interesting because there ISN'T ONE (instance 2)");
			throw new NullPointerException("device is too old"); }

		return toBase64String(encryptedText);
	}
	
	
	/**Encrypts data using provided AES key
	 * @param plainText Any plain text data.
	 * @param aesKey A byte array, must contain 128 bits, used as the AES key.
	 * @return a string containing colon separated url-safe Base64 encoded data. First value is the Initialization Vector, second is the encrypted data.
	 * @throws InvalidKeyException exception
	 * @throws InvalidKeySpecException exception*/
	public static String encryptAES(String plainText, SecretKey aesKey) throws InvalidKeyException {
		return encryptAES( plainText.getBytes(), aesKey );
	}
	
	static String encryptAES(byte[] plainText, SecretKey aesKey) throws InvalidKeyException {
		Cipher cipher;
		try {
			cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init( Cipher.ENCRYPT_MODE, aesKey);
			return toBase64String(cipher.getIV()) + ":" + toBase64String(cipher.doFinal(plainText));
		} catch (NoSuchAlgorithmException e) { // seems unlikely and should fail at the previous AES
			TextFileManager.getDebugLogFile().write("EncryptionEngine::encryptAES: " + DebugInterfaceActivity.printStack(e));
			if(BuildConfig.APP_IS_DEBUG) Log.e("Encryption Engine", "device does not know what AES is, instance 2" );
			throw new NullPointerException("device is too stupid to live");
		} catch (NoSuchPaddingException e) { //seems unlikely
			TextFileManager.getDebugLogFile().write("EncryptionEngine::encryptAES: " + DebugInterfaceActivity.printStack(e));
			if(BuildConfig.APP_IS_DEBUG) Log.e("Encryption Engine", "device does not know what PKCS5 padding is" );
			throw new NullPointerException("device is too stupid to live");
		} catch (BadPaddingException e) {
			TextFileManager.getDebugLogFile().write("EncryptionEngine::encryptAES: " + DebugInterfaceActivity.printStack(e));
			if(BuildConfig.APP_IS_DEBUG) Log.e("Encryption Engine", "an unknown error occurred in AES padding" );
			throw new NullPointerException("an unknown error occurred in AES encryption.");
		} catch (IllegalBlockSizeException e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("Encryption Engine", "an impossible error occurred" );
			TextFileManager.getDebugLogFile().write("EncryptionEngine::encryptAES: " + DebugInterfaceActivity.printStack(e));
			throw new NullPointerException("device is too stupid to live");
		}
	}
	
	/* #######################################################################
	 * ########################## Key Management #############################  
	 * #####################################################################*/
	
	/**Checks for and reads in the RSA key file. 
	 * @throws InvalidKeySpecException Thrown most commonly when there is no key file, this is expected behavior.*/
	public static void readKey() throws InvalidKeySpecException {
		String key_content = null;
		try {
			key_content = TextFileManager.getKeyFile().read();
			assert key_content!=null;
		} catch (Exception e) {
			try {
				AlertDialog alertDialog = new AlertDialog.Builder(BackgroundService.appContext).create();
				alertDialog.setTitle("Error");
				alertDialog.setMessage("Encryption key file not found, please re-register the study!");
				alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
						new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int which) {
								dialog.dismiss();
								DebugInterfaceActivity.RESET(false, true);
							}
						});
				alertDialog.show();
			} catch (Exception e1) {
				DebugInterfaceActivity.RESET(false, true);
			}
			throw new InvalidKeySpecException("Encryption key file not found!");
		}

		if (IS_RSA) {
			byte[] key_bytes = Base64.decode(key_content, Base64.DEFAULT);
			X509EncodedKeySpec x509EncodedKey = new X509EncodedKeySpec( key_bytes );

			try {
				KeyFactory keyFactory = KeyFactory.getInstance("RSA");
				RSAkey = keyFactory.generatePublic( x509EncodedKey );
			} catch (NoSuchAlgorithmException e1) {
				TextFileManager.getDebugLogFile().write("EncryptionEngine::readKey: " + DebugInterfaceActivity.printStack(e1));
				if(BuildConfig.APP_IS_DEBUG) Log.e("Encryption Engine", "ENCRYPTION HAS FAILED BECAUSE RSA IS NOT SUPPORTED?");
				throw new NullPointerException("ENCRYPTION HAS FAILED BECAUSE RSA IS NOT SUPPORTED?");
			} catch (InvalidKeySpecException e2) {
				TextFileManager.getDebugLogFile().write("EncryptionEngine::readKey: " + DebugInterfaceActivity.printStack(e2));
				if(BuildConfig.APP_IS_DEBUG) Log.e("Encryption Engine", "The provided RSA public key is NOT VALID." );
				throw e2;
			}
		} else {
			ECPoint key = ECPoint.fromString(key_content);
			if (key == null) {
				if(BuildConfig.APP_IS_DEBUG) Log.e("Encryption Engine", "The provided ECC public key is NOT VALID." );
				return;
			}
		    EncryptorECC.getInstance(key);
		}
	}
	
	/**Generates a new 128 bit AES Encryption key.
	 * @return a byte array 128 bits long for use as an AES Encryption key*/
	public static SecretKey newAESKey() {
		// setup seed and key generator
		SecureRandom random = new SecureRandom();
		KeyGenerator aesKeyGen;
		try { aesKeyGen = KeyGenerator.getInstance("AES"); }
		catch (NoSuchAlgorithmException e) { //seems unlikely
			if(BuildConfig.APP_IS_DEBUG) Log.e("Encryption Engine", "device does not know what AES is... instance 1" );
			TextFileManager.getDebugLogFile().write("EncryptionEngine::newAESKey: " + DebugInterfaceActivity.printStack(e));
			throw new NullPointerException("device does not know what AES is... instance 1"); }

		aesKeyGen.init( 128, random );
		//from key generator, generate a key!  yay...
		SecretKey secretKey = aesKeyGen.generateKey();
		return secretKey;
	}
	
	
	/* #######################################################################
	 * ########################## Data Wrapping ##############################  
	 * #####################################################################*/

	/* converts data into url-safe Base64 encoded blobs, as either a string or a byte array. */
	public static String toBase64String( byte[] data ) { return Base64.encodeToString(data, Base64.NO_WRAP | Base64.URL_SAFE ); }
//	private static String toBase64String( String data ) { return Base64.encodeToString(data.getBytes(), Base64.NO_WRAP | Base64.URL_SAFE ); }
	public static byte[] toBase64Array( byte[] data ) { return Base64.encode(data, Base64.NO_WRAP | Base64.URL_SAFE ); }
//	private static byte[] toBase64Array( String data ) { return Base64.encode(data.getBytes(), Base64.NO_WRAP | Base64.URL_SAFE ); }
}