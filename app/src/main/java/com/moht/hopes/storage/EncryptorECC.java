package com.moht.hopes.storage;

import com.moht.hopes.storage.models.AESData;
import com.moht.hopes.storage.models.ECPoint;
import com.moht.hopes.storage.models.EllipticCurve;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Arrays;

public class EncryptorECC {
    private static final String TAG = "EncryptorECC";
    private static EncryptorECC encryptorECC;

    private final EllipticCurve curve = EllipticCurve.NIST_P_256;
    private final ECPoint pubKey;


    private EncryptorECC(ECPoint pubKey) {
        this.pubKey = pubKey;
    }

    public static EncryptorECC getInstance() {
        return encryptorECC;
    }

    public static EncryptorECC getInstance(ECPoint pubKey) {
        if (encryptorECC == null) {
            encryptorECC = new EncryptorECC(pubKey);
        }
        return encryptorECC;
    }

    public static boolean hasInstance() {
        return encryptorECC != null;
    }

    public AESData generateAESData() throws Exception {
        BigInteger ciphertextPrivKey = randbelow(curve.getN());
        ECPoint AESPoint = curve.multiply(pubKey, ciphertextPrivKey);
        byte [] AESKey = ecc_point_to_256_bit_key(AESPoint);
        ECPoint encryptedAESKey = curve.multiply(curve.getG(), ciphertextPrivKey);
        return new AESData(encryptedAESKey, AESKey);
    }

    private BigInteger randbelow(BigInteger upperLimit) {
        BigInteger randomNumber;
        do {
            randomNumber = new BigInteger(upperLimit.bitLength(), new SecureRandom());
        } while (randomNumber.compareTo(upperLimit) >= 0);
        return randomNumber;
    }

    public static byte [] ecc_point_to_256_bit_key(ECPoint p) throws Exception {
        MessageDigest sha = MessageDigest.getInstance("SHA-256");
        sha.update(bigInteger2byteArray(p.x, 32));
        return sha.digest(bigInteger2byteArray(p.y, 32));
    }

    public static byte [] bigInteger2byteArray(BigInteger i, int length) {
        byte [] ret = new byte [length];
        byte [] I = i.toByteArray();
        // delete leading null bytes (for sign)
        while(I[0]==0 && I.length>0) I = Arrays.copyOfRange(I, 1, I.length);
        System.arraycopy(I, 0, ret, length-I.length, I.length);
        return ret;
    }
}