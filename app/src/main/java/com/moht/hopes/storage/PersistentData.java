package com.moht.hopes.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;
import android.widget.TextView;

import com.moht.hopes.BuildConfig;
import com.moht.hopes.R;
import com.moht.hopes.listeners.*;
import com.moht.hopes.ui.user.LicenceActivity;
import com.moht.hopes.utils.GS_JSON;
import com.moht.hopes.utils.KeyStoreUtil;
import com.moht.hopes.utils.Secrets;
import com.moht.hopes.utils.GS_JSON.*;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static java.nio.charset.StandardCharsets.UTF_8;

/**A class for managing patient login sessions.
 * Uses SharedPreferences in order to save username-password combinations.
 * @author Dor Samet, Eli Jones, Josh Zagorsky, Wang Xuancong */
public class PersistentData {
	private static final long MAX_LONG = 9223372036854775807L;

	private static boolean isInitialized = false;
	private static Context appContext;

	public static SharedPreferences pref;
	public static Editor editor;
	public static boolean isUnregisterDebugMode = false;
	public static boolean mergeAndCompress = false;

	/**  Editor key-strings */
	private static final String PREF_NAME = "HOPESPref";
	private static final String SERVER_URL_KEY = "serverUrl";
	private static final String KEY_USER_ID = "uid";
	public static final String KEY_STUDY_ID = "sid";
	private static final String KEY_PASSWORD = "password";
	private static final String CIPHER = "cipher";
	private static final String IS_REGISTERED = "IsRegistered";
	private static final String LOGIN_EXPIRATION = "loginExpirationTimestamp";
	public static final String PCP_PHONE_NUMBER = "primary_care";
	public static final String PASSWORD_RESET_NUMBER_KEY = "reset_number";
	public static final String PHONE_NUMBER_LENGTH = "phone_number_length";
	public static final String USE_GPS_FUZZING = "use_gps_fuzzing";
	public static final String USE_ANONYMIZED_HASHING = "use_anonymized_hashing";
	public static final String USE_COMPRESSION = "use_compression";
	public static final String GPS_LAST_UPDATE_TIME = "gps_last_update";
	public static final String GPS_RECORD_COUNT = "gps_record_count";
	public static final String FCM_token = "fcm_token";    // Firebase token

	public static final String ACCELEROMETER = AccelerometerListener.name;
	public static final String ACCESSIBILITY = AccessibilityListener.name;
	public static final String AMBIENTLIGHT = AmbientLightListener.name;
	public static final String GYROSCOPE = GyroscopeListener.name;
	public static final String MAGNETOMETER = MagnetoListener.name;
	public static final String STEPS = PedometerListener.name;
	public static final String USAGE = UsageListener.name;
	public static final String GPS = GPSListener.name;
	public static final String CALLS = CallListener.name;
	public static final String TEXTS = SmsSentLogger.name;
	public static final String TAPS = TapsListener.name;
	public static final String WIFI = WifiListener.name;
	public static final String BLUETOOTH = BluetoothListener.name;
	public static final String BLUETOOTH_STATE = BluetoothStateListener.name;
	public static final String SIGNIFICANT_MOTION = SignificantMotionListener.name;
	public static final String SOCIABILITY_CALL = SociabilityMsgLogger.name;
	public static final String SOCIABILITY_MSG = SociabilityCallLogger.name;
	public static final String POWER_STATE = PowerStateListener.name;
	public static final String ALLOW_UPLOAD_OVER_CELLULAR_DATA = "allow_upload_over_cellular_data";

	public static String [] feature_list = {
			ACCELEROMETER,
			ACCESSIBILITY,
			AMBIENTLIGHT,
			GYROSCOPE,
			MAGNETOMETER,
			USAGE,
			STEPS,
			GPS,
			CALLS,
			TEXTS,
			TAPS,
			WIFI,
			BLUETOOTH,
			BLUETOOTH_STATE,
			SIGNIFICANT_MOTION,
			SOCIABILITY_CALL,
			SOCIABILITY_MSG,
			POWER_STATE,
			ALLOW_UPLOAD_OVER_CELLULAR_DATA,
			USE_COMPRESSION
	};

	private static final String ACCELEROMETER_OFF_DURATION_SECONDS = "accelerometer_off_duration_seconds";
	private static final String ACCELEROMETER_ON_DURATION_SECONDS = "accelerometer_on_duration_seconds";
	private static final String AMBIENTLIGHT_INTERVAL_SECONDS = "ambientlight_interval_seconds";
	private static final String AMBIENTTEMPERATURE_INTERVAL_SECONDS = "ambienttemperature_interval_seconds";
	private static final String BLUETOOTH_ON_DURATION_SECONDS = "bluetooth_on_duration_seconds";
	private static final String BLUETOOTH_TOTAL_DURATION_SECONDS = "bluetooth_total_duration_seconds";
	private static final String BLUETOOTH_GLOBAL_OFFSET_SECONDS = "bluetooth_global_offset_seconds";
	private static final String CHECK_FOR_NEW_SURVEYS_FREQUENCY_SECONDS = "check_for_new_surveys_frequency_seconds";
	private static final String CREATE_NEW_DATA_FILES_FREQUENCY_SECONDS = "create_new_data_files_frequency_seconds";
	private static final String GPS_OFF_DURATION_SECONDS = "gps_off_duration_seconds";
	private static final String GPS_ON_DURATION_SECONDS = "gps_on_duration_seconds";
	private static final String GYRO_OFF_DURATION_SECONDS = "gyro_off_duration_seconds";
	private static final String GYRO_ON_DURATION_SECONDS = "gyro_on_duration_seconds";
	private static final String MAGNETOMETER_OFF_DURATION_SECONDS = "magnetometer_off_duration_seconds";
	private static final String MAGNETOMETER_ON_DURATION_SECONDS = "magnetometer_on_duration_seconds";
	private static final String STEPS_OFF_DURATION_SECONDS = "steps_off_duration_seconds";
	private static final String STEPS_ON_DURATION_SECONDS = "steps_on_duration_seconds";
	private static final String SECONDS_BEFORE_AUTO_LOGOUT = "seconds_before_auto_logout";
	private static final String USAGE_UPDATE_INTERVAL_SECONDS = "usage_update_interval_seconds";
	private static final String UPLOAD_DATA_FILES_FREQUENCY_SECONDS = "upload_data_files_frequency_seconds";
	private static final String VOICE_RECORDING_MAX_TIME_LENGTH_SECONDS = "voice_recording_max_time_length_seconds";
	private static final String WIFI_LOG_FREQUENCY_SECONDS = "wifi_log_frequency_seconds";

	static String [] time_param_list = {
			ACCELEROMETER_OFF_DURATION_SECONDS,
			ACCELEROMETER_ON_DURATION_SECONDS,
			AMBIENTLIGHT_INTERVAL_SECONDS,
			AMBIENTTEMPERATURE_INTERVAL_SECONDS,
			BLUETOOTH_ON_DURATION_SECONDS,
			BLUETOOTH_TOTAL_DURATION_SECONDS,
			BLUETOOTH_GLOBAL_OFFSET_SECONDS,
			CHECK_FOR_NEW_SURVEYS_FREQUENCY_SECONDS,
			CREATE_NEW_DATA_FILES_FREQUENCY_SECONDS,
			GPS_OFF_DURATION_SECONDS,
			GPS_ON_DURATION_SECONDS,
			GYRO_OFF_DURATION_SECONDS,
			GYRO_ON_DURATION_SECONDS,
			MAGNETOMETER_OFF_DURATION_SECONDS,
			MAGNETOMETER_ON_DURATION_SECONDS,
			STEPS_OFF_DURATION_SECONDS,
			STEPS_ON_DURATION_SECONDS,
			SECONDS_BEFORE_AUTO_LOGOUT,
			USAGE_UPDATE_INTERVAL_SECONDS,
			UPLOAD_DATA_FILES_FREQUENCY_SECONDS,
			VOICE_RECORDING_MAX_TIME_LENGTH_SECONDS,
			WIFI_LOG_FREQUENCY_SECONDS,
	};

	private static final String SURVEY_IDS = "survey_ids";

	/*#####################################################################################
	################################### Initializing ######################################
	#####################################################################################*/

	/**The publicly accessible initializing function for the LoginManager,
	 * initializes the internal variables.
	 * @param context app context*/
	public static void initialize( Context context ) {
		if ( isInitialized ) { return; }
		appContext = context;
		int PRIVATE_MODE = 0;
		//sets Shared Preferences private mode
		pref = appContext.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
		editor = pref.edit();
		editor.apply();
		isInitialized = true;
		mergeAndCompress = getEnabled(USE_COMPRESSION);
	}

	public static void resetAPP(boolean deleteAll){
		if(deleteAll) TextFileManager.deleteEverything();
		appContext.deleteSharedPreferences(PREF_NAME);
		isInitialized = false;
	}

	/*#####################################################################################
	##################################### User State ######################################
	#####################################################################################*/

	/** Quick check for login. **/
	public static boolean isLoggedIn(){
		if ( pref == null ) return false;
		if ( getLong( SECONDS_BEFORE_AUTO_LOGOUT, 0 ) == 0 ) return true;
		// If the current time is earlier than the expiration time, return TRUE; else FALSE
		return (System.currentTimeMillis() < pref.getLong(LOGIN_EXPIRATION, 0)); }

	/** Set the login session to expire a fixed amount of time in the future */
	public static void loginOrRefreshLogin() {
		editor.putLong(LOGIN_EXPIRATION, System.currentTimeMillis() + getMillisecondsBeforeAutoLogout());
		editor.commit(); }

	/** Set the login session to "expired" */
	public static void logout() {
		editor.putLong(LOGIN_EXPIRATION, 0);
		editor.commit(); }

	/**Getter for the IS_REGISTERED value. */
	public static boolean isRegistered() {
		if (pref == null) return false;
		return pref.getBoolean(IS_REGISTERED, false); }

	/**Setter for the IS_REGISTERED value.
	 * @param value value*/
	public static void setRegistered(boolean value) { 
		editor.putBoolean(IS_REGISTERED, value);
		editor.commit(); }

	/*######################################################################################
	##################################### Passwords ########################################
	######################################################################################*/

	/**Checks that an input matches valid password requirements. (this only checks length)
	 * Throws up an alert notifying the user if the password is not valid.
	 * @param password password
	 * @return true or false based on password requirements.*/
	public static boolean passwordMeetsRequirements(String password) {
		return true;
	}

 	/**Takes an input string and returns a boolean value stating
	 * whether the input matches the current password.
	 * @param input input password
	 * @return boolean */
	public static boolean checkPassword(String input){
		return getPassword().equals(KeyStoreUtil.decryptString(input, PersistentData.getPatientID()));
	}

	/**Sets a password to a hash of the provided value.
	 * @param password password */
	public static void setPassword(String password) {
		String encryptedPassword = KeyStoreUtil.encryptString(password, PersistentData.getPatientID());
		editor.putString(KEY_PASSWORD, encryptedPassword);
		editor.commit();
	}

	public static boolean getEnabled(String feature){ return pref.getBoolean(feature, false); }
	static void setEnabled(String feature, boolean enabled){
		if( !isUnregisterDebugMode ) {
			editor.putBoolean(feature, enabled);
			editor.commit();
		}
	}

	public static boolean getBoolean(String feature){ return pref.getBoolean(feature, false); }
//	public static void setBoolean(String feature, boolean enabled){
//		editor.putBoolean(feature, enabled);
//		editor.commit();
//	}

	/*#####################################################################################
	################################## Timer Settings #####################################
	#####################################################################################*/

	// Default timings (only used if hopes doesn't download custom timings)
	private static final long DEFAULT_ACCELEROMETER_OFF_DURATION = 10 * 60;
	private static final long DEFAULT_ACCELEROMETER_ON_DURATION = 10 * 60;
	private static final long DEFAULT_AMBIENTLIGHT_INTERVAL = 60;
	private static final long DEFAULT_BLUETOOTH_ON_DURATION = 60;
	private static final long DEFAULT_BLUETOOTH_TOTAL_DURATION = 5 * 60;
	private static final long DEFAULT_BLUETOOTH_GLOBAL_OFFSET = 0;
	private static final long DEFAULT_CHECK_FOR_NEW_SURVEYS_PERIOD = 0;
	private static final long DEFAULT_CREATE_NEW_DATA_FILES_PERIOD = 15 * 60;
	private static final long DEFAULT_GPS_OFF_DURATION = 5 * 60;
	private static final long DEFAULT_GPS_ON_DURATION = 5 * 60;
	private static final long DEFAULT_GYRO_OFF_DURATION = 10 * 60;
	private static final long DEFAULT_GYRO_ON_DURATION = 10 * 60;
	private static final long DEFAULT_MAGNETOMETER_OFF_DURATION = 10 * 60;
	private static final long DEFAULT_MAGNETOMETER_ON_DURATION = 10 * 60;
	private static final long DEFAULT_USAGE_UPDATE_INTERVAL = 60 * 60;
	private static final long DEFAULT_STEPS_OFF_DURATION = 0;
	private static final long DEFAULT_STEPS_ON_DURATION = 30 * 60;
	private static final long DEFAULT_SECONDS_BEFORE_AUTO_LOGOUT = 5 * 60;
	private static final long DEFAULT_UPLOAD_DATA_FILES_PERIOD = 60;
	private static final long DEFAULT_VOICE_RECORDING_MAX_TIME_LENGTH = 4 * 60;
	private static final long DEFAULT_WIFI_LOG_FREQUENCY = 5 * 60;
	
	public static long getAccelerometerOffDurationMilliseconds() { return 1000L *
			pref.getLong(ACCELEROMETER_OFF_DURATION_SECONDS,
				DEFAULT_ACCELEROMETER_OFF_DURATION); }
	public static long getAccelerometerOnDurationMilliseconds() { return 1000L *
			pref.getLong(ACCELEROMETER_ON_DURATION_SECONDS, DEFAULT_ACCELEROMETER_ON_DURATION); }
	public static long getAmbientLightIntervalMilliseconds() { return 1000L *
			pref.getLong(AMBIENTLIGHT_INTERVAL_SECONDS, DEFAULT_AMBIENTLIGHT_INTERVAL); }
	public static long getBluetoothOnDurationMilliseconds() { return 1000L *
			pref.getLong(BLUETOOTH_ON_DURATION_SECONDS, DEFAULT_BLUETOOTH_ON_DURATION); }
	public static long getBluetoothTotalDurationMilliseconds() { return 1000L *
			pref.getLong(BLUETOOTH_TOTAL_DURATION_SECONDS, DEFAULT_BLUETOOTH_TOTAL_DURATION); }
	public static long getBluetoothGlobalOffsetMilliseconds() { return 1000L *
			pref.getLong(BLUETOOTH_GLOBAL_OFFSET_SECONDS, DEFAULT_BLUETOOTH_GLOBAL_OFFSET); }
	public static long getCheckForNewSurveysFrequencyMilliseconds() { return 1000L *
			pref.getLong(CHECK_FOR_NEW_SURVEYS_FREQUENCY_SECONDS,
					DEFAULT_CHECK_FOR_NEW_SURVEYS_PERIOD); }
	public static long getCreateNewDataFilesFrequencyMilliseconds() { return 1000L *
			pref.getLong(CREATE_NEW_DATA_FILES_FREQUENCY_SECONDS,
					DEFAULT_CREATE_NEW_DATA_FILES_PERIOD); }
	public static long getGpsOffDurationMilliseconds() { return 1000L *
			pref.getLong(GPS_OFF_DURATION_SECONDS, DEFAULT_GPS_OFF_DURATION); }
	public static long getGpsOnDurationMilliseconds() { return 1000L *
			pref.getLong(GPS_ON_DURATION_SECONDS, DEFAULT_GPS_ON_DURATION); }
	public static long getGyroOffDurationMilliseconds() { return 1000L *
			pref.getLong(GYRO_OFF_DURATION_SECONDS, DEFAULT_GYRO_OFF_DURATION); }
	public static long getGyroOnDurationMilliseconds() { return 1000L *
			pref.getLong(GYRO_ON_DURATION_SECONDS, DEFAULT_GYRO_ON_DURATION); }
	public static long getMagnetometerOffDurationMilliseconds() { return 1000L *
			pref.getLong(MAGNETOMETER_OFF_DURATION_SECONDS, DEFAULT_MAGNETOMETER_OFF_DURATION); }
	public static long getMagnetometerOnDurationMilliseconds() { return 1000L *
			pref.getLong(MAGNETOMETER_ON_DURATION_SECONDS, DEFAULT_MAGNETOMETER_ON_DURATION); }
	public static long getUsageUpdateIntervalMilliseconds() { return 1000L *
			pref.getLong(USAGE_UPDATE_INTERVAL_SECONDS, DEFAULT_USAGE_UPDATE_INTERVAL); }
	public static long getStepsOffDurationMilliseconds() { return 1000L *
			pref.getLong(STEPS_OFF_DURATION_SECONDS, DEFAULT_STEPS_OFF_DURATION); }
	public static long getStepsOnDurationMilliseconds() { return 1000L *
			pref.getLong(STEPS_ON_DURATION_SECONDS, DEFAULT_STEPS_ON_DURATION); }
	public static long getMillisecondsBeforeAutoLogout() { return 1000L *
			pref.getLong(SECONDS_BEFORE_AUTO_LOGOUT, DEFAULT_SECONDS_BEFORE_AUTO_LOGOUT); }
	public static long getUploadDataFilesFrequencyMilliseconds() { return 1000L *
			pref.getLong(UPLOAD_DATA_FILES_FREQUENCY_SECONDS, DEFAULT_UPLOAD_DATA_FILES_PERIOD); }
	public static long getVoiceRecordingMaxTimeLengthMilliseconds() { return 1000L *
			pref.getLong(VOICE_RECORDING_MAX_TIME_LENGTH_SECONDS,
					DEFAULT_VOICE_RECORDING_MAX_TIME_LENGTH); }
	public static long getWifiLogFrequencyMilliseconds() { return 1000L *
			pref.getLong(WIFI_LOG_FREQUENCY_SECONDS, DEFAULT_WIFI_LOG_FREQUENCY); }

	public static String getString(String feature){ return pref.getString(feature,""); }
	public static String getString(String feature, String defaultValue){ return pref.getString(feature,defaultValue); }
	public static void setString(String feature, String value){
		editor.putString(feature, value);
		editor.commit();
	}

	public static float getFloat(String feature, float default_v){ return pref.getFloat(feature,default_v); }
	public static void setFloat(String feature, float value){
		editor.putFloat(feature, value);
		editor.commit();
	}

	public static int getInteger(String feature){ return pref.getInt( feature, 0 ); }
	public static int getInteger(String feature, int default_v){ return pref.getInt( feature, default_v ); }
	public static void setInteger(String feature, int value){
		editor.putInt(feature, value);
		editor.commit();
	}

	public static long getLong(String feature, long default_v){ return pref.getLong( feature, default_v ); }
	public static void setLong(String feature, long value){
		editor.putLong(feature, value);
		editor.commit();
	}

	//accelerometer, bluetooth, new surveys, create data files, gps, logout,upload,
	// wifi log (not voice recording, that doesn't apply
	public static void setMostRecentAlarmTime(String identifier, long time) {
		editor.putLong(identifier + "-prior_alarm", time);
		editor.commit(); }
	public static long getMostRecentAlarmTime(String identifier) {
		return pref.getLong( identifier + "-prior_alarm", 0); }
	//we want default to be 0 so that checks "is this value less than the current expected value" (
	// eg "did this timer event pass already")

	/*###########################################################################################
	################################### Text Strings ############################################
	###########################################################################################*/

	public static final String ABOUT_PAGE_TEXT_KEY = "about_page_text";
	public static final String MAINPAGE_TITLE_KEY = "mainpage_title";
	public static final String CONSENT_FORM_TEXT_KEY = "consent_form_text";
	public static final String SURVEY_SUBMIT_SUCCESS_TOAST_TEXT_KEY = "survey_submit_success_toast_text";
	public static final String LASY_UPLOAD_TIME_KEY = "last_upload_time";
	private static final String MAIN_UPLOAD_INFO_TEXT_KEY = "main_upload_info_text";
	private static final String FAIL_UPLOAD_INFO_TEXT_KEY = "fail_upload_info_text";

	public static String getAboutPageText() {
		String defaultText = appContext.getString(R.string.default_about_page_text);
		return pref.getString(ABOUT_PAGE_TEXT_KEY, defaultText); }
	public static String getCallClinicianButtonText() {
		String defaultText = appContext.getString(R.string.default_mainpage_title);
		return pref.getString(MAINPAGE_TITLE_KEY, defaultText); }
	public static String getConsentFormText() {
		String defaultText = appContext.getString(R.string.default_consent_form_text);
		return pref.getString(CONSENT_FORM_TEXT_KEY, defaultText); }
	public static String getSurveySubmitSuccessToastText() {
		String defaultText = appContext.getString(R.string.default_survey_submit_success_message);
		return pref.getString(SURVEY_SUBMIT_SUCCESS_TOAST_TEXT_KEY, defaultText); }

	public static String getMainUploadInfo() {
		String textToSet = PersistentData.isRegistered() ? "Registration successful" : "Phone not registered";
		return pref.getString(MAIN_UPLOAD_INFO_TEXT_KEY, textToSet);
	}

	public static void setMainUploadInfo(String text) {
		editor.putString(MAIN_UPLOAD_INFO_TEXT_KEY, text);
		editor.commit();
		String textToSet = text + "\n" + getFailUploadInfo();
		try {
			LicenceActivity.mSelf.runOnUiThread(() -> (
					(TextView) LicenceActivity.mSelf.findViewById(R.id.last_upload_info))
					.setText(textToSet));
		} catch (Exception ignored){}
	}

	public static String getFailUploadInfo() {
		String textToSet = PersistentData.isRegistered() ? "" : "Phone not registered";
		return pref.getString(FAIL_UPLOAD_INFO_TEXT_KEY, textToSet);
	}

	public static void setFailUploadInfo(String text) {
		editor.putString(FAIL_UPLOAD_INFO_TEXT_KEY, text);
		editor.commit();
		String textToSet = getMainUploadInfo() + "\n" + text;
		try {
			LicenceActivity.mSelf.runOnUiThread(() -> (
					(TextView) LicenceActivity.mSelf.findViewById(R.id.last_upload_info))
					.setText(textToSet));
		} catch (Exception ignored){}
	}

	/*###########################################################################################
	################################### User Credentials ########################################
	###########################################################################################*/

	public static void setServerUrl(String serverUrl) {
		if (!serverUrl.startsWith("http"))
			serverUrl = "https://" + serverUrl;
		editor.putString(SERVER_URL_KEY, serverUrl);
		editor.commit();
	}
	public static String getServerUrl() { return pref.getString(SERVER_URL_KEY, null); }

	public static void setLoginCredentials( String userID, String password ) {
		if (editor == null){
			if(BuildConfig.APP_IS_DEBUG) Log.e("LoginManager.java", "editor is null in setLoginCredentials()");
			return;
		}
		editor.putString(KEY_USER_ID, userID);
		editor.commit();
		setPassword(password);
		editor.commit();
	}

	public static String getPassword() {
		String encryptedPassword =  pref.getString( KEY_PASSWORD, null );
		return KeyStoreUtil.decryptString(encryptedPassword, PersistentData.getPatientID());
	}

	public static byte[] getCipherIV() {
		byte[] array = new byte[12];
		String iv = pref.getString(CIPHER, null);
		if (iv != null) {
			String[] split = iv.substring(1, iv.length()-1).split(", ");
			array = new byte[split.length];
			for (int i = 0; i < split.length; i++) {
				array[i] = Byte.parseByte(split[i]);
			}
		}
		return array;
	}

	public static String getPatientID() { return pref.getString(KEY_USER_ID, "NULL_ID"); }
	public static String getStudyID() { return pref.getString(KEY_STUDY_ID, "NULL_ID"); }

	/*###########################################################################################
	###################################### Survey Info ##########################################
	###########################################################################################*/
	
	public static JsonArray getSurveyIds() { return getSurveyIdsJsonArray(); }
	public static JsonArray getSurveyQuestionMemory(String surveyId) {
		return getSurveyQuestionMemoryJsonArray(surveyId); }
	public static String getSurveyTimes(String surveyId){
		return pref.getString(surveyId + "-times", "[[],[],[],[],[],[],[]]"); }
	public static String getSurveyExpiry(String surveyId){
		return pref.getString(surveyId + "-expiry", ""); }
	public static String getSurveyContent(String surveyId){
		return new String(TextFileManager.loadFile(surveyId + "-content"), UTF_8);
//		return pref.getString(surveyId + "-content", null);
	}
	public static String getSurveyType(String surveyId){
		return pref.getString(surveyId + "-type", null); }
	public static String getSurveySettings(String surveyId){
		return pref.getString(surveyId + "-settings", null); }
	public static Boolean getSurveyNotificationState( String surveyId) {
		return pref.getBoolean(surveyId + "-notificationState", false ); }
	public static long getMostRecentSurveyAlarmTime(String surveyId) {
		return pref.getLong( surveyId + "-prior_alarm", MAX_LONG); }
	
	public static void createSurveyData(String surveyId, String content, String checksum,
										String timings, String expiry, String type, String settings){
		setSurveyContent(surveyId,  content);
		setSurveyChecksum(surveyId, checksum);
		setSurveyTimes(surveyId, timings);
		setSurveyExpiry(surveyId, expiry);
		setSurveyType(surveyId, type);
		setSurveySettings(surveyId, settings);
	}

	//individual setters
	public static void setSurveyContent(String surveyId, String content){
		TextFileManager.saveFile(surveyId + "-content", content);
//		editor.putString(surveyId + "-content", content);
//		editor.commit();
	}

	public static void setSurveyChecksum(String surveyId, String checksum){
		editor.putString(surveyId + "-checksum", checksum);
		editor.commit();
	}

	public static void setSurveyTimes(String surveyId, String times){
		editor.putString(surveyId + "-times", times);
		editor.commit();
	}

	public static void setSurveyExpiry(String surveyId, String expiry){
		if(expiry.isEmpty()) return;
		editor.putString(surveyId + "-expiry", expiry);
		editor.commit();
	}

	public static void setSurveyType(String surveyId, String type){
		editor.putString(surveyId + "-type", type);
		editor.commit();
	}

	public static void setSurveySettings(String surveyId, String settings){
		editor.putString(surveyId + "-settings", settings);
		editor.commit();
	}
	
	//survey state storage
	public static void setSurveyNotificationState(String surveyId, Boolean bool ) {
		editor.putBoolean(surveyId + "-notificationState", bool );
		editor.commit();
	}

	public static void setMostRecentSurveyAlarmTime(String surveyId, long time) {
		editor.putLong(surveyId + "-prior_alarm", time);
		editor.commit();
	}
	
	public static void deleteSurvey(String surveyId) {
//		editor.remove(surveyId + "-content");
		TextFileManager.delete(surveyId+"-content");
		editor.remove(surveyId + "-checksum");
		editor.remove(surveyId + "-times");
		editor.remove(surveyId + "-expiry");
		editor.remove(surveyId + "-type");
		editor.remove(surveyId + "-settings");
		editor.remove(surveyId + "-notificationState");
		editor.remove(surveyId + "-prior_alarm");
		editor.remove(surveyId + "-questionIds");
		editor.commit();
		removeSurveyId(surveyId);
	}

	public static String getSurveyChecksum(String surveyId) {
		return pref.getString(surveyId + "-checksum", "");
	}

	//array style storage and removal for surveyIds and questionIds	
	private static JsonArray getSurveyIdsJsonArray() {
		// the survey JSON data can contain video/audio/image, its size can be large, cannot store using SharedPreferences
		String surveyData = pref.getString(SURVEY_IDS,null);
		if (surveyData==null || surveyData.length()==0) {
			return new JsonArray(); } //return empty if the list is empty
		try { return (JsonArray) GS_JSON.parse(surveyData); }
		catch (Exception e) {
			TextFileManager.getDebugLogFile().write("PersistentData::getSurveyIdsJsonArray: " + Arrays.toString(e.getStackTrace()));
			return new JsonArray();
		}
	}

	public static Map <String, String> getSurveyChecksums() {
		List<String> list = getSurveyIdsJsonArray();
		HashMap checksums = new HashMap <String, String> ();
		for (String id: list)
			checksums.put(id, getSurveyChecksum(id));
		return (Map<String, String>) checksums;
	}
		
	public static void addSurveyId(String surveyId) {
		JsonArray list = getSurveyIdsJsonArray();
		if ( !list.contains(surveyId) ) {
			list.add(surveyId);
			editor.putString(SURVEY_IDS, list.toJSON() );
			editor.commit();
		} else {
			TextFileManager.getDebugLogFile().write("PersistentData::addSurveyId: duplicate survey id added " + surveyId);
		} //we ensure uniqueness in the downloader, this should be unreachable.
	}
	
	private static void removeSurveyId(String surveyId) {
		JsonArray list = getSurveyIdsJsonArray();
		if ( list.contains(surveyId) ) {
			list.remove(surveyId);
			editor.putString(SURVEY_IDS, list.toJSON() );
			editor.commit();
		}
	}
	
	private static JsonArray getSurveyQuestionMemoryJsonArray( String surveyId ) {
		String jsonString = pref.getString(surveyId + "-questionIds", "0");
		if (Objects.equals(jsonString, "0")) {
			return new JsonArray(); } //return empty if the list is empty
		try { return (JsonArray)GS_JSON.parse(jsonString); }
		catch (Exception e) {
			TextFileManager.getDebugLogFile().write(
					"PersistentData::getSurveyQuestionMemoryJsonArray: " +
							"getSurveyIds failed, json string was: " + jsonString );
			throw new NullPointerException("getSurveyIds failed, json string was: " + jsonString );
		}
	}
		
	public static void addSurveyQuestionMemory(String surveyId, String questionId) {
		JsonArray list = getSurveyQuestionMemory(surveyId);
		if ( !list.contains(questionId) ) {
			list.add(questionId);
			editor.putString(surveyId + "-questionIds", list.toJSON() );
			editor.commit();
		} else {
			TextFileManager.getDebugLogFile().write(
					"PersistentData::addSurveyQuestionMemory: " +
							"duplicate question id added: " + questionId);
			throw new NullPointerException("duplicate question id added: " + questionId);
		} //we ensure uniqueness in the downloader, this should be unreachable.
	}
	
	public static void clearSurveyQuestionMemory(String surveyId) {
		editor.putString(surveyId + "-questionIds", new JsonArray().toString() );
		editor.commit();
	}

	public static void setReportContent(String content){
		TextFileManager.saveFile(TextFileManager.reportFileName, content);
		editor.putLong(TextFileManager.reportFileName, System.currentTimeMillis());
		editor.commit();
	}

	public static String getReportContent(){
		byte [] data = TextFileManager.loadFile(TextFileManager.reportFileName);
		return data==null ? "" : new String(data, UTF_8);
	}

	public static void deleteReportContent() {
		TextFileManager.delete(TextFileManager.reportFileName);
		editor.remove(TextFileManager.reportFileName);
		editor.commit();
	}

	/*###########################################################################################
	###################################### Encryption ###########################################
	###########################################################################################*/

	private static final String HASH_SALT_KEY = "hash_salt_key";
	private static final String HASH_ITERATIONS_KEY = "hash_iterations_key";
	private static final String USE_ANONYMIZED_HASHING_KEY = "use_anonymized_hashing";

	// Get salt for pbkdf2 hashing
	public static byte[] getHashSalt() {
		String saltString = pref.getString(HASH_SALT_KEY, null);
		if(saltString == null) { // create salt if it does not exist
			byte[] newSalt = SecureRandom.getSeed(64);
			editor.putString(HASH_SALT_KEY, new String(newSalt));
			editor.commit();
			return newSalt;
		}
		else {
			return saltString.getBytes();
		}
	}

	// Set re-registration values from qr code secrets
	public static void setReregistrationData(Secrets secrets) {
		editor.putString(HASH_SALT_KEY, new String(
				Secrets.Base64Decode(secrets.hashKey)));
		editor.putInt(HASH_ITERATIONS_KEY, Integer.parseInt(secrets.hashIteration));
		editor.putFloat(LATITUDE_OFFSET_KEY, Float.parseFloat(secrets.latitudeOffset));
		editor.putFloat(LONGITUDE_OFFSET_KEY, Float.parseFloat(secrets.longitudeOffset));
		editor.commit();
	}

	// Get iterations for pbkdf2 hashing
	public static int getHashIterations() {
		int iterations = pref.getInt(HASH_ITERATIONS_KEY, 0);
		if(iterations == 0) { // create iterations if it does not exist
			// create random iteration count from 900,000 to 1,100,000
			int newIterations = 10 + new SecureRandom().nextInt(90);
			editor.putInt(HASH_ITERATIONS_KEY, newIterations);
			editor.commit();
			return newIterations;
		}
		else {
			return iterations;
		}
	}

//	public static void setUseAnonymizedHashing(boolean useAnonymizedHashing) {
//		editor.putBoolean(USE_ANONYMIZED_HASHING_KEY, useAnonymizedHashing);
//		editor.commit();
//	}

	static boolean getUseAnonymizedHashing() {
		return pref.getBoolean(USE_ANONYMIZED_HASHING_KEY, true);
		//If not present, default to safe hashing
	}


	/*###########################################################################################
	###################################### FUZZY GPS ############################################
	###########################################################################################*/

	public static final String LATITUDE_OFFSET_KEY = "latitude_offset_key";
	public static final String LONGITUDE_OFFSET_KEY = "longitude_offset_key";
	public static final String ROTATION_OFFSET_KEY = "rotation_offset_key";

	public static double getLatitudeOffset() {
		float latitudeOffset = pref.getFloat(LATITUDE_OFFSET_KEY, 0.0f);
		if(latitudeOffset == 0.0f && getBoolean(USE_GPS_FUZZING)) {
			//create latitude offset if it does not exist
			float newLatitudeOffset = (float)(Math.random()*1000.0-500.0);
			// create random latitude offset between (-1, -.2) or (.2, 1)
			editor.putFloat(LATITUDE_OFFSET_KEY, newLatitudeOffset);
			editor.commit();
			return newLatitudeOffset;
		}
		else {
			return latitudeOffset;
		}
	}

	public static float getLongitudeOffset() {
		float longitudeOffset = pref.getFloat(LONGITUDE_OFFSET_KEY, 0.0f);
		if(longitudeOffset == 0.0f && getBoolean(USE_GPS_FUZZING)) {
			//create longitude offset if it does not exist
			float newLongitudeOffset = (float)(Math.random()*1000.0-500.0);
			// create random longitude offset between (-180, -10) or (10, 180)
			editor.putFloat(LONGITUDE_OFFSET_KEY, newLongitudeOffset);
			editor.commit();
			return newLongitudeOffset;
		}
		else {
			return longitudeOffset;
		}
	}

	public static int getGpsRecordCount() { return pref.getInt(GPS_RECORD_COUNT, 0); }
	public static void setGpsRecordCount(int value) {
		pref.edit().putInt(GPS_RECORD_COUNT, value).apply();
	}

	public static long getLastGpsUpdateTime() {
		return pref.getLong(GPS_LAST_UPDATE_TIME, 0L);
	}
	public static void setLastGpsUpdateTime(long value) {
		pref.edit().putLong(GPS_LAST_UPDATE_TIME, value).apply();}

	public static void writeDeviceSettings(JsonObject deviceSettings) throws Exception {
		// Write data stream booleans
		for(String feature : PersistentData.feature_list)
			PersistentData.setEnabled(feature, (boolean)deviceSettings.getOrDefault(feature,false));

		// Write timer settings
		for(String feature : PersistentData.time_param_list)
			PersistentData.setLong(feature, (long)deviceSettings.getOrDefault(feature,0L));

		// Other settings
		setString(ABOUT_PAGE_TEXT_KEY, (String)deviceSettings.get(ABOUT_PAGE_TEXT_KEY));
		setString(MAINPAGE_TITLE_KEY, (String)deviceSettings.get(MAINPAGE_TITLE_KEY));
		setString(CONSENT_FORM_TEXT_KEY, (String)deviceSettings.get(CONSENT_FORM_TEXT_KEY));
		setString(SURVEY_SUBMIT_SUCCESS_TOAST_TEXT_KEY, (String)deviceSettings.get(SURVEY_SUBMIT_SUCCESS_TOAST_TEXT_KEY));
		setString(PCP_PHONE_NUMBER, (String)deviceSettings.getOrDefault(PCP_PHONE_NUMBER, ""));
		setInteger(PHONE_NUMBER_LENGTH, (int)((long)deviceSettings.getOrDefault(PHONE_NUMBER_LENGTH, 8L)));
		setEnabled(USE_GPS_FUZZING, (boolean)deviceSettings.getOrDefault(USE_GPS_FUZZING,false ));
		setEnabled(USE_ANONYMIZED_HASHING, (boolean) deviceSettings.getOrDefault(USE_ANONYMIZED_HASHING,true));

		// Redundant variables for performance
		mergeAndCompress = getEnabled(USE_COMPRESSION);
	}
}
