package com.moht.hopes.storage;

import android.content.Context;
import android.util.Log;

import com.moht.hopes.BuildConfig;
import com.moht.hopes.CrashHandler;
import com.moht.hopes.listeners.AccelerometerListener;
import com.moht.hopes.listeners.AccessibilityListener;
import com.moht.hopes.listeners.AmbientLightListener;
import com.moht.hopes.listeners.BluetoothListener;
import com.moht.hopes.listeners.BluetoothStateListener;
import com.moht.hopes.listeners.CallListener;
import com.moht.hopes.listeners.GPSListener;
import com.moht.hopes.listeners.GyroscopeListener;
import com.moht.hopes.listeners.MagnetoListener;
import com.moht.hopes.listeners.PowerStateListener;
import com.moht.hopes.listeners.SignificantMotionListener;
import com.moht.hopes.listeners.SmsSentLogger;
import com.moht.hopes.listeners.SociabilityCallLogger;
import com.moht.hopes.listeners.SociabilityMsgLogger;
import com.moht.hopes.listeners.PedometerListener;
import com.moht.hopes.listeners.TapsListener;
import com.moht.hopes.listeners.UsageListener;
import com.moht.hopes.listeners.WifiListener;
import com.moht.hopes.storage.models.AESData;
import com.moht.hopes.survey.AdBlock;
import com.moht.hopes.survey.SurveyRecorder;
import com.moht.hopes.ui.DebugInterfaceActivity;
import com.moht.hopes.utils.Debouncer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

import javax.crypto.Cipher;
import javax.crypto.CipherOutputStream;
import javax.crypto.SecretKey;

/**The (Text)FileManager.
 * The FileManager is implemented as a Singleton.  More accurately the static object contains several
 * singletons, static instances of FileManager Objects.  Before using the FileManager the hopes must
 * provide it with a Context and call the Start() function.  Failure to do so causes the hopes to crash.
 * This Is Intentional.  The Point of the hopes is to record data.
 * The Reason for this construction is to construct a file write system where there is only ever a
 * single pointer to each file type, and that these files are never overwritten, written to asynchronously,
 * or left accidentally empty.
 * The files handled here are the GPSFile, accelFile, powerStateLog, audioSurveyInfo, callLog, textsLog, surveyTimings,
 * currentDailyQuestions, currentWeeklyQuestions, deviceData, and debugLogFile.
 * On construction you provide a boolean flag ("persistent").  Persistent files do not get overwritten on application start.
 * To access a file use the following construction: TextFileManager.getXXXFile()
 * @author Eli */

public class TextFileManager {

	//Delimiter and newline strings
	public static final String DELIMITER = ",";
	private Debouncer debouncer = new Debouncer();

	//"global" static variables
	private static Context appContext;
	private static TextFileManager dummyFile = new TextFileManager("dummy", true, false, false, false,true);
	private static TextFileManager keyFile = null;
	private static String keyFileName = "keyFile";
	public static String surveyFileName = "surveys.json.gz";
	public static String reportFileName = "completion-report.html";

	private static Map<Class, TextFileManager> fileMap = new HashMap<Class, TextFileManager>() {{
		put(AccelerometerListener.class, null);
		put(AccessibilityListener.class, null);
		put(AmbientLightListener.class, null);
		put(GyroscopeListener.class, null);
		put(MagnetoListener.class, null);
		put(PedometerListener.class, null);
		put(UsageListener.class, null);
		put(GPSListener.class, null);
		put(PowerStateListener.class, null);
		put(BluetoothStateListener.class, null);
		put(CallListener.class, null);
		put(SmsSentLogger.class, null);
		put(TapsListener.class, null);
		put(BluetoothListener.class, null);
		put(WifiListener.class, null);
		put(SurveyRecorder.class, null);
		put(SignificantMotionListener.class, null);
		put(SociabilityMsgLogger.class, null);
		put(SociabilityCallLogger.class, null);
		put(DebugInterfaceActivity.class, null);
	}};

	//and (finally) the non-static object instance variables
	public String name;
	private String fileName = null;
	private String dataBuffer;
	private String header;
	private Method hasPermission;
	private Boolean persistent;
	private Boolean encrypted;
	private Boolean compressed;
	private Boolean isDummy;
	private SecretKey AESKey = null;

	/*###############################################################################
	########################### Class Initialization ################################
	###############################################################################*/

	/**Starts the TextFileManager
	 * This must be called before code attempts to access files using getXXXFile().
	 * Initializes all TextFileManager object instances.  Initialization is idempotent.
	 * @param appContext a Context, provided by the hopes. */
	public static synchronized void initialize(Context appContext, boolean isFirst){
		TextFileManager.appContext = appContext;

		//the key file for encryption (it is persistent and never written to)
		keyFile = new TextFileManager(keyFileName, true, true, false, false,false);

		// create file for each feature
		for(Class cls : fileMap.keySet()) try {
			if (!PersistentData.isRegistered()){
				fileMap.put(cls, dummyFile);
				continue;
			}
			boolean enabled = PersistentData.getEnabled((String)cls.getField("name").get(null))
					|| cls==DebugInterfaceActivity.class || cls==SurveyRecorder.class;
			TextFileManager file = new TextFileManager(cls, false, false,true, PersistentData.mergeAndCompress, !enabled);
			fileMap.put(cls, file);
			if(PersistentData.mergeAndCompress && isFirst){
				String header = (String)cls.getField("header").get(null);
				if(header!=null && header.length()>0)
					file.write(header);
			}
		} catch (Exception e) {}
	}

	/*###############################################################################
	################## Instance Construction and Initialization #####################
	###############################################################################*/

	/** This class has a PRIVATE constructor.  The constructor is only ever called
	 * internally, via the static initialize() function, it creatse the "FileHandlers" used throughout the codebase.
	 * @param cls The class that contains a static name and short_name.
	 * @param persistent Set this to true for a persistent file.  Persistent files are not currently encryptable.
	 * @param openOnCreate This boolean value dictates whether the file should be opened, mostly this is used in conjunction persistent files so that they can be read from.
	 * @param encrypted Set this to True if the file will have encrypted writes. */
	private TextFileManager(Object cls, Boolean persistent, Boolean openOnCreate, Boolean encrypted, Boolean compressed, Boolean isDummy ){
		if ( persistent && encrypted ) { throw new NullPointerException("Persistent files do not support encryption."); }
		if (cls instanceof Class) {
			Class cls1 = (Class)cls;
			try {
				this.name = (String) cls1.getField("short_name").get(null);
				this.header = (String) cls1.getField("header").get(null);
				this.hasPermission = cls1.getMethod("checkPermission");
			} catch (Exception e) {}
		}else{
			this.name = (String) cls;
			this.header = "";
			this.hasPermission = null;
		}
		this.persistent = persistent;
		this.encrypted = encrypted;
		this.isDummy = isDummy;
		this.compressed = compressed;
		this.write = compressed?(this::writeCompressed):(this::writeNoncompressed);
		if (openOnCreate) { this.newFile(); } //immediately creating a file on instantiation was a common code pattern.
	}

	/** When PersistentData.mergeAndCompress==true, all non-persistent files share the same GZIPOutputStream
	 * If the stream is open and writable, sharedGzipStream!=null */
	static OutputStream sharedGzipStream = null;
	static String sharedGzipFilename = null;

	/** Makes a new file.
	 * Persistent files do not get a time stamp.
	 * Encrypted files get a key and have the key encrypted using RSA and written as the first line of the file.
	 * If a file has a header it is written as the second line.
	 * Fails when files are not allowed to be written to. (the rule is no encrypted writes until registraction is complete.
	 * @return A boolean value of whether a new file has been created.*/
	void writeTimestampIfpermission(){
		try {
			if (hasPermission == null || (Integer) hasPermission.invoke(null) == 1)
				write(System.currentTimeMillis()+"\n");
		}catch (Exception e){}
	}
	public synchronized boolean newFile(){
		if (this.isDummy) { return false; }
		this.dataBuffer = "";

		// handle the naming cases for persistent vs. non-persistent files
		if ( this.persistent ) { this.fileName = this.name; }
		else { // if user has not registered, stop non-persistent file generation
			if ( !PersistentData.isRegistered() ) return false;
			if ( compressed && sharedGzipStream!=null ) try{
				writeTimestampIfpermission();
				sharedGzipStream.flush();
				return true;
			} catch (Exception e){ return false; }
			if ( compressed )
				sharedGzipFilename = System.currentTimeMillis()+".csv.gz";
			else
				this.fileName = this.name + "_" + System.currentTimeMillis() + ".csv";
		}

		try {
			// write the key to the file (if it has one)
            Cipher c = null;
			if (this.encrypted) {
				if (!EncryptorECC.hasInstance()) {
					EncryptionEngine.readKey();
					if (!EncryptorECC.hasInstance()) return false;
				}
				final AESData aesData = EncryptorECC.getInstance().generateAESData();
				this.AESKey = aesData.getAESKey();
				this.unsafeWritePlaintext(aesData.getEncryptedAESKey());
				c = Cipher.getInstance("AES/CBC/PKCS5Padding");
				c.init(Cipher.ENCRYPT_MODE, this.AESKey);
				this.unsafeWritePlaintext(EncryptionEngine.toBase64String(c.getIV()));
			}
			if (this.compressed) {
				if (this.AESKey!=null && c!=null) {
					sharedGzipStream = new GZIPOutputStream(new CipherOutputStream(appContext.openFileOutput(sharedGzipFilename, Context.MODE_APPEND), c));
//					sharedGzipStream = new CipherOutputStream(appContext.openFileOutput(sharedGzipFilename, Context.MODE_APPEND), c);
				} else
					sharedGzipStream = new GZIPOutputStream(appContext.openFileOutput(sharedGzipFilename, Context.MODE_APPEND));
				writeTimestampIfpermission();
			} else if (header != null && header.length() > 0) { // write the csv header, if the file has a header
				// We will not call writeEncrypted here because we need to handle the specific case of the new file not being created properly.
				if (hasPermission == null || (Integer)hasPermission.invoke(null)==1)
					this.unsafeWritePlaintext(this.encrypted ? EncryptionEngine.encryptAES(header, this.AESKey) : header);
			}
		} catch (Exception e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("TextFileManager", "newFile(): " + this.name + "\n" + e.toString());
			CrashHandler.writeCrashlog(e, appContext);
			// Set filename null so that the system tries to create the file again later
			if(compressed)
				sharedGzipFilename = null;
			else
				this.fileName = null;
			return false;
		}
		return true;
	}
//
//	/** If it's a SurveyAnswers or SurveyTimings file, we want to append the
//	 * Survey ID so that the file name reads like this:
//	 * [USERID]_SurveyAnswers[SURVEYID]_[TIMESTAMP].csv
//	 * @param surveyId surveyId*/
//	//does not require dummy check, just setting attributes on the in-memory variable
//	public synchronized void newFile(String surveyId) {
//		String nameHolder = this.name;
//		this.name += surveyId;
//		newFile(); //We do not care about return value, it is only used for handling encrypted files.
//		this.name = nameHolder;
//	}

	/*###############################################################################
	########################## Read and Write Operations ############################
	###############################################################################*/

	/** Takes a string. writes that to the file, adds a new line to the string.
	 * Prints a stacktrace on a write error, but does not crash. If there is no
	 * file, a new file will be created.
	 * @param data any unicode valid string*/
	private synchronized void unsafeWritePlaintext(String data) throws IOException {
		if (this.isDummy) { return; }
		FileOutputStream outStream = appContext.openFileOutput(compressed?sharedGzipFilename:fileName, Context.MODE_APPEND);
		outStream.write( ( data+"\n" ).getBytes() );
		outStream.flush();
		outStream.close();
	}

	private synchronized void safeWritePlaintext(String data) {
		if (this.isDummy) { return; }
		if (fileName == null) this.newFile();
		try {
			unsafeWritePlaintext(data);
		} catch (FileNotFoundException e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("TextFileManager", "could not find file to write to, " + this.fileName);
			CrashHandler.writeCrashlog(e, appContext);
		} catch (IOException e) {
			if(BuildConfig.APP_IS_DEBUG) {
				if (e.getMessage().toLowerCase().contains("enospc")) // If the device is out of storage, alert the user
					Log.e("ENOSPC", "Out of storage space");
				else
					Log.e("TextFileManager", "error in the write operation");
			}
			CrashHandler.writeCrashlog(e, appContext);
		}
	}

	// File writer
	private Runnable write;
	private void writeNoncompressed() {
		try {
			if(BuildConfig.APP_IS_DEBUG) Log.d("DEBOUNCED", "writing encrypted file " + TextFileManager.this.name);
			this.safeWritePlaintext( this.encrypted?EncryptionEngine.encryptAES(this.dataBuffer, this.AESKey):this.dataBuffer );
			this.dataBuffer = "";
		} catch (InvalidKeyException e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("TextFileManager", "encrypted write operation without an AES key: " + this.name + ", " + this.fileName);
			CrashHandler.writeCrashlog(e, appContext);
		}
	}
	private void writeCompressed() {
		try {
			if(BuildConfig.APP_IS_DEBUG) Log.d("DEBOUNCED", "writing compressed file " + TextFileManager.this.name);
			synchronized (sharedGzipStream) {
				sharedGzipStream.write(this.dataBuffer.getBytes());
			}
			this.dataBuffer = "";
		} catch (Exception e) { //this occurs when an encrypted write operation occurs without an RSA key file, we eat this error because it only happens during registration/initial config.
			if(BuildConfig.APP_IS_DEBUG) Log.e("TextFileManager", "writeCompressed(): " + e.toString());
			CrashHandler.writeCrashlog(e, appContext);
		}
	}

	/**Encrypts string data and writes it to a file.
	 * @param data any unicode valid string */
	public synchronized void write(String data) {
		if ( isDummy || data==null || data.length()==0 ) return;
		if ( (compressed?sharedGzipStream:fileName)==null ) { // when newFile fails we are not allowed to write to files.
			if (!newFile() ) { return; }
		}

		dataBuffer = data.endsWith("\n")?data:data+"\n";
		if(compressed)
			dataBuffer = name + "," + dataBuffer;

		if (this.dataBuffer.getBytes().length > 2048) {
			this.debouncer.debounce(TextFileManager.this, () -> {
				TextFileManager t = TextFileManager.this;
				t.write.run();
			}, 1000, TimeUnit.MILLISECONDS);
		} else {
			this.write.run();
		}
	}

	public static TextFileManager getFile(Object cls) { return getFileWithTimeout((cls instanceof Class)?((Class)cls):cls.getClass()); }
	public static TextFileManager getDebugLogFile() { return getFileWithTimeout(DebugInterfaceActivity.class); }
	public static TextFileManager getKeyFile() { return keyFile==null?dummyFile:keyFile; }

	/** We check for the availability of the given TextFile, if it fails to exist we wait GETTER_TIMEOUT milliseconds and then try again.
	 * On a regular case error we throw the getter error, if the sleep operation breaks we throw the broken timeout error. */
	private static TextFileManager getFileWithTimeout(Class cls) {
		TextFileManager ret = fileMap.get(cls);
		if ( ret!=null ) return ret;
		try {
			// The Background Service should be getting restarted as we speak
			for(int x = 0; x < 40; x++) {
				// Previously, flat 75 ms. Now 50ms over 40 iterations.
				// If this still fails then we have bigger problems, value is in milliseconds
				Thread.sleep(50);

				// From the documentation
				// No response to an input event (such as key press or screen touch events) within 5 seconds.
				// A BroadcastReceiver hasn't finished executing within 10 seconds.
				// https://developer.android.com/training/articles/perf-anr  as of: 2018-04-25
				if ( (ret=fileMap.get(cls)) != null ) return ret;
			}
			if(BuildConfig.APP_IS_DEBUG) {
				Log.e("TextFileManager:getFileWithTimeout()", "cannot get file for class " + cls.getName() + " fileMap.size()=" + fileMap.size());
				Log.e("TextFileManager:getFileWithTimeout()", DebugInterfaceActivity.printStack());
			}
		} catch (Exception e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("TextFileManager:getFileWithTimeout()", DebugInterfaceActivity.printStack(e));
		}
		return dummyFile;
	}


	/**@return A string of the file contents. */
	public synchronized String read() {
		if (this.isDummy) { return this.name + " is a dummy file."; }
		byte [] bytes = null;
		try {  // Read through the (buffered) input stream, append to a stringbuffer.  Catch exceptions
			FileInputStream fis = appContext.openFileInput(fileName);
			try {
				bytes = new byte [(int)(fis.getChannel().size())];
				fis.read(bytes);
			} catch (IOException e) {
				if(BuildConfig.APP_IS_DEBUG) Log.e("Upload", "read error in " + this.fileName);
				e.printStackTrace();
				CrashHandler.writeCrashlog(e, appContext);
			}
			fis.close();
		} catch (FileNotFoundException e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("TextFileManager", "file " + this.fileName + " does not exist");
			e.printStackTrace();
			if(!fileName.equals(keyFileName))
				CrashHandler.writeCrashlog(e, appContext);
		} catch (IOException e){
			if(BuildConfig.APP_IS_DEBUG) Log.e("DataFileManager", "could not close " + this.fileName);
			e.printStackTrace();
			if(!fileName.equals(keyFileName))
				CrashHandler.writeCrashlog(e, appContext);
		}

		return new String(bytes, StandardCharsets.UTF_8);
	}


	/*###############################################################################
	#################### Miscellaneous Utility Functions ############################
	###############################################################################*/

	/** Delete the reference to the file so that it can be uploaded */
	public synchronized void closeFile() {
		if(compressed) try{
			sharedGzipStream.flush();
		} catch (Exception e){}
		else
			this.fileName = null;
	}

	/** Deletes a file in the safest possible way, based on the file type (persistent-nonpersistent). */
	public synchronized void deleteSafely() {
		if (this.isDummy) { return; }
		String oldFileName = this.fileName;
		// For files that are persistant we have to do a slightly unsafe deletion, for everything else
		// we allocate the new file and then delete the old file.

		if ( this.persistent ) {    //delete then create (unsafe, potential threading issues)
			TextFileManager.delete(oldFileName);
			this.newFile();
		} else                      //create then delete
			TextFileManager.delete(oldFileName);
	}

	/** Deletes a file.  Exists to make file deletion thread-safe.
	 * @param fileName file name*/
	public static synchronized void delete(String fileName){
		try { appContext.deleteFile(fileName); }
		catch (Exception e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("TextFileManager", "cannot delete file " + fileName );
			CrashHandler.writeCrashlog(e, appContext); }
	}

	/** Make new files for all the non-persistent files. */
	public static synchronized void makeNewFilesForEverything() {
		try {   // compressed stream need explicit closure
			if( sharedGzipStream !=null )
				sharedGzipStream.close();
		} catch (Exception e){
			if(BuildConfig.APP_IS_DEBUG) Log.e("TextFileManager", "error while closing gzip archive\n " + e.toString());
			CrashHandler.writeCrashlog(e, appContext);
		} finally {
			sharedGzipStream = null;
			sharedGzipFilename = null;
		}
		for(TextFileManager file: fileMap.values())
			if(!file.persistent) file.newFile();
	}

	/** Very simple function, exists to make any function that needs to grab all extant files thread-safe.
	 * DO NOT USE THIS FUNCTION, USE getAllFilesSafely() INSTEAD.
	 * @return a string array of all files in the hopes's file directory. */
	public static synchronized String[] getAllFiles() { return appContext.getFilesDir().list(); }

	/*###############################################################################
	######################## DEBUG STUFF ############################################
	###############################################################################*/

	/** Returns a list of file names, all files in that list are retired and will not be written to again.
	 * @return a string array of files*/
	private static synchronized String[] getAllFilesSafely() {
		String[] file_list = getAllFiles();
		makeNewFilesForEverything();
		return file_list;
	}

	/**For Debug Only.  Deletes all files, creates new ones. */
	public static synchronized void deleteEverything() {
		//Get complete list of all files, then make new files, then delete all files from the old files list.
		Set<String> files = new HashSet<>();
		Collections.addAll(files, getAllFilesSafely());
		if(sharedGzipStream!=null){
			try{ sharedGzipStream.close(); } catch (Exception e) {}
			sharedGzipStream = null;
			sharedGzipFilename = null;
		}

		//and delete things
		for (String file_name : files) {
			try { appContext.deleteFile(file_name); }
			catch (Exception e) {
				if(BuildConfig.APP_IS_DEBUG) Log.e("TextFileManager", "could not delete file " + file_name);
			}
		}
	}

	public static String CS2S(CharSequence seq){
		String returnString = "";
		try {
			returnString = seq==null ? "" : (String) seq;
		} catch (Exception ignored) {}
		return returnString;
	}

	public static String CS2S(String seq){
		String returnString = "";
		try {
			returnString = seq==null ? "" : seq;
		} catch (Exception ignored) {}
		return returnString;
	}

	public static String CS2S(List<CharSequence> seq) {
		String returnString = "";
		try {
			returnString = seq == null ? "" : seq.toString();
		} catch (Exception ignored) {}
		return returnString;
	}

	public static synchronized String[] getAllUploadableFiles() {
		Set<String> files = new HashSet<>();

		// only upload .csv and .csv.gz files
		for(String filename : getAllFiles())
			if(filename.endsWith(".csv") || filename.endsWith(".csv.gz") || filename.startsWith("crashlog_"))
				files.add(filename);

		// exclude currently open files
		if(sharedGzipFilename!=null) files.remove(sharedGzipFilename);
		for(TextFileManager fp: fileMap.values())
			if(fp.fileName!=null && !fp.fileName.isEmpty())
				files.remove(fp.fileName);

		return files.toArray(new String[0]);
	}

	public static boolean saveFile(String filename, Object data){
		try {
			FileOutputStream fos = appContext.openFileOutput(filename, Context.MODE_PRIVATE);
			OutputStream os = filename.toLowerCase().endsWith(".gz")?new GZIPOutputStream(fos):fos;
			if ( data instanceof String )
				os.write(((String) data).getBytes());
			else if (data instanceof byte [])
				os.write((byte [])data);
			else
				os.write(data.toString().getBytes());
			os.flush();
			os.close();
		}catch (Exception e){
			getDebugLogFile().write(e.toString());
			return false;
		}
		return true;
	}

	public static byte [] loadFile(String filename){
		try {
			FileInputStream fis = appContext.openFileInput(filename);
			InputStream is = filename.toLowerCase().endsWith(".gz")?new GZIPInputStream(fis):fis;
			byte [] data = new byte [(int)(fis.getChannel().size())];
			is.read(data);
			return data;
		}catch (Exception e){
			getDebugLogFile().write(e.toString());
			return null;
		}
	}
}
