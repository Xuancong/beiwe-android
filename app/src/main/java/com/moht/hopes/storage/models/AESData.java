package com.moht.hopes.storage.models;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public class AESData {
    private final ECPoint encryptedAESKey;
    private final SecretKey AESKey;

    public AESData(ECPoint encryptedAESKey, byte[] AESKey) {
        this.encryptedAESKey = encryptedAESKey;
        this.AESKey = new SecretKeySpec(AESKey, "AES");
    }

    public String getEncryptedAESKey() {
        return encryptedAESKey.toString(16);
    }

    public SecretKey getAESKey() {
        return AESKey;
    }
}
