package com.moht.hopes.storage.models;

import androidx.annotation.NonNull;

import java.math.BigInteger;

public class ECPoint {
    public BigInteger x;
    public BigInteger y;
    private boolean pointOfInfinity;

    public ECPoint() {
        this.x = this.y = BigInteger.ZERO;
        this.pointOfInfinity = false;
    }

    public ECPoint(BigInteger x, BigInteger y) {
        this.x = x;
        this.y = y;
        this.pointOfInfinity = false;
    }

    public ECPoint(long x, long y) {
        this.x = BigInteger.valueOf(x);
        this.y = BigInteger.valueOf(y);
        this.pointOfInfinity = false;
    }

    private static BigInteger getBigInteger(String n) {
        if (n.toLowerCase().startsWith("0x")) {
            n = n.substring(2);
        }
        return new BigInteger(n, 16);
    }

    public static ECPoint fromString(String ecPoint) {
        if (ecPoint.equals("INFINITY")) return INFINITY;
        final String[] numbers = ecPoint.trim().split(",");
        if (numbers.length < 2) return null;
        try {
            BigInteger x = getBigInteger(numbers[0]);
            BigInteger y = getBigInteger(numbers[1]);
            return new ECPoint(x, y);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public ECPoint(ECPoint p) {
        this.x = p.x;
        this.y = p.y;
        this.pointOfInfinity = p.pointOfInfinity;
    }

    public boolean equals(ECPoint point) {
        if (point == null)
            return false;

        if (this.pointOfInfinity == point.pointOfInfinity)
            return true;

        return (this.x.compareTo(point.x) | this.y.compareTo(point.y)) == 0;
    }

    public boolean isPointOfInfinity() {
        return pointOfInfinity;
    }

    public ECPoint negate() {
        if (isPointOfInfinity()) {
            return INFINITY;
        } else {
            return new ECPoint(x, y.negate());
        }
    }

    private static ECPoint infinity() {
        ECPoint point = new ECPoint();
        point.pointOfInfinity = true;
        return point;
    }

    public static final ECPoint INFINITY = infinity();

    @NonNull
    @Override
    public String toString() {
        return this.toString(16);
    }

    public String toString(int radix) {
        if (isPointOfInfinity()) {
            return "INFINITY";
        } else {
            return x.toString(radix) + "," + y.toString(radix);
        }
    }
}
