package com.moht.hopes.survey;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.util.Log;

import com.moht.hopes.BuildConfig;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;

@SuppressWarnings("ResultOfMethodCallIgnored")
public class AdBlock {
	public static final String FILE = "hosts.txt";
	private static final Set<String> hosts = new HashSet<>();
	@SuppressLint("ConstantLocale")
	private static final Locale locale = Locale.getDefault();

	public static String getHostsDate(Context context) {
		File file = new File(context.getDir("filesdir", Context.MODE_PRIVATE) + "/" + FILE);
		String date = "";
		if (!file.exists()) {
			return "";
		}

		try {
			FileReader in = new FileReader(file);
			BufferedReader reader = new BufferedReader(in);
			String line;
			while ((line = reader.readLine()) != null) {
				if (line.contains("Date:")) {
					date = "hosts.txt " + line.substring(2);
					in.close();
					break;
				}
			}
			in.close();
		} catch (IOException i) {
			if(BuildConfig.APP_IS_DEBUG) Log.w("browser", "Error getting hosts date", i);
		}
		return date;
	}

	private static void loadHosts(final Context context) {
		Thread thread = new Thread(() -> {
			try {
				File file = new File(context.getDir("filesdir", Context.MODE_PRIVATE) + "/" + FILE);
				FileReader in = new FileReader(file);
				BufferedReader reader = new BufferedReader(in);
				String line;
				while ((line = reader.readLine()) != null) {
					if (line.startsWith("#")) continue;
					hosts.add(line.toLowerCase(locale));
				}
				in.close();
			} catch (IOException i) {
				if(BuildConfig.APP_IS_DEBUG) Log.w("browser", "Error loading adBlockHosts", i);
			}
		});
		thread.start();
	}

	public static void downloadHosts(final Context context, Map<Object, Object> sp) {
		Thread thread = new Thread(() -> {

			String hostURL = getString(sp,"ab_hosts", "https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts");

			try {
				URL url = new URL(hostURL);
				URLConnection ucon = url.openConnection();
				ucon.setReadTimeout(5000);
				ucon.setConnectTimeout(10000);

				InputStream is = ucon.getInputStream();
				BufferedInputStream inStream = new BufferedInputStream(is, 1024 * 5);

				File tempfile = new File(context.getDir("filesdir", Context.MODE_PRIVATE) + "/temp.txt");

				if (tempfile.exists()) {
					tempfile.delete();
				}
				tempfile.createNewFile();

				FileOutputStream outStream = new FileOutputStream(tempfile);
				byte[] buff = new byte[5 * 1024];

				int len;
				while ((len = inStream.read(buff)) != -1) {
					outStream.write(buff, 0, len);
				}

				outStream.flush();
				outStream.close();
				inStream.close();

				//now remove leading 0.0.0.0 from file
				FileReader in = new FileReader(tempfile);
				BufferedReader reader = new BufferedReader(in);
				File outfile = new File(context.getDir("filesdir", Context.MODE_PRIVATE) + "/" + FILE);
				FileWriter out = new FileWriter(outfile);
				String line;
				while ((line = reader.readLine()) != null) {
					if (line.startsWith("0.0.0.0 ")) {
						line = line.substring(8);
					}
					out.write(line + "\n");
				}
				in.close();
				out.close();

				tempfile.delete();

				hosts.clear();
				loadHosts(context);  //reload hosts after update

			} catch (IOException i) {
				if(BuildConfig.APP_IS_DEBUG) Log.w("browser", "Error updating AdBlock hosts", i);
			}
		});
		thread.start();
	}

	private static String getDomain(String url) throws URISyntaxException {
		url = url.toLowerCase(locale);

		int index = url.indexOf('/', 8); // -> http://(7) and https://(8)
		if (index != -1) {
			url = url.substring(0, index);
		}

		URI uri = new URI(url);
		String domain = uri.getHost();
		if (domain == null) {
			return url;
		}
		return domain.startsWith("www.") ? domain.substring(4) : domain;
	}

	static boolean getBoolean(Map<Object, Object> sp, String key, boolean def_val) {
		return sp.containsKey(key) ? (boolean) sp.get(key) : def_val;
	}

    static String getString(Map<Object, Object> sp, String key, String def_val) {
        return sp.containsKey(key) ? (String) sp.get(key) : def_val;
    }

    public AdBlock(Context context) {
        this(context, new LinkedHashMap<Object, Object>());
    }
	public AdBlock(Context context, Map<Object, Object> sp) {
		File file = new File(context.getDir("filesdir", Context.MODE_PRIVATE) + "/" + FILE);
		if (!file.exists()) {
			//copy hosts.txt from assets if not available
			try {
				AssetManager manager = context.getAssets();
				copyFile(manager.open(FILE), new FileOutputStream(file));
				downloadHosts(context, sp);  //try to update hosts.txt from internet
			} catch (IOException e) {
				if(BuildConfig.APP_IS_DEBUG) Log.e("browser", "Failed to copy asset file", e);
			}
		}

		Calendar time = Calendar.getInstance();

		if (getBoolean(sp, "sp_savedata", false)) {
			time.add(Calendar.DAY_OF_YEAR, -7);
		} else {
			time.add(Calendar.DAY_OF_YEAR, -1);
		}

		Date lastModified = new Date(file.lastModified());
		if (lastModified.before(time.getTime()) || getHostsDate(context).equals("")) {  //also download again if something is wrong with the file
			//update if file is older than a day
			downloadHosts(context, sp);
		}

		if (hosts.isEmpty()) {
			loadHosts(context);
		}
	}

	private void copyFile(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[1024];
		int read;
		while ((read = in.read(buffer)) != -1) {
			out.write(buffer, 0, read);
		}
	}

	boolean isAd(String url) {
		String domain;
		try {
			domain = getDomain(url);
		} catch (URISyntaxException u) {
			return false;
		}
		return hosts.contains(domain.toLowerCase(locale));
	}
}
