package com.moht.hopes.survey;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;

import com.moht.hopes.BuildConfig;
import com.moht.hopes.CrashHandler;
import com.moht.hopes.utils.GS_JSON.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

/** Edited by Wang Xuancong in 2021.5; created by elijones on 12/1/16. */

//TODO: implement a serialization and/or iterable output blob.  need to see what survey anwsers actually look like to determine all the values it needs

public class JsonSkipLogic {

	/*############################# Assets ###################################*/

	//Comparator sets
	private static Set<String> COMPARATORS = new HashSet<String>(Arrays.asList("<", "<=", ">", ">=", "==", "!="));
	private static Set<String> BOOLEAN_OPERATORS = new HashSet<String>(Arrays.asList("and", "or"));
	private static Set<String> SET_OPERATORS = new HashSet<String>(Arrays.asList("∋", "∌"));
	static { //Block modification of all the sets.
		COMPARATORS = Collections.unmodifiableSet(COMPARATORS);
		BOOLEAN_OPERATORS = Collections.unmodifiableSet(BOOLEAN_OPERATORS);
		SET_OPERATORS = Collections.unmodifiableSet(SET_OPERATORS);
	}

	// For checking equality between Doubles.
	// http://stackoverflow.com/questions/25160375/comparing-double-values-for-equality-in-java
	private static final String NUMERIC_OPEN_RESPONSE_FORMAT = "%.5f";
	public static final Double ANSWER_COMPARISON_EQUALITY_DELTA = 0.00001;


	private static boolean isEqual(double d1, double d2) {
		return d1 == d2 || isRelativelyEqual(d1,d2); //this short circuit just makes it faster
	}

	/** checks if the numbers are separated by a predefined absolute difference.*/
	private static boolean isRelativelyEqual(double d1, double d2) {
		return ANSWER_COMPARISON_EQUALITY_DELTA > Math.abs(d1 - d2) / Math.max(Math.abs(d1), Math.abs(d2));
	}

	private HashMap<String, QuestionData> QuestionAnswer;
	private HashMap<String, JsonObject> QuestionSkipLogic;
	public HashMap<String, JsonObject> Questions;
	public HashSet<Integer>  SkippedQuestions;
	public ArrayList<String> QuestionOrder;
	public Integer currentQuestion;
	private Boolean runDisplayLogic;
	private Context appContext;

	/**@param jsonQuestions The content of the "content" key in a survey
	 * @param runDisplayLogic A boolean value for whether skip Logic should be run on this survey.
	 * @param applicationContext An application context is required in order to extend exception handling.
	 * @throws Exception thrown if there are any questions without question ids. */
	public JsonSkipLogic(List jsonQuestions, Boolean runDisplayLogic, Context applicationContext) throws Exception {
		appContext = applicationContext;
		final int MAX_SIZE = jsonQuestions.size();
		String questionId;
		JsonObject question;
		JsonObject displayLogic;

		//construct the various question id collections
		QuestionAnswer = new HashMap<String, QuestionData> (MAX_SIZE);
		QuestionSkipLogic = new HashMap<String, JsonObject> (MAX_SIZE);
		Questions = new HashMap<String, JsonObject> (MAX_SIZE);
		QuestionOrder = new ArrayList<String> (MAX_SIZE);
		SkippedQuestions = new HashSet<Integer>(MAX_SIZE);

		for (int i = 0; i < MAX_SIZE; i++) { //uhg, you can't iterate over a JsonArray.
			question = (JsonObject)jsonQuestions.get(i);
			questionId = (String)question.get("question_id");

			Questions.put(questionId, question); //store questions by id
			QuestionOrder.add(questionId); //setup question order

			//setup question logic
			displayLogic = (JsonObject)question.getOrDefault("display_if", null);
			if ( displayLogic!=null ) { //skip item if it has no display_if item
				QuestionSkipLogic.put(questionId, displayLogic);
			}
		}
		this.runDisplayLogic = runDisplayLogic;
		currentQuestion = 0;
	}


	/**@param questionId Takes a question id
	 * @return returns a QuestionData if it has been answered, otherwise null. */
	public QuestionData getQuestionData(String questionId) { return QuestionAnswer.get(questionId); }

	/** @return the QuestionData object for the current question, null otherwise */
	public QuestionData getCurrentQuestionData() {
		try {
			return QuestionAnswer.get(QuestionOrder.get(currentQuestion));
		}catch (Exception e){
			return null;
		}
	}

	/** Determines question should be displayed next.
	 * @return a question id string, null if there is no next item. */
	@SuppressWarnings("TailRecursion")
	private int getQuestion(Boolean goForward) {
		if (goForward) currentQuestion++;
		else currentQuestion--;

		// this means error, 1st question should have prev button disabled
		if (currentQuestion < 0)
			currentQuestion = 0;

		//if it is the first question it should invariably display.
		if (currentQuestion == 0)
			return 0;

		// caller should handle end-of-list
		if (currentQuestion >= QuestionOrder.size())
			return currentQuestion;

		// if display logic has been disabled we skip logic processing and return the next question
		if (!runDisplayLogic)
			return currentQuestion;

		String questionId = QuestionOrder.get(currentQuestion);

		// if questionId does not have skip logic we display it.
		if ( !QuestionSkipLogic.containsKey(questionId) )
			return currentQuestion;

		if ( shouldQuestionDisplay(questionId) ) {
			SkippedQuestions.remove(currentQuestion);
			return currentQuestion;
		} else {
			SkippedQuestions.add(currentQuestion);
			return getQuestion(goForward);
		}
	}

	public int goNextQuestion() { return getQuestion(true); }

	public int goPrevQuestion() { return getQuestion(false); }

	/** This function wraps the logic processing code.  If the logic processing encounters an error
	 * due to json parsing the behavior is to invariably return true.
	 * @param questionId
	 * @return Boolean result of the logic */
	public boolean shouldQuestionDisplay(String questionId){
		try {
			JsonObject question = QuestionSkipLogic.get(questionId);
			//If the survey display logic object is null or is empty, display
			if (question == null || question.size() == 0) { return true; }
			return parseLogicTree(questionId, question);
		} catch (Exception e) {
			if(BuildConfig.APP_IS_DEBUG) Log.w("shouldQuestionDisplay", "json exception while doing a logic parse");
			CrashHandler.writeCrashlog(e, appContext);
		}
		return true;
	}
	public boolean shouldQuestionDisplay(int index){
		return shouldQuestionDisplay(QuestionOrder.get(index));
	}

	/** This function checks through all questions and determines whether the survey is in a submittable state */
	public boolean canSubmit(){
		for(String qid : QuestionOrder)
			if(shouldQuestionDisplay(qid)){
				QuestionData qdata = QuestionAnswer.get(qid);
				if(qdata.compulsory && !qdata.questionIsAnswered())
					return false;
			}
		return true;
	}

	private boolean parseLogicTree(String questionId, JsonObject logic ) throws Exception {
		// extract the comparator, force it as lower case.
		String comparator = ((String)logic.keySet().iterator().next()).toLowerCase();

		// logic.getXXX(comparator_as_key) is the only way to grab the value due to strong typing.
		// This object has many uses, so the following logic has been written for explicit clarity,
		// rather than optimized code length or performance.

		//We'll get the NOT out of the way first.
		//todo: Eli. test not, it may not be in the reference.
		if ( comparator.equals("not") ) {
			//we need to pass in the Json _Object_ of the next layer in
			return !parseLogicTree(questionId, (JsonObject)logic.get(comparator) );
		}

		if ( COMPARATORS.contains(comparator) ) {
			// in this case logic.getString(comparator) contains a json list/array with the first
			// element being the referencing question ID, and the second being a value to compare to.
			return runNumericLogic(comparator, (JsonArray)logic.get(comparator) );
		}

		if ( SET_OPERATORS.contains(comparator) ) {
			// in this case logic.getString(comparator) contains a json list/array with the first
			// element being the referencing question ID, and the second being a value to compare to.
			return runSetLogic(comparator, (JsonArray)logic.get(comparator) );
		}

		if ( BOOLEAN_OPERATORS.contains(comparator) ) {
			//get array of logic operations
			JsonArray<JsonObject> manyLogics = (JsonArray<JsonObject>)logic.get(comparator);
			List<Boolean> results = new ArrayList<Boolean>(manyLogics.size());

			//iterate over array, get the booleans into a list
			for (JsonObject obj : manyLogics) { //jsonArrays are not iterable...
				results.add( parseLogicTree(questionId, obj ) );
			} //results now contains the boolean evaluation of all nested logics.

			//And. if anything is false, return false. If those all pass, return true.
			if ( comparator.equals("and") ) {
				for (Boolean bool : results) { if ( !bool ) { return false; } }
				return true;
			}
			//Or. if anything is true, return true. If those all pass, return false.
			if ( comparator.equals("or") ) {
				for (Boolean bool : results) { if ( bool ) { return true; } }
				return false;
			}
		}
		throw new NullPointerException("received invalid comparator: " + comparator);
	}


	/** Processes the logical operation implemented of a comparator.
	 * If there has been no answer for the question a logic operation references this function returns false.
	 * @param comparator a string that is in the COMPARATORS constant.
	 * @param parameters json array 2 elements in length.  The first element is a target question ID to pull an answer from, the second is the survey's value to compare to.
	 * @return Boolean result of the operation, or false if the referenced question has no answer.
	 * @throws Exception */
	private Boolean runNumericLogic(String comparator, JsonArray parameters) throws Exception {
		String targetQuestionId = (String)parameters.get(0);
		if ( !QuestionAnswer.containsKey(targetQuestionId) ) { return false; } // false if DNE
		Double userAnswer = Objects.requireNonNull(QuestionAnswer.get(targetQuestionId)).getAnswerDouble();
		Double surveyValue = ((Long)parameters.get(1)).doubleValue();

		//If we encounter an unanswered question, that evaluates as false. (defined in the spec.)
		if ( userAnswer == null ) { return false; }

		if ( comparator.equals("<") ) {
			return userAnswer < surveyValue && !isEqual(userAnswer, surveyValue);  }
		if ( comparator.equals(">") ) {
			return userAnswer > surveyValue && !isEqual(userAnswer, surveyValue); }
  		if ( comparator.equals("<=") ) {
		    return userAnswer <= surveyValue || isEqual(userAnswer, surveyValue); } //the <= is slightly redundant, its fine.
		if ( comparator.equals(">=") ) {
			return userAnswer >= surveyValue || isEqual(userAnswer, surveyValue); } //the >= is slightly redundant, its fine.
		if ( comparator.equals("==") ) {
			return isEqual(userAnswer, surveyValue); }
		if ( comparator.equals("!=") ) {
			return !isEqual(userAnswer, surveyValue); }
		throw new NullPointerException("numeric logic fail");
	}

	private Boolean runSetLogic(String comparator, JsonArray parameters) throws Exception {
		String targetQuestionId = (String)parameters.get(0);
		if ( !QuestionAnswer.containsKey(targetQuestionId) ) { return false; } // false if DNE
		String userAnswer;
		try{ userAnswer = QuestionAnswer.get(targetQuestionId).getAnswerString(); }
		catch (Exception e) { userAnswer = null; }
		long surveyValue = ((Long)parameters.get(1)).longValue();

		//If we encounter an unanswered question, that evaluates as false. (defined in the spec.)
		if ( userAnswer == null ) return false;

		if ( comparator.equals("∋") )
			return (" "+userAnswer+" ").contains(" "+surveyValue+" ");
		if ( comparator.equals("∌") )
			return !(" "+userAnswer+" ").contains(" "+surveyValue+" ");
		throw new NullPointerException("numeric logic fail");
	}

    @SuppressLint("DefaultLocale")
	public void linkAnswer(QuestionData questionData) {
	    QuestionAnswer.put(questionData.getId(), questionData);
    }


	/** @return a list of QuestionData objects for serialization to the answers file. */
	@SuppressWarnings("ObjectAllocationInLoop")
	public List<QuestionData> getQuestionsForSerialization() {
		List<QuestionData> answers = new ArrayList<QuestionData>(QuestionOrder.size());
		for (String questionId : QuestionOrder)
			if ( QuestionAnswer.containsKey(questionId) )
				answers.add(QuestionAnswer.get(questionId));
		return answers;
	}

	/**@return a list of QuestionData objects A) should have displayed, B) will be accessible by paging back, C) don't have answers.*/
	public ArrayList<String> getUnansweredQuestions() {
		ArrayList<String> unanswered = new ArrayList<> (QuestionOrder.size());
		QuestionData question;
		int questionDisplayNumber = 0;
		Boolean questionWouldDisplay;

		//Guarantee: the questions in QuestionAnswer will consist of all _displayed_ questions.
		for (String questionId : QuestionOrder) {

			//QuestionData objects are put in the QuestionAnswers dictionary if they are ever
			// displayed. (The user must also proceed to the next question, but that has no effect.)
			//No QuestionAnswer object means question did not display, which means we can skip it.
			if ( QuestionAnswer.containsKey(questionId) ) {
				//A user may have viewed questions along a Display Logic Path A, but failed to answer
				// certain questions, then reversed and changed some answers. If so they may have created
				// a logic path B that no longer displays a previously answered question.
				//If that occurred then we have a a QuestionData object in QuestionAnswers that
				// effectively should not have displayed (returns false on evaluation of
				// shouldQuestionDisplay), so we need to catch that case.
				//The only way to catch that case is to run shouldQuestionDisplay on every QuestionData
				// object in QuestionAnswers.
				//There is one exception: INFO_TEXT_BOX questions are always ignored.
				question = QuestionAnswer.get(questionId);

				//INFO_TEXT_BOX and WEB_SURVEY => skip
				String q_type = Objects.requireNonNull(question).getType();
				if ( q_type.equals(QuestionData.INFO_TEXT_BOX) )
					continue;

				//check if should display, store value
				questionWouldDisplay = shouldQuestionDisplay(questionId);

				if ( questionWouldDisplay ) { //If would display, increment question number.
					questionDisplayNumber++;
				} else { //If would not display not, skip it without incrementing.
					continue;
				}

				//If question is actually unanswered construct a display string.
				if ( !question.questionIsAnswered() ){
					unanswered.add("Question " + questionDisplayNumber + ": " + question.getText());
				}
			}
		}
		return unanswered;
	}
}
