package com.moht.hopes.survey;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.net.http.SslError;
import android.os.Build;
import android.os.CountDownTimer;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.GeolocationPermissions;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebResourceResponse;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.R;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.utils.GS_JSON;

import java.io.ByteArrayInputStream;
import java.time.Duration;
import java.time.LocalDate;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by elijones on 1/20/17.
 */

public class MarkDownTextView extends android.webkit.WebView {
	public static final String MIME_TYPE_TEXT_PLAIN = "text/plain";
	public static final String URL_ENCODING = "UTF-8";
	public SurveyRecorder surveyRecorder;

	// Duplicating all Constructors for maximum trivial compatibility.
	public MarkDownTextView(Context context) { super(context); setBackgroundColor(Color.TRANSPARENT); }
	public MarkDownTextView(Context context, AttributeSet attrs) { super(context, attrs); }
	public MarkDownTextView(Context context, AttributeSet attrs, int defStyleAttr) { super(context, attrs, defStyleAttr); }
	public void setContent(String html){
		getSettings().setJavaScriptEnabled(true);
		getSettings().setDomStorageEnabled(true);
		loadData(html, "text/html; charset=utf-8", "UTF-8");
	}

	private static class MyWebViewClient extends WebViewClient {
		AdBlock adBlock;
		MyWebViewClient(Context context, Map settings){
			super();
			adBlock = (boolean)settings.getOrDefault("adBlock", true)?new AdBlock(context, settings):null;
		}

		@Override
		public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
			return false;
		}
		@Override
		public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
			if(BuildConfig.APP_IS_DEBUG) handler.proceed();
			else handler.cancel();
		}
		@Override
		@SuppressWarnings("deprecation")
		public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
			if (adBlock!=null && adBlock.isAd(url))
				return new WebResourceResponse(MIME_TYPE_TEXT_PLAIN, URL_ENCODING, new ByteArrayInputStream("".getBytes()));
			return super.shouldInterceptRequest(view, url);
		}

		@Override
		public WebResourceResponse shouldInterceptRequest(WebView view, WebResourceRequest request) {
			if (adBlock!=null && adBlock.isAd(request.getUrl().toString()))
				return new WebResourceResponse(MIME_TYPE_TEXT_PLAIN, URL_ENCODING, new ByteArrayInputStream("".getBytes()));
			return super.shouldInterceptRequest(view, request);
		}
	}

//	String event_log = "";

	@Override
	public boolean onTouchEvent (MotionEvent event){
		if(surveyRecorder != null) {
//			event_log += event.getEventTime() + " " + event.getAction() + " " + event.getX() + " " + event.getY() + "\n";
			surveyRecorder.appendLineToLogFile(event.getEventTime() + " " + event.getAction() + " " + event.getX() + " " + event.getY() + ";");
		}
		return super.onTouchEvent(event);
	}

	public void setURL(String url, String options){
		Map<Object, Object> settings = (Map)GS_JSON.parse(options, new LinkedHashMap());
		webviewSetURL(this, url, settings);
	}

	public static void webviewSetURL(MarkDownTextView webView, String url, Map<Object, Object> webview_settings){
		if((boolean)webview_settings.getOrDefault("allowBrowsing", true))
			webView.setWebViewClient(new MyWebViewClient(webView.getContext(), webview_settings));
		initPreferences(webView, url, webview_settings);
		webView.loadUrl(url);
	}

	public static String getUserAgent(Boolean desktopMode) {
		if(desktopMode==null) return "";
		String mobilePrefix = "Mozilla/5.0 (Linux; Android " + Build.VERSION.RELEASE + ")";
		String desktopPrefix = "Mozilla/5.0 (X11; Linux " + System.getProperty("os.arch") + ")";

		try {
			String newUserAgent = WebSettings.getDefaultUserAgent(BackgroundService.appContext);
			String prefix = newUserAgent.substring(0, newUserAgent.indexOf(")") + 1);
			return newUserAgent.replace(prefix, desktopMode?desktopPrefix:mobilePrefix);
		} catch (Exception e) {
			TextFileManager.getDebugLogFile().write(e.toString());
			e.printStackTrace();
		}
		return "";
	}

	public static void initPreferences(WebView webView, String url, Map opt) {
		WebSettings webSettings = webView.getSettings();
		if (android.os.Build.VERSION.SDK_INT >= 26)
			webSettings.setSafeBrowsingEnabled((boolean)opt.getOrDefault("safeBrowsingEnabled", true));

		String userAgent = getUserAgent((Boolean) opt.getOrDefault("desktopMode", null));
		if(!userAgent.isEmpty()) webSettings.setUserAgentString(userAgent);
		webSettings.setUseWideViewPort((boolean)opt.getOrDefault("useWideViewPort", true));
		webSettings.setLoadWithOverviewMode((boolean)opt.getOrDefault("loadWithOverviewMode", true));
		webSettings.setAllowFileAccess((boolean)opt.getOrDefault("allowFileAccess", true));
		webSettings.setAllowContentAccess((boolean)opt.getOrDefault("allowContentAccess", true));
		webSettings.setSupportZoom((boolean)opt.getOrDefault("supportZoom", true));
		webSettings.setBuiltInZoomControls((boolean)opt.getOrDefault("builtInZoomControls", true));
		webSettings.setDisplayZoomControls((boolean)opt.getOrDefault("displayZoomControls", false));
		webSettings.setSupportMultipleWindows((boolean)opt.getOrDefault("supportMultipleWindows", true));
		webSettings.setBlockNetworkImage((boolean)opt.getOrDefault("blockNetworkImage", false));
		webSettings.setGeolocationEnabled((boolean)opt.getOrDefault("geolocationEnabled", false));
		webSettings.setMediaPlaybackRequiresUserGesture((boolean)opt.getOrDefault("mediaPlaybackRequiresUserGesture", true));
		webSettings.setJavaScriptEnabled((boolean)opt.getOrDefault("javaScriptEnabled", true));
		webSettings.setDomStorageEnabled((boolean)opt.getOrDefault("domStorageEnabled", true));
		webSettings.setJavaScriptCanOpenWindowsAutomatically((boolean)opt.getOrDefault("javaScriptCanOpenWindowsAutomatically", true));
		webSettings.setTextZoom((int)opt.getOrDefault("textZoom", 100));

		if((boolean)opt.getOrDefault("autoFill", true)) {
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
				webView.setImportantForAutofill(View.IMPORTANT_FOR_AUTOFILL_YES);
			else
				webSettings.setSaveFormData(true);
		}

		if((boolean)opt.getOrDefault("geolocationEnabled", false)) {
			webView.setWebChromeClient(new WebChromeClient() {
				@Override
				public void onGeolocationPermissionsShowPrompt(String origin, GeolocationPermissions.Callback callback) {
					callback.invoke(origin, true, false);
				}
			});
		}

		CookieManager manager = CookieManager.getInstance();
		manager.setAcceptCookie((boolean)opt.getOrDefault("acceptCookie", true));
		manager.getCookie(url);
	}
}
