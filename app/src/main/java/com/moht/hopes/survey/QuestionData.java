package com.moht.hopes.survey;

import com.moht.hopes.BuildConfig;
import com.moht.hopes.storage.TextFileManager;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * This class makes it easier to pass around the description of a question so
 * that it can be recorded in Survey Answers and Survey Timings files.
 * That way, instead of just recording "user answered '7' to question #5", the
 * Survey Answers and Survey Timings files record something along the lines of
 * "user answered '7' to question #5, which asked 'how many hours of sleep did
 * you get last night' and had a numeric input field, with options..."
 */

public class QuestionData {

	private String id = null;
	private String type = null;
	private String text = null;
	private String answerString = null;
	private Integer answerInteger = null;
	private Double answerDouble = null;
	public boolean save_state_variable = false;
	public boolean compulsory = false;
	public static SurveyActivity surveyActivity = null;

	public static final String INFO_TEXT_BOX    = "info_text_box";
	public static final String SLIDER           = "slider";
	public static final String RADIO_BUTTON     = "radio_button";
	public static final String CHECKBOX         = "checkbox";
	public static final String FREE_RESPONSE    = "free_response";
	public static final String WEB_SURVEY       = "web_survey";

	public static final Set<String> QuestionTypes = new HashSet<>(Arrays.asList(INFO_TEXT_BOX, SLIDER, RADIO_BUTTON, CHECKBOX, FREE_RESPONSE));

	public QuestionData(String id, String type, String text) {
		this.setId(id);
		if(!this.setType(type))
			if(BuildConfig.APP_IS_DEBUG) TextFileManager.getDebugLogFile().write("Invalid QuestionType: "+type);
		this.setText(text);
	}
	
	public String getId() { return id; }
	public void setId(String id) { this.id = id; }
	
	public String getType() { return type; }
	public boolean setType(String type) { this.type = type; return QuestionTypes.contains(type); }
	
	public String getText() { return text; }
	public void setText(String text) { this.text = text; }
	
	public String getAnswerString() { return answerString; }
	public void setAnswerString(String answerString) {
		this.answerString = answerString;
		if(surveyActivity!=null){surveyActivity.setButtonStatus(); surveyActivity.updateQuestionsVisibility();}
	}
	public void setAnswerStringNotUpdatingVisibility(String answerString) {
		this.answerString = answerString;
		if(surveyActivity!=null)surveyActivity.setButtonStatus();
	}

	public Integer getAnswerInteger() { return answerInteger; }
	public void setAnswerInteger(Integer answerIntegerValue) {
		this.answerInteger = answerIntegerValue;
		if(surveyActivity!=null){surveyActivity.setButtonStatus(); surveyActivity.updateQuestionsVisibility();}
	}

	public Double getAnswerDouble() {
		if(answerDouble!=null)
			return answerDouble;
		if(answerInteger!=null)
			return (double)answerInteger;
		if(answerString!=null)
			try{ return Double.parseDouble(answerString); }
			catch (Exception e) {}
		return null;
	}
//	public void setAnswerDouble(Double answerDoubleValue) { this.answerDouble = answerDoubleValue; }

	/**  @return False if the answer is null, true if an answer exists. */
	public Boolean questionIsAnswered(){ return (answerString!=null)||(answerDouble!=null)||(answerInteger!=null); }
}
