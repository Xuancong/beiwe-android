package com.moht.hopes.survey;

import android.os.Bundle;

import com.moht.hopes.utils.GS_JSON.*;

import java.util.ArrayList;

public class QuestionJSONParser {
	public static Bundle getQuestionArgsFromJSONString(JsonObject jsonQuestion) {
		Bundle args = new Bundle();
		String questionType = getStringFromJSONObject(jsonQuestion, "question_type");
		args.putString("question_type", questionType);
		if (questionType.equals(QuestionData.INFO_TEXT_BOX)) return getArgsForInfoTextBox(jsonQuestion, args);
		else if (questionType.equals(QuestionData.SLIDER)) return getArgsForSliderQuestion(jsonQuestion, args);
		else if (questionType.equals(QuestionData.RADIO_BUTTON)) return getArgsForRadioButtonQuestion(jsonQuestion, args);
		else if (questionType.equals(QuestionData.CHECKBOX)) return getArgsForCheckboxQuestion(jsonQuestion, args);
		else if (questionType.equals(QuestionData.FREE_RESPONSE)) return getArgsForFreeResponseQuestion(jsonQuestion, args);
		else if (questionType.equals(QuestionData.WEB_SURVEY)) return getArgsForWebView(jsonQuestion, args);
		return args;
	}


	// Gets and cleans the parameters necessary to create an Info Text Box
	private static Bundle getArgsForInfoTextBox(JsonObject jsonQuestion, Bundle args) {
		args.putString("question_id", getStringFromJSONObject(jsonQuestion, "question_id"));
		args.putString("question_text", getStringFromJSONObject(jsonQuestion, "question_text"));
		return args;
	}


	// Gets and cleans the parameters necessary to create an Info Text Box
	private static Bundle getArgsForWebView(JsonObject jsonQuestion, Bundle args) {
		args.putString("question_id", getStringFromJSONObject(jsonQuestion, "question_id"));
		args.putString("question_text", getStringFromJSONObject(jsonQuestion, "question_text"));
		args.putString("webview_settings", getStringFromJSONObject(jsonQuestion, "webview_settings"));
        args.putString("play_time_limits", getStringFromJSONObject(jsonQuestion, "play_time_limits"));
		return args;
	}


	// Gets and cleans the parameters necessary to create a Slider Question
	private static Bundle getArgsForSliderQuestion(JsonObject jsonQuestion, Bundle args) {
		args.putString("question_id", getStringFromJSONObject(jsonQuestion, "question_id"));
		args.putString("question_text", getStringFromJSONObject(jsonQuestion, "question_text"));
		args.putInt("min", getIntFromJSONObject(jsonQuestion, "min"));
		args.putInt("max", getIntFromJSONObject(jsonQuestion, "max"));
		return args;
	}


	// Gets and cleans the parameters necessary to create a Radio Button Question
	private static Bundle getArgsForRadioButtonQuestion(JsonObject jsonQuestion, Bundle args) {
		args.putString("question_id", getStringFromJSONObject(jsonQuestion, "question_id"));
		args.putString("question_text", getStringFromJSONObject(jsonQuestion, "question_text"));
		args.putStringArray("answers", getStringArrayFromJSONObject(jsonQuestion, "answers"));
		return args;
	}


	// Gets and cleans the parameters necessary to create a Checkbox Question
	private static Bundle getArgsForCheckboxQuestion(JsonObject jsonQuestion, Bundle args) {
		args.putString("question_id", getStringFromJSONObject(jsonQuestion, "question_id"));
		args.putString("question_text", getStringFromJSONObject(jsonQuestion, "question_text"));
		args.putStringArray("answers", getStringArrayFromJSONObject(jsonQuestion, "answers"));
		return args;
	}


	// Gets and cleans the parameters necessary to create a Free-Response Question
	private static Bundle getArgsForFreeResponseQuestion(JsonObject jsonQuestion, Bundle args) {
		args.putString("question_id", getStringFromJSONObject(jsonQuestion, "question_id"));
		args.putString("question_text", getStringFromJSONObject(jsonQuestion, "question_text"));
		args.putInt("text_field_type", getTextFieldTypeAsIntFromJSONObject(jsonQuestion, "text_field_type"));
		return args;
	}


	/**
	 * Get a String from a JsonObject key
	 *
	 * @param obj a generic JsonObject
	 * @param key the JSON key
	 * @return return an empty String instead of throwing a Exception
	 */
	private static String getStringFromJSONObject(JsonObject obj, String key) {
		try {
			return (String) obj.get(key);
		} catch (Exception e) {
			return "";
		}
	}


	/**
	 * Get an int from a JsonObject key
	 *
	 * @param obj a generic JsonObject
	 * @param key the JSON key
	 * @return return -1 instead of throwing a Exception
	 */
	private static int getIntFromJSONObject(JsonObject obj, String key) {
		try {
			return ((Long) obj.get(key)).intValue();
		} catch (Exception e) {
		}
		try {
			return (int) Long.parseLong((String) obj.get(key));
		} catch (Exception e) {
			return -1;
		}
	}


	/**
	 * Get an array of Strings from a JsonObject key
	 *
	 * @param obj a generic JsonObject
	 * @param key the JSON key
	 * @return return a one-String array instead of throwing a Exception
	 */
	public static String[] getStringArrayFromJSONObject(JsonObject obj, String key) {
		JsonArray jsonArray;
		try {
			jsonArray = (JsonArray) obj.get(key);
		} catch (Exception e1) {
			return new String[]{""};
		}
		ArrayList<String> ret = new ArrayList<String>();
		for (Object obj1 : jsonArray)
			try {
				ret.add((String) ((JsonObject) obj1).get("text"));
			} catch (Exception e) {
			}
		return ret.toArray(new String[ret.size()]);
	}


	/**
	 * Get an Enum TextFieldType from a JsonObject key
	 *
	 * @param obj a generic JsonObject
	 * @param key the JSON key
	 * @return return SINGLE_LINE_TEXT as the default instead of throwing a Exception
	 */
	private static int getTextFieldTypeAsIntFromJSONObject(JsonObject obj, String key) {
		try {
			return TextFieldType.Type.valueOf((String) obj.get(key)).ordinal();
		} catch (Exception e) {
			return TextFieldType.Type.SINGLE_LINE_TEXT.ordinal();
		}
	}

}