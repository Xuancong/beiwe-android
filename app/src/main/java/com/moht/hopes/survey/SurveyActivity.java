package com.moht.hopes.survey;

import static android.view.View.*;

import android.animation.Animator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.moht.hopes.BuildConfig;
import com.moht.hopes.networking.PostRequest;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.ui.DebugInterfaceActivity;
import com.moht.hopes.ui.user.MainMenuActivity;
import com.moht.hopes.ui.utils.SurveyNotifications;

import com.moht.hopes.R;
import com.moht.hopes.session.SessionActivity;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.utils.GS_JSON;
import com.moht.hopes.utils.GS_JSON.*;
import com.moht.hopes.utils.PlayTimeLimiter;

import java.util.ArrayList;
import java.util.Collections;

import static com.moht.hopes.survey.QuestionJSONParser.getStringArrayFromJSONObject;
import static com.moht.hopes.survey.SurveyScheduler.*;
import static com.moht.hopes.ui.DebugInterfaceActivity.printStack;
import static java.security.AccessController.getContext;

/**
 * The SurveyActivity displays to the user the survey that has been pushed to the device.
 * Layout in this activity is rendered, not static.
 *
 * @author Wang Xuancong, Josh Zagorsky, Eli Jones
 */

public class SurveyActivity extends SessionActivity {
	private String surveyId;
	private JsonSkipLogic surveySkipLogic;
	private long initialViewMoment;
	private int n_questions;
	private boolean isWebSurvey, allowNavigateBack = true, allow_navigation = true, showAllAtOnce = false;
	private LinearLayout questionContainer;
	private View [] questionViews;
	private String [] questionTypes;
	private MarkDownTextView last_msg;
	private Animation anim_fade_in, anim_in_from_left, anim_in_from_right, anim_out_to_left, anim_out_to_right;
	private Button prevButton, nextButton, submitButton;
	public SurveyRecorder surveyRecorder = null;
	public Context context;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		initialViewMoment = System.currentTimeMillis();
		context = getApplicationContext();
		if(surveyRecorder==null)
			surveyRecorder = new SurveyRecorder();
		setContentView(R.layout.activity_survey);
		prevButton = findViewById(R.id.prevButton);
		nextButton = findViewById(R.id.nextButton);
		submitButton = findViewById(R.id.submitButton);
		Intent triggerIntent = getIntent();
		surveyId = triggerIntent.getStringExtra("surveyId");
		questionContainer = findViewById(R.id.questionContainer);
		anim_fade_in = AnimationUtils.loadAnimation( this, R.anim.fade_in);
		anim_in_from_left = AnimationUtils.loadAnimation( this, R.anim.slide_in_from_left);
		anim_in_from_right = AnimationUtils.loadAnimation( this, R.anim.slide_in_from_right);
		anim_out_to_left = AnimationUtils.loadAnimation( this, R.anim.slide_out_to_left);
		anim_out_to_right = AnimationUtils.loadAnimation( this, R.anim.slide_out_to_right);

		// initialize question buffer and display the very 1st question
		QuestionData.surveyActivity = null;
		setUpQuestions(surveyId);
		initQuestions(true);
	}

	@Override
	public void onDestroy(){
		if(surveyRecorder!=null)
			surveyRecorder.appendLineToLogFile(surveyId, "", "", "Survey wipe-off close");
		for(View view : questionViews)
			if(view instanceof MarkDownTextView)
				((MarkDownTextView)view).destroy();  // otherwise, Web content will continue to run
		super.onDestroy();
		finish();
	}

	@Override
	public void onConfigurationChanged(Configuration newConfig) {
		super.onConfigurationChanged(newConfig);
		if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE)
			getSupportActionBar().hide();
		else if(newConfig.orientation == Configuration.ORIENTATION_PORTRAIT)
			getSupportActionBar().show();
	}


	@Override
	protected void doBackgroundDependentTasks() {
		super.doBackgroundDependentTasks();
	}

	void inflateQuestion(LayoutInflater inflater, int x, int visibility) {
		String question_id = surveySkipLogic.QuestionOrder.get(x);
		JsonObject args = surveySkipLogic.Questions.get(question_id);
//		Bundle args = QuestionJSONParser.getQuestionArgsFromJSONString(q);
		View questionView = createQuestion(inflater, args);
		if((boolean) (Boolean) args.getOrDefault("save_state_variable", false)
				&& surveySkipLogic.getQuestionData(question_id) != null)
			surveySkipLogic.getQuestionData(question_id).save_state_variable = true;
		if((boolean) (Boolean) args.getOrDefault("compulsory", false))
			surveySkipLogic.getQuestionData(question_id).compulsory = true;
		questionView.setVisibility(visibility);
		questionContainer.addView(questionView);
		questionViews[x] = questionView;
		questionTypes[x] = (String)args.get("question_type");
	}

	public void initQuestions(boolean isInitial){
		LayoutInflater inflater = getLayoutInflater();
		n_questions = surveySkipLogic.QuestionOrder.size();
		if(n_questions==0){
			finish();
			return;
		}
		if(isInitial) {
			questionViews = new View[n_questions];
			questionTypes = new String[n_questions];
			inflateQuestion(inflater, 0, VISIBLE);

			View questionView = questionViews[0];
			questionView.setAlpha(0f);
			questionView.animate().alpha(1f).setDuration(1000).setListener(new Animator.AnimatorListener() {
				@Override
				public void onAnimationStart(Animator animation) { }

				@Override
				public void onAnimationEnd(Animator animation) {
					initQuestions(false);
					// Record the time that the survey was first visible to the user
					surveyRecorder.recordSurveyFirstDisplayed(surveyId);
					// Onnela lab requested this line in the debug log
					QuestionData.surveyActivity = SurveyActivity.this;
					setButtonStatus();
				}

				@Override
				public void onAnimationCancel(Animator animation) {}

				@Override
				public void onAnimationRepeat(Animator animation) {}
			});
			isWebSurvey = questionTypes[0].equals(QuestionData.WEB_SURVEY);
			if(isWebSurvey) {
				activateWebview(questionView);
				setButtonStatus();
			}else if(!allowNavigateBack){
				prevButton.setVisibility(GONE);
				nextButton.setVisibility(GONE);
				submitButton.setVisibility(GONE);
			}
			return;
		}
		for (int x = 1; x < n_questions; x++)
			inflateQuestion(inflater, x, (showAllAtOnce&&surveySkipLogic.shouldQuestionDisplay(x))?VISIBLE:GONE);

		last_msg = new MarkDownTextView(this);
		last_msg.setVisibility(INVISIBLE);
		questionContainer.addView(last_msg);
	}

	public void updateVisibility(){
		for(int x=0; x<questionViews.length; ++x)
			questionViews[x].setVisibility(surveySkipLogic.shouldQuestionDisplay(x)?VISIBLE:GONE);
	}

	public void goToNextQuestion(View view) {
		if(view == findViewById(R.id.nextButton))
			surveyRecorder.appendLineToLogFile("User hit NEXT");
		View webView = questionViews[surveySkipLogic.currentQuestion];
		if(webView instanceof MarkDownTextView)
			((MarkDownTextView)webView).goForward();
		else {
			// current animate out
			int curr_i = surveySkipLogic.currentQuestion;
			int next_i = surveySkipLogic.goNextQuestion();
			View curr_view = questionViews[curr_i], next_view;
			if (next_i < n_questions) {
				next_view = questionViews[next_i];
				boolean is_web_survey = questionTypes[next_i].equals(QuestionData.WEB_SURVEY);

				if(is_web_survey) {
					DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							if(which==DialogInterface.BUTTON_POSITIVE){
								isWebSurvey = true;
								// work around stupid Android SDK bug: findViewById incorrectly returns null
								activateWebview(next_view);
								curr_view.startAnimation(anim_out_to_left);
								curr_view.setVisibility(GONE);
								setButtonStatus();
							} else {
								surveySkipLogic.currentQuestion = curr_i;
							}
						}
					};
					AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
					builder.setMessage(R.string.survey_enter_websurvey).setPositiveButton(R.string.yes, dialogClickListener)
							.setNegativeButton(R.string.not_yet, dialogClickListener).create().show();
				}
			} else {
				int n_unanswered = surveySkipLogic.getUnansweredQuestions().size();
				next_view = last_msg;
				last_msg.setContent(getString(n_unanswered > 0 ? R.string.survey_msg_incomplete : R.string.survey_msg_complete));
			}
			curr_view.startAnimation(anim_out_to_left);
			curr_view.setVisibility(GONE);
			next_view.setVisibility(VISIBLE);
			next_view.startAnimation(anim_in_from_right);
		}

		setButtonStatus();
	}

	public void goToPrevQuestion(View view) {
		if(view == findViewById(R.id.prevButton))
			surveyRecorder.appendLineToLogFile("User hit PREV");
		View webView = questionViews[surveySkipLogic.currentQuestion];
		if(webView instanceof MarkDownTextView)
			((MarkDownTextView)webView).goBack();
		else {
			// current animate out
			int curr_i = surveySkipLogic.currentQuestion;
			View curr = (curr_i < n_questions)?questionViews[curr_i]:last_msg;
			curr.startAnimation(anim_out_to_right);
			curr.setVisibility(GONE);

			// next animate in
			int prev_i = surveySkipLogic.goPrevQuestion();
			if(questionTypes[prev_i].equals(QuestionData.WEB_SURVEY))   // forbid re-entering web-survey
				surveySkipLogic.currentQuestion = prev_i = curr_i;
			View next = questionViews[prev_i];
			next.setVisibility(VISIBLE);
			next.startAnimation(anim_in_from_left);
		}

		setButtonStatus();
	}

	public void updateQuestionsVisibility(){
		for (int x = 1; x < n_questions; x++) {
			View view = questionViews[x];
			if(view == null) continue;
			if(surveySkipLogic.shouldQuestionDisplay(x)) {
				view.setVisibility(VISIBLE);
				if (view instanceof MarkDownTextView)
					activateWebview(view);
			}else{
				view.setVisibility(GONE);
			}
		}
	}

	public void setButtonStatus(){
		if(isWebSurvey){
			prevButton.setVisibility(allow_navigation?VISIBLE:GONE);
			prevButton.setEnabled(true);
			prevButton.setText("←");
			nextButton.setVisibility(allow_navigation?VISIBLE:GONE);
			nextButton.setEnabled(true);
			nextButton.setText("→");
			submitButton.setText(R.string.survey_complete_websurvey);
			submitButton.setEnabled(true);
		} else if (showAllAtOnce) {
			prevButton.setEnabled(false);
			prevButton.setVisibility(GONE);
			nextButton.setEnabled(false);
			nextButton.setVisibility(GONE);
			submitButton.setEnabled(surveySkipLogic.canSubmit());
			submitButton.setText(R.string.survey_submit_button);
			submitButton.setVisibility(VISIBLE);
		} else {
			int curr = surveySkipLogic.currentQuestion;
			prevButton.setEnabled(curr > 0);
			prevButton.setVisibility(allowNavigateBack ? VISIBLE : GONE);
			nextButton.setEnabled(curr < n_questions);
			nextButton.setText(allowNavigateBack?R.string.survey_next_button_symbol:R.string.survey_next_button_text);
			submitButton.setText(R.string.survey_submit_button);
			submitButton.setEnabled(curr == n_questions || surveySkipLogic.getUnansweredQuestions().isEmpty());
			submitButton.setVisibility(submitButton.isEnabled() || allowNavigateBack ? VISIBLE : GONE);
			nextButton.setVisibility(nextButton.isEnabled() || allowNavigateBack ? VISIBLE : GONE);
		}
	}

	@Override
	public void onBackPressed() {
		if(surveySkipLogic.currentQuestion>0 && allowNavigateBack)
			goToPrevQuestion(findViewById(R.id.prevButton));
		else
			super.onBackPressed();
	}

	private void setUpQuestions(String surveyId) {
		// Get survey settings
		Boolean randomizeWithMemory = false;
		Boolean randomize = false;
		int numberQuestions = 0;
		try {
			JsonObject <String, Object> surveySettings = (JsonObject <String, Object>) GS_JSON.parse(PersistentData.getSurveySettings(surveyId));
			allowNavigateBack = (boolean) surveySettings.getOrDefault("allow_navigate_back", true);
			showAllAtOnce = (boolean) surveySettings.getOrDefault("show_all_at_once", false);
			randomizeWithMemory = (boolean)surveySettings.getOrDefault(getString(R.string.randomizeWithMemory), false);
			randomize = (boolean)surveySettings.getOrDefault(getString(R.string.randomize), false);
			Long val = (Long)surveySettings.getOrDefault(getString(R.string.numberQuestions), 0L);
			numberQuestions = (val==null?0:(int)((long)val));
		} catch (Exception e) {
			String dbg_msg = DebugInterfaceActivity.printStack(e);
			TextFileManager.getDebugLogFile().write("Survey Activity: error parsing survey settings, "+dbg_msg);
			if(BuildConfig.APP_IS_DEBUG) Log.e("Error parsing survey settings", dbg_msg);
		}

		try { // Get survey content as an array of questions; each question is a JSON object
			ArrayList jsonQuestions = (JsonArray)GS_JSON.parse(PersistentData.getSurveyContent(surveyId));
			// If randomizing the question order, reshuffle the questions in the JsonArray
			if (randomize && !randomizeWithMemory) {
				jsonQuestions = shuffleJSONArray(jsonQuestions, numberQuestions);
			}
			if (randomize && randomizeWithMemory) {
				jsonQuestions = shuffleJSONArrayWithMemory(jsonQuestions, numberQuestions, surveyId);
			}
			//construct the survey's skip logic.
			//(param 2: If randomization is enabled do not run the skip logic for the survey.)
			surveySkipLogic = new JsonSkipLogic(jsonQuestions, !randomize, context);
		} catch (Exception ignored) {}
	}


	/**
	 * Called when the user presses "Submit" at the end of the survey,
	 * saves the answers, and takes the user back to the main page.
	 */
	public void submitButtonClicked(View view) {
		// Check whether there are more questions
		if(isWebSurvey){
			isWebSurvey = false;
			questionContainer.setVisibility(VISIBLE);
			goToNextQuestion(view);
			if(surveySkipLogic.currentQuestion < n_questions) {
				surveyRecorder.appendLineToLogFile("User hit complete");
				return;
			}
		}

		surveyRecorder.appendLineToLogFile("User hit submit");

		// Show a Toast telling the user either "Thanks, success!" or "Oops, there was an error"
		String toastMsg;
		if (surveyRecorder.writeLinesToFile(surveyId, surveySkipLogic.getQuestionsForSerialization(), initialViewMoment))
			toastMsg = PersistentData.getSurveySubmitSuccessToastText();
		else
			toastMsg = getString(R.string.survey_submit_error_message);
		surveyRecorder = null;

		Toast.makeText(context, toastMsg, Toast.LENGTH_LONG).show();

		// Close the Activity
		startActivity(new Intent(context, MainMenuActivity.class));
		deactivateSurvey(surveyId);
		SurveyNotifications.dismissNotification(context, surveyId);

		// Force data upload for getting prompt feedback
		TextFileManager.makeNewFilesForEverything();
		PostRequest.uploadAllFiles();
		finish();
	}

	private View createQuestion(LayoutInflater inflater, JsonObject args) {
		String questionID = (String) args.get("question_id");
		String questionType = (String) args.get("question_type");
		String questionText = (String) args.get("question_text");

		// Create text strings that represent the question and its answer choices
		QuestionData questionData = new QuestionData(questionID, questionType, questionText);
		surveySkipLogic.linkAnswer(questionData);

		if (questionType.equals(QuestionData.INFO_TEXT_BOX)) {
			return createInfoTextbox(inflater, questionID, questionText);
		} else if (questionType.equals(QuestionData.SLIDER)) {
			int min = Integer.parseInt((String)args.getOrDefault("min", "0"));
			int max = Integer.parseInt((String)args.getOrDefault("max", "10"));
			return createSliderQuestion(inflater, questionID, questionText, min, max);
		} else if (questionType.equals(QuestionData.RADIO_BUTTON)) {
			String[] answers = getStringArrayFromJSONObject(args, "answers");
			return createRadioButtonQuestion(inflater, questionID, questionText, answers);
		} else if (questionType.equals(QuestionData.CHECKBOX)) {
			String[] answers = getStringArrayFromJSONObject(args, "answers");
			return createCheckboxQuestion(inflater, questionID, questionText, answers);
		} else if (questionType.equals(QuestionData.FREE_RESPONSE)) {
			TextFieldType.Type textFieldType = TextFieldType.Type.valueOf((String)args.get("text_field_type"));
			return createFreeResponseQuestion(inflater, questionID, questionText, textFieldType);
		} else if (questionType.equals(QuestionData.WEB_SURVEY)) {
			return createWebSurvey(inflater, questionID, args);
		}
		// Default: return an error message
		return new LinearLayout(context);
	}


	private View createWebSurvey(LayoutInflater inflater, String questionID, JsonObject obj) {
		View layout = new MarkDownTextView(context);
		layout.setTag(obj);
		return layout;
	}


	/**
	 * Creates an informational text view that does not have an answer type
	 *
	 * @param infoText The informational text
	 * @return TextView (to be displayed as question text)
	 */
	private LinearLayout createInfoTextbox(LayoutInflater inflater, String questionID, String infoText) {
		LinearLayout layout = (LinearLayout)inflater.inflate(R.layout.survey_info_textbox, null);
		MarkDownTextView infoTextbox = layout.findViewById(R.id.surveyWebview);

		// Set the question text to the error string
		if (infoText == null)
			infoText = getResources().getString(R.string.question_error_text);

		infoTextbox.setContent(infoText);
		surveySkipLogic.getQuestionData(questionID).setAnswerString("");    // mark as answered
		return layout;
	}


	/**
	 * Creates a slider with a range of discrete values
	 *
	 * @param questionText The text of the question to be asked
	 * @return LinearLayout A slider bar
	 */
	private LinearLayout createSliderQuestion(LayoutInflater inflater, String questionID,
	                                          String questionText, int min, int max) {

		LinearLayout question = (LinearLayout) inflater.inflate(R.layout.survey_slider_question, null);
		SeekBarEditableThumb slider = question.findViewById(R.id.slider);

		// Set the text of the question itself
		MarkDownTextView questionTextView = question.findViewById(R.id.questionText);
		if (questionText != null)
			questionTextView.setContent(questionText);

		// The min must be less than the max
		if (min > max) {
			int tmp = max;
			max = min;
			min = tmp;
		}

		// Set the slider's range and default/starting value
		slider.setMax(max - min);
		slider.setMin(0);

		makeSliderInvisibleUntilTouched(slider);

		// Add a label above the slider with numbers that mark points on a scale
		addNumbersLabelingToSlider(inflater, question, min, max);

		// Set the slider to listen for and record user input
		slider.setOnSeekBarChangeListener(new SliderListener(surveySkipLogic.getQuestionData(questionID), question, min));
		question.clearFocus();

		return question;
	}


	/**
	 * Creates a group of radio buttons
	 *
	 * @param questionText The text of the question
	 * @param answers      An array of strings that are options matched with radio buttons
	 * @return RadioGroup A vertical set of radio buttons
	 */
	private LinearLayout createRadioButtonQuestion(LayoutInflater inflater, String questionID, String questionText, String[] answers) {

		LinearLayout question = (LinearLayout) inflater.inflate(R.layout.survey_radio_button_question, null);
		RadioGroupPlus radioGroup = question.findViewById(R.id.radioGroupPlus);

		// Set the text of the question itself
		MarkDownTextView questionTextView = question.findViewById(R.id.questionText);
		if (questionText != null)
			questionTextView.setContent(questionText);

		// If the array of answers is null or too short, replace it with an error message
		if ((answers == null) || (answers.length < 2)) {
			String replacementAnswer = getResources().getString(R.string.question_error_text);
			answers = new String[]{replacementAnswer, replacementAnswer};
		}

		// Loop through the answer strings, and make each one a radio button option
		for (int i = 0; i < answers.length; i++) {
			LinearLayout radioButton = (LinearLayout) inflater.inflate(R.layout.survey_radio_button, null);
			((RadioButton)radioButton.findViewById(R.id.radio0)).setId(i+1);
			if (answers[i] != null)
				((MarkDownTextView)radioButton.findViewById(R.id.content0)).setContent(answers[i]);
			radioGroup.addView(radioButton);
		}

		// Set the group of radio buttons to listen for and record user input
		radioGroup.setOnCheckedChangeListener(new RadioButtonListener(surveySkipLogic.getQuestionData(questionID), answers.length));

		return question;
	}


	/**
	 * Creates a question with an array of checkboxes
	 *
	 * @param questionText The text of the question
	 * @param options      Each string in options[] will caption one checkbox
	 * @return LinearLayout a question with a list of checkboxes
	 */
	private LinearLayout createCheckboxQuestion(LayoutInflater inflater, String questionID, String questionText, String[] options) {

		LinearLayout question = (LinearLayout) inflater.inflate(R.layout.survey_checkbox_question, null);
		LinearLayout checkboxesList = question.findViewById(R.id.checkboxesList);

		// Set the text of the question itself
		MarkDownTextView questionTextView = question.findViewById(R.id.questionText);
		if (questionText != null)
			questionTextView.setContent(questionText);

		// Checkbox question does not require any click to mark as answered, the default answer is none
		QuestionData questionData = surveySkipLogic.getQuestionData(questionID);
		questionData.setAnswerString("");

		// Loop through the options strings, and make each one a checkbox option
		if (options != null) {
			for (int i = 0; i < options.length; i++) {

				// Inflate the checkbox horizontal layout from an XML layout file
				LinearLayout checkbox_layout = (LinearLayout) inflater.inflate(R.layout.survey_checkbox, null);

				// Make the checkbox listen for and record user input
				CheckBox checkbox = checkbox_layout.findViewById(R.id.check0);
				checkbox.setId(i+1);
				checkbox.setOnClickListener(new CheckboxListener(questionData, checkboxesList));

				// Set the content in the 2nd view if provided; otherwise leave text as default error message
				if (options[i] != null)
					((MarkDownTextView)checkbox_layout.findViewById(R.id.content0)).setContent(options[i]);

				// Add the checkbox horizontal layout to the list of checkboxes
				checkboxesList.addView(checkbox_layout);
			}
		}

		return question;
	}


	/**
	 * Creates a question with an open-response, text-input field
	 *
	 * @param questionText  The text of the question
	 * @param inputTextType The type of answer (number, text, etc.)
	 * @return LinearLayout question and answer
	 */
	private LinearLayout createFreeResponseQuestion(LayoutInflater inflater, String questionID,
	                                                String questionText, TextFieldType.Type inputTextType) {
		//TODO: Josh. Give open response questions autofocus and make the keyboard appear
		LinearLayout question = (LinearLayout) inflater.inflate(R.layout.survey_open_response_question, null);

		// Set the text of the question itself
		MarkDownTextView questionTextView = question.findViewById(R.id.questionText);
		if (questionText != null) {
			questionTextView.setContent(questionText);
		}

		EditText editText;
		switch (inputTextType) {
			case NUMERIC:
				editText = (EditText) inflater.inflate(R.layout.survey_free_number_input, null);
				break;

			case MULTI_LINE_TEXT:
				editText = (EditText) inflater.inflate(R.layout.survey_multiline_text_input, null);
				break;

			case SINGLE_LINE_TEXT:

			default:
				editText = (EditText) inflater.inflate(R.layout.survey_free_text_input, null);
				break;
		}

		// Set the text field to listen for and record user input
		editText.setOnFocusChangeListener(new OpenResponseListener(surveySkipLogic.getQuestionData(questionID), context, this));

		LinearLayout textFieldContainer = question.findViewById(R.id.textFieldContainer);
		textFieldContainer.addView(editText);

		return question;
	}


	/**
	 * Adds a numeric scale above a Slider Question
	 *
	 * @param question the Slider Question that needs a number scale
	 * @param min      the lowest number on the scale
	 * @param max      the highest number on the scale
	 */
	private void addNumbersLabelingToSlider(LayoutInflater inflater, LinearLayout question, int min, int max) {
		// Replace the numbers label placeholder view (based on http://stackoverflow.com/a/3760027)
		View numbersLabel = question.findViewById(R.id.numbersPlaceholder);
		int index = question.indexOfChild(numbersLabel);
		question.removeView(numbersLabel);
		numbersLabel = inflater.inflate(R.layout.survey_slider_numbers_label, question, false);
		LinearLayout label = numbersLabel.findViewById(R.id.linearLayoutNumbers);

		/* Decide the number of labels. Pick the highest number of labels that can be achieved with each label
		    above an integer value, and even spacing between all labels. */
		int range = max - min, numberOfLabels = 6;
		for (; numberOfLabels > 1; numberOfLabels--) {
			if (range % numberOfLabels == 0) break;
		}
		numberOfLabels++;

		// Create labels and spacers
		int numberResourceID = R.layout.survey_slider_single_number_label;
		for (int i = 0; i < numberOfLabels - 1; i++) {
			TextView number = (TextView) inflater.inflate(numberResourceID, label, false);
			label.addView(number);
			number.setText("" + (min + (i * range) / (numberOfLabels - 1)));

			View spacer = inflater.inflate(R.layout.horizontal_spacer, label, false);
			label.addView(spacer);
		}
		// Create one last label (the rightmost one) without a spacer to its right
		TextView number = (TextView) inflater.inflate(numberResourceID, label, false);
		label.addView(number);
		number.setText("" + max);

		// Add the set of numeric labels to the question
		question.addView(numbersLabel, index);
	}


	/**
	 * Make the "thumb" (the round circle/progress knob) of a Slider almost
	 * invisible until the user touches it.  This way the user is forced to
	 * answer every slider question; otherwise, we would not be able to tell
	 * the difference between a user ignoring a slider and a user choosing to
	 * leave a slider at the default value.  This makes it like there is no
	 * default value.
	 *
	 * @param slider
	 */
	@SuppressLint("ClickableViewAccessibility")
	private void makeSliderInvisibleUntilTouched(SeekBarEditableThumb slider) {
		// Before the user has touched the slider, make the "thumb" transparent/ almost invisible
		/* Note: this works well on Android 4; there's a weird bug on Android 2 in which the first
		 * slider question in the survey sometimes appears with a black thumb (once you touch it,
		 * it turns into a white thumb). */
		slider.markAsUntouched();
		slider.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				// When the user touches the slider, make the "thumb" opaque and fully visible
				SeekBarEditableThumb slider = (SeekBarEditableThumb) v;
				slider.markAsTouched();
				return false;
			}
		});
	}


	/**************************** ANSWER LISTENERS ***************************/

	/**
	 * Listens for a touch/answer to a Slider Question, and records the answer
	 */
	private class SliderListener implements SeekBar.OnSeekBarChangeListener {

		QuestionData questionData;
		View view;
		int min;

		public SliderListener(QuestionData questionData, View view, int min) {
			this.questionData = questionData;
			this.view = view;
			this.min = min;
		}

		@Override
		public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
			((TextView) view.findViewById(R.id.currentNumbersDisplay)).setText("" + (progress + min));
		}

		@Override
		public void onStartTrackingTouch(SeekBar seekBar) {
			/* Improvement idea: record when the user started touching the
			 * slider bar, not just when they let go of it */
		}

		@Override
		public void onStopTrackingTouch(SeekBar seekBar) {
			int answer;
			if (seekBar instanceof SeekBarEditableThumb) {
				SeekBarEditableThumb slider = (SeekBarEditableThumb) seekBar;
				answer = slider.getProgress() + min;
			} else {
				answer = seekBar.getProgress();
			}
			questionData.setAnswerInteger(answer);
			surveyRecorder.recordAnswerTiming(""+answer, questionData);
		}
	}


	/**
	 * Listens for a touch/answer to a Radio Button Question, and records the answer
	 */
	private class RadioButtonListener implements RadioGroupPlus.OnCheckedChangeListener {

		QuestionData questionData;
		int N_options;

		public RadioButtonListener(QuestionData questionData, int n_options) {
			this.questionData = questionData;
			this.N_options = n_options;
		}

		@Override
		public void onCheckedChanged(RadioGroupPlus group, int checkedId) {
			RadioButton selectedButton = group.findViewById(checkedId);
			if (selectedButton.isChecked())
				surveyRecorder.recordAnswerTiming(selectedButton.getId()+"", questionData);
			if(checkedId > N_options)
				TextFileManager.getDebugLogFile().write("Survey Error: RadioButtonListener::onCheckedChanged() checkedId="
					+checkedId+" N_options="+N_options);
			questionData.setAnswerInteger(checkedId);
		}
	}


	/**
	 * Listens for a touch/answer to a Checkbox Question, and records the answer
	 */
	private class CheckboxListener implements View.OnClickListener {

		QuestionData questionData;
		ViewGroup root;

		public CheckboxListener(QuestionData questionData, ViewGroup root) {
			this.questionData = questionData;
			this.root = root;
		}

		@Override
		public void onClick(View view) {
			String answersList = SurveyRecorder.getSelectedCheckboxes(root);
			surveyRecorder.recordAnswerTiming(answersList, questionData);
			questionData.setAnswerString(answersList);
			long answerInt = SurveyRecorder.getSelectedCheckboxesLong(root);
			questionData.setAnswerInteger((int)answerInt);
		}
	}


	/**
	 * Listens for an input/answer to an Open/Free Response Question, and records the answer
	 */
	private class OpenResponseListener implements View.OnFocusChangeListener {

		QuestionData questionData;
		Context context;
		Activity activity;

		public OpenResponseListener(QuestionData questionData, Context context, Activity activity) {
			this.questionData = questionData;
			this.context = context;
			this.activity = activity;
		}

		// TODO: Josh. replace this with a listener on the Next button; that'd probably make more sense
		@Override
		public void onFocusChange(View v, boolean hasFocus) {
			if (hasFocus) { // The user just selected the input box
				/* Improvement idea: record when the user first touched the
				 * input field; right now it only records when the user
				 * selected away from the input field. */

				// Set the EditText so that if the user taps outside, the keyboard disappears
				if (v instanceof EditText) {
					TextFieldKeyboard keyboard;
					try {
						keyboard = new TextFieldKeyboard(context);
					} catch (NoSuchMethodError e) {
						keyboard = new TextFieldKeyboard(activity);
					}
					keyboard.makeKeyboardBehave((EditText) v);
				}
			} else {
				// The user just selected away from the input box
				if (v instanceof EditText) {
					EditText textField = (EditText) v;
					String answer = textField.getText().toString();
					surveyRecorder.recordAnswerTiming(answer, questionData);
					questionData.setAnswerString(answer);
				}
			}
		}
	}


	// WebView timeout control (for game addition prevention)
	public static PlayTimeLimiter timer = null;
	public static boolean is_alert_showing = false;

	@Override
	protected void onPause (){
		super.onPause();
		if(timer!=null)
			timer.pause();
	}

	@Override
	protected void onResume (){
		super.onResume();
		if(timer!=null)
			timer.resume();
	}

	void activateWebview(View view){
		JsonObject obj = (JsonObject)view.getTag();
		String url = (String)obj.getOrDefault("question_text", "");
		String question_id = (String)obj.getOrDefault("question_id", "");
		QuestionData questionData = surveySkipLogic.getQuestionData(question_id);
		if(questionData.questionIsAnswered()) return;

		boolean isURL = url.startsWith("http");
		if (isURL && url.contains("\n")) { // randomly select one if contains multiple URLs
			String[] urls = url.split("\n");
			ArrayList<String> arr = new ArrayList<String>();
			for (String s : urls)
				if (s.startsWith("http"))
					arr.add(s);
			Collections.shuffle(arr);
			url = arr.get(0);
		}
		questionData.setAnswerStringNotUpdatingVisibility(isURL?url:("HTML content hash "+url.hashCode()));
		allow_navigation = (Boolean) obj.getOrDefault("allow_navigation", true);
		String webview_settings = (String)obj.getOrDefault("webview_settings", "{}");
		String play_time_limits = (String)obj.getOrDefault("play_time_limits", "");
		timer = PlayTimeLimiter.CreateTimeLimiter(play_time_limits, 10, isURL?url:(""+url.hashCode()));

		if(timer!=null) {
			timer.onCheck = new Runnable(){
				@Override
				public void run() {
					if(!is_alert_showing) {
						is_alert_showing = true;
						timer.pause();
						new AlertDialog.Builder(SurveyActivity.this)
								.setTitle(R.string.survey_time_limit_title)
								.setMessage(R.string.survey_time_limit_msg)
								.setIcon(R.drawable.ic_timeup_alarm)
								.setCancelable(false)
								.setNeutralButton("OK", new DialogInterface.OnClickListener() {
									public void onClick(DialogInterface dialog, int which) {
										is_alert_showing = false;
										timer.resume();
									}
								}).create().show();
					}
				}
			};
			if(timer.isTimeout()) {
				timer.onCheck.run();
				return;
			}
		}

		MarkDownTextView webView = (MarkDownTextView)view;
		webView.startAnimation(anim_fade_in);
		if(isURL)
			webView.setURL(url, webview_settings);
		else
			webView.loadData(url, "text/html; charset=utf-8", "UTF-8");

		if(timer!=null && timer.time_limits_sec[0]>0)
			timer.start();

		boolean track_event = (Boolean) obj.getOrDefault("track_event", false);
		webView.surveyRecorder = track_event ? surveyRecorder : null;

		DisplayMetrics displayMetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
		int screen_height = displayMetrics.heightPixels;
		view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, screen_height));
	}

}
