package com.moht.hopes.survey;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import com.moht.hopes.storage.TextFileManager;
import static com.moht.hopes.storage.TextFileManager.DELIMITER;

import java.util.List;

public class SurveyRecorder {
	public static final String name = "survey";
	public static final String short_name = "svy";
	public static final String header = "timestamp,survey_id,question_id,question_type,answer";


	/**Create a line (that will get written to a CSV file) that includes
	 * question metadata and the user's answer
	 * @param questionData metadata on the question
	 * @return a String that can be written as a line to a file */
	private String answerFileLine(QuestionData questionData, String qid_override) {
		String line = System.currentTimeMillis() + DELIMITER + qid_override + DELIMITER;
		line += sanitizeString(questionData.getId()) + DELIMITER;
		line += sanitizeString(questionData.getType()) + DELIMITER;
		if (questionData.getAnswerString() != null)
			line += sanitizeString(questionData.getAnswerString());
		else if (questionData.getAnswerInteger() != null)
			line += (int)questionData.getAnswerInteger();
		else if (questionData.getAnswerDouble() != null)
			line += (double)questionData.getAnswerDouble();
		else
			line += "NO_ANSWER";

		return line;
	}

	
	/** Create a new SurveyAnswers file, and write all of the answers to it
	 * @return TRUE if wrote successfully; FALSE if caught an exception */
	public Boolean writeLinesToFile(String surveyId, List<QuestionData> answers, long initialViewMoment) {
		try {
			appendLineToLogFile(surveyId, "initialViewMoment", "", initialViewMoment+"");
			for (QuestionData answer : answers)
				TextFileManager.getFile(this).write(answerFileLine(answer, ""));
			for (QuestionData answer : answers)
				if(answer.save_state_variable)
					TextFileManager.getFile(this).write(answerFileLine(answer, "save-state-variable"));
			TextFileManager.getFile(this).closeFile();
			return true;
		} catch (Exception e) {
			TextFileManager.getDebugLogFile().write(e.toString());
			return false;
		}
	}


	/**
	 * Return a list of the selected checkboxes in a list of checkboxes
	 * @param view a LinearLayout, presumably containing only checkboxes
	 * @return a String formatted like a String[] printed to a single String
	 */
	public static String getSelectedCheckboxes(View view) {

		// Make a list of the checked answers that reads like a printed array of strings
		String answers = "";

		if (view instanceof CheckBox) {
			CheckBox checkBox = (CheckBox) view;
			answers += checkBox.isChecked()?checkBox.getId():"";
		} else if (view instanceof ViewGroup) {
			ViewGroup viewGroup = (ViewGroup)view;

			// Iterate over the whole list of CheckBoxes in this LinearLayout
			for (int i = 0; i < viewGroup.getChildCount(); i++) {
				String answer = getSelectedCheckboxes(viewGroup.getChildAt(i));
				answers += ((!answers.isEmpty()&&!answer.isEmpty()) ? " " : "") + answer;
			}
		}

		return answers;
	}

	public static long getSelectedCheckboxesLong(View view) {
		return getSelectedCheckboxesLong(view, 1L);
	}

	public static long getSelectedCheckboxesLong(View view, long val) {
		if (view instanceof CheckBox) {
			CheckBox checkBox = (CheckBox) view;
			return checkBox.isChecked()?val:0;
		} else if (view instanceof ViewGroup) {
			// Make an integer with each bit corresponding to the a corresponding checkbox
			long answers = 0L, bit = val;
			ViewGroup viewGroup = (ViewGroup)view;

			// Iterate over the whole list of CheckBoxes in this LinearLayout
			for (int i = 0; i < viewGroup.getChildCount(); i++, bit<<=1)
				answers |= getSelectedCheckboxesLong(viewGroup.getChildAt(i), bit);
			return answers;
		}
		return 0L;
	}

	// Below are for timing recorder

	/**Create a new Survey Response file, and record the timestamp of when
	 * the survey first displayed to the user */
	public void recordSurveyFirstDisplayed(String surveyId) {
		/* In the unlikely event that the user starts one survey, doesn't finish it, and starts a
		 * second survey, a new surveyTimingsFile should be created when the second survey is
		 * started, so hopefully there will never be more than one survey associated with a
		 * surveyTimingsFile. There should be no mechanism that allows a user to switch back and
		 * forth from one open survey to another, since there can be only one SurveyActivity. */

		appendLineToLogFile(surveyId, "", "", "Survey finished loading and displayed to user");
	}


	/**
	 * Record (in the Survey Response file) the answer to a single survey question
	 * @param answer the user's input answer
	 * @param questionData the question that was answered
	 */
	public void recordAnswerTiming(String answer, QuestionData questionData) {
		appendLineToLogFile("", questionData.getId(), questionData.getType(), sanitizeString(answer));
	}

	/**
	 * Write a line to the bottom of the Survey Response file
	 * @param message
	 */
	public void appendLineToLogFile(String message) {
		/** Handles the logging, includes a new line for the CSV files.
		 * This code is otherwise reused everywhere.*/
		appendLineToLogFile("", "", "", message);
	}

	public void appendLineToLogFile(String survey_id, String question_id, String question_type, String message) {
		/** Handles the logging, includes a new line for the CSV files.
		 * This code is otherwise reused everywhere.*/
		String line = System.currentTimeMillis() + DELIMITER + survey_id + DELIMITER + question_id + DELIMITER
				+ question_type + DELIMITER + message;
		TextFileManager.getFile(this).write(line);
	}


	/**
	 * Sanitize a string for use in a Tab-Separated Values file
	 * @param input string to be sanitized
	 * @return String with tabs and newlines removed
	 */
	public static String sanitizeString(String input) {
		input = input.replaceAll("[\t\n\r]", "  ");
		// Replace all commas in the text with semicolons, because commas are the delimiters
		input = input.replaceAll(",", ";");
		return input;
	}
}
