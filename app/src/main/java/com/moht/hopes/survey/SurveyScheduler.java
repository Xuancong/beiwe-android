package com.moht.hopes.survey;

import android.content.Context;
import android.content.Intent;
import android.os.Debug;
import android.util.Log;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.CrashHandler;

import com.moht.hopes.R;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.utils.GS_JSON;
import com.moht.hopes.utils.GS_JSON.*;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Random;

//TODO: Low priority. Eli. document.
/**I am ... 85% certain time zones work like this:
When a survey trigger is scheduled, that event is in terms of relative time, i.e. "in 3 hours do a thing."
That relative-time event is (pretty sure) calculated using a time-zone aware entity.
The result is that any currently-waiting event triggers will not be changed by a time zone transition, but any new survey trigger calculations will be in the current (new) timezone.
*/

public class SurveyScheduler {

	public static Object getSurveySettingOption(String surveyId, String optionName, Object defaultVal) {
		JsonObject surveySettings;
		try { surveySettings = (JsonObject)GS_JSON.parse( PersistentData.getSurveySettings(surveyId) ); }
		catch (Exception e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e("SurveyScheduler", "There was an error parsing survey settings");
			CrashHandler.writeCrashlog(e);
			surveySettings = new JsonObject();
		}
		Object ret = surveySettings.getOrDefault(optionName, defaultVal);
		return ret;
	}

	public static boolean isSurveyPushType( String surveyID ){
		try {
			boolean trigger_on_first_download = (boolean) getSurveySettingOption(surveyID, BackgroundService.appContext.getString(R.string.triggeredSurvey), false);
			String timing = PersistentData.getSurveyTimes(surveyID);
			if(!trigger_on_first_download && timing.replace(" ", "").equals("[[],[],[],[],[],[],[]]"))
				return true;
		}catch (Exception e){
			if(Debug.isDebuggerConnected())
				throw new RuntimeException("DEBUG fatal: isSurveyPushType()");
		}
		return false;
	}

	public static void deactivateSurvey( String surveyId ){
		if(isSurveyPushType(surveyId))
			PersistentData.deleteSurvey(surveyId);
		else
			PersistentData.setSurveyNotificationState(surveyId, false);
	}

	public static void checkImmediateTriggerSurvey(String surveyId) {
		if((boolean) getSurveySettingOption(surveyId, BackgroundService.appContext.getString(R.string.triggeredSurvey), false)
			|| isSurveyPushType(surveyId))
			BackgroundService.appContext.sendBroadcast(new Intent(surveyId));
	}
	
	public static void scheduleSurvey(String surveyId) {
		int today;
		JsonArray JSONTimes;
		JsonArray dayJSON;
		ArrayList<Long> dayInts;
		ArrayList<ArrayList<Long>> timesList = new ArrayList<ArrayList<Long>>(7);
		String timeString = PersistentData.getSurveyTimes(surveyId);
		
		Calendar thisDay = Calendar.getInstance();
		//turns out the first day of the week is not necessarily Sunday. So, in case the user is in such a Locale we manually set that.
		thisDay.setFirstDayOfWeek(Calendar.SUNDAY);
		today = thisDay.get(Calendar.DAY_OF_WEEK) - 1;
		
		try {
			JSONTimes = (JsonArray) GS_JSON.parse(Objects.requireNonNull(timeString));
		} catch (Exception e) { //If this fails we have significant problems, but probably the errors come from external factors.
			if(BuildConfig.APP_IS_DEBUG) Log.e("Fatal error", e.toString());
			TextFileManager.getDebugLogFile().write(e.toString());
			return;
		}
		ArrayList jsonDays = (ArrayList)JSONTimes.clone();
		
		//create a list of days of the week, with order like this:
		// 0: today, 1: tomorrow, 2: day after ...
		ArrayList reorderedDays = new ArrayList();
		reorderedDays.addAll( jsonDays.subList( today, jsonDays.size() ) );
		reorderedDays.addAll( jsonDays.subList(0, today) );
		
		for (int i=0; i < 7; i++) {
			try { dayJSON = (JsonArray)reorderedDays.get(i); }  //convert to json array
			catch (Exception e) { //Again, if this crashes we have problems but they probably come from external factors.
				throw new NullPointerException(e.getMessage()); }
			dayInts = (ArrayList<Long>)dayJSON; //convert to (iteraable) list of ints
			Collections.sort(dayInts); //ensure sorted because... because.
			timesList.add( dayInts );
		}

		// we now have a double nested list of lists.  element 0 is today, element 1 is tomorrow
		// the inner list contains the times of day at which the survey should trigger, these times are values between 0 and 86,400
		// these values are should be sorted.
		// these values indicate the "seconds past midnight" of the day that the alarm should trigger.
		// we iterate through the nested list to come to the next time that is after right now.
		Calendar newAlarmTime = findNextAlarmTime(timesList);
		if (newAlarmTime == null)
			return;
		BackgroundService.setSurveyAlarm(surveyId, newAlarmTime);
	}
	
	private static Calendar findNextAlarmTime( ArrayList<ArrayList<Long>> timesList) {
		Calendar now = Calendar.getInstance();
		Calendar possibleAlarmTime;
		Calendar firstPossibleAlarmTime = null;
		boolean firstFlag = true;
		int days = 0;
		for ( ArrayList<Long> day : timesList ) { //iterate through the days of the week
			for (long time : day) { //iterate through the times in each day
				if (time > 86400 || time < 0) { throw new NullPointerException("time parser received an invalid value in the time parsing: " + time); }
				possibleAlarmTime = getTimeFromStartOfDayOffset((int)time);
				if (firstFlag) { //grab the first time we come across in case it falls into the edge case.
					firstPossibleAlarmTime = (Calendar) possibleAlarmTime.clone();
					firstFlag = false;
				}
				possibleAlarmTime.add(Calendar.DATE, days); //add to this time the appropriate number of days
				if ( possibleAlarmTime.after( now ) ) { //If the time is in the future, return that time.
					return possibleAlarmTime;
				}
			}
			days++; //advance to next day...
		}
		/* Warning: for some reason... if you try to throw a null pointer exception in here the hopes freezes. */
		//TODO: Low priority. Eli/Josh.  determine why the hopes stalls when nullpointerexceptions are thrown on... non gui threads?  insert a null pointer exception here and comment out the remainer of the function to see what I am talking about.
//		throw new NullPointerException("totally arbitrary message");
		if (firstPossibleAlarmTime == null) { return null; }
		firstPossibleAlarmTime.add(Calendar.DATE, 7);  // advance the date to the following week.
		return firstPossibleAlarmTime;
	}
	
	private static Calendar getTimeFromStartOfDayOffset(int offset) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, offset / 3600 ); //seconds divided by seconds per hour yields hour of day (this is the 24-hour time of day
//		calendar.set(Calendar.HOUR, offset / 3600 ); //do not set this value, it will override the hour_of_day value and do it incorrectly.
		calendar.set(Calendar.MINUTE, offset / 60 % 60); //seconds divided by sixty mod sixty yields minutes
		calendar.set(Calendar.SECOND, offset % 60); //seconds mod 60 yields seconds into minute
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.setFirstDayOfWeek(Calendar.SUNDAY);
		return calendar;
	}

	public static Calendar getTimeFromStartOfDayOffset(int hour, int minute, int second) {
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.HOUR_OF_DAY, hour ); //seconds divided by seconds per hour yields hour of day (this is the 24-hour time of day
//		calendar.set(Calendar.HOUR, offset / 3600 ); //do not set this value, it will override the hour_of_day value and do it incorrectly.
		calendar.set(Calendar.MINUTE, minute); //seconds divided by sixty mod sixty yields minutes
		calendar.set(Calendar.SECOND, second); //seconds mod 60 yields seconds into minute
		calendar.set(Calendar.MILLISECOND, 0);
		calendar.setFirstDayOfWeek(Calendar.SUNDAY);
		return calendar;
	}

	public static Calendar getTimeFromNow(int second) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(calendar.getTimeInMillis()+second*1000);
		return calendar;
	}

	public static ArrayList shuffleJSONArray(ArrayList jsonArray, int numberElements) {
		ArrayList javaList = (ArrayList)jsonArray.clone();
		Collections.shuffle(javaList, new Random(System.currentTimeMillis()) );
		//if length supplied is 0 or greater than number of elements...
		return (numberElements == 0 || numberElements > javaList.size()) ? javaList : new ArrayList(javaList.subList(0, numberElements));
	}

	public static ArrayList shuffleJSONArrayWithMemory(ArrayList jsonArray, int numberElements, String surveyId) {
		ArrayList<JsonObject> javaQuestionsList = (ArrayList)jsonArray.clone();
		Collections.shuffle(javaQuestionsList, new Random(System.currentTimeMillis()) );
		ArrayList<String> returnList = new ArrayList<>(numberElements);
		ArrayList<String> existingQuestionIds = PersistentData.getSurveyQuestionMemory(surveyId);

		String questionId;

		for (JsonObject questionJSON : javaQuestionsList) {
			questionId = (String)questionJSON.get("question_id");
			if (questionId == null) {
				TextFileManager.getDebugLogFile().write(
						"JSONUtils::shuffleJSONArrayWithMemory: question_id does not exist in question...");
				throw new NullPointerException("question_id does not exist in question..."); }

			if (!existingQuestionIds.contains(questionId)) {
				PersistentData.addSurveyQuestionMemory(surveyId, questionId);
				returnList.add(questionJSON.getSourceString());
				if (returnList.size() == numberElements) { break; }
			}
		}

		if (returnList.size() == 0 && javaQuestionsList.size() > 0) {
			PersistentData.clearSurveyQuestionMemory(surveyId);
			return shuffleJSONArrayWithMemory(jsonArray, numberElements, surveyId);
		}

		return returnList;
	}
}