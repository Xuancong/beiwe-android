package com.moht.hopes.tasks;

import static com.moht.hopes.networking.PostRequest.addWebsitePrefix;
import static com.moht.hopes.networking.PostRequest.setupHTTP;
import static com.moht.hopes.survey.SurveyScheduler.getTimeFromNow;
import static com.moht.hopes.survey.SurveyScheduler.getTimeFromStartOfDayOffset;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;

import androidx.core.app.NotificationCompat;

import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.moht.hopes.BackgroundService;
import com.moht.hopes.DeviceInfo;
import com.moht.hopes.R;
import com.moht.hopes.networking.PostRequest;
import com.moht.hopes.networking.SurveyDownloader;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.survey.SurveyActivity;
import com.moht.hopes.survey.SurveyScheduler;
import com.moht.hopes.ui.user.MainMenuActivity;

import java.net.URL;
import java.util.Calendar;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

public class FCMService extends FirebaseMessagingService {
	private static final String TAG = "FCMService";
	private static final String PUSH_NOTIFICATION_CHANNEL_ID = "push_notification_channel";

	@Override
	public void onNewToken(String token) {
		super.onNewToken(token);
		String old_token = PersistentData.getString(PersistentData.FCM_token, "");
		if(old_token.equals(token)) return;

		// New token obtained
		PersistentData.setString(PersistentData.FCM_token, token);
		if(PersistentData.isRegistered())
			update_server_new_token();
	}

	public void update_server_new_token(){
		// Inform the server on the updated token
		String parameters = PostRequest.makeParameter("os_version", DeviceInfo.getAndroidVersion()) +
				PostRequest.makeParameter("hardware_id", DeviceInfo.getHardwareId()) +
				PostRequest.makeParameter("fcm_token", DeviceInfo.getFCMtoken()) +
				PostRequest.makeParameter("beiwe_version", DeviceInfo.getBeiweVersion());
		String url = addWebsitePrefix(getApplicationContext().getString(R.string.request_otp_url));
		boolean success = false;
		try {
			HttpsURLConnection connection = setupHTTP(parameters, new URL(url), PersistentData.getPassword());
			success = connection.getResponseCode()==200;
		} catch (Exception e) { }
		PersistentData.setInteger("fcm_sync", success?1:0);
	}

	@Override
	public void onMessageReceived(RemoteMessage remoteMessage) {
		super.onMessageReceived(remoteMessage);
		Map <String,String> push_info = remoteMessage.getData();
		try {
			String type = push_info.get("type");
			Context appContext = getApplicationContext();
			if(type.equals("notify-only"))
				displayPushNotification(appContext, push_info);
			else if(type.equals("update-survey"))
				SurveyDownloader.downloadSurveys(appContext);
		}catch (Exception e){}
	}

	public static String getToken() {
		String token = PersistentData.getString(PersistentData.FCM_token, "");
		if(token.isEmpty()){
			Task<String> task = FirebaseMessaging.getInstance().getToken();
			try{
				for(int x=0; x<10 && token.isEmpty(); ++x){
					Thread.sleep(1000);
					token = task.getResult();
				}
			} catch(Exception e) {}

			if(!token.isEmpty())
				PersistentData.setString(PersistentData.FCM_token, token);
		}
		return token;
	}

	public static void displayPushNotification(Context appContext, Map <String, String> push_info) {
		if(!PersistentData.isRegistered() && !PersistentData.isUnregisterDebugMode) return;

		// Check whether the survey notification has expired before shown
		String expiryString = PersistentData.getSurveyExpiry(push_info.getOrDefault("expiry", ""));
		Calendar expire_tms = null;
		if(!expiryString.isEmpty()) {
			try {
				String[] expiry = expiryString.split(":");
				expire_tms = (expiry.length < 3 ? getTimeFromNow(Integer.parseInt(expiry[0])) :
						getTimeFromStartOfDayOffset(Integer.parseInt(expiry[0]), Integer.parseInt(expiry[1]), Integer.parseInt(expiry[2])));
				// Unless survey notification, push notification is stateless, if the App is killed and relaunched, push notifications will not restore
				if (expire_tms.getTimeInMillis() < System.currentTimeMillis()) return;
			} catch (Exception e) { }
		}

		// Display survey notification
		String title = push_info.getOrDefault("title", "");
		String content = push_info.getOrDefault("content", "");
		String data = push_info.getOrDefault("data", null);
		boolean dismissable = !push_info.getOrDefault("dismissable", "").isEmpty();
		int pushInfoHash = (title+dismissable+content).hashCode();

		Intent activityIntent = new Intent(appContext, MainMenuActivity.class);
		if(data!=null)
			activityIntent.putExtra("data", data);
		activityIntent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP ); //modifies behavior when the user is already in the hopes.
		PendingIntent pendingIntent = PendingIntent.getActivity(appContext, pushInfoHash, activityIntent, PendingIntent.FLAG_CANCEL_CURRENT);

		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			registerPushNotificationChannel(appContext);
			displayPushNotificationNew(appContext, title, content, dismissable, pendingIntent);
		} else {
			displayPushNotificationOld(appContext, title, content, dismissable, pendingIntent);
		}

		// Schedule notification expiry
		if(expire_tms!=null)
			BackgroundService.timer.setupNotificationExpire(pushInfoHash, expire_tms);
	}

	// Apps targeting api 26 or later need a notification channel to display notifications
	private static void registerPushNotificationChannel(Context context) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			if (notificationManager == null || notificationManager.getNotificationChannel(PUSH_NOTIFICATION_CHANNEL_ID) != null) {
				return;
			}

			NotificationChannel notificationChannel = new NotificationChannel(PUSH_NOTIFICATION_CHANNEL_ID, "Push Notification", NotificationManager.IMPORTANCE_HIGH);

			// Copied these from an example, these values should change
			notificationChannel.setDescription("Channel description");
			notificationChannel.enableLights(true);
			notificationChannel.setLightColor(Color.RED);
			notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
			notificationChannel.enableVibration(true);
			notificationChannel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), Notification.AUDIO_ATTRIBUTES_DEFAULT);

			notificationManager.createNotificationChannel(notificationChannel);
		}
	}

	public static PendingIntent createDismissIntent(int notificationId){
		Intent intent = new Intent(BackgroundService.appContext.getString(R.string.dismiss_push_notification));
		intent.putExtra("push_hash", notificationId);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(BackgroundService.appContext, notificationId, intent, PendingIntent.FLAG_ONE_SHOT);
		return pendingIntent;
	}

	private static void displayPushNotificationNew(Context appContext, String title, String content, boolean dismissable, PendingIntent pendingIntent) {
		Notification.Builder notificationBuilder = new Notification.Builder(appContext, PUSH_NOTIFICATION_CHANNEL_ID);

		notificationBuilder.setContentTitle(title);
		notificationBuilder.setAutoCancel(true);
		notificationBuilder.setShowWhen(true); // As of API 24 this no longer defaults to true and must be set explicitly
		notificationBuilder.setTicker( appContext.getResources().getString(R.string.new_android_push_notification_ticker) );
		notificationBuilder.setContentText(content);
		notificationBuilder.setLights(0x00ffffff, 1000, 1000);
		notificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
		notificationBuilder.setSmallIcon(R.drawable.survey_icon);
		notificationBuilder.setLargeIcon( BitmapFactory.decodeResource(appContext.getResources(), R.drawable.survey_icon ) );

		//This value is used inside the notification (and the pending intent) as the unique Identifier of that notification, this value must be an int.
		//note: recommendations about not using the .hashCode function in java are in usually regards to Object.hashCode(),
		// or are about the fact that the specific hash algorithm is not necessarily consistent between versions of the JVM.
		// If you look at the source of the String class hashCode function you will see that it operates on the value of the string, this is all we need.
		int pushInfoHash = (title+dismissable+content).hashCode();

		/* The pending intent defines properties of the notification itself.
		 * BUG. Cannot use FLAG_UPDATE_CURRENT, which handles conflicts of multiple notification with the same id,
		 * so that the new notification replaces the old one.  if you use FLAG_UPDATE_CURRENT the notification will
		 * not launch the provided activity on android api 19.
		 * Solution: use FLAG_CANCEL_CURRENT, it provides the same functionality for our purposes.
		 * (or add android:exported="true" to the activity's permissions in the Manifest.)
		 * http://stackoverflow.com/questions/21250364/notification-click-not-launch-the-given-activity-on-nexus-phones */
		//we manually cancel the notification anyway, so this is likely moot.
		notificationBuilder.setContentIntent(pendingIntent);
		notificationBuilder.setOngoing(!dismissable);
		if(dismissable)
			notificationBuilder.setDeleteIntent(createDismissIntent(pushInfoHash));
		Notification surveyNotification = notificationBuilder.build();

		NotificationManager notificationManager = (NotificationManager) appContext.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(pushInfoHash); //cancel any current notification with this id hash
		notificationManager.notify(pushInfoHash, // If another notification with the same ID pops up, this notification will be updated/cancelled.
				surveyNotification);
	}

	/**
	 * Survey notification function for phones running api versions older than O
	 * Uses NotificationCompat.Builder
	 * @param appContext
	 * @param title
	 * @param content
	 * @param dismissable
	 * @param pendingIntent
	 */
	private static void displayPushNotificationOld(Context appContext, String title, String content, boolean dismissable, PendingIntent pendingIntent) {
		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(appContext);

		notificationBuilder.setContentTitle(title);
		notificationBuilder.setAutoCancel(true);
		notificationBuilder.setTicker( appContext.getResources().getString(R.string.new_android_push_notification_ticker) );
		notificationBuilder.setContentText(content);
		notificationBuilder.setSmallIcon(R.drawable.survey_icon);
		notificationBuilder.setLargeIcon( BitmapFactory.decodeResource(appContext.getResources(), R.drawable.survey_icon ) );

		//This value is used inside the notification (and the pending intent) as the unique Identifier of that notification, this value must be an int.
		//note: recommendations about not using the .hashCode function in java are in usually regards to Object.hashCode(),
		// or are about the fact that the specific hash algorithm is not necessarily consistent between versions of the JVM.
		// If you look at the source of the String class hashCode function you will see that it operates on the value of the string, this is all we need.
		int pushInfoHash = (title+dismissable+content).hashCode();

		/* The pending intent defines properties of the notification itself.
		 * BUG. Cannot use FLAG_UPDATE_CURRENT, which handles conflicts of multiple notification with the same id,
		 * so that the new notification replaces the old one.  if you use FLAG_UPDATE_CURRENT the notification will
		 * not launch the provided activity on android api 19.
		 * Solution: use FLAG_CANCEL_CURRENT, it provides the same functionality for our purposes.
		 * (or add android:exported="true" to the activity's permissions in the Manifest.)
		 * http://stackoverflow.com/questions/21250364/notification-click-not-launch-the-given-activity-on-nexus-phones */
		//we manually cancel the notification anyway, so this is likely moot.
		notificationBuilder.setContentIntent(pendingIntent);
		notificationBuilder.setOngoing(!dismissable);
		notificationBuilder.setLights(0x00ffffff, 1000, 1000);
		notificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
		if(dismissable)
			notificationBuilder.setDeleteIntent(createDismissIntent(pushInfoHash));
		Notification surveyNotification = notificationBuilder.build();

		NotificationManager notificationManager = (NotificationManager) appContext.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(pushInfoHash); // cancel any current notification with this id hash
		notificationManager.notify(pushInfoHash, // If another notification with the same ID pops up, this notification will be updated/cancelled.
				surveyNotification);
	}
}
