package com.moht.hopes.tasks;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;

import com.moht.hopes.BuildConfig;
import com.moht.hopes.utils.SociabilityMessageStructure;
import com.moht.hopes.utils.WhatsappUtils;

public class NotificationProcessTask implements Runnable {
    private Bundle bundle;
    private Context appContext;

    public NotificationProcessTask(Bundle eventBundle, Context context){
        bundle = eventBundle;
        appContext = context;
    }

    @Override
    public void run() {
        SociabilityMessageStructure bms = WhatsappUtils.processNotification(bundle);
        if(bms==null) return;
        if (BuildConfig.APP_IS_DEBUG) Log.d("Notification Message: ", bms.toString());
        WhatsappUtils.broadcastToSociabilityMessages(appContext, bms);
    }
}
