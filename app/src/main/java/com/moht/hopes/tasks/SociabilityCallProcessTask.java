package com.moht.hopes.tasks;

import android.content.Context;
import android.util.Log;

import com.moht.hopes.BuildConfig;
import com.moht.hopes.utils.AccessibilityEventInformation;
import com.moht.hopes.utils.SociabilityCallStructure;
import com.moht.hopes.utils.WhatsappUtils;

import java.util.List;

public class SociabilityCallProcessTask implements Runnable {
    private List<AccessibilityEventInformation> callEventInfos;
    private Context appContext;

    public SociabilityCallProcessTask(List<AccessibilityEventInformation> infos, Context context){
        callEventInfos = infos;
        appContext = context;
    }

    @Override
    public void run() {
        List<SociabilityCallStructure> scs = WhatsappUtils.processSociabilityCallEvents(callEventInfos);
        if (scs==null) return;
        if (BuildConfig.APP_IS_DEBUG) Log.d("Sociability Call: ", scs.toString());
        for (SociabilityCallStructure s: scs){
            WhatsappUtils.broadcastToSociabilityCalls(appContext, s);
        }
    }
}
