package com.moht.hopes.tasks;

import android.content.Context;
import android.util.Log;

import com.moht.hopes.BuildConfig;
import com.moht.hopes.utils.AccessibilityEventInformation;
import com.moht.hopes.utils.SociabilityMessageStructure;
import com.moht.hopes.utils.WhatsappUtils;

import java.util.List;

public class SociabilityProcessTask implements Runnable {
    private List<AccessibilityEventInformation> eventInfos;
    private Context appContext;

    public SociabilityProcessTask(List<AccessibilityEventInformation> infos, Context context){
        eventInfos = infos;
        appContext = context;
    }

    @Override
    public void run() {
        List<SociabilityMessageStructure> bms = WhatsappUtils.processSociabilityEvents(eventInfos);
        if (BuildConfig.APP_IS_DEBUG) Log.d("Sociability Message: ", bms.toString());
        for (SociabilityMessageStructure b : bms){
            WhatsappUtils.broadcastToSociabilityMessages(appContext, b);
        }
    }
}
