package com.moht.hopes.threadManagers;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class NotificationManager {
    private final ThreadPoolExecutor notificationThreadPool;

    private static final int CORE_POOL_SIZE = 1;
    private static final int MAX_POOL_SIZE = 2;
    private static final int KEEP_ALIVE_TIME = 1;

    private static NotificationManager notificationManager;

    static {
        notificationManager = new NotificationManager();
    }

    private NotificationManager(){
        BlockingQueue<Runnable> notificationQueue = new LinkedBlockingQueue<>();

        notificationThreadPool = new ThreadPoolExecutor(
                CORE_POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_TIME,
                TimeUnit.MILLISECONDS, notificationQueue);
    }

    public static NotificationManager getNotificationManager(){
        return notificationManager;
    }

    public void processNotification(Runnable task){
        notificationThreadPool.execute(task);
    }
}
