package com.moht.hopes.threadManagers;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class SociabilityCallManager {
    private final ThreadPoolExecutor sociabilityCallThreadPool;

    private static final int CORE_POOL_SIZE = 1;
    private static final int MAX_POOL_SIZE = 2;
    private static final int KEEP_ALIVE_TIME = 1;

    private static SociabilityCallManager sociabilityCallManager;

    static {
        sociabilityCallManager = new SociabilityCallManager();
    }

    private SociabilityCallManager(){
        BlockingQueue<Runnable> sociabilityCallQueue = new LinkedBlockingQueue<>();
        sociabilityCallThreadPool = new ThreadPoolExecutor(
                CORE_POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_TIME,
                TimeUnit.MILLISECONDS, sociabilityCallQueue);
    }

    public static SociabilityCallManager getSociabilityCallManager(){
        return sociabilityCallManager;
    }

    public void processSociabilityCallEvents(Runnable task){
        sociabilityCallThreadPool.execute(task);
    }
}
