package com.moht.hopes.threadManagers;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class SociabilityManager {
    private final ThreadPoolExecutor sociabilityThreadPool;

    private static final int CORE_POOL_SIZE = 1;
    private static final int MAX_POOL_SIZE = 2;
    private static final int KEEP_ALIVE_TIME = 1;

    private static SociabilityManager sociabilityManager;

    static {
        sociabilityManager = new SociabilityManager();
    }

    private SociabilityManager(){
        BlockingQueue<Runnable> sociabilityQueue = new LinkedBlockingQueue<>();
        sociabilityThreadPool = new ThreadPoolExecutor(
                CORE_POOL_SIZE, MAX_POOL_SIZE, KEEP_ALIVE_TIME,
                TimeUnit.MILLISECONDS, sociabilityQueue);
    }

    public static SociabilityManager getSociabilityManager(){
        return sociabilityManager;
    }

    public void processSociabilityEvents(Runnable task){
        sociabilityThreadPool.execute(task);
    }
}
