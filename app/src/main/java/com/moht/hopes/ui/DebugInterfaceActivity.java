package com.moht.hopes.ui;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.CrashHandler;
import com.moht.hopes.PermissionHandler;
import com.moht.hopes.R;
import com.moht.hopes.RunningBackgroundServiceActivity;
import com.moht.hopes.Timer;
import com.moht.hopes.listeners.AccelerometerListener;
import com.moht.hopes.listeners.AccessibilityListener;
import com.moht.hopes.listeners.AmbientLightListener;
import com.moht.hopes.listeners.BluetoothListener;
import com.moht.hopes.listeners.GPSListener;
import com.moht.hopes.listeners.GyroscopeListener;
import com.moht.hopes.listeners.MagnetoListener;
import com.moht.hopes.listeners.PedometerListener;
import com.moht.hopes.listeners.SignificantMotionListener;
import com.moht.hopes.listeners.TapsListener;
import com.moht.hopes.listeners.UsageListener;
import com.moht.hopes.listeners.WifiListener;
import com.moht.hopes.networking.PostRequest;
import com.moht.hopes.networking.SurveyDownloader;
import com.moht.hopes.session.SessionActivity;
import com.moht.hopes.storage.EncryptionEngine;
import com.moht.hopes.storage.EncryptorECC;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.storage.models.AESData;
import com.moht.hopes.survey.JsonSkipLogic;
import com.moht.hopes.ui.user.MainMenuActivity;
import com.moht.hopes.ui.utils.SurveyNotifications;
import com.moht.hopes.utils.GS_JSON;
import com.moht.hopes.utils.GS_JSON.*;

import org.beiwe.app.ui.qrcode.BarcodeCaptureActivity;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.crypto.SecretKey;

@SuppressLint("StaticFieldLeak")
public class DebugInterfaceActivity extends SessionActivity {
	//extends a session activity.
	Context appContext;
	public static final String name = "logFile";
	public static final String short_name = "dbg";
	public static final String header = "DEBUG_LOG_FILE";

	private static TextView logcat_view;
	private static TextView debug_intro_text;
	private static Button debug_stop_button;
	private static ScrollView logcat_scroll;
	private static String logcat_text = "";
	private static Button toggle_gesture_button;

	public class LogFile {
		public static final String name = DebugInterfaceActivity.name;
		public static final String header = DebugInterfaceActivity.header;
	}

	public class ListFile {
		public static final String name = "listFile";
		public static final String header = "LIST ALL FILES";
	}

	public class ListFeature {
		public static final String name = "listFeature";
		public static final String header = "LIST ALL FEATURES";
	}

	public class ListPermission {
		public static final String name = "permissions";
		public static final String header = "LIST ALL PERMISSIONS";
	}

	public class ScanQR {
		public static final String name = "scanQR";
		public static final String header = "QR code raw text";
	}

	public class UploadFiles {
		public static final String name = "uploadFiles";
		public static final String header = "File upload log";
	}

	public class EncryptFiles {
		public static final String name = "encryptFiles";
		public static final String header = "Test encryption log";
	}

	private static boolean atBottom = true;
	private static boolean isActive = false;

	public void setConsoleMode(String new_feature){
		show_feature = new_feature;
		boolean active = !new_feature.isEmpty();
		logcat_view.setText(active?logcat_text:"HOPES debug console");
		debug_stop_button.setVisibility(active?View.VISIBLE:View.GONE);
		debug_intro_text.setVisibility(active?View.GONE:View.VISIBLE);
	}

	@Override
	protected void onStart() {
		super.onStart();
		isActive = true;
		setConsoleMode(show_feature); // activity has restarted, restore the original display content
	}

	@Override
	protected void onStop(){
		super.onStop();
		isActive = false;
		if(!BuildConfig.APP_IS_DEBUG)
			unlocked = false;
	}

	@Override
	protected void onDestroy(){
		super.onDestroy();

		// Create notification on onStop if the permission are revoked / not granted
		if (PermissionHandler.getNextPermission(
				getApplicationContext(), false)!=null) {
			createPermissionNotification();
		}

		// Close the alert if it is open -> If not closed memory/window leak
		if (alertDialog!=null && alertDialog.isShowing()) {
			alertDialog.dismiss();
			alertDialog = null;
		}

		// Initialize the permission flags onResume
		initializePermissionAlertFlags();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_debug_interface);
		appContext = getApplicationContext();
		debug_intro_text = findViewById(R.id.debugIntro);
		debug_stop_button = findViewById(R.id.buttonStopConsole);
		logcat_view = findViewById(R.id.logcat_view);
		logcat_scroll = findViewById(R.id.logcat_scroll);
		toggle_gesture_button = findViewById(R.id.buttonToggleGesture);
		logcat_scroll.setOnScrollChangeListener((view, i, i1, i2, i3) -> {
			int diff = (logcat_view.getBottom() - (logcat_scroll.getHeight() + logcat_scroll.getScrollY()));
			atBottom = (diff == 0);	// if diff is zero, then the bottom has been reached
		});

		// set long click listener
		Object[][] longClickButtons = {
				{BluetoothListener.class, R.id.buttonStartBluetooth, R.id.buttonStopBluetooth},
				{GPSListener.class, R.id.buttonEnableGPS, R.id.buttonDisableGPS},
				{AccelerometerListener.class, R.id.buttonEnableAccelerometer, R.id.buttonDisableAccelerometer},
				{AccessibilityListener.class, R.id.buttonEnableAccessibility, R.id.buttonDisableAccessibility},
				{AmbientLightListener.class, R.id.buttonEnableAmbientLight},
				{GyroscopeListener.class, R.id.buttonEnableGyroscope, R.id.buttonDisableGyroscope},
				{MagnetoListener.class, R.id.buttonEnableMagnetometer, R.id.buttonDisableMagnetometer},
				{PedometerListener.class, R.id.buttonEnableSteps, R.id.buttonDisableSteps},
				{TapsListener.class, R.id.buttonEnableTaps, R.id.buttonDisableTaps},
				{UsageListener.class, R.id.buttonUpdateUsage},
				{WifiListener.class, R.id.buttonWifiScan},
				{ScanQR.class, R.id.buttonTestQRscan},
				{LogFile.class, R.id.buttonPrintInternalLog, R.id.buttonClearInternalLog},
				{ListFile.class, R.id.buttonListFiles},
				{ListFeature.class, R.id.buttonFeaturesEnabled},
				{ListPermission.class, R.id.buttonFeaturesPermissable},
				{UploadFiles.class, R.id.buttonUpload},
				{EncryptFiles.class, R.id.testEncryption},
		};
		for(Object[] longClickButton : longClickButtons )
			for( int x=1; x<longClickButton.length; ++x ) {
				try {
					Class cls = (Class)longClickButton[0];
					final String name, header;
					name = (String) (cls.getField("name").get(null));
					header = (String) (cls.getField("header").get(null));
					Button button = findViewById((int)longClickButton[x]);
					String textToSet = "*" + button.getText();
					button.setText(textToSet);
					button.setOnLongClickListener(view -> {
						try {
							logcat_text = name+": "+header;
							setConsoleMode(name);
							Toast.makeText(appContext,"Output console shows "+name, Toast.LENGTH_SHORT).show();
						} catch (Exception ignored) {}
						return true;
					});
				} catch ( Exception ignored) { }
			}
	}

	public static boolean unlocked = BuildConfig.APP_IS_DEBUG;
	public static String show_feature = "";
	public static void smartLog( String tag, String data ){
		if(data==null)
			data = "(null)";
		if( BuildConfig.APP_IS_DEBUG ) Log.d( tag, data );
		if( unlocked && tag.equals(show_feature) ){
			logcat_text += "\n"+data;
			while ( logcat_text.length() > 1000000 ) {	// limit text view buffer
				int p = logcat_text.indexOf('\n');
				logcat_text = (p<0?"":logcat_text.substring(p+1));
			}
			if(isActive)
				try {
					logcat_view.setText(logcat_text);
					if (atBottom)	// previously it is at bottom
						logcat_scroll.scrollTo(0, logcat_view.getBottom());
				} catch (Exception ignored) { }
		}
	}

	//Intent triggers caught in BackgroundService
	public void accelerometerOn (View view) { appContext.sendBroadcast( Timer.accelerometerOnIntent ); }
	public void accelerometerOff (View view) { appContext.sendBroadcast( Timer.accelerometerOffIntent ); }
	public void significantMotionOn (View view) {
		if (backgroundService.significantMotionListener == null)
			backgroundService.significantMotionListener = new SignificantMotionListener(getApplicationContext());
		backgroundService.significantMotionListener.turn_on();
	}
	public void significantMotionOff (View view) {
		if (backgroundService.significantMotionListener == null)
			backgroundService.significantMotionListener = new SignificantMotionListener(getApplicationContext());
		backgroundService.significantMotionListener.turn_off();
	}
	public void accessibilityOn (View view) { AccessibilityListener.listen = true; }
	public void accessibilityOff (View view) { AccessibilityListener.listen = false; }
	public void gyroscopeOn (View view) { appContext.sendBroadcast( Timer.gyroscopeOnIntent ); }
	public void gyroscopeOff (View view) { appContext.sendBroadcast( Timer.gyroscopeOffIntent ); }
	public void magnetometerOn (View view) { appContext.sendBroadcast( Timer.magnetometerOnIntent ); }
	public void magnetometerOff (View view) { appContext.sendBroadcast( Timer.magnetometerOffIntent ); }
	public void stepsOn (View view) { backgroundService.pedometerListener.turn_on(); }
	public void stepsOff (View view) { backgroundService.pedometerListener.turn_off(); }
	public void ambientLightOn (View view) { appContext.sendBroadcast( Timer.ambientLightIntent); }
	public void ambientTemperatureOn (View view) { appContext.sendBroadcast( Timer.ambientTemperatureIntent); }
	public void gpsOn (View view) { backgroundService.gpsListener.turn_on(); }
	public void gpsOff (View view) { backgroundService.gpsListener.turn_off(); }
	public void tapsOn (View view) { backgroundService.tapsListener.addView(); }
	public void tapsOff (View view) { backgroundService.tapsListener.removeView(); }
	public void scanWifi (View view) { appContext.sendBroadcast( Timer.wifiLogIntent ); }
	public void usageUpdate (View view) { appContext.sendBroadcast( Timer.usageIntent ); }
	public void bluetoothButtonStart (View view) { appContext.sendBroadcast(Timer.bluetoothOnIntent); }
	public void bluetoothButtonStop (View view) { appContext.sendBroadcast(Timer.bluetoothOffIntent); }
	public void stopConsole (View view) { logcat_text = ""; setConsoleMode(""); }
	public void testScanQR (View view){
		BarcodeCaptureActivity.checkQR = () -> true;
		startActivityForResult( new Intent( appContext, BarcodeCaptureActivity.class ), BARCODE_READER_REQUEST_CODE );
	}

	public void toggleGestureMode (View view) {
		AccessibilityListener.mSelf.toggleGestureMode();
		toggle_gesture_button.setText(AccessibilityListener.isGestureMode?
				"Turn Off Gesture Mode (Accessibility)":"Turn On Gesture Mode (Accessibility)");
	}

	private final static int BARCODE_READER_REQUEST_CODE = 2600;
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == BARCODE_READER_REQUEST_CODE) {
			smartLog(ScanQR.name, BarcodeCaptureActivity.scan_result);
		}
		super.onActivityResult(requestCode, resultCode, data);
	}

	// raw debugging info
	public void printInternalLog(View view) {
		smartLog( LogFile.name, TextFileManager.getDebugLogFile().read() );
	}
	public void testEncrypt (View view) {
		// Step 1: read encryption key
		smartLog(EncryptFiles.name, "Reading keyFile ...");
		try {
			EncryptionEngine.readKey();
		} catch (InvalidKeySpecException e) {
			smartLog(EncryptFiles.name,"Error: encryption engine failed to read key");
			throw new NullPointerException("some form of encryption error, type 1");
		}

		// Step 2: list all .csv_ files
		String [] filelist = appContext.getFilesDir().list((dir, name) -> name.endsWith("__"));

		// Step 3: if there exist any .csv_ file
		if (filelist.length > 0){	// Step 3a: encrypt all .csv_ files and save to .csv and delete .csv_
			for (String oldfn : filelist) try {
				if (!EncryptorECC.hasInstance()) EncryptionEngine.readKey();
				AESData aesData = EncryptorECC.getInstance().generateAESData();
				SecretKey AESKey = aesData.getAESKey();
				String newfn = oldfn.substring( 0, oldfn.length()-2 );
				FileInputStream fp_in0 = appContext.openFileInput( oldfn );
				BufferedReader fp_in = new BufferedReader(new InputStreamReader(fp_in0));
				FileOutputStream fp_out = appContext.openFileOutput( newfn, MODE_PRIVATE );
				fp_out.write((aesData.getEncryptedAESKey() + "\n").getBytes());
				StringBuilder data= new StringBuilder();
				String line;
				while((line=fp_in.readLine()) != null) {
					data.append(line).append("\n");
					if(data.length()>1024) {
						data = new StringBuilder(data.substring(0, data.length() - 1));
						fp_out.write((EncryptionEngine.encryptAES(data.toString(), AESKey) + "\n").getBytes());
						data = new StringBuilder();
					}
				}
				if(data.length() > 0) {
					data = new StringBuilder(data.substring(0, data.length() - 1));
					fp_out.write((EncryptionEngine.encryptAES(data.toString(), AESKey) + "\n").getBytes());
				}
				fp_in.close();
				fp_out.close();
				TextFileManager.delete(oldfn);
//				PersistentData.setMainUploadInfo( "Encrypting files ...\nProgress: " + (++n_done) + "/" + filelist.length);
			} catch (Exception e ){
				smartLog(EncryptFiles.name,"Error: failed to encrypt file " + oldfn);
			}
		} else { // Step 3b: encrypt a specific string
		    smartLog(EncryptFiles.name, "Can't to Step 3b with ECC");
		}
	}

	public void getAlarmStates(View view) {
		List<String> ids = PersistentData.getSurveyIds();
		smartLog(show_feature, "most recent alarm state: surveyId, MostRecentSurveyAlarmTime, SurveyNotificationState");
		for (String surveyId : ids){
			smartLog(show_feature, surveyId + ", " +PersistentData.getMostRecentSurveyAlarmTime(surveyId) + ", " + PersistentData.getSurveyNotificationState(surveyId)) ;
		}
	}
	
	public void getEnabledFeatures(View view) {
		for(String feature : PersistentData.feature_list)
			smartLog( ListFeature.name,feature+" is "+(PersistentData.getEnabled(feature)?"Enabled":"Disabled") );
	}
	
	public void getPermissableFeatures(View view) {
		smartLog(ListPermission.name, PermissionHandler.checkAccessFineLocation(appContext)?"AccessFineLocation enabled.":"AccessFineLocation disabled.");
		smartLog(ListPermission.name, PermissionHandler.checkAccessCoarseLocation(appContext)?"AccessCoarseLocation enabled.":"AccessCoarseLocation disabled.");
		if(Build.VERSION.SDK_INT>=29)
			smartLog(ListPermission.name, PermissionHandler.checkAccessBackgroundLocation(appContext)?"AccessBackgroundLocation enabled.":"AccessBackgroundLocation disabled.");
		smartLog(ListPermission.name, PermissionHandler.checkAccessNetworkState(appContext)?"AccessNetworkState enabled.":"AccessNetworkState disabled.");
		smartLog(ListPermission.name, PermissionHandler.checkAccessWifiState(appContext)?"AccessWifiState enabled.":"AccessWifiState disabled.");
		smartLog(ListPermission.name, PermissionHandler.checkAccessBluetooth(appContext)?"Bluetooth enabled.":"Bluetooth disabled.");
		smartLog(ListPermission.name, PermissionHandler.checkAccessBluetoothAdmin(appContext)?"BluetoothAdmin enabled.":"BluetoothAdmin disabled.");
		smartLog(ListPermission.name, PermissionHandler.checkAccessCallPhone(appContext)?"CallPhone enabled.":"CallPhone disabled.");
		smartLog(ListPermission.name, PermissionHandler.checkAccessReadCallLog(appContext)?"ReadCallLog enabled.":"ReadCallLog disabled.");
		smartLog(ListPermission.name, PermissionHandler.checkAccessReadContacts(appContext)?"ReadContacts enabled.":"ReadContacts disabled.");
		smartLog(ListPermission.name, PermissionHandler.checkAccessReadPhoneState(appContext)?"ReadPhoneState enabled.":"ReadPhoneState disabled.");
		smartLog(ListPermission.name, PermissionHandler.checkAccessReadSms(appContext)?"ReadSms enabled.":"ReadSms disabled.");
		smartLog(ListPermission.name, PermissionHandler.checkAccessReceiveMms(appContext)?"ReceiveMms enabled.":"ReceiveMms disabled.");
		smartLog(ListPermission.name, PermissionHandler.checkAccessReceiveSms(appContext)?"ReceiveSms enabled.":"ReceiveSms disabled.");
		smartLog(ListPermission.name, PermissionHandler.checkAccessRecordAudio(appContext)?"RecordAudio enabled.":"RecordAudio disabled.");
		smartLog(ListPermission.name, PermissionHandler.checkAccessUsagePermission()?"Package Usage enabled.":"Package Usage disabled.");
		smartLog(ListPermission.name, PermissionHandler.checkActivityRecognition(appContext)?"ActivityRecognition enabled.":"ActivityRecognition disabled.");
		smartLog(ListPermission.name, AccessibilityListener.isEnabled(appContext)?"AccessibilityService enabled.":"AccessibilityService disabled.");
	}
	
	public void clearInternalLog(View view) { TextFileManager.getDebugLogFile().deleteSafely(); }

	//network operations
	public void uploadDataFiles(View view) { PostRequest.uploadAllFiles(); }
	public void runSurveyDownload(View view) { SurveyDownloader.downloadSurveys(getApplicationContext()); }
	public void buttonTimer(View view) { backgroundService.startTimers(); }

	public void broadcastToSociability(View view) {
		smartLog(show_feature,"BROADCASTING WHATSAPP: TEST");
	}

	//file operations
	public void makeNewFiles(View view) { TextFileManager.makeNewFilesForEverything(); }
	public void deleteEverything(View view) {
		smartLog( LogFile.name, "Delete Everything button pressed, poke.");
		String[] files = TextFileManager.getAllFiles();
		Arrays.sort(files);
		for( String file : files ) smartLog(LogFile.name, "file: "+file);
		TextFileManager.deleteEverything(); }
	public void listFiles(View view){
		smartLog(ListFile.name, "Listing UPLOADABLE FILES");
		String[] files = TextFileManager.getAllUploadableFiles();
		Arrays.sort(files);
		for( String file : files ) smartLog(ListFile.name, "file: " + file);

		smartLog(ListFile.name, "Listing ALL FILES");
		files = TextFileManager.getAllFiles();
		Arrays.sort(files);
		for( String file : files ) smartLog(ListFile.name, "file: " + file);
	}

	//ui operations
	public void loadMainMenu(View view) { startActivity(new Intent(appContext, MainMenuActivity.class) ); }
	public void popSurveyNotifications(View view) {
		for ( String surveyId : (ArrayList<String>)PersistentData.getSurveyIds() )
			SurveyNotifications.displaySurveyNotification(appContext, surveyId);
	}
	
	//crash operations (No, really, we actually need this.)
	public void crashUi(View view) { throw new NullPointerException("oops, you bwoke it."); }
	public void crashBackground(View view) { BackgroundService.timer.setupExactSingleAlarm((long) 0, new Intent("crashBeiwe")); }
	public void crashBackgroundInFive(View view) { BackgroundService.timer.setupExactSingleAlarm((long) 5000, new Intent("crashBeiwe")); }
	public void enterANRUI(View view) { try { Thread.sleep(100000); } catch(InterruptedException ignored) { } }
	public void enterANRBackground(View view) { BackgroundService.timer.setupExactSingleAlarm((long) 0, new Intent("enterANR")); }
	public void stopBackgroundService(View view) { backgroundService.stop(); }
	public void testManualErrorReport(View view) {
		try{ throw new NullPointerException("this is a test null pointer exception from the debug interface"); }
		catch (Exception e) { CrashHandler.writeCrashlog(e, getApplicationContext()); }
	}

	//runs tests on the json logic parser
	public void testJsonLogicParser(View view) {
		String JsonQuestionsListString = "[{\"question_text\": \"In the last 7 days, how OFTEN did you EAT BROCCOLI?\", \"question_type\": \"radio_button\", \"answers\": [{\"text\": \"Never\"}, {\"text\": \"Rarely\"}, {\"text\": \"Occasionally\"}, {\"text\": \"Frequently\"}, {\"text\": \"Almost Constantly\"}], \"question_id\": \"6695d6c4-916b-4225-8688-89b6089a24d1\"}, {\"display_if\": {\">\": [\"6695d6c4-916b-4225-8688-89b6089a24d1\", 0]}, \"question_text\": \"In the last 7 days, what was the SEVERITY of your CRAVING FOR BROCCOLI?\", \"question_type\": \"radio_button\", \"answers\": [{\"text\": \"None\"}, {\"text\": \"Mild\"}, {\"text\": \"Moderate\"}, {\"text\": \"Severe\"}, {\"text\": \"Very Severe\"}], \"question_id\": \"41d54793-dc4d-48d9-f370-4329a7bc6960\"}, {\"display_if\": {\"and\": [{\">\": [\"6695d6c4-916b-4225-8688-89b6089a24d1\", 0]}, {\">\": [\"41d54793-dc4d-48d9-f370-4329a7bc6960\", 0]}]}, \"question_text\": \"In the last 7 days, how much did your CRAVING FOR BROCCOLI INTERFERE with your usual or daily activities, (e.g. eating cauliflower)?\", \"question_type\": \"radio_button\", \"answers\": [{\"text\": \"Not at all\"}, {\"text\": \"A little bit\"}, {\"text\": \"Somewhat\"}, {\"text\": \"Quite a bit\"}, {\"text\": \"Very much\"}], \"question_id\": \"5cfa06ad-d907-4ba7-a66a-d68ea3c89fba\"}, {\"display_if\": {\"or\": [{\"and\": [{\"<=\": [\"6695d6c4-916b-4225-8688-89b6089a24d1\", 3]}, {\"==\": [\"41d54793-dc4d-48d9-f370-4329a7bc6960\", 2]}, {\"<\": [\"5cfa06ad-d907-4ba7-a66a-d68ea3c89fba\", 3]}]}, {\"and\": [{\"<=\": [\"6695d6c4-916b-4225-8688-89b6089a24d1\", 3]}, {\"<\": [\"41d54793-dc4d-48d9-f370-4329a7bc6960\", 3]}, {\"==\": [\"5cfa06ad-d907-4ba7-a66a-d68ea3c89fba\", 2]}]}, {\"and\": [{\"==\": [\"6695d6c4-916b-4225-8688-89b6089a24d1\", 4]}, {\"<=\": [\"41d54793-dc4d-48d9-f370-4329a7bc6960\", 1]}, {\"<=\": [\"5cfa06ad-d907-4ba7-a66a-d68ea3c89fba\", 1]}]}]}, \"question_text\": \"While broccoli is a nutritious and healthful food, it's important to recognize that craving too much broccoli can have adverse consequences on your health.  If in a single day you find yourself eating broccoli steamed, stir-fried, and raw with a 'vegetable dip', you may be a broccoli addict.  This is an additional paragraph (following a double newline) warning you about the dangers of broccoli consumption.\", \"question_type\": \"info_text_box\", \"question_id\": \"9d7f737d-ef55-4231-e901-b3b68ca74190\"}, {\"display_if\": {\"or\": [{\"and\": [{\"==\": [\"6695d6c4-916b-4225-8688-89b6089a24d1\", 4]}, {\"or\": [{\">=\": [\"41d54793-dc4d-48d9-f370-4329a7bc6960\", 2]}, {\">=\": [\"5cfa06ad-d907-4ba7-a66a-d68ea3c89fba\", 2]}]}]}, {\"or\": [{\">=\": [\"41d54793-dc4d-48d9-f370-4329a7bc6960\", 3]}, {\">=\": [\"5cfa06ad-d907-4ba7-a66a-d68ea3c89fba\", 3]}]}]}, \"question_text\": \"OK, it sounds like your broccoli habit is getting out of hand.  Please call your clinician immediately.\", \"question_type\": \"info_text_box\", \"question_id\": \"59f05c45-df67-40ed-a299-8796118ad173\"}, {\"question_text\": \"How many pounds of broccoli per day could a woodchuck chuck if a woodchuck could chuck broccoli?\", \"text_field_type\": \"NUMERIC\", \"question_type\": \"free_response\", \"question_id\": \"9745551b-a0f8-4eec-9205-9e0154637513\"}, {\"display_if\": {\"<\": [\"9745551b-a0f8-4eec-9205-9e0154637513\", 10]}, \"question_text\": \"That seems a little low.\", \"question_type\": \"info_text_box\", \"question_id\": \"cedef218-e1ec-46d3-d8be-e30cb0b2d3aa\"}, {\"display_if\": {\"==\": [\"9745551b-a0f8-4eec-9205-9e0154637513\", 10]}, \"question_text\": \"That sounds about right.\", \"question_type\": \"info_text_box\", \"question_id\": \"64a2a19b-c3d0-4d6e-9c0d-06089fd00424\"}, {\"display_if\": {\">\": [\"9745551b-a0f8-4eec-9205-9e0154637513\", 10]}, \"question_text\": \"What?! No way- that's way too high!\", \"question_type\": \"info_text_box\", \"question_id\": \"166d74ea-af32-487c-96d6-da8d63cfd368\"}, {\"max\": \"5\", \"question_id\": \"059e2f4a-562a-498e-d5f3-f59a2b2a5a5b\", \"question_text\": \"On a scale of 1 (awful) to 5 (delicious) stars, how would you rate your dinner at Chez Broccoli Restaurant?\", \"question_type\": \"slider\", \"min\": \"1\"}, {\"display_if\": {\">=\": [\"059e2f4a-562a-498e-d5f3-f59a2b2a5a5b\", 4]}, \"question_text\": \"Wow, you are a true broccoli fan.\", \"question_type\": \"info_text_box\", \"question_id\": \"6dd9b20b-9dfc-4ec9-cd29-1b82b330b463\"}, {\"question_text\": \"THE END. This survey is over.\", \"question_type\": \"info_text_box\", \"question_id\": \"ec0173c9-ac8d-449d-d11d-1d8e596b4ec9\"}]";
		JsonSkipLogic steve;
		JsonArray questions;
		try {
			questions = (JsonArray) GS_JSON.parse(JsonQuestionsListString);
			steve = new JsonSkipLogic(questions, true, getApplicationContext());
		} catch (Exception e) {
			smartLog(show_feature,"Survey JsonLogicParser has went wrong!");
			throw new NullPointerException("Survey JsonLogicParser has went wrong!");
		}
		for(int i = 0; i<24; ++i) {
			smartLog(show_feature, "" + steve.currentQuestion);
			steve.goNextQuestion();
		}
	}

	public void resetAPP(View view){
		if (!BuildConfig.APP_IS_DEBUG) return;

		DialogInterface.OnClickListener dialogClickListener = (dialog, which) -> {
			if( which == DialogInterface.BUTTON_POSITIVE ) RESET(true, true);
		};

		new AlertDialog.Builder(RunningBackgroundServiceActivity.mSelf)
				.setTitle("Warning")
				.setMessage("This will delete all files, unregister any study, reset and quit the APP. Are you sure?")
				.setPositiveButton("OK", dialogClickListener)
				.setNegativeButton("Cancel", dialogClickListener).show();
	}

	public static void RESET(boolean delete_all, boolean quit){
		PersistentData.setRegistered(false);
		PersistentData.resetAPP(delete_all);
		if(quit) {
			if(AccessibilityListener.isEnabled(BackgroundService.appContext))
				AccessibilityListener.mSelf.disableSelf();
			android.os.Process.killProcess(android.os.Process.myPid());
			System.exit(0);
		}
	}

	public static String printStack(Throwable e){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw);
		e.printStackTrace(pw);
		return sw.toString();
	}

	public static String printStack(){
		return printStack(new Exception("Stack trace"));
	}
}
