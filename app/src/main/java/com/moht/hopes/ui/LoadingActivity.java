package com.moht.hopes.ui;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.R;
import com.moht.hopes.RunningBackgroundServiceActivity;
import com.moht.hopes.storage.EncryptionEngine;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.ui.registration.RegisterActivity;
import com.moht.hopes.ui.user.MainMenuActivity;
import com.moht.hopes.ui.utils.AlertsManager;

import java.security.NoSuchAlgorithmException;
import java.util.Arrays;

/**The LoadingActivity is a temporary RunningBackgroundServiceActivity (Not a SessionActivity,
 * check out those classes if you are confused) that pops up when the user opens the hopes.
 * This activity runs some simple checks to make sure that the device can actually run the hopes,
 * and then bumps the user to the correct screen (Register or MainMenu).
 * 
 * note: this cannot be a SessionActivity, doing so would cause it to instantiate itself infinitely when a user is logged out.
 * @author Eli Jones, Dor Samet */

public class LoadingActivity extends RunningBackgroundServiceActivity {
	
	//TODO: Low priority.  Eli.  Why does this reimplement functionality in RunningBackgroundService? investigate.
	protected BackgroundService backgroundService;
	protected boolean isBound = false;
	
	/**The ServiceConnection Class is our trigger for events that rely on the BackgroundService */
	protected ServiceConnection backgroundServiceConnection = new ServiceConnection() {
	    @Override
	    public void onServiceConnected(ComponentName name, IBinder binder) {
	        if(BuildConfig.APP_IS_DEBUG)
	        	Log.d("loading ServiceConnection", "Background Service Connected");
	        BackgroundService.BackgroundServiceBinder some_binder = (BackgroundService.BackgroundServiceBinder) binder;
	        backgroundService = some_binder.getService();
	        isBound = true;
	        loadingSequence();
	    }

	    @Override
	    public void onServiceDisconnected(ComponentName name) {
		    if(BuildConfig.APP_IS_DEBUG)
		    	Log.d("loading ServiceConnection", "Background Service Disconnected");
	        backgroundService = null;
	        isBound = false;
	    }
	};
	
	
	/**onCreate - right now it just calls on checkLogin() in SessionManager, and moves the activity
	 * to the appropriate page. In the future it could hold a splash screen before redirecting activity. */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		BackgroundService.activity = this;

		setContentView(R.layout.activity_loading);

		if ( testHashing() ) {
			Intent startingIntent = new Intent(this.getApplicationContext(), BackgroundService.class);
			startingIntent.addFlags(Intent.FLAG_FROM_BACKGROUND);
			startService(startingIntent);
			bindService( startingIntent, backgroundServiceConnection, Context.BIND_AUTO_CREATE);
		}
		else { failureExit(); }
		
		/* In order to have additional compatibility tests we need to guarantee that the background service is already running,
		this is complex, and all compatibility documentation for android indicates that the encryption we use are implemented for all 
		versions of android.  is currently a very low priority problem.  The following lines of code tends to crash the hopes.
		LoginManager.initialize( getApplicationContext() ); // probably fixed when we moved the LoginManager initialization to an earlier point in the activity cycle.
		if ( !testEncryption( ) ) { failureExit(); } //probably still broken. */
	}
	

	/**Checks whether device is registered, sends user to the correct screen. */
	private void loadingSequence() {
		if ( !PersistentData.isRegistered() ) //if the device is not registered, push the user to the register activity
			startActivity(new Intent(this, RegisterActivity.class) );
		else		//if device is registered push user to the main menu.
			startActivity(new Intent(this, MainMenuActivity.class));

		unbindService(backgroundServiceConnection);
		finish(); //destroy the loading screen
	}
	
	
	/*##################################################################################
	############################### Testing Function ###################################
	##################################################################################*/
	
	/**Tests whether the device can run the hash algorithm the hopes requires
	 * @return boolean of whether hashing works */
	private Boolean testHashing() {
		// Runs the unsafe hashing function and catches errors, if it catches errors.
		// The hashMAC function does not need to be tested here because it should not actually blow up.
		// The source indicates that it should not blow up.
		try { EncryptionEngine.unsafeHash("input"); }
		catch (NoSuchAlgorithmException e) {
			TextFileManager.getDebugLogFile().write("LoadingActivity::testHashing: " + Arrays.toString(e.getStackTrace()));
			return false;
		}
		return true;
	}
	
//	/** Returns true if the device cannot use the necessary encryption.
//	 * I apologize for the double negative, but the boolean logic is stupid if .*/
//	private boolean testEncryption() {
//		if ( LoginManager.isRegistered() ) {	
//			byte[] testKey = EncryptionEngine.newAESKey();
//			try { EncryptionEngine.encryptAES("test", testKey); }
//			catch (InvalidKeyException e) { return false; } //device does not support aes
//			catch (InvalidKeySpecException e) { return false; } //device does not support rsa
//		}
//		return true;
//	}
	

	/**Displays error, then exit.*/
	private void failureExit() {
		AlertsManager.showErrorAlert( getString( R.string.invalid_device), this, 1);
	}
}