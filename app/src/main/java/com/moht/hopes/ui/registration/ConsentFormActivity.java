package com.moht.hopes.ui.registration;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.R;
import com.moht.hopes.RunningBackgroundServiceActivity;
import com.moht.hopes.listeners.AccelerometerListener;
import com.moht.hopes.networking.PostRequest;
import com.moht.hopes.networking.SurveyDownloader;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.ui.LoadingActivity;

public class ConsentFormActivity extends RunningBackgroundServiceActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// skip consent page if consent form text is empty
		if(PersistentData.getConsentFormText().isEmpty()){
			consentButton(null );
			return;
		}

		setContentView(R.layout.activity_consent_form);
		
		TextView consentFormBody = findViewById(R.id.consent_form_body);
		consentFormBody.setText(PersistentData.getConsentFormText());
	}
	
	/** On the press of the do not consent button, we pop up an alert, allowing the user
	 * to press "Cancel" if they did not mean to press the do not consent. */
	public void doNotConsentButton(View view) {
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(ConsentFormActivity.this);
		alertBuilder.setTitle("Do Not Consent");
		alertBuilder.setMessage(getString(R.string.doNotConsentAlert));
		alertBuilder.setPositiveButton("I Understand", (dialog, which) -> {
			finish();
			System.exit(0);
		});
		alertBuilder.setNegativeButton("Cancel", (dialog, which) -> {
		});
		alertBuilder.create().show();
	}
	
	public void consentButton(View view) {
		PersistentData.setRegistered(true);
		PersistentData.loginOrRefreshLogin();
		PersistentData.initialize(getApplicationContext());

		// Download the survey questions and schedule the surveys
		if (PersistentData.getCheckForNewSurveysFrequencyMilliseconds()!=0)
			SurveyDownloader.downloadSurveys(getApplicationContext());

		// Create new data files, these will now have a patientID prepended to those files
		TextFileManager.initialize(getApplicationContext(), true);
		TextFileManager.makeNewFilesForEverything();

		// Initial upload to check upload works
		PostRequest.uploadAllFiles();
		TextFileManager.makeNewFilesForEverything();

		//This is important.  we need to start timers...
		BackgroundService.localHandle.doSetup();

		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setTitle(getString(R.string.registration_success));
		builder.setMessage(getString(R.string.registration_success_msg));
		builder.setPositiveButton("OK", (arg0, arg1) -> {});
		builder.setOnDismissListener(dialog -> {
			// Start the Main Screen Activity, destroy this activity
			startActivity(new Intent(getApplicationContext(), LoadingActivity.class) );
			finish();
		});
		builder.create().show();
	}
}
