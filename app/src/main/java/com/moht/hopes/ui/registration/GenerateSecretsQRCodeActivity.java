package com.moht.hopes.ui.registration;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.moht.hopes.R;
import com.moht.hopes.RunningBackgroundServiceActivity;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.utils.QRCodeHelper;
import com.moht.hopes.utils.Secrets;

/**
 * Activity used to show the secrets QR code to the user stored in the application.
 * This secret is useful for porting the application from one phone to another.
 * This screen should be shown only ONCE after the user logged in and data is saved on the phone.
 */

public class GenerateSecretsQRCodeActivity extends RunningBackgroundServiceActivity {

	/** Users will go to this activity after registration */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_generate_secrets_qr_code);

		ImageView qrCode = findViewById(R.id.qrCodeImageView);
		TextView userId = findViewById(R.id.userId);

		Secrets secrets = new Secrets(
				PersistentData.getPatientID(),
				PersistentData.getHashSalt(),
				PersistentData.getHashIterations(),
				PersistentData.getLatitudeOffset(),
				PersistentData.getLongitudeOffset(),
				PersistentData.getServerUrl()
		);
		Bitmap bitmap = QRCodeHelper
				.newInstance(this)
				.setContent(new Gson().toJson(secrets))
				.setErrorCorrectionLevel(ErrorCorrectionLevel.Q)
				.setMargin(2)
				.getQRCOde();

		userId.setText(PersistentData.getPatientID());
		qrCode.setImageBitmap(bitmap);
	}

	public void proceedToNextScreen(View view) {
		Intent intent = new Intent(
				GenerateSecretsQRCodeActivity.this,
				PhoneNumberEntryActivity.class);
		startActivity(intent);
	}
}
