package com.moht.hopes.ui.registration;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Debug;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.moht.hopes.BuildConfig;
import com.moht.hopes.DeviceInfo;
import com.moht.hopes.PermissionHandler;
import com.moht.hopes.R;
import com.moht.hopes.RunningBackgroundServiceActivity;
import com.moht.hopes.networking.HTTPUIAsync;
import com.moht.hopes.networking.PostRequest;
import com.moht.hopes.storage.EncryptionEngine;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.ui.DebugInterfaceActivity;
import com.moht.hopes.ui.utils.AlertsManager;
import com.moht.hopes.utils.KeyStoreUtil;
import com.moht.hopes.utils.Secrets;
import com.scottyab.rootbeer.RootBeer;

import org.beiwe.app.ui.qrcode.BarcodeCaptureActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;

import static com.moht.hopes.networking.PostRequest.addWebsitePrefix;

public class QRRegisterActivity extends RunningBackgroundServiceActivity {
//	private Button scanInitialRegisterQRButton;
//	private Button scanReRegisterQRButton;
//	private Button scanStudyRootTrustQRButton;
//	private Button scanParticipantLoginIdentifierQRButton;
//	private Button scanParticipantLoginKeyQRButton;
//	private Button scanParticipantEncryptionQRButton;
//	@SuppressLint("StaticFieldLeak")
//	public static Button registerButton;
//
//	private String userId;
//	private String userTempPassword;
//	private String userNewPassword;
//	private String serverUrl;
//	private String hashKey;
//	private int hashIteration;
//	private double latitudeOffset;
//	private double longitudeOffset;
//	private String participantLoginIdentifier;
//
//	//This callback value can be anything, we are not really using it
//	private final static int PERMISSION_CALLBACK = 0;
//	private final static int REQUEST_PERMISSIONS_IDENTIFIER = 1500;
//	private final static int BARCODE_READER_REQUEST_CODE = 2500;
//	private final static String phone_number_permission = Manifest.permission.READ_PHONE_STATE;
//	private final static String TAG = "NewRegisterActivity: ";
//
//	@Override
//	protected void onCreate(Bundle savedInstanceState) {
//		super.onCreate(savedInstanceState);
//		setContentView(R.layout.activity_new_register);
//
//		scanInitialRegisterQRButton = findViewById(R.id.scanInitialRegisterButton);
//		scanReRegisterQRButton = findViewById(R.id.scanReRegisterButton);
//		scanStudyRootTrustQRButton = findViewById(R.id.scanStudyRootTrustButton);
//		scanParticipantLoginIdentifierQRButton = findViewById(R.id.scanParticipantLoginIdentifierButton);
//		scanParticipantLoginKeyQRButton = findViewById(R.id.participantLoginKeyButton);
//		scanParticipantEncryptionQRButton = findViewById(R.id.participantEncryptionButton);
//		registerButton = findViewById(R.id.registerSubmitButton);
//
//		// Check if the app runs on emulator and then prompt user with alert and kill the app
//		if (!BuildConfig.APP_IS_DEBUG && isEmulator())
//			securityAlert("Emulator Detected",
//					"Due to security reasons this app can not be run on an emulator");
//
//		RootBeer rootBeer = new RootBeer(getApplicationContext());
//		// Check if the app runs on root device and then prompt user with alert and kill the app
//		if (!BuildConfig.APP_IS_DEBUG && rootBeer.isRootedWithoutBusyBoxCheck())
//			securityAlert("Rooted Device Detected",
//					"Due to security reasons this app can not be run on a rooted device");
//
//		// Check if the app has debug enabled and then prompt user with alert and kill the app
//		if (!BuildConfig.APP_IS_DEBUG && (isDebuggable(getApplicationContext()) || detectDebugger()))
//			securityAlert("Debugger Detected",
//					"Due to security reasons this app can not be run with debug enabled");
//	}
//
//	private static boolean isDebuggable(Context context){
//		return ((context.getApplicationContext().getApplicationInfo().flags & ApplicationInfo.FLAG_DEBUGGABLE) != 0);
//	}
//
//	private static boolean detectDebugger() {
//		return Debug.isDebuggerConnected();
//	}
//
//	private void securityAlert(String title, String message){
//		AlertDialog.Builder builder = new AlertDialog.Builder(this);
//		builder.setTitle(title);
//		builder.setMessage(message);
//		builder.setPositiveButton("OK", (arg0, arg1) -> { });
//		builder.setOnDismissListener(dialog -> {
//			finishAndRemoveTask();
//			System.exit(0);
//		});
//		builder.create().show();
//	}
//
//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		getMenuInflater().inflate(R.menu.register, menu);
//		return true;
//	}
//
//	@Override
//	public boolean onOptionsItemSelected(MenuItem item) {
//		if (item.getItemId() == R.id.action_enter_debug_interface) {
//			PersistentData.isUnregisterDebugMode = true;
//			startActivity(new Intent(getApplicationContext(), DebugInterfaceActivity.class));
//			return true;
//		}
//		return super.onOptionsItemSelected(item);
//	}
//
//	public synchronized void scanQrButtonPressed(View v) {
//		BarcodeCaptureActivity.checkQR = () -> {
//			JSONObject jObject = new JSONObject(BarcodeCaptureActivity.scan_result);
//			if (!jObject.has("step")) return false;
//			switch (v.getId()) {
//				case (R.id.scanInitialRegisterButton):
//					if (jObject.getInt("step") != 0) return false;
//					break;
//				case (R.id.scanReRegisterButton):
//					if (jObject.getInt("step") != 1) return false;
//					break;
//				case (R.id.scanStudyRootTrustButton):
//					if (jObject.getInt("step") != 2) return false;
//					break;
//				case (R.id.scanParticipantLoginIdentifierButton):
//					if (jObject.getInt("step") != 3) return false;
//					break;
//				case (R.id.participantLoginKeyButton):
//					if (jObject.getInt("step") != 4) return false;
//					break;
//				case (R.id.participantEncryptionButton):
//					if (jObject.getInt("step") != 5) return false;
//					break;
//			}
//			return true;
//		};
//		Intent intent = new Intent(getApplicationContext(), BarcodeCaptureActivity.class);
//		startActivityForResult(intent, BARCODE_READER_REQUEST_CODE);
//	}
//
//	private void showQRAlert(String title, String message){
//			AlertDialog.Builder builder = new AlertDialog.Builder(this);
//			builder.setTitle(title);
//			builder.setMessage(message);
//			builder.setPositiveButton("OK", (arg0, arg1) -> {});
//			if (alertDialog==null || !alertDialog.isShowing()) {
//				alertDialog = builder.create();
//				alertDialog.show();
//			}
//		}
//
//	public synchronized void registerButtonPressed(View view) {
//		registerButton.setEnabled(false);
//		KeyStoreUtil.createNewKey(userId);
//		if (serverUrl.length() == 0) {
//			// If the study URL is empty, alert the user
//			AlertsManager.showAlert(getString(R.string.url_too_short),
//					getString(R.string.couldnt_register), this);
//		} else if (userId.length() == 0) {
//			// If the user id length is too short, alert the user
//			AlertsManager.showAlert(getString(R.string.invalid_user_id),
//					getString(R.string.couldnt_register), this);
//		} else if (userTempPassword.length() < 1) {
//			// If the temporary registration password isn't filled in
//			AlertsManager.showAlert(getString(R.string.empty_temp_password),
//					getString(R.string.couldnt_register), this);
//		} else {
//			PersistentData.setServerUrl(serverUrl);
//			PersistentData.setLoginCredentials(userId, userTempPassword);
//			Secrets secrets = new Secrets(
//					userId,
//					hashKey.getBytes(StandardCharsets.UTF_8),
//					hashIteration,
//					latitudeOffset,
//					longitudeOffset,
//					serverUrl);
//			PersistentData.setReregistrationData(secrets);
//			tryToRegisterWithTheServer(this,
//					addWebsitePrefix(getApplicationContext().getString(R.string.register_url)),
//					userNewPassword);
//		}
//	}
//
//	static private void tryToRegisterWithTheServer(final Activity currentActivity,
//												   final String url, final String newPassword) {
//		new HTTPUIAsync(url, currentActivity) {
//			@Override
//			protected Void doInBackground(Void... arg0) {
//				DeviceInfo.initialize(currentActivity.getApplicationContext());
//				// Always use anonymized hashing when first registering the phone.
//				parameters = PostRequest.makeParameter("bluetooth_id", DeviceInfo.getBluetoothMAC()) +
//						PostRequest.makeParameter("phone_number", ((QRRegisterActivity) activity).getPhoneInfo()) +
//						PostRequest.makeParameter("device_id", DeviceInfo.getAndroidID()) +
//						PostRequest.makeParameter("device_os", "Android") +
//						PostRequest.makeParameter("os_version", DeviceInfo.getAndroidVersion()) +
//						PostRequest.makeParameter("hardware_id", DeviceInfo.getHardwareId()) +
//						PostRequest.makeParameter("brand", DeviceInfo.getBrand()) +
//						PostRequest.makeParameter("manufacturer", DeviceInfo.getManufacturer()) +
//						PostRequest.makeParameter("model", DeviceInfo.getModel()) +
//						PostRequest.makeParameter("product", DeviceInfo.getProduct()) +
//						PostRequest.makeParameter("beiwe_version", DeviceInfo.getBeiweVersion());
//				responseCode = PostRequest.httpRegister(parameters, url);
//				return null;
//			}
//
//			@Override
//			protected void onPostExecute(Void arg) {
//				super.onPostExecute(arg);
//				if (responseCode == 200) {
//					AlertDialog.Builder builder = new AlertDialog.Builder(currentActivity);
//					builder.setTitle("Registration Success");
//					builder.setMessage("Registration process successfully completed");
//					builder.setPositiveButton("OK", (arg0, arg1) -> {});
//					builder.setOnDismissListener(dialog -> {
//						PersistentData.setPassword(newPassword);
//						activity.startActivity(new Intent(activity.getApplicationContext(), PhoneNumberEntryActivity.class));
//						activity.finish();
//					});
//					builder.create().show();
//				} else {
//					AlertsManager.showAlert(responseCode, currentActivity.getString(R.string.couldnt_register), currentActivity);
//					currentActivity.runOnUiThread(() -> registerButton.setEnabled(true));
//				}
//			}
//		};
//	}
//
//	/**This is the function that requires SMS permissions.
//	 * We need to supply a (unique) identifier for phone numbers to the registration arguments.
//	 * @return hashed phone info*/
//	private String getPhoneInfo() {
//		TelephonyManager phoneManager = (TelephonyManager)
//				this.getSystemService(Context.TELEPHONY_SERVICE);
//		@SuppressLint({"MissingPermission", "HardwareIds"})
//		String phoneInfo = phoneManager.getLine1Number();
//		if (phoneInfo == null) {
//			phoneInfo = "0000-" + PersistentData.getPatientID();
//		}
//		// crashes if permission is not given on install
//		return EncryptionEngine.hashPhoneNumber(phoneInfo);
//	}
//
//	/*####################################################################
//	###################### Permission Prompting ##########################
//	####################################################################*/
//	private static Boolean prePromptActive = false;
//	private static Boolean postPromptActive = false;
//	private static Boolean thisResumeCausedByFalseActivityReturn = false;
//	private static Boolean aboutToResetFalseActivityReturn = false;
//	private static Boolean activityNotVisible = false;
//
//	private void goToSettings() {
//		Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
//				Uri.parse("package:" + getPackageName()));
//		myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
//		myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//		startActivityForResult(myAppSettings, REQUEST_PERMISSIONS_IDENTIFIER);
//	}
//
//	@Override
//	protected void onResume() {
//		super.onResume();
//		activityNotVisible = false;
//		PersistentData.isUnregisterDebugMode = false;
//
//		// This used to be in an else block,
//		// its idempotent and we appear to have been having problems with it not having been run.
//		DeviceInfo.initialize(getApplicationContext());
//
//		if (aboutToResetFalseActivityReturn) {
//			aboutToResetFalseActivityReturn = false;
//			thisResumeCausedByFalseActivityReturn = false;
//			return;
//		}
//		if ( !PermissionHandler.checkAccessReadPhoneState(getApplicationContext()) &&
//				!thisResumeCausedByFalseActivityReturn) {
//			if ( !prePromptActive && !postPromptActive )
//				if ( shouldShowRequestPermissionRationale(phone_number_permission) )
//					showPostPermissionAlert(this);
//				else
//					showPrePermissionAlert(this);
//		}
//	}
//
//	@Override
//	protected void onPause() {
//		super.onPause();
//		activityNotVisible = true;
//	}
//
//	private static boolean isEmulator() {
//		return Build.FINGERPRINT.startsWith("generic")
//				|| Build.FINGERPRINT.startsWith("unknown")
//				|| Build.MODEL.contains("google_sdk")
//				|| Build.MODEL.contains("Emulator")
//				|| Build.MODEL.contains("Android SDK built for x86")
//				|| Build.MANUFACTURER.contains("Genymotion")
//				|| (Build.BRAND.startsWith("generic") && Build.DEVICE.startsWith("generic"))
//				|| "google_sdk".equals(Build.PRODUCT);
//	}
//
//	public synchronized void reRegisterScanQrButtonPressed(View view){
//		AlertDialog.Builder builder = new AlertDialog.Builder(this);
//		builder.setTitle("Re-Registration: ");
//		builder.setMessage("Please make sure you unregister the old phone from study before you registering again. Are you sure you want to move to a new phone?");
//		builder.setPositiveButton("OK", (arg0, arg1) -> scanQrButtonPressed(view));
//		if (alertDialog==null || !alertDialog.isShowing()) {
//			alertDialog = builder.create();
//			alertDialog.show();
//		}
//	}
//
//	private void scanRegisterQr(JSONObject jsonObject){
//		try {
//			userId = jsonObject.getString("uid");
//			userTempPassword = jsonObject.getString("utp");
//			userNewPassword = jsonObject.getString("unp");
//			hashIteration = jsonObject.getInt("hashIteration");
//			hashKey = jsonObject.getString("hashKey");
//			serverUrl = jsonObject.getString("url");
//			latitudeOffset = jsonObject.getDouble("latitudeOffset");
//			longitudeOffset = jsonObject.getDouble("longitudeOffset");
//		} catch (Exception e) {
//			Log.e(TAG, "Exception while parsing register QR");
//			return;
//		}
//		scanInitialRegisterQRButton.setEnabled(false);
//		scanReRegisterQRButton.setEnabled(false);
//		scanStudyRootTrustQRButton.setEnabled(true);
//		showQRAlert("Step 1 - Success", "Register QR scan complete. Please click Step 2 - Study Root of Trust Scan QR button");
//	}
//
//	private void scanStudyRootTrustQr(JSONObject jsonObject){
//		try {
//			String certChain = jsonObject.getString("body");
//			if (certChain.isEmpty()) throw new Exception("Study root trust empty");
//			String certChainFilePath = getApplicationContext().getFilesDir() +
//					File.separator +
//					userId +
//					"_certChain.crt";
//			File caFile = new File(certChainFilePath);
//			if (caFile.exists())
//				if (caFile.delete()) Log.d(TAG, "CA file deleted");
//				else Log.d(TAG, "CA file deletion failed");
//			FileOutputStream outputStream = new FileOutputStream(caFile);
//			outputStream.write(certChain.getBytes(StandardCharsets.UTF_8));
//			outputStream.flush();
//			outputStream.close();
//			if (!caFile.exists())
//				throw new Exception("Cert chain file creation failed");
//		} catch (Exception e) {
//			Log.e(TAG, "Exception while processing participant login identifier QR");
//			return;
//		}
//		scanStudyRootTrustQRButton.setEnabled(false);
//		scanParticipantLoginIdentifierQRButton.setEnabled(true);
//		showQRAlert("Step 2 - Success", "Study Root of Trust QR scan complete. Please click Step 3 - Participant Login Identifier Scan QR button");
//	}
//
//	private void scanParticipantLoginIdentifierQr(JSONObject jsonObject){
//		try {
//			participantLoginIdentifier = jsonObject.getString("body");
//			if (participantLoginIdentifier.isEmpty())
//				throw new Exception("Participant login identifier empty");
//		} catch (Exception e) {
//			Log.e(TAG, "Exception while processing participant login identifier QR");
//			return;
//		}
//		scanParticipantLoginIdentifierQRButton.setEnabled(false);
//		scanParticipantLoginKeyQRButton.setEnabled(true);
//		showQRAlert("Step 3 - Success", "Participant Login Identifier QR scan complete. Please click Step 4 - Participant Login Key Scan QR button");
//	}
//
//	private void scanParticipantLoginKeyQr(JSONObject jsonObject){
//		try {
//			String participantLoginKey = jsonObject.getString("body");
//			if (participantLoginKey.isEmpty()) throw new Exception("Participant login key empty");
//
//			// create pfx file from pem file and public cert
//			String pfxFilePath = getApplicationContext().getFilesDir() +
//					File.separator + userId + ".pfx";
//			File pfxFile = new File(pfxFilePath);
//			if (pfxFile.exists())
//				if (pfxFile.delete()) Log.d(TAG, "PFX file deleted");
//				else Log.d(TAG, "PFX file deletion failed");
//			KeyStoreUtil.createPfxFile(
//				pfxFilePath, participantLoginIdentifier, participantLoginKey, userId);
//			if (!pfxFile.exists()) throw new Exception("PFX file creation failed");
//		} catch (Exception e) {
//			Log.e(TAG, "Exception while processing participant login key QR");
//			return;
//		}
//		scanParticipantLoginKeyQRButton.setEnabled(false);
//		scanParticipantEncryptionQRButton.setEnabled(true);
//		showQRAlert("Step 4 - Success", "Participant Login Key QR scan complete. Please click Step 5 - Participant Encryption Key Scan QR button");
//	}
//
//	private void scanParticipantEncryptionQr(JSONObject jsonObject){
//		try {
//			int success = 1;
//			String encryptionKey = jsonObject.getString("body");
//			if (encryptionKey.isEmpty()) throw new Exception("Encryption key empty");
//			encryptionKey = encryptionKey
//					.replace("-----BEGIN PUBLIC KEY-----", "")
//					.replace("-----END PUBLIC KEY-----", "")
//					.replace(" ", "")
//					.replace("\\n", "");
//			if (!encryptionKey.startsWith("MIIBI")) encryptionKey = encryptionKey.substring(1);
//			success = PostRequest.writeKey(encryptionKey, success);
//			if (success!=1) throw new Exception("Encryption key file writing failed");
//		} catch (Exception e) {
//			Log.e(TAG, "Exception while processing participant encryption key QR");
//			return;
//		}
//		scanParticipantEncryptionQRButton.setEnabled(false);
//		registerButton.setEnabled(true);
//		showQRAlert("Step 5 - Success", "Participant Encryption Key QR scan complete. Please click Step 6 - Register button");
//	}
//
//	@Override
//	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//		switch (requestCode) {
//			case BARCODE_READER_REQUEST_CODE:
//				try {
//					String qrResult = data.getStringExtra(BarcodeCaptureActivity.BarcodeObject);
//					JSONObject jObject = new JSONObject(qrResult);
//					int step = jObject.getInt("step");
//					switch(step){
//						// Initial Registration / Re Registration QR
//						case 0:
//						case 1:
//							scanRegisterQr(jObject);
//							break;
//						// Study Root Trust QR
//						case 2:
//							scanStudyRootTrustQr(jObject);
//							break;
//						// Participant Login Identifier QR
//						case 3:
//							scanParticipantLoginIdentifierQr(jObject);
//							break;
//						// Participant Login Key QR
//						case 4:
//							scanParticipantLoginKeyQr(jObject);
//							break;
//						// Participant Encryption QR
//						case 5:
//							scanParticipantEncryptionQr(jObject);
//							break;
//						default:
//							break;
//					}
//				} catch (Exception e) {
//					Log.e(TAG, "Exception while processing QR");
//				}
//				break;
//			case REQUEST_PERMISSIONS_IDENTIFIER:
//				aboutToResetFalseActivityReturn = true;
//		}
//	}
//
//	@Override
//	public void onRequestPermissionsResult (int requestCode,
//											String[] permissions, int[] grantResults) {
//		//this is identical logical progression to the way it works in SessionActivity.
//		if ( activityNotVisible ) return;
//		for ( int i = 0; i < grantResults.length; ++i ) {
//			if (permissions[i].equals( phone_number_permission)) {
//				if ( grantResults[i] == PermissionHandler.PERMISSION_GRANTED ) { break; }
//				if ( shouldShowRequestPermissionRationale(permissions[i]) ) {
//					//(shouldShow... "This method returns true
//					// if the hopes has requested this permission previously
//					// and the user denied the request.")
//					showPostPermissionAlert(this);
//				}
//			}
//		}
//	}
//
//	/* Message Popping */
//	public static void showPrePermissionAlert(final Activity activity) {
//		// Log.i("reg", "showPreAlert");
//		if (prePromptActive) { return; }
//		prePromptActive = true;
//		activity.requestPermissions(new String[]{ phone_number_permission }, PERMISSION_CALLBACK );
//		prePromptActive = false;
////		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
////		builder.setTitle("Permissions Requirement:");
////		builder.setMessage(R.string.permission_registration_read_sms_alert);
////		builder.setOnDismissListener(dialog -> {
////			activity.requestPermissions(
////					new String[]{ phone_number_permission },
////					PERMISSION_CALLBACK );
////			prePromptActive = false;
////		});
////		builder.setPositiveButton("OK", (arg0, arg1) -> { }); //Okay button
////		builder.create().show();
//	}
//
//	public static void showPostPermissionAlert(final QRRegisterActivity activity) {
//		// Log.i("reg", "showPostAlert");
//		if (postPromptActive) { return; }
//		postPromptActive = true;
//		thisResumeCausedByFalseActivityReturn = true;
//		activity.goToSettings();
//		postPromptActive = false;
////		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
////		builder.setTitle("Permissions Requirement:");
////		builder.setMessage(R.string.permission_registration_actually_need_sms_alert);
////		builder.setOnDismissListener(dialog -> {
////			thisResumeCausedByFalseActivityReturn = true;
////			activity.goToSettings();
////			postPromptActive = false;
////		});
////		builder.setPositiveButton("OK", (arg0, arg1) -> {  }); //Okay button
////		builder.create().show();
//	}
}
