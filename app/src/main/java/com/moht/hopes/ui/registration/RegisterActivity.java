// We are using NewRegisterActivity for the new QR Code scan flow.
// If you want to switch back to old flow
// 1) uncomment this whole file
// 2) Add this activity to manifest
// 3) Uncomment the line that has RegisterActivity on loadingSequence() on LoadingActivity
// 4) Comment the line that has NewRegisterActivity on loadingSequence() on LoadingActivity

package com.moht.hopes.ui.registration;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;

import com.moht.hopes.BuildConfig;
import com.moht.hopes.DeviceInfo;
import com.moht.hopes.PermissionHandler;
import com.moht.hopes.R;
import com.moht.hopes.RunningBackgroundServiceActivity;
import com.moht.hopes.networking.HTTPUIAsync;
import com.moht.hopes.networking.PostRequest;
import com.moht.hopes.storage.EncryptionEngine;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.ui.DebugInterfaceActivity;
import com.moht.hopes.ui.utils.AlertsManager;
import com.moht.hopes.utils.GS_JSON;
import com.moht.hopes.utils.GS_JSON.*;

import org.beiwe.app.ui.qrcode.BarcodeCaptureActivity;

import java.util.regex.Pattern;

import static com.moht.hopes.networking.PostRequest.addWebsitePrefix;

/**Activity used to log a user in to the application for the first time.
 * This activity should only be called on ONCE,
 * as once the user is logged in, data is saved on the phone.
 * @author Dor Samet, Eli Jones, Josh Zagorsky */

@SuppressLint("ShowToast")
public class RegisterActivity extends RunningBackgroundServiceActivity {
	private EditText serverUrlInput;
	private EditText studyIdInput;
	private EditText userIdInput;
	private EditText tempPasswordInput;

	//This callback value can be anything, we are not really using it
	private final static int PERMISSION_CALLBACK = 0;
	private final static int REQUEST_PERMISSIONS_IDENTIFIER = 1500;
	private final static int BARCODE_READER_REQUEST_CODE = 2500;
	private final static String [] phone_number_permissions = Build.VERSION.SDK_INT>=26?
			new String[] {Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_PHONE_NUMBERS}:
			new String[] {Manifest.permission.READ_PHONE_STATE};

	/** Users will go into this activity first to
	 * register information on the phone and on the server. */
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_register);

		serverUrlInput = findViewById(R.id.serverUrlInput);
		studyIdInput = findViewById(R.id.studyIdInput);
		userIdInput = findViewById(R.id.registerUserIdInput);
		tempPasswordInput = findViewById(R.id.registerTempPasswordInput);
		if(BuildConfig.APP_IS_DEBUG){
			serverUrlInput.setText(getString(R.string.debug_study_website));
			userIdInput.setText(getString(R.string.default_user_id));
			tempPasswordInput.setText(getString(R.string.default_password));
		}else
			serverUrlInput.setText(getString(R.string.default_study_website));
	}

	/** Common UI element, the menu button.*/
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.register, menu);
		return true;
	}

	/** Common UI element, items in menu.*/
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here.
		// The action bar will automatically handle clicks on the
		// Home/Up button, so long as you specify a parent activity in AndroidManifest.xml.
		if (item.getItemId() == R.id.action_enter_debug_interface) {
			PersistentData.isUnregisterDebugMode = true;
			startActivity(new Intent(getApplicationContext(), DebugInterfaceActivity.class));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public synchronized void scanQrButtonPressed(View view) {
		BarcodeCaptureActivity.checkQR = () -> {
			JsonObject jObject = (JsonObject) GS_JSON.parse(BarcodeCaptureActivity.scan_result);
			if (jObject.has("url") && jObject.has("uid") && jObject.has("utp"))
				return true;
			throw new Exception("Invalid QR code!");
		};
		Intent intent = new Intent( getApplicationContext(), BarcodeCaptureActivity.class );
		startActivityForResult( intent, BARCODE_READER_REQUEST_CODE );
	}

	public static boolean isValidEmailAddress(String email) {
		String emailRegex = "^[a-zA-Z0-9_+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$";
		Pattern pat = Pattern.compile(emailRegex);
		return email == null? false: pat.matcher(email).matches();
	}

	/** Registration sequence begins here, called when the submit button is pressed.
	 * * @param view */
	public synchronized void registerButtonPressed(View view) {
		String serverUrl = serverUrlInput.getText().toString();
		String studyID = studyIdInput.getText().toString();
		String userID = userIdInput.getText().toString();
		String tempPassword = tempPasswordInput.getText().toString();

		// If username is an Email, use request-for-OTP mode
		boolean requestOTP = view.getId()==R.id.requestOTPbutton;
		if (requestOTP){
			if(!isValidEmailAddress(userID)){
				AlertsManager.showAlert(getString(R.string.msg1_409), getString(R.string.couldnt_otp), this);
				return;
			}
			tempPassword = "request-OTP";
		}

		if (serverUrl.length() == 0) {
			// If the study URL is empty, alert the user
			AlertsManager.showAlert(getString(R.string.url_too_short), getString(R.string.couldnt_register), this);
		} else if (studyID.length() == 0) {
			// If the study id length is missing, alert the user
			AlertsManager.showAlert(getString(R.string.invalid_study_id), getString(R.string.couldnt_register), this);
		}else if (userID.length() == 0) {
			// If the user id length is too short, alert the user
			AlertsManager.showAlert(getString(R.string.invalid_user_id), getString(R.string.couldnt_register), this);
		} else if (tempPassword.length() == 0) {
			// If OTP is absent, alert the user
			AlertsManager.showAlert(getString(R.string.empty_temp_password), getString(R.string.couldnt_register), this);
		} else {
			PersistentData.setServerUrl(serverUrl);
			PersistentData.setString(PersistentData.KEY_STUDY_ID, studyID);
			PersistentData.setLoginCredentials(userID, tempPassword);
			if(requestOTP)
				tryToRequestOTPfromServer(this, addWebsitePrefix(getApplicationContext().getString(R.string.request_otp_url)));
			else
				tryToRegisterWithTheServer(this, addWebsitePrefix(getApplicationContext().getString(R.string.self_register_url)));
		}
	}

	/**Implements the server request logic for user, device registration.
	 * @param url the URL for device registration*/
	private void tryToRequestOTPfromServer(final Activity currentActivity, final String url) {
		new HTTPUIAsync(url, currentActivity ) {
			@Override
			protected Void doInBackground(Void... arg0) {
				DeviceInfo.initialize(currentActivity.getApplicationContext());
				responseCode = PostRequest.httpRequestcode(parameters, url, null);
				return null;
			}

			@Override
			protected void onPostExecute(Void arg) {
				super.onPostExecute(arg);
				switch (responseCode){
					case 200:
						AlertsManager.showAlert(currentActivity.getString(R.string.msg1_otp), currentActivity.getString(R.string.title_otp), currentActivity);
						break;
					case 406:
						AlertsManager.showAlert(currentActivity.getString(R.string.msg1_406), currentActivity.getString(R.string.couldnt_otp), currentActivity);
						break;
					case 401:
						AlertsManager.showAlert(currentActivity.getString(R.string.msg1_401), currentActivity.getString(R.string.couldnt_otp), currentActivity);
						break;
					case 425:
						AlertsManager.showAlert(currentActivity.getString(R.string.msg1_425), currentActivity.getString(R.string.couldnt_otp), currentActivity);
						break;
					case 417:
						AlertsManager.showAlert(currentActivity.getString(R.string.msg1_417), currentActivity.getString(R.string.couldnt_otp), currentActivity);
						break;
					case 409:
						AlertsManager.showAlert(currentActivity.getString(R.string.msg1_409), currentActivity.getString(R.string.couldnt_otp), currentActivity);
						break;
					case 502:
						AlertsManager.showAlert(currentActivity.getString(R.string.msg_502), currentActivity.getString(R.string.couldnt_register), currentActivity);
						break;
					default:
						AlertsManager.showAlert(responseCode, currentActivity.getString(R.string.couldnt_otp), currentActivity);
				}
			}
		};
	}

	/**Implements the server request logic for user, device registration.
	 * @param url the URL for device registration*/
	private void tryToRegisterWithTheServer(final Activity currentActivity, final String url) {
		new HTTPUIAsync(url, currentActivity ) {
			@Override
			protected Void doInBackground(Void... arg0) {
				DeviceInfo.initialize(currentActivity.getApplicationContext());
				// Always use anonymized hashing when first registering the phone.
				parameters = PostRequest.makeParameter("bluetooth_id", DeviceInfo.getBluetoothMAC()) +
							PostRequest.makeParameter("phone_number",((RegisterActivity) activity).getPhoneInfo() ) +
							PostRequest.makeParameter("device_os", "Android") +
							PostRequest.makeParameter("os_version", DeviceInfo.getAndroidVersion() ) +
							PostRequest.makeParameter("hardware_id", DeviceInfo.getHardwareId() ) +
							PostRequest.makeParameter("fcm_token", DeviceInfo.getFCMtoken() ) +
							PostRequest.makeParameter("brand", DeviceInfo.getBrand() ) +
							PostRequest.makeParameter("manufacturer", DeviceInfo.getManufacturer() ) +
							PostRequest.makeParameter("model", DeviceInfo.getModel() ) +
							PostRequest.makeParameter("product", DeviceInfo.getProduct() ) +
							PostRequest.makeParameter("beiwe_version", DeviceInfo.getBeiweVersion() );
				responseCode = PostRequest.httpRegister(parameters, url);
				return null;
			}

			@Override
			protected void onPostExecute(Void arg) {
				super.onPostExecute(arg);
				switch (responseCode){
					case 200:
						activity.startActivity(new Intent(activity.getApplicationContext(), PhoneNumberEntryActivity.class));
						activity.finish();
						break;
					case 408:
						AlertsManager.showAlert(currentActivity.getString(R.string.msg2_408), currentActivity.getString(R.string.couldnt_register), currentActivity);
						break;
					case 406:
						AlertsManager.showAlert(currentActivity.getString(R.string.msg2_406), currentActivity.getString(R.string.couldnt_register), currentActivity);
						break;
					case 405:
						AlertsManager.showAlert(currentActivity.getString(R.string.msg2_405), currentActivity.getString(R.string.couldnt_register), currentActivity);
						break;
					case 409:
						AlertsManager.showAlert(currentActivity.getString(R.string.msg2_409), currentActivity.getString(R.string.couldnt_register), currentActivity);
						break;
					case 502:
						AlertsManager.showAlert(currentActivity.getString(R.string.msg_502), currentActivity.getString(R.string.couldnt_register), currentActivity);
						break;
					default:
						AlertsManager.showAlert(responseCode, currentActivity.getString(R.string.couldnt_register), currentActivity);
				}
			}
		};
	}

	/**This is the function that requires SMS permissions.
	 * We need to supply a (unique) identifier for phone numbers to the registration arguments.
	 * @return hashed phone info*/
	@SuppressLint("MissingPermission")
	private String getPhoneInfo() {
		TelephonyManager phoneManager = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);
		@SuppressLint("HardwareIds") String phoneInfo = phoneManager.getLine1Number();
		if (phoneInfo == null) {
			phoneInfo = "0000-" + userIdInput.getText().toString();
		}
		// crashes if permission is not given on install
		return EncryptionEngine.hashPhoneNumber(phoneInfo);
	}

	/*####################################################################
	###################### Permission Prompting ##########################
	####################################################################*/
	private static Boolean prePromptActive = false;
	private static Boolean postPromptActive = false;
	private static Boolean thisResumeCausedByFalseActivityReturn = false;
	private static Boolean aboutToResetFalseActivityReturn = false;
	private static Boolean activityNotVisible = false;

	private void goToSettings() {
		Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getPackageName()));
		myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
		myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivityForResult(myAppSettings, REQUEST_PERMISSIONS_IDENTIFIER);
	}

	@Override
	protected void onResume() {
		super.onResume();
		activityNotVisible = false;
		PersistentData.isUnregisterDebugMode = false;

		// This used to be in an else block,
		// its idempotent and we appear to have been having problems with it not having been run.
		DeviceInfo.initialize(getApplicationContext());

		if (aboutToResetFalseActivityReturn) {
			aboutToResetFalseActivityReturn = false;
			thisResumeCausedByFalseActivityReturn = false;
			return;
		}
		if ( !PermissionHandler.checkPermissions(getApplicationContext(), phone_number_permissions) && !thisResumeCausedByFalseActivityReturn) {
			if ( !prePromptActive && !postPromptActive )
				if ( shouldShowRequestPermissionRationale(phone_number_permissions[0]) )
					showPostPermissionAlert(this);
				else
					showPrePermissionAlert(this);
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		activityNotVisible = true;
	}

	@SuppressLint("MissingSuperCall")
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
			case BARCODE_READER_REQUEST_CODE:
				try {
					String qrResult = data.getStringExtra(BarcodeCaptureActivity.BarcodeObject);
					JsonObject jObject = (JsonObject) GS_JSON.parse(qrResult);
					serverUrlInput.setText((String) jObject.get("url"));
					userIdInput.setText((String) jObject.get("uid"));
					studyIdInput.setText("*");
					tempPasswordInput.setText((String) jObject.get("utp"));
					aboutToResetFalseActivityReturn = true;
					registerButtonPressed(findViewById(R.id.submitButton));
				} catch (Exception ignored) {
				}
				break;
			case REQUEST_PERMISSIONS_IDENTIFIER:
				aboutToResetFalseActivityReturn = true;
		}
	}

	@SuppressLint("MissingSuperCall")
	@Override
	public void onRequestPermissionsResult (int requestCode,
											String[] permissions, int[] grantResults) {
		//this is identical logical progression to the way it works in SessionActivity.
		if (activityNotVisible) return;
		for (int i = 0; i < grantResults.length; ++i) {
			if (permissions[i].equals(phone_number_permissions)) {
				if (grantResults[i] == PermissionHandler.PERMISSION_GRANTED) {
					break;
				}
				if (shouldShowRequestPermissionRationale(permissions[i])) {
					//(shouldShow... "This method returns true
					// if the hopes has requested this permission previously
					// and the user denied the request.")
					showPostPermissionAlert(this);
				}
			}
		}
	}

	/* Message Popping */
	public static void showPrePermissionAlert(final Activity activity) {
		if (prePromptActive) { return; }
		prePromptActive = true;
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle("Permissions Requirement:");
		builder.setMessage(R.string.permission_registration_read_sms_alert);
		builder.setOnDismissListener(dialog -> {
			activity.requestPermissions(phone_number_permissions, PERMISSION_CALLBACK );
			prePromptActive = false;
		});
		builder.setPositiveButton("OK", (arg0, arg1) -> { }); //Okay button
		builder.create().show();
	}

	public static void showPostPermissionAlert(final RegisterActivity activity) {
		if (postPromptActive) { return; }
		postPromptActive = true;
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle("Permissions Requirement:");
		builder.setMessage(R.string.permission_registration_actually_need_sms_alert);
		builder.setOnDismissListener(dialog -> {
			thisResumeCausedByFalseActivityReturn = true;
			activity.goToSettings();
			postPromptActive = false;
		});
		builder.setPositiveButton("OK", (arg0, arg1) -> {  }); //Okay button
		builder.create().show();
	}
}
