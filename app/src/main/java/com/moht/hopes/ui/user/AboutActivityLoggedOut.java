package com.moht.hopes.ui.user;


import android.os.Bundle;
import android.widget.TextView;

import com.moht.hopes.R;
import com.moht.hopes.RunningBackgroundServiceActivity;
import com.moht.hopes.storage.PersistentData;

/**The about page!
 * @author Everyone! */
public class AboutActivityLoggedOut extends RunningBackgroundServiceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
//		RunningBackgroundServiceActivity.nNeedToClick = 10;
		((TextView)findViewById(R.id.about_page_body)).setText(PersistentData.getAboutPageText());
	}
}
