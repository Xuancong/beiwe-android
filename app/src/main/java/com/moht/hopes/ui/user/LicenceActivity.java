package com.moht.hopes.ui.user;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.moht.hopes.BuildConfig;
import com.moht.hopes.R;
import com.moht.hopes.RunningBackgroundServiceActivity;
import com.moht.hopes.networking.PostRequest;
import com.moht.hopes.networking.SurveyDownloader;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.ui.DebugInterfaceActivity;

public class LicenceActivity extends RunningBackgroundServiceActivity {

	public static LicenceActivity mSelf = null;
	private Menu mOptionsMenu = null;
	private boolean isHiddenButtonShown = false;
	private boolean isUnregisterButtonShown = false;
	private String lastInputSequence = "";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_licence);

		mSelf = this;

		((TextView)findViewById(R.id.licence_body)).setText(getString(R.string.default_licence_text, BuildConfig.VERSION_NAME));

		TextView tv1 = findViewById(R.id.licence_body);
		TextView tv2 = findViewById(R.id.last_upload_info);
		String textToSet = PersistentData.getMainUploadInfo() + "\n" + PersistentData.getFailUploadInfo();
		tv2.setText(textToSet);

		tv1.setOnLongClickListener(v -> {
			int visibility = (tv2.getVisibility()==View.VISIBLE ? View.GONE:View.VISIBLE);
			tv2.setVisibility(visibility);

			if(PersistentData.isRegistered() && !isHiddenButtonShown) {
				mOptionsMenu.add(R.string.force_data_upload);
				mOptionsMenu.add(R.string.force_download_survey);
				isHiddenButtonShown = true;
			}
			return true;
		});

		tv1.setOnClickListener(v -> {
			lastInputSequence += "1";
		});

		tv2.setOnClickListener(v -> {
			lastInputSequence += "2";
		});

		tv2.setOnLongClickListener(v -> {
			lastInputSequence += "3";
			if(PersistentData.isRegistered()){
				if(lastInputSequence.endsWith("1212123") && !isUnregisterButtonShown) {
					mOptionsMenu.add(R.string.force_unregister);
					isUnregisterButtonShown = true;
				}
			}
			return true;
		});
	}

	@Override
	public boolean onCreateOptionsMenu(final Menu menu) {
		mOptionsMenu = menu;
        return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will automatically handle clicks on the
		// Home/Up button, so long as you specify a parent activity in AndroidManifest.xml.
		if (item.getTitle().equals(getString(R.string.force_data_upload))) {
			TextFileManager.makeNewFilesForEverything();
			PostRequest.uploadAllFiles();
			return true;
		} else if (item.getTitle().equals(getString(R.string.force_download_survey))) {
			SurveyDownloader.downloadSurveys(this);
			return true;
		} else if (item.getTitle().equals(getString(R.string.force_unregister))) {
			DebugInterfaceActivity.RESET(false, true);
			return true;
		} else
			return super.onOptionsItemSelected(item);
	}
}
