package com.moht.hopes.ui.user;

import static android.view.View.GONE;

import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.moht.hopes.BuildConfig;
import com.moht.hopes.PermissionHandler;
import com.moht.hopes.R;
import com.moht.hopes.session.SessionActivity;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;

import static com.moht.hopes.storage.PersistentData.*;

/**The main menu activity of the app. Currently displays 4 buttons - Audio Recording, Graph, Call Clinician, and Sign out.
 * @author Dor Samet */
public class MainMenuActivity extends SessionActivity {
    public static String TAG = "MainMenuActivity";
	public static MainMenuActivity mSelf = null;
	private static WebView webView;
	private static String html = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_main_menu);
		((TextView)findViewById(R.id.main_menu_title)).setText(PersistentData.getString(MAINPAGE_TITLE_KEY));
		webView = findViewById(R.id.main_menu_webview);
		mSelf = this;
		html = getIntent().getStringExtra("data");
		if(html != null)
			((TextView)findViewById(R.id.main_menu_title)).setVisibility(GONE);

		showReportOnWebView();
	}

	@Override
	protected void onDestroy(){
		super.onDestroy();

		// Create notification on onStop if the permission are revoked / not granted
		if (PermissionHandler.getNextPermission(getApplicationContext(), false)!=null)
			createPermissionNotification();

		// Close the alert if it is open -> If not closed memory/window leak
		if (alertDialog!=null && alertDialog.isShowing()) {
			alertDialog.dismiss();
			alertDialog = null;
		}

		// Initialize the permission flags onDestroy
		initializePermissionAlertFlags();
	}

	private static void showReportOnWebView() {
		if (mSelf != null) {
			if(mSelf.html==null)
				mSelf.html = PersistentData.getReportContent();
			mSelf.webView.getSettings().setBuiltInZoomControls(true);
			mSelf.webView.getSettings().setJavaScriptEnabled(true);
			mSelf.webView.getSettings().setDomStorageEnabled(true);
			mSelf.webView.loadDataWithBaseURL(null, html,"text/html","utf-8",null);
		}
	}

	public static void displayGraphs() {
		if (mSelf != null) {
			mSelf.html = null;
			mSelf.runOnUiThread(MainMenuActivity::showReportOnWebView);
		}
	}

}
