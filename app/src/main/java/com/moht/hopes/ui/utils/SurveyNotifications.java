package com.moht.hopes.ui.utils;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.os.Build;
import androidx.core.app.NotificationCompat;

import com.moht.hopes.BackgroundService;
import com.moht.hopes.Timer;
import com.moht.hopes.survey.SurveyScheduler;
import com.moht.hopes.utils.GS_JSON;
import com.moht.hopes.utils.GS_JSON.*;

import com.moht.hopes.R;
import com.moht.hopes.storage.PersistentData;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.survey.AudioRecorderActivity;
import com.moht.hopes.survey.AudioRecorderEnhancedActivity;
import com.moht.hopes.survey.SurveyActivity;
import static com.moht.hopes.survey.SurveyScheduler.*;

import java.util.Calendar;

/**The purpose of this class is to deal with all that has to do with Survey Notifications.
 * This is a STATIC method, and is called from the background service.
 * @author Eli Jones */
//TODO: Low priority: Eli. Redoc.

public class SurveyNotifications {
	private static final String SURVEY_CHANNEL_ID = "survey_notification_channel";
	/**Creates a survey notification that transfers the user to the survey activity. 
	 * Note: the notification can only be dismissed through submitting the survey
	 * @param appContext */
	public static void displaySurveyNotification(Context appContext, String surveyId) {
		if(!PersistentData.isRegistered() && !PersistentData.isUnregisterDebugMode) return;

		// Check whether the survey notification has expired before shown
		String expiryString = PersistentData.getSurveyExpiry(surveyId);
		Calendar expire_tms = null;
		if(!expiryString.isEmpty()) {
			try {
				String[] expiry = expiryString.split(":");
				expire_tms = (expiry.length < 3 ? getTimeFromNow(Integer.parseInt(expiry[0])) :
						getTimeFromStartOfDayOffset(Integer.parseInt(expiry[0]), Integer.parseInt(expiry[1]), Integer.parseInt(expiry[2])));
				if (expire_tms.getTimeInMillis() < System.currentTimeMillis()) {
					deactivateSurvey(surveyId);
					return;
				}
			} catch (Exception e) { }
		}

		// Display survey notification
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			registerSurveyNotificationChannel(appContext);
			displaySurveyNotificationNew(appContext, surveyId);
		} else {
			displaySurveyNotificationOld(appContext, surveyId);
		}

		// Schedule notification expiry
		if(expire_tms!=null)
			BackgroundService.timer.setupSurveyExpire(surveyId, expire_tms);
	}

	public static PendingIntent createDismissIntent(int notificationId, String surveyId){
		Intent intent = new Intent(BackgroundService.appContext.getString(R.string.dismiss_survey_notification));
		intent.putExtra("survey_id", surveyId);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(BackgroundService.appContext, notificationId, intent, PendingIntent.FLAG_ONE_SHOT);
		return pendingIntent;
	}

	/**
	 * Survey notification function for phones running api versions O or newer
	 * Uses Notification.Builder
	 * @param appContext
	 * @param surveyId
	 */
	private static void displaySurveyNotificationNew(Context appContext, String surveyId) {
		Notification.Builder notificationBuilder = new Notification.Builder(appContext, SURVEY_CHANNEL_ID);

		Intent activityIntent;
		notificationBuilder.setContentTitle( (String)SurveyScheduler.getSurveySettingOption(surveyId, "survey_notibar_title",
				appContext.getString(R.string.survey_notification_app_name)));
		notificationBuilder.setShowWhen(true); // As of API 24 this no longer defaults to true and must be set explicitly
		if ( PersistentData.getSurveyType(surveyId).equals("tracking_survey" ) ) {
			activityIntent = new Intent(appContext, SurveyActivity.class);
			activityIntent.setAction( appContext.getString(R.string.start_tracking_survey) );
			notificationBuilder.setTicker( appContext.getResources().getString(R.string.new_android_survey_notification_ticker) );
			notificationBuilder.setContentText( (String)SurveyScheduler.getSurveySettingOption(surveyId, "survey_notibar_desc",
					appContext.getResources().getString(R.string.new_android_survey_notification_details)));
			notificationBuilder.setSmallIcon(R.drawable.survey_icon);
			notificationBuilder.setLargeIcon( BitmapFactory.decodeResource(appContext.getResources(), R.drawable.survey_icon ) );
			notificationBuilder.setGroup(surveyId);
		} else if ( PersistentData.getSurveyType(surveyId).equals("audio_survey" ) ) {
			activityIntent = new Intent( appContext, getAudioSurveyClass(surveyId) );
			activityIntent.setAction( appContext.getString(R.string.start_audio_survey) );
			notificationBuilder.setTicker( appContext.getResources().getString(R.string.new_audio_survey_notification_ticker) );
			notificationBuilder.setContentText( (String)SurveyScheduler.getSurveySettingOption(surveyId, "survey_notibar_desc",
					appContext.getResources().getString(R.string.new_audio_survey_notification_details)));
			notificationBuilder.setSmallIcon( R.drawable.voice_recording_icon );
			notificationBuilder.setLargeIcon( BitmapFactory.decodeResource(appContext.getResources(), R.drawable.voice_recording_icon) );
			notificationBuilder.setGroup(surveyId);
		} else {
			TextFileManager.getDebugLogFile().write(System.currentTimeMillis()+" "+" encountered unknown survey type: " + PersistentData.getSurveyType(surveyId)+", cannot schedule survey.");
			return;
		}

		activityIntent.putExtra( "surveyId", surveyId );
		activityIntent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP ); //modifies behavior when the user is already in the hopes.

		//This value is used inside the notification (and the pending intent) as the unique Identifier of that notification, this value must be an int.
		//note: recommendations about not using the .hashCode function in java are in usually regards to Object.hashCode(),
		// or are about the fact that the specific hash algorithm is not necessarily consistent between versions of the JVM.
		// If you look at the source of the String class hashCode function you will see that it operates on the value of the string, this is all we need.
		int surveyIdHash = surveyId.hashCode();
		boolean allow_dismiss = (boolean)SurveyScheduler.getSurveySettingOption(surveyId, "allow_notibar_dismiss", false);

		/* The pending intent defines properties of the notification itself.
		 * BUG. Cannot use FLAG_UPDATE_CURRENT, which handles conflicts of multiple notification with the same id,
		 * so that the new notification replaces the old one.  if you use FLAG_UPDATE_CURRENT the notification will
		 * not launch the provided activity on android api 19.
		 * Solution: use FLAG_CANCEL_CURRENT, it provides the same functionality for our purposes.
		 * (or add android:exported="true" to the activity's permissions in the Manifest.)
		 * http://stackoverflow.com/questions/21250364/notification-click-not-launch-the-given-activity-on-nexus-phones */
		//we manually cancel the notification anyway, so this is likely moot.
		PendingIntent pendingActivityIntent = PendingIntent.getActivity(appContext, surveyIdHash, activityIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		notificationBuilder.setContentIntent(pendingActivityIntent);
		notificationBuilder.setOngoing(!allow_dismiss);
		if(allow_dismiss)
			notificationBuilder.setDeleteIntent(createDismissIntent(surveyIdHash, surveyId));
		notificationBuilder.setLights(0x00ffffff, 1000, 1000);
		notificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
		Notification surveyNotification = notificationBuilder.build();

		NotificationManager notificationManager = (NotificationManager) appContext.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(surveyIdHash); //cancel any current notification with this id hash
		notificationManager.notify(surveyIdHash, // If another notification with the same ID pops up, this notification will be updated/cancelled.
				surveyNotification);

		//And, finally, set the notification state for zombie alarms.
		PersistentData.setSurveyNotificationState(surveyId, true);
	}

	/**
	 * Survey notification function for phones running api versions older than O
	 * Uses NotificationCompat.Builder
	 * @param appContext
	 * @param surveyId
	 */
	private static void displaySurveyNotificationOld(Context appContext, String surveyId) {
		//activityIntent contains information on the action triggered by tapping the notification.

		NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(appContext);

		Intent activityIntent;
		notificationBuilder.setContentTitle((String)SurveyScheduler.getSurveySettingOption(surveyId, "survey_notibar_title",
				appContext.getString(R.string.survey_notification_app_name)));
		if ( PersistentData.getSurveyType(surveyId).equals("tracking_survey" ) ) {
			activityIntent = new Intent(appContext, SurveyActivity.class);
			activityIntent.setAction( appContext.getString(R.string.start_tracking_survey) );
			notificationBuilder.setTicker( appContext.getResources().getString(R.string.new_android_survey_notification_ticker) );
			notificationBuilder.setContentText( (String)SurveyScheduler.getSurveySettingOption(surveyId, "survey_notibar_desc",
					appContext.getResources().getString(R.string.new_android_survey_notification_details)));
			notificationBuilder.setSmallIcon(R.drawable.survey_icon);
			notificationBuilder.setLargeIcon( BitmapFactory.decodeResource(appContext.getResources(), R.drawable.survey_icon ) );
			notificationBuilder.setGroup(surveyId);
		} else if ( PersistentData.getSurveyType(surveyId).equals("audio_survey" ) ) {
			activityIntent = new Intent( appContext, getAudioSurveyClass(surveyId) );
			activityIntent.setAction( appContext.getString(R.string.start_audio_survey) );
			notificationBuilder.setTicker( appContext.getResources().getString(R.string.new_audio_survey_notification_ticker) );
			notificationBuilder.setContentText( (String)SurveyScheduler.getSurveySettingOption(surveyId, "survey_notibar_desc",
					appContext.getResources().getString(R.string.new_audio_survey_notification_details)));
			notificationBuilder.setSmallIcon( R.drawable.voice_recording_icon );
			notificationBuilder.setLargeIcon( BitmapFactory.decodeResource(appContext.getResources(), R.drawable.voice_recording_icon) );
			notificationBuilder.setGroup(surveyId);
		} else {
			TextFileManager.getDebugLogFile().write(System.currentTimeMillis()+" "+" encountered unknown survey type: " + PersistentData.getSurveyType(surveyId)+", cannot schedule survey.");
			return;
		}

		activityIntent.putExtra( "surveyId", surveyId );
		activityIntent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP ); //modifies behavior when the user is already in the hopes.

		//This value is used inside the notification (and the pending intent) as the unique Identifier of that notification, this value must be an int.
		//note: recommendations about not using the .hashCode function in java are in usually regards to Object.hashCode(),
		// or are about the fact that the specific hash algorithm is not necessarily consistent between versions of the JVM.
		// If you look at the source of the String class hashCode function you will see that it operates on the value of the string, this is all we need.
		int surveyIdHash = surveyId.hashCode();
		boolean allow_dismiss = (boolean)SurveyScheduler.getSurveySettingOption(surveyId, "allow_notibar_dismiss", false);

		/* The pending intent defines properties of the notification itself.
		 * BUG. Cannot use FLAG_UPDATE_CURRENT, which handles conflicts of multiple notification with the same id,
		 * so that the new notification replaces the old one.  if you use FLAG_UPDATE_CURRENT the notification will
		 * not launch the provided activity on android api 19.
		 * Solution: use FLAG_CANCEL_CURRENT, it provides the same functionality for our purposes.
		 * (or add android:exported="true" to the activity's permissions in the Manifest.)
		 * http://stackoverflow.com/questions/21250364/notification-click-not-launch-the-given-activity-on-nexus-phones */
		//we manually cancel the notification anyway, so this is likely moot.
		PendingIntent pendingActivityIntent = PendingIntent.getActivity(appContext, surveyIdHash, activityIntent, PendingIntent.FLAG_CANCEL_CURRENT);
		notificationBuilder.setContentIntent(pendingActivityIntent);
		notificationBuilder.setOngoing(!allow_dismiss);
		if(allow_dismiss)
			notificationBuilder.setDeleteIntent(createDismissIntent(surveyIdHash, surveyId));
		notificationBuilder.setLights(0x00ffffff, 1000, 1000);
		notificationBuilder.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION));
		Notification surveyNotification = notificationBuilder.build();

		NotificationManager notificationManager = (NotificationManager) appContext.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(surveyIdHash); //cancel any current notification with this id hash
		notificationManager.notify(surveyIdHash, // If another notification with the same ID pops up, this notification will be updated/cancelled.
				surveyNotification);

		//And, finally, set the notification state for zombie alarms.
		PersistentData.setSurveyNotificationState(surveyId, true);
	}

	// Apps targeting api 26 or later need a notification channel to display notifications
	private static void registerSurveyNotificationChannel(Context context) {
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
//			if (notificationManager == null || notificationManager.getNotificationChannel(SURVEY_CHANNEL_ID) != null) {
			if (notificationManager == null)
				return;

			NotificationChannel notificationChannel = new NotificationChannel(SURVEY_CHANNEL_ID, "Survey Notification", NotificationManager.IMPORTANCE_HIGH);

			// Copied these from an example, these values should change
			notificationChannel.setDescription("Channel description");
			notificationChannel.enableLights(true);
			notificationChannel.setLightColor(Color.RED);
			notificationChannel.setVibrationPattern(new long[]{0, 1000, 500, 1000});
			notificationChannel.enableVibration(true);
			notificationChannel.setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION), Notification.AUDIO_ATTRIBUTES_DEFAULT);

			notificationManager.createNotificationChannel(notificationChannel);
		}
	}

	/**Use to dismiss the notification corresponding the surveyIdInt.
	 * @param appContext
	 * @param surveyId */
	public static void dismissNotification(Context appContext, String surveyId) {
 		NotificationManager notificationManager = (NotificationManager) appContext.getSystemService(Context.NOTIFICATION_SERVICE);
		notificationManager.cancel(surveyId.hashCode());
	}
	
	
	/**Tries to determine the type of audio survey.  If it is an Enhanced audio survey AudioRecorderEnhancedActivity.class is returned,
	 * any other outcome (including an inability to determine type) returns AudioRecorderActivity.class instead. */
	@SuppressWarnings("rawtypes")
	private static Class getAudioSurveyClass(String surveyId) {
		try { JsonObject surveySettings = (JsonObject) GS_JSON.parse( PersistentData.getSurveySettings(surveyId) );
			if ( ((String)surveySettings.get("audio_survey_type")).equals("raw") ) {
				return AudioRecorderEnhancedActivity.class; } }
		catch (Exception ignored) { }
		return AudioRecorderActivity.class;
	}
}