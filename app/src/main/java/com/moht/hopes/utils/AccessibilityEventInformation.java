package com.moht.hopes.utils;

import android.os.Bundle;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import java.util.Map;

public class AccessibilityEventInformation {
    public AccessibilityEvent event;
    AccessibilityNodeInfo root;
    Map<String, AccessibilityNodeInfo> nodeInfoByViewIds;
    long timeStamp;
    Bundle bundle;

    public AccessibilityEventInformation(
            AccessibilityEvent accessibilityEvent,
            AccessibilityNodeInfo node,
            Map<String, AccessibilityNodeInfo> nodeInfosByIds,
            Bundle extras,
            long timeStampMilliSeconds) {
        event = accessibilityEvent;
        root = node;
        nodeInfoByViewIds = nodeInfosByIds;
        timeStamp = timeStampMilliSeconds;
        bundle = extras;
    }

    public AccessibilityEventInformation(
            AccessibilityEvent accessibilityEvent,
            AccessibilityNodeInfo node,
            Map<String, AccessibilityNodeInfo> nodeInfosByIds,
            long timeStampMilliSeconds) {
        event = accessibilityEvent;
        root = node;
        nodeInfoByViewIds = nodeInfosByIds;
        timeStamp = timeStampMilliSeconds;
    }

    @Override
    public String toString() {
        return "******************************" +
                "\nEvent: " + event.toString() +
                "\nRoot: " + root.toString() +
                "\nNodeInfo by Ids: " + nodeInfoByViewIds.toString() +
                "\nTimeStamp: " + timeStamp +
                "\n***************************";
    }
}
