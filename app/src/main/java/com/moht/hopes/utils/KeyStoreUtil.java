package com.moht.hopes.utils;

import android.security.keystore.KeyGenParameterSpec;
import android.security.keystore.KeyProperties;
import android.util.Log;

import com.moht.hopes.BuildConfig;
import com.moht.hopes.networking.PKCS12CertificationEnrollment;
import com.moht.hopes.storage.TextFileManager;
import com.moht.hopes.ui.DebugInterfaceActivity;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Enumeration;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;

class CryptoUtils {

	public static byte[] getRandomNonce(int numBytes) {
		byte[] nonce = new byte[numBytes];
		new SecureRandom().nextBytes(nonce);
		return nonce;
	}

	// AES secret key
	public static SecretKey getAESKey(int keysize) throws NoSuchAlgorithmException {
		KeyGenerator keyGen = KeyGenerator.getInstance("AES");
		keyGen.init(keysize, SecureRandom.getInstanceStrong());
		return keyGen.generateKey();
	}

	// Password derived AES 256 bits secret key
	public static SecretKey getAESKeyFromPassword(char[] password, byte[] salt)
			throws NoSuchAlgorithmException, InvalidKeySpecException {

		SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
		KeySpec spec = new PBEKeySpec(password, salt, 1024, 256);
		SecretKey secret = new SecretKeySpec(factory.generateSecret(spec).getEncoded(), "AES");
		return secret;
	}

	// hex representation
	public static String hex(byte[] bytes) {
		StringBuilder result = new StringBuilder();
		for (byte b : bytes) {
			result.append(String.format("%02x", b));
		}
		return result.toString();
	}

	// print hex with block size split
	public static String hexWithBlockSize(byte[] bytes, int blockSize) {

		String hex = hex(bytes);

		// one hex = 2 chars
		blockSize = blockSize * 2;

		// better idea how to print this?
		List<String> result = new ArrayList<>();
		int index = 0;
		while (index < hex.length()) {
			result.add(hex.substring(index, Math.min(index + blockSize, hex.length())));
			index += blockSize;
		}

		return result.toString();

	}

}

public class KeyStoreUtil {
	private static String TAG = "KeyStoreUtil: ";
	private static final String AndroidKeyStore = "AndroidKeyStore";
	private static final String AES_MODE = "AES/GCM/NoPadding";

	private static final String ENCRYPT_ALGO = "AES/GCM/NoPadding";

	private static final int TAG_LENGTH_BIT = 128; // must be one of {128, 120, 112, 104, 96}
	private static final int IV_LENGTH_BYTE = 12;
	private static final int SALT_LENGTH_BYTE = 16;
	private static final Charset UTF_8 = StandardCharsets.UTF_8;

	// return a base64 encoded AES encrypted text
	public static String encryptString(String sText, String password) {
		try {
			// 16 bytes salt
			byte[] salt = CryptoUtils.getRandomNonce(SALT_LENGTH_BYTE);

			// GCM recommended 12 bytes iv?
			byte[] iv = CryptoUtils.getRandomNonce(IV_LENGTH_BYTE);

			// secret key from password
			SecretKey aesKeyFromPassword = CryptoUtils.getAESKeyFromPassword(password.toCharArray(), salt);

			Cipher cipher = Cipher.getInstance(ENCRYPT_ALGO);

			// ASE-GCM needs GCMParameterSpec
			cipher.init(Cipher.ENCRYPT_MODE, aesKeyFromPassword, new GCMParameterSpec(TAG_LENGTH_BIT, iv));

			byte[] cipherText = cipher.doFinal(sText.getBytes());

			// prefix IV and Salt to cipher text
			byte[] cipherTextWithIvSalt = ByteBuffer.allocate(iv.length + salt.length + cipherText.length)
					.put(iv)
					.put(salt)
					.put(cipherText)
					.array();

			// string representation, base64, send this string to other for decryption.
			return Base64.getEncoder().encodeToString(cipherTextWithIvSalt);
		}catch (Exception e){
			return sText;
		}

	}

	// we need the same password, salt and iv to decrypt it
	public static String decryptString(String cText, String password) {
		try {
			byte[] decode = Base64.getDecoder().decode(cText.getBytes(UTF_8));

			// get back the iv and salt from the cipher text
			ByteBuffer bb = ByteBuffer.wrap(decode);

			byte[] iv = new byte[IV_LENGTH_BYTE];
			bb.get(iv);

			byte[] salt = new byte[SALT_LENGTH_BYTE];
			bb.get(salt);

			byte[] cipherText = new byte[bb.remaining()];
			bb.get(cipherText);

			// get back the aes key from the same password and salt
			SecretKey aesKeyFromPassword = CryptoUtils.getAESKeyFromPassword(password.toCharArray(), salt);

			Cipher cipher = Cipher.getInstance(ENCRYPT_ALGO);

			cipher.init(Cipher.DECRYPT_MODE, aesKeyFromPassword, new GCMParameterSpec(TAG_LENGTH_BIT, iv));

			byte[] plainText = cipher.doFinal(cipherText);

			return new String(plainText, UTF_8);
		}catch (Exception e){
			return cText;
		}
	}

//	public static String encryptString(String textToEncrypt, String alias) {
//		String encryptedString = null;
//		try {
//			KeyStore keyStore = KeyStore.getInstance(AndroidKeyStore);
//			keyStore.load(null);
//			final KeyStore.SecretKeyEntry secretKeyEntry = (KeyStore.SecretKeyEntry) keyStore
//					.getEntry(alias, null);
//			final SecretKey secretKey = secretKeyEntry.getSecretKey();
//			final Cipher cipher = Cipher.getInstance(AES_MODE);
//			cipher.init(Cipher.ENCRYPT_MODE, secretKey);
//			PersistentData.setCipherIV(cipher.getIV());
//			encryptedString = Arrays.toString(cipher.doFinal(
//					textToEncrypt.getBytes(StandardCharsets.UTF_8)));
//		} catch (Exception e) {
//			TextFileManager.getDebugLogFile().writeEncrypted(
//					"KeyStoreUtil::encryptString: " + Arrays.toString(e.getStackTrace()));
//			Log.e(TAG, "Android key store failed while encrypting");
//		}
//		return encryptedString;
//	}
//
//	public static String decryptString(String encryptedText, String alias) {
//		String decryptedText = null;
//		try {
//			byte[] array = new byte[12];
//			if (encryptedText != null) {
//				String[] split = encryptedText
//						.substring(1, encryptedText.length() - 1).split(", ");
//				array = new byte[split.length];
//				for (int i = 0; i < split.length; i++) {
//					array[i] = Byte.parseByte(split[i]);
//				}
//			}
//			KeyStore keyStore = KeyStore.getInstance(AndroidKeyStore);
//			keyStore.load(null);
//			final KeyStore.SecretKeyEntry secretKeyEntry = (KeyStore.SecretKeyEntry) keyStore
//					.getEntry(alias, null);
//			final SecretKey secretKey = secretKeyEntry.getSecretKey();
//			final Cipher cipher = Cipher.getInstance(AES_MODE);
//			final GCMParameterSpec spec =
//					new GCMParameterSpec(128, PersistentData.getCipherIV());
//			cipher.init(Cipher.DECRYPT_MODE, secretKey, spec);
//			final byte[] decodedData = cipher.doFinal(array);
//			decryptedText = new String(decodedData, StandardCharsets.UTF_8);
//		} catch (Exception e) {
//			TextFileManager.getDebugLogFile().writeEncrypted(
//					"KeyStoreUtil::decryptString: " + Arrays.toString(e.getStackTrace()));
//			Log.e(TAG, "Android key store decryption of text failed");
//		}
//		return decryptedText;
//	}

	public static void createNewKey(String alias) {
		try {
			final KeyGenerator keyGenerator = KeyGenerator
					.getInstance(KeyProperties.KEY_ALGORITHM_AES, AndroidKeyStore);
			final KeyGenParameterSpec keyGenParameterSpec = new KeyGenParameterSpec.Builder(alias,
					KeyProperties.PURPOSE_ENCRYPT | KeyProperties.PURPOSE_DECRYPT)
					.setBlockModes(KeyProperties.BLOCK_MODE_GCM)
					.setEncryptionPaddings(KeyProperties.ENCRYPTION_PADDING_NONE)
					.build();
			keyGenerator.init(keyGenParameterSpec);
			keyGenerator.generateKey();
		} catch (Exception e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e(TAG, "Android Key store key creation failed");
			TextFileManager.getDebugLogFile().write("KeyStoreUtil::createNewKey: " + DebugInterfaceActivity.printStack(e));
		}
	}

	public static KeyStore addCertChainToTrustStore(String certChainString) {
		String crtChain = certChainString.replace(" ", "\n");
		crtChain = crtChain
				.replace("-BEGIN\n", "-BEGIN ")
				.replace("-END\n", "-END ");
		InputStream crtInputStream = new ByteArrayInputStream(crtChain.getBytes());
		CertificateFactory certificateFactory;
		KeyStore trustStore = null;
		try {
			certificateFactory = CertificateFactory.getInstance("X.509");
			X509Certificate cert = (X509Certificate) certificateFactory
					.generateCertificate(crtInputStream);
			String alias = cert.getSubjectX500Principal().getName();
			trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
			trustStore.load(null);
			trustStore.setCertificateEntry(alias, cert);
		} catch (Exception e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e(TAG, "Exception while adding cert chain to trust manager");
			TextFileManager.getDebugLogFile().write("KeyStoreUtil::addCertChainToTrustStore: " + DebugInterfaceActivity.printStack(e));
		}
		return trustStore;
	}

	private static X509Certificate generatePublicCertFromString(String certString) {
		InputStream sslCertInputStream = new ByteArrayInputStream(certString.getBytes());
		CertificateFactory certificateFactory;
		X509Certificate sslCertificate = null;
		try {
			certificateFactory = CertificateFactory.getInstance("X.509");
			sslCertificate = (X509Certificate) certificateFactory.generateCertificate(sslCertInputStream);
		} catch (Exception e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e(TAG, "Exception while creating x509 cert from cert string");
			TextFileManager.getDebugLogFile().write("KeyStoreUtil::generatePublicCertFromString: " + DebugInterfaceActivity.printStack(e));
		}
		return sslCertificate;
	}

	public static void createPfxFile(String filePath, String sslCert, String pvtKey,
	                                 String userId) {
		// generate X509 cert from public cert string
		X509Certificate sslCertificate = KeyStoreUtil.generatePublicCertFromString(sslCert);

		// create file output stream
		FileOutputStream outputStream;
		try {
			outputStream = new FileOutputStream(new File(filePath));

			// Create pfx file content from private key and
			// add public cert generated to it and set password
			PKCS12CertificationEnrollment enrollment =
					new PKCS12CertificationEnrollment(userId, outputStream, pvtKey);
			enrollment.addCert(sslCertificate);
			enrollment.enroll(userId);

			// close the output stream
			outputStream.flush();
			outputStream.close();
		} catch (Exception e) {
			if(BuildConfig.APP_IS_DEBUG) Log.e(TAG, "Exception while creating pfx file");
			TextFileManager.getDebugLogFile().write("KeyStoreUtil::createPfxFile: " + DebugInterfaceActivity.printStack(e));
		}
	}

	public static KeyStore getCertsFromPfx(String pathToFile, String passphrase) {
		KeyStore pfxKeyStore = null;
		try {
			pfxKeyStore = KeyStore.getInstance("pkcs12");
			pfxKeyStore.load(new FileInputStream(pathToFile), passphrase.toCharArray());
			Enumeration e = pfxKeyStore.aliases();
			while (e.hasMoreElements()) {
				String alias = (String) e.nextElement();
				X509Certificate c = (X509Certificate) pfxKeyStore.getCertificate(alias);
				addCertificateToKeyStore(c);
			}
		} catch (Exception e) {
			TextFileManager.getDebugLogFile().write(
					"KeyStoreUtil::getCertsFromPfx: " + Arrays.toString(e.getStackTrace()));
		}
		return pfxKeyStore;
	}

	private static void addCertificateToKeyStore(X509Certificate c) {
		try {
			KeyStore keyStore = KeyStore.getInstance("AndroidKeyStore");
			keyStore.load(null);
			keyStore.setCertificateEntry("hopes_ca", c);
		} catch (Exception e) {
			TextFileManager.getDebugLogFile().write(
					"KeyStoreUtil::addCertificationToKeyStore: " +
							Arrays.toString(e.getStackTrace()));
		}
	}
}
