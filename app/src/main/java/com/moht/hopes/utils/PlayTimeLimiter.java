package com.moht.hopes.utils;

import android.os.CountDownTimer;

import com.moht.hopes.storage.PersistentData;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Map;

public class PlayTimeLimiter extends CountDownTimer {
	public static long TOTAL_TIME = 3600000000000L;
	public static String registry_prefix = "playtime_history ";
	public long timer_elapse = 0, timer_elapse_prev = 0, timer_limit_time;
	public long [] time_limits_sec;
	public String timer_key;
	private GS_JSON.JsonObject timer_info = null;   // {"yyyy-mm-dd":sec}

	public static long [] parseTimeout(String play_time_limits){
		long [] ret = {0, 0, 0};
		if(play_time_limits==null) return ret;
		play_time_limits = play_time_limits.trim();
		if(play_time_limits.isEmpty()) return ret;
		String [] limits = play_time_limits.split(":");
		for(int x=0, X=Math.min(ret.length, limits.length); x<X; ++x)
			ret[x] = Long.parseLong(limits[x]);
		return ret;
	}

	public static PlayTimeLimiter CreateTimeLimiter(String time_limits, long check_interval_sec, String registry_key){
		long [] three_time_limits_sec = parseTimeout(time_limits);
		if(Arrays.stream(three_time_limits_sec).sum()==0) return null;
		return new PlayTimeLimiter(three_time_limits_sec, check_interval_sec, registry_key);
	}

	private PlayTimeLimiter(long [] three_time_limits_sec, long check_interval_sec, String registry_key){
		super(TOTAL_TIME, check_interval_sec*1000);
		time_limits_sec = three_time_limits_sec;
		long timer_limit_sec = three_time_limits_sec[0];
		if(time_limits_sec[1]+time_limits_sec[2]>0){  // adjust one-time limit from daily/weekly limits
			boolean touched = false;
			long sec_today = 0, sec_this_week = 0;
			LocalDate today = LocalDate.now();
			timer_key = registry_key;
			timer_info = (GS_JSON.JsonObject) GS_JSON.parse(
					PersistentData.getString(registry_prefix + registry_key, ""),
					new GS_JSON.JsonObject());
			for(Object entry1 : timer_info.entrySet()){ // auto-remove play-time records more than 1 week ago
				Map.Entry entry = (Map.Entry)entry1;
				LocalDate date = LocalDate.parse((String)entry.getKey());
				if(ChronoUnit.DAYS.between(date, today)>=7) {
					timer_info.remove(entry.getKey());
					touched = true;
				}else{
					if(date.equals(today))
						sec_today = (Long)entry.getValue();
					sec_this_week += (Long)entry.getValue();
				}
			}
			// 0 means no time limit
			if(timer_limit_sec==0)
				timer_limit_sec = TOTAL_TIME;
			if(time_limits_sec[1]>0)
				timer_limit_sec = Math.min(time_limits_sec[1]-sec_today, timer_limit_sec);
			if(time_limits_sec[2]>0)
				timer_limit_sec = Math.min(time_limits_sec[2]-sec_this_week, timer_limit_sec);
			if(touched) // update registry for the auto-remove
				PersistentData.setString(registry_prefix + registry_key, GS_JSON.toJSON(timer_info));
		}
		timer_limit_time = timer_limit_sec*1000;
	}

	// To override
	public Runnable onCheck = null;
	private long timestamp;
	private long total_time = TOTAL_TIME;

	public void pause(){
		timestamp = System.currentTimeMillis();
	}

	public void resume(){
		total_time -= System.currentTimeMillis()-timestamp;
		timestamp = 0;
	}

	@Override
	public void onTick(long millisUntilFinished) {
		if(timestamp>0) return;
		timer_elapse = total_time-millisUntilFinished;
		long delta = timer_elapse-timer_elapse_prev;
		if(timer_info!=null && delta>60000){   // update registry every minute
			String today = LocalDate.now().toString();
			long sec = (Long)timer_info.getOrDefault(today, 0L);
			timer_info.put(today, sec+delta/1000);
			PersistentData.setString(registry_prefix + timer_key, GS_JSON.toJSON(timer_info));
			timer_elapse_prev = timer_elapse;
		}
		if(onCheck!=null && isTimeout())
			onCheck.run();
	}

	@Override
	public void onFinish() {} // It should never finish

	public boolean isTimeout(){
		return timer_elapse>timer_limit_time;
	}
}
