package com.moht.hopes.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import androidx.annotation.IntRange;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import com.moht.hopes.storage.TextFileManager;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class QRCodeHelper {
    private static QRCodeHelper qrCodeHelper = null;
    private ErrorCorrectionLevel mErrorCorrectionLevel;
    private int mMargin;
    private String mContent;
    private int mWidth, mHeight;

    /**
     * private constructor of this class only access by stying in this class.
     */
    private QRCodeHelper(Context context) {
        mHeight = (int) (context.getResources().getDisplayMetrics().heightPixels / 2.4);
        mWidth = (int) (context.getResources().getDisplayMetrics().widthPixels / 1.3);
    }

    /**
     * This method is for singleton instance of this class.
     * @return the QrCode instance.
     */
    public static QRCodeHelper newInstance(Context context) {
        if (qrCodeHelper == null) {
            qrCodeHelper = new QRCodeHelper(context);
        }
        return qrCodeHelper;
    }

    /**
     * This method is called generate function who generate the qr code and return it.
     * @return qr code image with encrypted user in it.
     */
    public Bitmap getQRCOde() {
        return generate();
    }

    /**
     * Simply setting the correctionLevel to qr code.
     * @param level ErrorCorrectionLevel for Qr code.
     * @return the instance of QrCode helper class for to use remaining function in class.
     */
    public QRCodeHelper setErrorCorrectionLevel(ErrorCorrectionLevel level) {
        mErrorCorrectionLevel = level;
        return this;
    }

    /**
     * Simply setting the encrypted to qr code.
     * @param content encrypted content for to store in qr code.
     * @return the instance of QrCode helper class for to use remaining function in class.
     */
    public QRCodeHelper setContent(String content) {
        mContent = content;
        return this;
    }

//    /**
//     * Simply setting the width and height for qr code.
//     * @param width  for qr code it needs to greater than 1.
//     * @param height for qr code it needs to greater than 1.
//     * @return the instance of QrCode helper class for to use remaining function in class.
//     */
//    public QRCodeHelper setWidthAndHeight(@IntRange(from = 1) int width,
//                                          @IntRange(from = 1) int height) {
//        mWidth = width;
//        mHeight = height;
//        return this;
//    }

    /**
     * Simply setting the margin for qr code.
     * @param margin for qr code spaces.
     * @return the instance of QrCode helper class for to use remaining function in class.
     */
    public QRCodeHelper setMargin(@IntRange(from = 0) int margin) {
        mMargin = margin;
        return this;
    }

    /**
     * Generate the qr code with giving the properties.
     * @return the qr code image.
     */
    private Bitmap generate() {
        Map<EncodeHintType, Object> hintsMap = new HashMap<>();
        hintsMap.put(EncodeHintType.CHARACTER_SET, "utf-8");
        hintsMap.put(EncodeHintType.ERROR_CORRECTION, mErrorCorrectionLevel);
        hintsMap.put(EncodeHintType.MARGIN, mMargin);
        try {
            BitMatrix bitMatrix = new QRCodeWriter().encode(mContent, BarcodeFormat.QR_CODE,
                    mWidth, mHeight, hintsMap);
            int[] pixels = new int[mWidth * mHeight];
            for (int i = 0; i < mHeight; i++) {
                for (int j = 0; j < mWidth; j++) {
                    pixels[i * mWidth + j] = bitMatrix.get(j, i) ? Color.BLACK : Color.WHITE;
                }
            }
            return Bitmap.createBitmap(pixels, mWidth, mHeight, Bitmap.Config.ARGB_8888);
        } catch (WriterException e) {
            TextFileManager.getDebugLogFile().write(
                    "QRCodeHelper::generate: " + Arrays.toString(e.getStackTrace()));
        }
        return null;
    }
}
