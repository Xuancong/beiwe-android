package com.moht.hopes.utils;

import android.util.Base64;

public class Secrets {
    private String uid;
    public String hashKey;
    public String hashIteration;
    public String latitudeOffset;
    public String longitudeOffset;
    public String url;

    public Secrets(
            String id,
            byte[] key,
            int iteration,
            double latitude,
            double longitude,
            String serverUrl) {
        uid = id;
        hashKey = Base64Encode(key);
        hashIteration = String.valueOf(iteration);
        latitudeOffset = String.valueOf(latitude);
        longitudeOffset = String.valueOf(longitude);
        url = serverUrl;
    }

    public static String Base64Encode(byte[] hashKey) {
        return Base64.encodeToString(hashKey, Base64.NO_WRAP | Base64.URL_SAFE );
    }
    public static byte[] Base64Decode(String key) { return Base64.decode(key, Base64.NO_WRAP | Base64.URL_SAFE ); }

    @Override
    public String toString() {
        return "****************************************" +
                "\nUser Id: " + this.uid +
                "\nHash Key: " + this.hashKey +
                "\nHash Iteration: " + this.hashIteration +
                "\nLatitude Offset: " + this.latitudeOffset +
                "\nLongitude Offset: " + this.longitudeOffset +
                "\nServer Url: " + this.url +
                "\n****************************************";
    }
}
