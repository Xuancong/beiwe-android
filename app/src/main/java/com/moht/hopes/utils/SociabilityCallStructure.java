package com.moht.hopes.utils;

public class SociabilityCallStructure {
    String callEventType;
    String callType;
    String contactName;
    String recordedDuration;
    long timeInMilliSeconds;
    String sessionId;

    SociabilityCallStructure(
            String eventType,
            String type,
            String contact,
            String duration,
            String session,
            long milliSeconds) {
        contactName = contact;
        timeInMilliSeconds = milliSeconds;
        callEventType = eventType;
        callType = type;
        recordedDuration = duration;
        sessionId = session;
    }

    @Override
    public String toString() {
        return "****************************************" +
                "\nCall Event Type: " + this.callEventType +
                "\nCall Type: " + this.callType +
                "\nContact Name: " + this.contactName +
                "\nRecorded Duration: " + this.recordedDuration +
                "\nSessionId: " + this.sessionId +
                "\nTime(Milliseconds): " + this.timeInMilliSeconds +
                "\n****************************************";
    }
}
