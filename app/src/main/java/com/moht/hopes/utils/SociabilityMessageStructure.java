package com.moht.hopes.utils;

public class SociabilityMessageStructure {
    private String messageText;
    String sendReceive;
    String messageTime;
    String contactName;
    int messageTextLength;
    long timeInMilliSeconds;
    String messageType;
    String groupName;
    String useCaseType;

    public SociabilityMessageStructure(
            String text,
            String sr,
            String contact,
            String msgTime,
            String msgType,
            String grpName,
            String type,
            long milliSeconds) {
        messageText = text;
        sendReceive = sr;
        contactName = contact;
        messageTime = msgTime;
        timeInMilliSeconds = milliSeconds;
        messageTextLength = text.length();
        messageType = msgType;
        groupName = grpName;
        useCaseType = type;
    }

    @Override
    public String toString() {
        return "****************************************" +
                "\nMessage Text: " + this.messageText +
                "\nSend/Receive: " + this.sendReceive +
                "\nContact Name: " + this.contactName +
                "\nMessage Length: " + this.messageTextLength +
                "\nMessage Time: " + this.messageTime +
                "\nMessage Type: " + this.messageType +
                "\nGroup Name: " + this.groupName +
                "\nUse Case Type: " + this.useCaseType +
                "\nTime(Milliseconds): " + this.timeInMilliSeconds +
                "\n****************************************";
    }
}
