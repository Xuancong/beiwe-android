package com.moht.hopes.utils;

import android.view.accessibility.AccessibilityNodeInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@SuppressWarnings("SameParameterValue")
public class SociabilityUtils {
    public static final String APP_PACKAGE_WHATSAPP = "com.whatsapp";
    private static final String APP_PACKAGE_WECHAT = "com.tencent.mm";
    private static final String APP_PACKAGE_FACEBOOK_MESSENGER = "com.facebook.orca";
    static final String APP_SYSTEM_UI = "com.android.systemui";
    static final String APP_ANDROID = "android";

    public static boolean isIMApp(String packageName) {
        return packageName == null
                || (!packageName.equals(APP_PACKAGE_WHATSAPP)
                && !packageName.equals(APP_PACKAGE_WECHAT)
                && !packageName.equals(APP_PACKAGE_FACEBOOK_MESSENGER))
                && !(packageName.equals(APP_SYSTEM_UI));
    }

    static String getFullResID(String packageName, final String id) {
        return String.format("%s:id/%s", packageName, id);
    }

    public static List<AccessibilityNodeInfo> preOrderTraverse(AccessibilityNodeInfo root){
        List<AccessibilityNodeInfo> list = new ArrayList<>();
        if(root == null) return list;
        list.add(root);
        int childCount = root.getChildCount();
        for(int i = 0; i < childCount; i ++){
            AccessibilityNodeInfo node = root.getChild(i);
            if(node != null)
                list.addAll(preOrderTraverse(node));
        }
        return list;
    }

    public static Map<String, AccessibilityNodeInfo> findNodesByViewIds(AccessibilityNodeInfo root,
                                                                        List<String> ids){
        Map<String, AccessibilityNodeInfo> nodesMap = new HashMap<>();
        for(String id: ids) nodesMap.put(id, null);
        try {
            // Serialize the nodes
            List<AccessibilityNodeInfo> nodes = preOrderTraverse(root);
            if(root == null) return nodesMap;

            // check for ids
            for (AccessibilityNodeInfo node: nodes){
                if (node.getViewIdResourceName()!=null && ids.contains(node.getViewIdResourceName()))
                    nodesMap.put(ids.get(ids.indexOf(node.getViewIdResourceName())), node);
            }
        } catch (Exception ignored){}
        return nodesMap;
    }
}
