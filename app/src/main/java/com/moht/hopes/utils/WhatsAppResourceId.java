package com.moht.hopes.utils;

public enum WhatsAppResourceId {
    AUDIO(WhatsappUtils.getWhatsappAttachmentAudioResourceId()),
    VIDEO(WhatsappUtils.getWhatsappAttachmentVideoResourceId()),
    IMAGE(WhatsappUtils.getWhatsappAttachmentImageResourceId()),
    FILE(WhatsappUtils.getWhatsappAttachmentFileResourceId()),
    CONTACT(WhatsappUtils.getWhatsappAttachmentContactResourceId()),
    GIF(WhatsappUtils.getWhatsappAttachmentGifResourceId()),
    LOCATION(WhatsappUtils.getWhatsappAttachmentLocationResourceId()),
    LIVELOCATION(WhatsappUtils.getWhatsappAttachmentLiveLocationResourceId()),
    DATE(WhatsappUtils.getWhatsappMessageDateResourceId()),
    TEXTBOX(WhatsappUtils.getWhatsappTextBoxResourceId()),
    POPUPCONTACT(WhatsappUtils.getWhatsappPopupTitleResourceId()),
    POPUPVOICENOTE(WhatsappUtils.getWhatsappPopupVoiceNoteResourceId()),
    POPUPCOUNT(WhatsappUtils.getWhatsappPopupCountResourceId()),
    CONTACTNAME(WhatsappUtils.getWhatsappContactNameResourceId()),
    MESSAGETEXT(WhatsappUtils.getWhatsappMessageListResourceId()),
    MESSAGELIST(WhatsappUtils.getWhatsappMessageList()),
    NOTIFICATIONAPPNAME(WhatsappUtils.getWhatsappNotificationAppName()),
    NOTIFICATIONCONTACTNAME(WhatsappUtils.getWhatsappNotificationContactName()),
    NOTIFICATIONMESSAGETEXT(WhatsappUtils.getWhatsappNotificationMessageText()),
    NOTIFICATIONTEXTBOX(WhatsappUtils.getWhatsappNotificationTextbox()),
    NOTIFICATIONGRPNAME(WhatsappUtils.getWhatsappNotificationGroupName()),
    NOTIFICATIONSEPARATOR(WhatsappUtils.getWhatsappNotificationSeparator()),
    CALLCONTACTNAME(WhatsappUtils.getWhatsappCallContact()),
    CALLSTATUS(WhatsappUtils.getWhatsappCallStatus()),
    CALLLABEL(WhatsappUtils.getWhatsappCallLabel()),
    CALLVIDEOCONTAINER(WhatsappUtils.getWhatsappCallVideoContainer()),
    CALLSCREEN(WhatsappUtils.getWhatsappCallScreen()),
    CALLVIDEOSTATUS(WhatsappUtils.getWhatsappCallVideoStatus());

    private final String id;
    WhatsAppResourceId(String resourceId) { this.id = resourceId; }
    public String getId() { return this.id; }
}
