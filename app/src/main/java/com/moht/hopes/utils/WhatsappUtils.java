package com.moht.hopes.utils;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.graphics.Rect;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.view.accessibility.AccessibilityNodeInfo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

@SuppressWarnings({"FieldCanBeLocal", "ConstantConditions"})
public class WhatsappUtils {

    // WhatsApp Resource IDs
    private static String WHATSAPP_MESSAGE_DATE = "date";
    private static String WHATSAPP_MESSAGE_CONTACT = "conversation_contact_name";
    private static String WHATSAPP_MESSAGE_ENTRY = "entry";
    private static String WHATSAPP_POPUP_TITLE = "popup_title";
    private static String WHATSAPP_POPUP_COUNT = "popup_count";
    private static String WHATSAPP_POPUP_VOICE_NOTE = "voice_note_info";
    private static String WHATSAPP_MESSAGE_TEXT = "message_text";
    private static String WHATSAPP_MESSAGE_ATTACHMENT_VIDEO = "thumb";
    private static String WHATSAPP_MESSAGE_ATTACHMENT_CONTACT = "contact_card";
    private static String WHATSAPP_MESSAGE_ATTACHMENT_AUDIO = "conversation_row_root";
    private static String WHATSAPP_MESSAGE_ATTACHMENT_FILE = "file_type";
    private static String WHATSAPP_MESSAGE_ATTACHMENT_LIVE_LOCATION = "live_location_info_holder";
    private static String WHATSAPP_MESSAGE_ATTACHMENT_GIF = "button_image";
    private static String WHATSAPP_MESSAGE_ATTACHMENT_IMAGE = "image";
    private static String WHATSAPP_MESSAGE_ATTACHMENT_LOCATION = "message_info_holder";
    private static String WHATSAPP_MESSAGE_LIST = "list";
    private static String WHATSAPP_NOTIFICATION_APPNAME = "app_name_text";
    private static String WHATSAPP_NOTIFICATION_CONTACT_NAME = "message_name";
    private static String WHATSAPP_NOTIFICATION_TEXT = "message_text";
    private static String WHATSAPP_NOTIFICATION_TEXTBOX = "remote_input_text";
    private static String WHATSAPP_NOTIFICATION_GROUPNAME = "header_text_secondary";
    private static String WHATSAPP_NOTIFICATION_SEPARATION = "fake_shadow";
    private static String WHATSAPP_CALL_CONTACT = "name";
    private static String WHATSAPP_CALL_STATUS = "call_status";
    private static String WHATSAPP_CALL_LABEL = "voip_call_label";
    private static String WHATSAPP_VIDEOCALL_CONTAINER = "video_pip_replacement";
    private static String WHATSAPP_CALL_SCREEN = "call_screen";
    private static String WHATSAPP_CALL_VIDEO_STATUS = "center_screen_call_status_text";

    public static final String DELIMITER = "&#&";
    private static final SimpleDateFormat date12Format = new SimpleDateFormat("hh:mm a");
    private static final SimpleDateFormat date24Format = new SimpleDateFormat("HH:mm");

    static String getWhatsappCallScreen() {
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_CALL_SCREEN);
    }

    static String getWhatsappCallVideoStatus() {
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_CALL_VIDEO_STATUS);
    }

    static String getWhatsappCallVideoContainer(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_VIDEOCALL_CONTAINER);
    }

    static String getWhatsappCallContact(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_CALL_CONTACT);
    }

    static String getWhatsappCallStatus() {
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_CALL_STATUS);
    }

    static String getWhatsappCallLabel() {
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_CALL_LABEL);
    }

    static String getWhatsappMessageList(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_ANDROID,
                WHATSAPP_MESSAGE_LIST);
    }

    static String getWhatsappNotificationAppName(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_ANDROID,
                WHATSAPP_NOTIFICATION_APPNAME);
    }

    static String getWhatsappNotificationSeparator(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_SYSTEM_UI,
                WHATSAPP_NOTIFICATION_SEPARATION);
    }

    static String getWhatsappNotificationGroupName(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_ANDROID,
                WHATSAPP_NOTIFICATION_GROUPNAME);
    }

    static String getWhatsappNotificationContactName(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_ANDROID,
                WHATSAPP_NOTIFICATION_CONTACT_NAME);
    }

    static String getWhatsappNotificationMessageText(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_ANDROID,
                WHATSAPP_NOTIFICATION_TEXT);
    }

    static String getWhatsappNotificationTextbox(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_SYSTEM_UI,
                WHATSAPP_NOTIFICATION_TEXTBOX);
    }

    static String getWhatsappPopupTitleResourceId(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_POPUP_TITLE);
    }

    static String getWhatsappPopupCountResourceId(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_POPUP_COUNT);
    }

    static String getWhatsappPopupVoiceNoteResourceId(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_POPUP_VOICE_NOTE);
    }

    static String getWhatsappAttachmentVideoResourceId(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_MESSAGE_ATTACHMENT_VIDEO);
    }

    static String getWhatsappAttachmentAudioResourceId(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_MESSAGE_ATTACHMENT_AUDIO);
    }

    static String getWhatsappAttachmentImageResourceId(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_MESSAGE_ATTACHMENT_IMAGE);
    }

    static String getWhatsappAttachmentFileResourceId(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_MESSAGE_ATTACHMENT_FILE);
    }

    static String getWhatsappAttachmentContactResourceId(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_MESSAGE_ATTACHMENT_CONTACT);
    }

    static String getWhatsappAttachmentGifResourceId(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_MESSAGE_ATTACHMENT_GIF);
    }

    static String getWhatsappAttachmentLiveLocationResourceId(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_MESSAGE_ATTACHMENT_LIVE_LOCATION);
    }

    static String getWhatsappAttachmentLocationResourceId(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_MESSAGE_ATTACHMENT_LOCATION);
    }

    static String getWhatsappMessageDateResourceId(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_MESSAGE_DATE);
    }

    static String getWhatsappTextBoxResourceId(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_MESSAGE_ENTRY);
    }

    static String getWhatsappMessageListResourceId(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_MESSAGE_TEXT);
    }

    static String getWhatsappContactNameResourceId(){
        return SociabilityUtils.getFullResID(SociabilityUtils.APP_PACKAGE_WHATSAPP,
                WHATSAPP_MESSAGE_CONTACT);
    }

    public static void broadcastToSociabilityMessages(Context context,
                                                      SociabilityMessageStructure sms) {
        Intent intent = new Intent();
        intent.setAction("com.moht.hopes.action.MESSAGE_PROCESSED");
        intent.putExtra("Namespace", SociabilityUtils.APP_PACKAGE_WHATSAPP);
        intent.putExtra("SendReceive", sms.sendReceive);
        intent.putExtra("HourMinute", sms.messageTime);
        intent.putExtra("MessageType", sms.messageType);
        intent.putExtra("ContactName", sms.contactName);
        intent.putExtra("MessageLength", sms.messageTextLength);
        intent.putExtra("ReceivedAt", sms.timeInMilliSeconds);
        intent.putExtra("GroupName", sms.groupName);
        intent.putExtra("UseCaseType", sms.useCaseType);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    public static void broadcastToSociabilityCalls(Context context,
                                                   SociabilityCallStructure scs) {
        Intent intent = new Intent();
        intent.setAction("com.moht.hopes.action.CALL_PROCESSED");
        intent.putExtra("Namespace", SociabilityUtils.APP_PACKAGE_WHATSAPP);
        intent.putExtra("ReceivedAt", scs.timeInMilliSeconds);
        intent.putExtra("CallEventType", scs.callEventType);
        intent.putExtra("CallType", scs.callType);
        intent.putExtra("ContactName", scs.contactName);
        intent.putExtra("RecordedDuration", scs.recordedDuration);
        intent.putExtra("SessionId", scs.sessionId);
        LocalBroadcastManager.getInstance(context).sendBroadcast(intent);
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public static SociabilityMessageStructure processNotification(Bundle bundle){
        String groupName = bundle.getString(Notification.EXTRA_CONVERSATION_TITLE);
        String contactName = bundle.getString(Notification.EXTRA_TITLE);
        CharSequence message = bundle.getCharSequence(Notification.EXTRA_TEXT);
        String messageText = message==null ? "" : message.toString();
        CharSequence[] lines = bundle.getCharSequenceArray(Notification.EXTRA_TEXT_LINES);
        messageText = lines==null || lines[lines.length-1]==null ?
                messageText : lines[lines.length-1].toString();
        if (messageText==null || messageText.equals("Checking for new messages") ||
                contactName==null) return null;
        if (contactName.equals("WhatsApp") && messageText.contains(" @ ") &&
                messageText.contains(": ")){
            contactName = messageText.substring(0,messageText.indexOf(" @ "));
            groupName = messageText.substring(messageText.indexOf(" @")+3,messageText.indexOf(":"));
            messageText = messageText.substring(messageText.indexOf(":"));
        }
        if (contactName.equals("WhatsApp") && messageText.contains(": ") && groupName==null){
            contactName = messageText.substring(0,messageText.indexOf(":"));
            messageText = messageText.substring(messageText.indexOf(":")+2);
        }
        if (groupName==null && messageText.contains(": ") && contactName!=null){
            groupName = contactName;
            contactName = messageText.substring(0,messageText.indexOf(": "));
            messageText = messageText.substring(messageText.indexOf(": ")+2);
        }
        if (groupName!=null && groupName.contains(" messages)"))
            groupName = groupName.substring(0,groupName.lastIndexOf(" ("));
        if (groupName!=null && contactName.contains(groupName) && contactName.contains(": "))
            contactName = contactName.substring(contactName.indexOf(": ")+2);
        String messageType = checkNotificationContainsWhatsappAttachment(messageText);
        messageText = messageType.equals("TEXT") ? messageText : "";
        if (contactName.isEmpty() ||
                (messageType.equals("TEXT") && messageText.isEmpty())) return null;
        groupName = groupName!=null ? groupName : "";
        if (messageText.isEmpty() && messageType.equals("TEXT")) return null;
        else if (contactName.equals("WhatsApp")
                && ( messageText.isEmpty() || messageText.contains("Sending") ) ) return null;
        return new SociabilityMessageStructure(
                messageText, "RECV", contactName, null, messageType,
                groupName, "NOTIFICATION", System.currentTimeMillis());
    }

    private static String checkNotificationContainsWhatsappAttachment(String message) {
        String messageType = "TEXT";
        if(message.contains("\uD83D\uDC64")) messageType = "CONTACT";
        else if (message.contains("\uD83D\uDCC4"))messageType = "FILE";
        else if (message.contains("Live location"))messageType = "LOCATION";
        else if (message.contains("Location"))messageType = "LOCATION";
        else if (message.contains("Audio"))messageType = "AUDIO";
        else if (message.contains("Video"))messageType = "VIDEO";
        else if (message.contains("Photo"))messageType = "IMAGE";
        else if (message.contains("GIF"))messageType = "VIDEO";
        else if (message.contains("Voice"))messageType = "VOICERECORDING";
        return messageType;
    }

    public static List<SociabilityMessageStructure> processSociabilityEvents(
            List<AccessibilityEventInformation> eventInfos) {
        AccessibilityNodeInfo popupContact =eventInfos.get(0)
                .nodeInfoByViewIds.get(WhatsAppResourceId.POPUPCONTACT.getId());
        AccessibilityNodeInfo notificationTextbox= eventInfos.get(0)
                .nodeInfoByViewIds.get(WhatsAppResourceId.NOTIFICATIONTEXTBOX.getId());
        if (popupContact!=null)
            return handlePopupMessages(eventInfos);
        else if (notificationTextbox!=null)
            return handleNotificationReply(eventInfos);
        return handleChatScreenMessages(eventInfos);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public static List<SociabilityCallStructure> processSociabilityCallEvents(
            List<AccessibilityEventInformation> callEventInfos) {
        List<SociabilityCallStructure> scs = new ArrayList<>();
        String contactName = "";
        String callType = "";
        String eventType = "";
        String sessionId = "";
        List<String> durationList = new ArrayList<>();

        // Check first notification event for incoming / outgoing call
        AccessibilityEventInformation eventInfo = callEventInfos.remove(0);
        if (eventInfo.bundle!=null){
            // set session id with first timestamp as it is unique always
            sessionId = String.valueOf(eventInfo.timeStamp);

            // process bundle messages
            CharSequence message = eventInfo.bundle.getCharSequence(Notification.EXTRA_TEXT);
            CharSequence contact = eventInfo.bundle.getCharSequence(Notification.EXTRA_TITLE);
            if (message!=null){
                String text = message.toString();
                contactName = contact==null ? "" : contact.toString();

                // check for incoming / outgoing calls
                if (text.contains("Incoming")) eventType = "INCOMING";
                else if (text.contains("Calling")) eventType = "OUTGOING";
                // missed call comes as contact and contact name comes in text
                // so swap text and contact and then set event
                else if (contactName.contains("issed")) {
                    String temp = contactName;
                    eventType = "MISSED";
                    contactName = text;
                    text = temp;
                }

                // check for audio / video
                if (text.contains("voice")) callType = "AUDIO";
                else if (text.contains("video")) callType = "VIDEO";

                // if event type not empty add the event to list
                if (!eventType.isEmpty())
                    scs.add(new SociabilityCallStructure(eventType, callType, contactName,
                            "", sessionId, eventInfo.timeStamp));
            }
        }

        // process the nodes for other events after incoming / outgoing event
        for (AccessibilityEventInformation info: callEventInfos){
            AccessibilityNodeInfo callStatusNode = info.nodeInfoByViewIds
                    .get(WhatsAppResourceId.CALLSTATUS.getId());
            AccessibilityNodeInfo callLabelNode = info.nodeInfoByViewIds
                    .get(WhatsAppResourceId.CALLLABEL.getId());
            AccessibilityNodeInfo callScreenNode = info.nodeInfoByViewIds
                    .get(WhatsAppResourceId.CALLSCREEN.getId());
            AccessibilityNodeInfo callVideoStatus = info.nodeInfoByViewIds
                    .get(WhatsAppResourceId.CALLVIDEOSTATUS.getId());

            // find call type by using nodes ->
            // for outgoing whatsapp audio / video call
            if (callLabelNode!=null && callLabelNode.getText()!=null) {
                callType = callLabelNode.getText().toString().contains("VOICE") ? "AUDIO" : "VIDEO";
                if (scs.size()>0 && scs.get(0).callType.isEmpty()) scs.get(0).callType = callType;
            }

            // if last event info is notification, it is a missed call
            if (callEventInfos.size()>0 && callEventInfos.get(callEventInfos.size()-1).bundle!=null){
                scs.add(new SociabilityCallStructure("MISSED", callType, contactName,
                        "", sessionId, callEventInfos.get(callEventInfos.size()-1).timeStamp));
                return scs;
            }

            // if video call and call status and video container not available
            // then add aborted status if previous event is ringing / calling
            if (callType.equals("VIDEO") && callStatusNode==null && callScreenNode==null &&
                    (eventType.equals("RINGING") || eventType.equals("CALLING"))) {
                scs.add(new SociabilityCallStructure("ABORTED", callType, contactName,
                        "", sessionId,
                        callEventInfos.get(callEventInfos.size()-1).timeStamp));
                return scs;
            }

            // if video call and video status is connecting / reconnecting
            // add accepted, connecting / reconnecting events to list
            if (callType.equals("VIDEO") && callVideoStatus!=null &&
                    callVideoStatus.getText()!=null){
                String status = callVideoStatus.getText().toString();
                switch (status) {
                    // if connecting event add accepted and connecting event to list
                    case "Connecting":
                        if (eventType.equals("CONNECTED") || eventType.equals("Connecting")) break;
                        scs.add(new SociabilityCallStructure("ACCEPTED", callType,
                                contactName, "", sessionId, info.timeStamp));
                        eventType = "Connecting";
                        scs.add(new SociabilityCallStructure(eventType.toUpperCase(), callType,
                                contactName, "", sessionId, info.timeStamp));
                        break;
                    // if reconnecting event add reconnecting event to list
                    case "Reconnecting":
                        if (!(eventType.equals("CONNECTED"))) break;
                        eventType = "Reconnecting";
                        scs.add(new SociabilityCallStructure(eventType.toUpperCase(), callType,
                                contactName, "", sessionId, info.timeStamp));
                        break;
                    default:
                        break;
                }
            }

            // if video call and call status not available but video still present after ringing ->
            // add accepted / connected / reconnected event to list
            if (callStatusNode==null && callType.equals("VIDEO") && callScreenNode!=null){
                switch (eventType) {
                    // if previous event type is ringing add accepted and connected event to list
                    case "RINGING":
                        scs.add(new SociabilityCallStructure("ACCEPTED", callType, contactName,
                                "", sessionId, info.timeStamp));
                        eventType = "CONNECTED";
                        scs.add(new SociabilityCallStructure(eventType, callType,
                                contactName, "", sessionId, info.timeStamp));
                        break;
                    // if previous event type is connecting / INCOMING add connected event alone
                    case "Connecting":
                    case "INCOMING":
                        eventType = "CONNECTED";
                        scs.add(new SociabilityCallStructure(eventType, callType,
                                contactName, "", sessionId, info.timeStamp));
                        break;
                    // if previous event type is reconnecting add reconnected event alone
                    case "Reconnecting":
                        eventType = "RECONNECTED";
                        scs.add(new SociabilityCallStructure(eventType, callType,
                                contactName, "", sessionId, info.timeStamp));
                        break;
                }
            }

            // determine the status of the calls if the call status is available
            if (callStatusNode!=null && callStatusNode.getText()!=null){
                String statusText = callStatusNode.getText().toString();
                // if status connecting then set event type to accepted and connecting
                if (statusText.equals("Connecting") && !statusText.equals(eventType)){
                    scs.add(new SociabilityCallStructure("ACCEPTED", callType, contactName,
                            "", sessionId, info.timeStamp));
                    eventType = statusText;
                    scs.add(new SociabilityCallStructure(eventType.toUpperCase(), callType,
                            contactName, "", sessionId, info.timeStamp));
                }
                // if status contains timer value mm:ss then
                // set event type to connected for the first instance,
                // add all the other duration along with timestamp to a list to be processed later
                // and if previous status is reconnecting then add reconnected status
                else if (statusText.contains(":")){
                    if (durationList.size()==0)
                        scs.add(new SociabilityCallStructure("CONNECTED", callType,
                                contactName, "", sessionId, info.timeStamp));
                    if (eventType.equals("Reconnecting")) {
                        eventType = "RECONNECTED";
                        scs.add(new SociabilityCallStructure(eventType, callType,
                                contactName, "", sessionId, info.timeStamp));
                    }
                    durationList.add(statusText + "--" + info.timeStamp);
                }
                // if status ringing then set event type to ringing
                else if (statusText.equals("RINGING") && !statusText.equals(eventType)){
                    eventType = statusText;
                    scs.add(new SociabilityCallStructure(eventType, callType,
                            contactName, "", sessionId, info.timeStamp));
                }
                // if status call declined then set event type to rejected
                else if (statusText.equals("Call declined") && !statusText.equals(eventType)){
                    eventType = statusText;
                    scs.add(new SociabilityCallStructure("REJECTED", callType,
                            contactName, "", sessionId, info.timeStamp));
                }
                // if status calling then set event type to calling
                else if (statusText.equals("CALLING") && !statusText.equals(eventType)) {
                    eventType = statusText;
                    scs.add(new SociabilityCallStructure(eventType, callType,
                            contactName, "", sessionId, info.timeStamp));
                }
                // if status not answered then set event type to not answered
                else if (statusText.equals("Not answered") && !statusText.equals(eventType)){
                    eventType = statusText;
                    scs.add(new SociabilityCallStructure("NOTANSWERED", callType,
                            contactName, "", sessionId, info.timeStamp));
                }
                // if status reconnecting then set event type to reconnecting
                else if (statusText.equals("Reconnecting") && !statusText.equals(eventType)){
                    eventType = statusText;
                    scs.add(new SociabilityCallStructure(eventType.toUpperCase(), callType,
                            contactName, "", sessionId, info.timeStamp));
                }
                // if status another call then set event type to busy
                else if (statusText.contains("another call") && !statusText.contains(eventType)){
                    eventType = "another call";
                    scs.add(new SociabilityCallStructure("BUSY", callType,
                            contactName, "", sessionId, info.timeStamp));
                }
            }
        }

        // if video call and last event type is connected / reconnected add ended event and return
        if (callType.equals("VIDEO") &&
                (scs.get(scs.size()-1).callEventType.equals("CONNECTED") ||
                scs.get(scs.size()-1).callEventType.equals("RECONNECTED") ||
                scs.get(scs.size()-1).callEventType.equals("RECONNECTING"))){
            scs.add(new SociabilityCallStructure("ENDED", callType, contactName,
                    "", sessionId, callEventInfos.get(callEventInfos.size()-1).timeStamp));
            return scs;
        }

        // if duration list not empty take the last value as duration along with timestamp
        // and set event type as ended
        if (durationList.size()>0) {
            String durationString = durationList.get(durationList.size()-1);
            String recordedDuration = durationString.split("--")[0];
            String time = durationString.split("--")[1];
            scs.add(new SociabilityCallStructure("ENDED", callType, contactName,
                    recordedDuration, sessionId, Long.valueOf(time)));
        }

        // if only one incoming event is present then treat this as call declined
        if (callEventInfos.size()>0 && scs.size()==1 && scs.get(0).callEventType.equals("INCOMING"))
            scs.add(new SociabilityCallStructure("DECLINED", callType, contactName,
                    "", sessionId, callEventInfos.get(callEventInfos.size()-1).timeStamp));

        // if outgoing call and call aborted before speaking /
        // other user is on another call / not answered add event type as aborted
        if (scs.get(scs.size()-1).callEventType.equals("CALLING") ||
                scs.get(scs.size()-1).callEventType.equals("RINGING") ||
                scs.get(scs.size()-1).callEventType.equals("BUSY") ||
                scs.get(scs.size()-1).callEventType.equals("NOTANSWERED"))
            scs.add(new SociabilityCallStructure("ABORTED", callType, contactName,
                    "", sessionId, callEventInfos.get(callEventInfos.size()-1).timeStamp));

        return scs;
    }

    private static List<SociabilityMessageStructure> handleNotificationReply(
            List<AccessibilityEventInformation> eventInfos) {
        String messageText = "";
        String contactName = "";
        String groupName = "";
        String appname = "";
        boolean isMessageSent = false;
        long time = eventInfos.get(eventInfos.size()-1).timeStamp;
        List<AccessibilityNodeInfo> nodes = SociabilityUtils.preOrderTraverse(
                eventInfos.get(eventInfos.size()-1).root);

        // Get the latest text value from the textbox from the recorded events
        for (int i=eventInfos.size()-1; i>=0; i--){
            AccessibilityNodeInfo textbox = eventInfos.get(i).nodeInfoByViewIds
                    .get(WhatsAppResourceId.NOTIFICATIONTEXTBOX.getId());
            if (textbox!=null && textbox.getText()!=null)
                messageText = textbox.getText().toString();
            if (!messageText.equals("")) break;
        }

        // Check if the last text value is already on the screen
        // so that we can assure message is sent
        // Also get the last contact name the message has sent
        for (AccessibilityNodeInfo node: nodes){
            if (node.getViewIdResourceName()==null) continue;
            if (node.getViewIdResourceName().equals(WhatsAppResourceId.NOTIFICATIONAPPNAME.getId()) &&
                    node.getText()!=null)
                appname = node.getText().toString();
            if (!appname.equals("WhatsApp")) continue;

            // If group name present store it
            if (node.getViewIdResourceName().equals(WhatsAppResourceId.NOTIFICATIONGRPNAME.getId()) &&
                    node.getText()!=null)
                groupName = node.getText().toString();

            // Store the last messaged contact name
            if (node.getViewIdResourceName().equals(WhatsAppResourceId.NOTIFICATIONCONTACTNAME.getId()) &&
                    node.getText()!=null && !node.getText().toString().equals("You"))
                contactName = node.getText().toString();

            // If it is next block reset group and contact name
            if (node.getViewIdResourceName().equals(WhatsAppResourceId.NOTIFICATIONSEPARATOR.getId())){
                groupName = "";
                contactName = "";
            }

            // If message typed found on UI break the loop and return
            if (node.getViewIdResourceName().equals(WhatsAppResourceId.NOTIFICATIONMESSAGETEXT.getId()) &&
                    node.getText()!=null && node.getText().toString().equals(messageText)){
                isMessageSent = true;
                break;
            }
        }

        if (!isMessageSent || messageText.isEmpty()) return new ArrayList<>();
        return new ArrayList<>(Collections.singletonList(new SociabilityMessageStructure(
                messageText, "SENT", contactName, null,
                "TEXT", "NOTIFICATION", groupName, time)));
    }

    private static List<SociabilityMessageStructure> handlePopupMessages(
            List<AccessibilityEventInformation> eventInfos) {
        long time = System.currentTimeMillis();
        String contactName = "";
        String messageText = "";
        String messageType = "TEXT";
        for (AccessibilityEventInformation ei: eventInfos){
            AccessibilityNodeInfo textBox = ei.nodeInfoByViewIds
                    .get(WhatsAppResourceId.TEXTBOX.getId());
            AccessibilityNodeInfo contact = ei.nodeInfoByViewIds
                    .get(WhatsAppResourceId.POPUPCONTACT.getId());
            AccessibilityNodeInfo voiceNote = ei.nodeInfoByViewIds
                    .get(WhatsAppResourceId.POPUPVOICENOTE.getId());
            if (voiceNote!=null && voiceNote.getText()!=null){
                String voiceTimer = voiceNote.getText().toString();
                if (Integer.valueOf(voiceTimer.split(":")[1])>0) {
                    messageText = "";
                    messageType = "VOICERECORDING";
                    contactName = contact.getText().toString();
                    time = ei.timeStamp;
                    break;
                }
            }
            if (textBox==null || textBox.getText()==null) continue;
            messageText = textBox.getText().toString();
            if (contact==null || contact.getText()==null) continue;
            contactName = contact.getText().toString();
            time = ei.timeStamp;
        }

        if (messageText.isEmpty() && messageType.equals("TEXT")) return new ArrayList<>();
        return new ArrayList<>(Collections.singletonList(new SociabilityMessageStructure(
                messageText, "SENT", contactName, null,
                messageType, "", "POPUP", time)));
    }

    private static List<SociabilityMessageStructure> handleChatScreenMessages(
            List<AccessibilityEventInformation> eventInfos) {
        String groupName = "";
        String contactName = "";
        boolean isGroup = false;
        List<SociabilityMessageStructure> bms = new ArrayList<>();
        long time = eventInfos.get(eventInfos.size()-1).timeStamp;

        // Traverse through nodes and check if it is group and also store the contact header
        List<AccessibilityNodeInfo> nodes = SociabilityUtils
                .preOrderTraverse(eventInfos.get(eventInfos.size()-1).root);
        for (AccessibilityNodeInfo n: nodes){
            if (n.getViewIdResourceName()!=null &&
                    n.getViewIdResourceName().equals(WhatsAppResourceId.CONTACTNAME.getId()))
                contactName = n.getText().toString();
            if (n.getContentDescription()!=null &&
                    n.getContentDescription().toString().equals("New group call")){
                isGroup = true;
                break;
            }
            if (n.getClassName()!=null && n.getClassName().toString().contains("list")) break;
        }

        // If group set group name as contact header
        if (isGroup) {
            groupName = contactName;
            contactName = "";
        }

        List<AccessibilityNodeInfo> previousNodes = SociabilityUtils.preOrderTraverse(
                eventInfos.get(eventInfos.size()-2).root);
        List<AccessibilityNodeInfo> newNodes = SociabilityUtils.preOrderTraverse(
                eventInfos.get(eventInfos.size()-2).root);
        List<String> previousMessages = processMessagesFromNodes(previousNodes);
        List<String> newMessages = processMessagesFromNodes(newNodes);

        // Making the two lists equal size by grabbing the last elements
        // Uneven lists are caused by keypad close and open
        int[] lengths = {newMessages.size(), previousMessages.size()};
        Arrays.sort(lengths);
        newMessages = newMessages
                .subList(newMessages.size()-lengths[0], newMessages.size());
        previousMessages = previousMessages
                .subList(previousMessages.size()-lengths[0], previousMessages.size());

        // Get diff list and add diff to HOPES list
        List<String> diffList = getDiffList(newMessages, previousMessages);
        for (String diffMessage: diffList){
            String messageTime = "";
            String messageType = "TEXT";

            String[] message = diffMessage.split(WhatsappUtils.DELIMITER);

            // If group get contact name and message time from message text
            if (isGroup){
                contactName = diffMessage.substring(diffMessage.indexOf("-GRP-")+5);
                messageTime = message[2].substring(0,message[2].indexOf("-GRP-"));
            }

            // set message text and send receive text
            String messageText = message[0];
            String sendReceive = message[1];

            // If not group set message time
            if (messageTime.isEmpty() && message.length>2) messageTime = message[2];

            // If it is group and message is sent then set contact to empty string
            if (isGroup && diffMessage.contains("SENT")) contactName = "";

            // Determine the type of message from the message text
            if(messageText.contains("HOPES_ATTACHMENT-")) {
                messageType = messageText.split("-")[1];
                messageText = "";
            }

            if (messageText.isEmpty() && messageType.equals("TEXT")) continue;
            bms.add(new SociabilityMessageStructure(messageText, sendReceive, contactName,
                    messageTime, messageType, groupName, "FOREGROUND", time));
        }
        return bms;
    }

    public static List<String> processMessagesFromNodes(List<AccessibilityNodeInfo> nodes){
        List<String> messageList = new ArrayList<>();
        boolean appendDate = false;
        boolean isGroup = false;
        String groupContactName = "";
        StringBuilder messageText = new StringBuilder();
        for (AccessibilityNodeInfo node : nodes){
            if (node!=null && node.getContentDescription()!=null &&
                    node.getContentDescription().toString().contains("New group call"))
                isGroup = true;
            if (node!=null && node.getViewIdResourceName()!=null){
                switch(node.getViewIdResourceName()){
                    case "com.whatsapp:id/name_in_group_tv":
                        if (!isGroup) break;
                        groupContactName = node.getText().toString();
                        break;
                    case "com.whatsapp:id/conversation_row_root":
                        if (appendDate) break;
                        messageText.append("HOPES_ATTACHMENT-AUDIO");
                        messageText.append(DELIMITER);
                        messageText.append(checkForIncomingMessage(node));
                        appendDate = true;
                        break;
                    case "com.whatsapp:id/image":
                    case "com.whatsapp:id/button_image":
                        if (appendDate) break;
                        messageText.append("HOPES_ATTACHMENT-IMAGE");
                        messageText.append(DELIMITER);
                        messageText.append(checkForIncomingMessage(node));
                        appendDate = true;
                        break;
                    case "com.whatsapp:id/thumb":
                        if (appendDate) break;
                        messageText.append("HOPES_ATTACHMENT-VIDEO");
                        messageText.append(DELIMITER);
                        messageText.append(checkForIncomingMessage(node));
                        appendDate = true;
                        break;
                    case "com.whatsapp:id/map_frame":
                        if (appendDate) break;
                        messageText.append("HOPES_ATTACHMENT-LOCATION");
                        messageText.append(DELIMITER);
                        messageText.append(checkForIncomingMessage(node));
                        appendDate = true;
                        break;
                    case "com.whatsapp:id/icon":
                        if (appendDate) break;
                        messageText.append("HOPES_ATTACHMENT-FILE");
                        messageText.append(DELIMITER);
                        messageText.append(checkForIncomingMessage(node));
                        appendDate = true;
                        break;
                    case "com.whatsapp:id/contact_card":
                        if (appendDate) break;
                        messageText.append("HOPES_ATTACHMENT-CONTACT");
                        messageText.append(DELIMITER);
                        messageText.append(checkForIncomingMessage(node));
                        appendDate = true;
                        break;
                    case "com.whatsapp:id/message_text":
                        if (appendDate) break;
                        messageText.append(node.getText());
                        messageText.append(DELIMITER);
                        messageText.append(checkForIncomingMessage(node));
                        appendDate = true;
                        break;
                    case "com.whatsapp:id/date":
                        if (!appendDate) break;
                        String sendReceive = messageText.substring(messageText.length()-4);
                        messageText.append(DELIMITER);
                        messageText.append(convertTo24hrFormat((String) node.getText()));
                        if (isGroup){
                            if (sendReceive.equals("SENT")) groupContactName = "SELF";
                            messageText.append("-GRP-").append(groupContactName);
                        }
                        messageList.add(messageText.toString());
                        messageText = new StringBuilder();
                        appendDate = false;
                        break;
                    default:
                        break;
                }
            }
        }
        return messageList;
    }

    private static String checkForIncomingMessage(AccessibilityNodeInfo node){
        Rect rect = new Rect();
        node.getBoundsInScreen(rect);

        // If message node is on the left side of the screen then message is received else sent
        if (rect.left < 100) return "RECV";
        return "SENT";
    }

    public static List<String> getDiffList(List<String> newMessages, List<String> previousMessages){
        String[] diffArray = newMessages.toArray(new String[0]);
        List<String> diffList = new ArrayList<>(Arrays.asList(diffArray));
        String time = getCurrentTimeUsingDate();

        // Find diff between two lists
        for (String toRemove: previousMessages){
            try { diffList.remove(toRemove); } catch (Exception ignored){}
        }

        // When user scrolls up to see old messages then also we get diff
        // so eliminating it by use of current time
        for (Iterator<String> iterator = diffList.iterator(); iterator.hasNext(); ) {
            String value = iterator.next();
            if (!value.contains(time)) {
                iterator.remove();
            }
        }
        return diffList;
    }

    private static String getCurrentTimeUsingDate() {
        Date date = new Date();
        String strDateFormat = "HH:mm";
        DateFormat dateFormat = new SimpleDateFormat(strDateFormat, Locale.ENGLISH);
        return dateFormat.format(date);
    }

    private static String convertTo24hrFormat(String time)
    {
        try{
            if(time.contains("AM") || time.contains("PM"))
                return date24Format.format(date12Format.parse(time));
        }
        catch (Exception e){
            return time;
        }
        return time;
    }
}
