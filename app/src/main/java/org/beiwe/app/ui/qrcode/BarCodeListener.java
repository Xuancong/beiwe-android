package org.beiwe.app.ui.qrcode;


import androidx.annotation.NonNull;

import com.google.mlkit.vision.barcode.Barcode;

import java.util.List;

public interface BarCodeListener {
    void onDetectBarCode(@NonNull List<Barcode> barcodes);
}
