package org.beiwe.app.ui.qrcode;

/*
 * Copyright (C) The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * This file and all BarcodeXXX and CameraXXX files in this project edited by
 * Daniell Algar (included due to copyright reason)
 */

import android.Manifest;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.CommonStatusCodes;
import com.google.mlkit.vision.barcode.Barcode;
import com.moht.hopes.R;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

public final class BarcodeCaptureActivity extends AppCompatActivity
        implements BarCodeListener {

    private static final String TAG = "Barcode-reader";

    // Intent request code to handle updating play services if needed.
    private static final int RC_HANDLE_GMS = 9001;

    // Permission request codes need to be < 256
    private static final int RC_HANDLE_CAMERA_PERM = 2;

    // Constants used to pass extra data in the intent
    public static final String BarcodeObject = "Barcode";

    // Public variable to store scan output temporarily
    public static String scan_result = "";

    private org.beiwe.app.ui.qrcode.CameraSource mCameraSource;
    private org.beiwe.app.ui.qrcode.CameraSourcePreview mPreview;
    public static ImageButton mTorchButton = null;

    /**
     * Initializes the UI and creates the detector pipeline.
     */
    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        setContentView(R.layout.layout_barcode_capture);

		mTorchButton = findViewById(R.id.torchButton);
		mPreview = findViewById(R.id.camera_preview);
		mPreview.mOverlayHolder = ((SurfaceView) findViewById(R.id.camera_overlay)).getHolder();
		mPreview.mOverlayHolder.setFormat(PixelFormat.RGBA_8888);

        // Check for the camera permission before accessing the camera.
        // If the permission is not granted yet, request permission.
        int rc = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        if (rc == PackageManager.PERMISSION_GRANTED)
            createCameraSource(true, false);
        else
            requestCameraPermission();
    }

    /**
     * Implement and pass in this function to handle upon receiving a QR code:
     * - return true to accept result and go back to activity
     * - return false to reject a result and stay in scanning mode
     * - throw an Exception to post an invalid QR code toast message
     */
    public static Callable<Boolean> checkQR = new Callable<Boolean>() {
        public Boolean call() {
            return true;
        }
    };

    @Override
    public void onDetectBarCode(@NonNull List<Barcode> barcodes) {
        try {
            for (Barcode barcode: barcodes) {
                String rawValue = barcode.getRawValue();
                scan_result = rawValue;
                Log.d(TAG, "onDetectBarCode: " + rawValue);
                if (checkQR.call()) {
                    Intent intent = new Intent();
                    intent.putExtra(BarcodeObject, rawValue);
                    setResult(CommonStatusCodes.SUCCESS, intent);
                    finish();
                    return;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "onDetectBarCode: " + e);
            e.printStackTrace();
            runOnUiThread(new Runnable() {
                public void run() {
                    Toast.makeText(CameraSourcePreview.mContext, R.string.barcode_error_format, Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    // Handles the requesting of the camera permission.
    private void requestCameraPermission() {
        final String[] permissions = new String[]{Manifest.permission.CAMERA};
        ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA);
        ActivityCompat.requestPermissions(this, permissions, RC_HANDLE_CAMERA_PERM);
    }

    /**
     * Creates and starts the camera.
     * <p>
     * Suppressing InlinedApi since there is a check that the minimum version is met before using
     * the constant.
     */
    @SuppressLint("InlinedApi")
    private void createCameraSource(boolean autoFocus, boolean useFlash) {
        // Creates and starts the camera.  Note that this uses a higher resolution in comparison
        // to other detection examples to enable the barcode_capture detector to detect small barcodes
        // at long distances.
        CameraSource.Builder builder = new CameraSource.Builder(getApplicationContext(), this)
                .setFacing(CameraSource.CAMERA_FACING_BACK);

        mCameraSource = builder.build();
    }

    // Restarts the camera
    @Override
    protected void onResume() {
        super.onResume();
        startCameraSource();
    }

    // Stops the camera
    @Override
    protected void onPause() {
        super.onPause();
        if (mPreview != null) {
            mPreview.stop();
        }
    }

    /* Releases the resources associated with the camera source, the associated detectors, and the
     * rest of the processing pipeline. */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mPreview != null) {
            mPreview.release();
        }
    }

    /* Callback for the result from requesting permissions. This method
     * is invoked for every call on {@link #requestPermissions(String[], int)}.
     * <strong>Note:</strong> It is possible that the permissions request interaction
     * with the user is interrupted. In this case you will receive empty permissions
     * and results arrays which should be treated as a cancellation.
     *
     * @param requestCode  The request code passed in {@link #requestPermissions(String[], int)}.
     * @param permissions  The requested permissions. Never null.
     * @param grantResults The grant results for the corresponding permissions
     *                     which is either {@link PackageManager#PERMISSION_GRANTED}
     *                     or {@link PackageManager#PERMISSION_DENIED}. Never null.
     * @see #requestPermissions(String[], int) */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        if (requestCode != RC_HANDLE_CAMERA_PERM) {
            Log.d(TAG, "Got unexpected permission result: " + requestCode);
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
            return;
        }

        if (grantResults.length != 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            // we have permission, so create the camera source
            createCameraSource(true, false);
            return;
        }

        Log.e(TAG, "Permission not granted: results len = " + grantResults.length +
                " Result code = " + (grantResults.length > 0 ? grantResults[0] : "(empty)"));

        DialogInterface.OnClickListener listener = (dialog, id) -> finish();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.title_activity_qrcode)
                .setMessage(R.string.no_camera_permission)
                .setPositiveButton(R.string.ok, listener)
                .show();
    }

    /* Starts or restarts the camera source, if it exists.  If the camera source doesn't exist yet
     * (e.g., because onResume was called before the camera source was created), this will be called
     * again when the camera source is created. */
    private void startCameraSource() throws SecurityException {
        // check that the device has play services available<uses-permission android:name="android.permission.CAMERA" />.
        int code = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getApplicationContext());
        if (code != ConnectionResult.SUCCESS)
            GoogleApiAvailability.getInstance().getErrorDialog(this, code, RC_HANDLE_GMS).show();

        if (mCameraSource != null) {
            try {
                mPreview.start(mCameraSource);
            } catch (IOException e) {
                Log.e(TAG, "Unable to start camera source.");
                mCameraSource.release();
                mCameraSource = null;
            }
        }
    }

    public static boolean torchStatus = false;

    @TargetApi(23)
    public synchronized void onToggleTorch(View view) {
        try {
            Camera.Parameters parameters = mCameraSource.mCamera.getParameters();
            parameters.setFlashMode(torchStatus ? Camera.Parameters.FLASH_MODE_OFF : Camera.Parameters.FLASH_MODE_TORCH);
            mCameraSource.mCamera.setParameters(parameters);
            torchStatus = !torchStatus;
            mTorchButton.setImageResource(torchStatus ? R.drawable.torch_on : R.drawable.torch_off);
        } catch (Exception e) {
            Log.e(TAG, "Unable to start torch light.");
        }
    }
}
