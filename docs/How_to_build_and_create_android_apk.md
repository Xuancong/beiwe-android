# How to build HOPES android project

### Step 1: Getting ready with project
- Clone the `HOPES android project`
- `cd <project>` and checkout `moht-android` branch
- Create `private` folder inside the `<project>` folder

### Step 2: Creating key store file
- Create a `ssh public private key pair` and then export it to `PKCS12` file format
- Store the PKCS12 file generated inside the private folder
- Create `keystore.properties` file inside private folder
    > `storePassword=<keystore-password>`
    > `keyPassword=<key-password>`
    > `keyAlias=<key-alias>`
    > `storeFile=<path-to-PKCS12-file>`

### Step 3: Android studio setup
- Open `Android Studio` and click `import project`
- Select the `<project>` folder and wait for the build to finish

### Step 4: Build and generate APK file
- Click `Build -> Generate Signed Bundle / APK`
- Click `APK ->  Next`
    > Module - `app` 
    > Key store path - `<path-to-the-PKCS12-file>` 
    > Key store password - `<keystore-password>` 
    > Key alias - `<key-alias>` 
    > Key password - `<key-password>` 
- Click `Next`
- Select the `required build variant` and click `V2 Full APK Signature`
- Click `Finish`
- The `APK` file can be found inside `app -> <build-type>` folder
