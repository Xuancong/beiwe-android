# How to test upload from client side (HOPES android application)

### Step 1: Install the development app
- Build and generate HOPES APK with build variant as `development`
	> Build variant with `development` will enable the `Debug interface` for the app
- Install the app
- Create a test participant from the server
- Follow the registration steps and register the test participant
- Now the app should be showing the `Debug Interface`

### Step 2: Create and upload files
- Click `Make New Files` on the screen
- Do some taps on the screen as all the files will contains only the header
- Click `Upload Files` on the screen

### Step 3: Verify files upload
- On the status bar, Click `Three dots -> License`
- Scroll to the bottom of the `License` page
- `Long-click` the last text paragraph
- Now the app will show the `Last upload status`
- Make sure there is a successful upload `Last Successful File Upload`
- `Long-click` the last text paragraph again for the upload status to hide
