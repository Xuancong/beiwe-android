# Sociability CSV Structure

## Sociability Messages

### Header
```csv
timestamp, app, contact, orientation, length, minute, type, groupName, useCaseType, received, tz, eventName, patientId
```

#### timestamp
> timestamp of the event processed in milliseconds (epoch time)

#### app
> package name of the app
> Eg: com.whatsapp

#### contact
> hashed contact name of the other person

#### orientation
> Sent -> 0
> Received -> 1

#### length
> message length -> integer
> attachments -> -1

#### minute
> whatsapp chat screen timestamp beside the messages 
> hh:mm format converted to hhmm
> applicable only for messages from chat screen

#### type
> type of message captured
> Voice Recording - 7
> File - 6
> Location - 5
> Contact - 4
> Audio - 3
> Video - 2
> Image - 1
> Text - 0

#### groupName
> hashed group name of the chat if available

#### useCaseType
> Where the message is recorded from
> Popup - 2
> Foreground - 1
> Notification - 0

#### received
> timestamp of the event received in milliseconds (epoch time)

#### tz
> timezone difference in minutes
> 480 (+8:00 * 60 minutes) for Singapore

#### eventName
> event type like wifi / sociability / accel etc.

#### patientId
> participant id of the patient

## Sociability Calls

### Header
```csv
timestamp, app, contact, eventType, callType, recordedDuration, sessionId, tz, eventName, patientId
```

#### timestamp
> timestamp of the event processed in milliseconds (epoch time)

#### app
> package name of the app
> Eg: com.whatsapp

#### contact
> hashed contact name of the other person

#### eventType
> audio and video call event types below
> Missed - 15
> Busy - 14
> Not Answered - 13
> Calling - 12
> Ringing - 11
> Rejected - 10
> Ended - 9
> Reconnected - 8
> Reconnecting - 7
> Connected - 6
> Connecting - 5
> Declined - 4
> Accepted - 3
> Outgoing - 2
> Incoming - 1
> Other type (if any) - 0

#### callType
> video - 2
> audio - 1
> other type (if any) - 0

#### recordedDuration
> call duration from the screen

#### sessionId
> unique id that identifies single session of call with different events

#### tz
> timezone difference in minutes
> 480 (+8:00 * 60 minutes) for Singapore

#### eventName
> event type like wifi / sociability / accel etc.

#### patientId
> participant id of the patient
