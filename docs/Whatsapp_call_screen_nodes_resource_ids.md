# Whatsapp Call Screen Resource Ids

> Important node infos required for screen scraping
> Format: resource_id - class name - text value (if any)

## Screen when we `receive incoming` call: `(Status - INCOMING)`

- contact_photo_layout - android.widget.FrameLayout
- contact_photo - android.widget.ImageView
- whatsapp_icon - android.widget.ImageView
- name - android.widget.TextView => (Contact Name)
- call_status - android.widget.TextView => (INCOMING)
- top_bar_layout - android.widget.RelativeLayout
- call_picture_grid_layout - android.widget.FrameLayout
- profile_picture_overlay - android.view.View
- call_picture_grid - androidx.recyclerview.widget.RecyclerView
- audio_call_participant_photo - android.widget.ImageView
- answer_call_btns - android.widget.LinearLayout
- decline_incoming_call_container - android.widget.FrameLayout
- decline_incoming_call_view - android.widget.ImageView
- accept_incoming_call_container - android.widget.FrameLayout
- accept_incoming_call_view - android.widget.ImageView
- reply_incoming_call_container - android.widget.FrameLayout
- reply_incoming_call_view - android.widget.ImageView
- accept_incoming_call_hint - android.widget.TextView

## Next screen when we `accept call`: `(Status - Connecting)`

- statusBarBackground - android.view.View
- action_bar_root - android.widget.LinearLayout
- content - android.widget.FrameLayout
- call_screen - android.widget.RelativeLayout
- video_participant_views - android.widget.FrameLayout
- call_details - android.widget.LinearLayout
- call_type - android.widget.FrameLayout
- voip_call_label - android.widget.TextView => (WHATSAPP VOICE CALL)
- name - android.widget.TextView => (Contact Name)
- call_status - android.widget.TextView => (Connecting)
- top_bar_layout - android.widget.RelativeLayout
- top_minimize_btn - android.widget.ImageButton
- top_add_participant_btn - android.widget.ImageButton
- call_picture_grid_layout - android.widget.FrameLayout
- call_picture_grid - androidx.recyclerview.widget.RecyclerView
- audio_call_participant_photo - android.widget.ImageView
- call_btns - android.widget.FrameLayout
- end_call_btn - android.widget.ImageButton
- footer - android.widget.LinearLayout
- speaker_btn - android.widget.ImageButton
- toggle_video_btn_layout - android.widget.FrameLayout
- toggle_video_btn - android.widget.ImageButton
- mute_btn - android.widget.ImageButton
- navigationBarBackground - android.view.View

## Next screen when `status changes`: `(Status - 0:01)`

- action_bar_root - android.widget.LinearLayout
- content - android.widget.FrameLayout
- call_screen - android.widget.RelativeLayout
- video_participant_views - android.widget.FrameLayout
- call_details - android.widget.LinearLayout
- call_type - android.widget.FrameLayout
- voip_call_label - android.widget.TextView => (WHATSAPP VOICE CALL)
- name - android.widget.TextView => (Contact Name)
- call_status - android.widget.TextView => (0:01)
- top_bar_layout - android.widget.RelativeLayout
- top_minimize_btn - android.widget.ImageButton
- top_add_participant_btn - android.widget.ImageButton
- call_picture_grid_layout - android.widget.FrameLayout
- call_picture_grid - androidx.recyclerview.widget.RecyclerView
- audio_call_participant_photo - android.widget.ImageView
- call_btns - android.widget.FrameLayout
- end_call_btn - android.widget.ImageButton
- footer - android.widget.LinearLayout
- speaker_btn - android.widget.ImageButton
- toggle_video_btn_layout - android.widget.FrameLayout
- toggle_video_btn - android.widget.ImageButton
- mute_btn - android.widget.ImageButton
- statusBarBackground - android.view.View
- navigationBarBackground - android.view.View
