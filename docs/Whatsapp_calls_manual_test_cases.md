# Whatsapp `Calls` manual test cases

## Scenarios handled in our HOPES application

### `Audio` calls

#### `Outgoing` audio calls

> Call from `contact chat screen`
> Call from `calls` tab
> Call from `call back` button from notification

- Outgoing audio -> calling -> abort
- Outgoing audio -> calling -> ringing -> abort
- Outgoing audio -> calling -> ringing -> accepted -> connecting -> connected -> ended
- Outgoing audio -> calling -> ringing -> accepted -> connecting -> connected -> reconnecting -> reconnected -> ended
- Outgoing audio -> calling -> ringing -> accepted -> connecting -> connected -> reconnecting -> ended
- Outgoing audio -> calling -> ringing -> rejected
- Outgoing audio -> calling -> ringing -> not answered -> aborted
- Outgoing audio -> calling -> ringing -> not answered -> calling -> aborted
- Outgoing audio -> calling -> ringing -> not answered -> calling -> ringing -> aborted
- Outgoing audio -> calling -> ringing -> not answered -> calling -> ringing -> accepted -> connecting -> connected -> ended
- Outgoing audio -> calling -> ringing -> not answered -> calling -> ringing -> accepted -> connecting -> connected -> reconnecting -> reconnected -> ended
- Outgoing audio -> calling -> ringing -> not answered -> calling -> ringing -> accepted -> connecting -> connected -> reconnecting -> ended
- Outgoing audio -> calling -> ringing -> not answered -> calling -> ringing -> rejected
- Outgoing audio -> calling -> ringing -> busy -> aborted

#### `Incoming` audio calls

> Receive call when you are using whatsapp (as notification popup)
> Receive call when you are in home screen
> Receive call when you are in notification tray

- Incoming audio -> decline (from notification popup)
- Incoming audio -> decline (from call screen)
- Incoming audio -> decline (from notification tray)
- Incoming audio -> missed
- Incoming audio -> accepted -> connecting -> connected -> ended
- Incoming audio -> accepted -> connecting -> connected -> reconnecting -> reconnected -> ended
- Incoming audio -> accepted -> connecting -> connected -> reconnecting -> ended

### `Video` calls

#### `Outgoing` video calls

> Call from `contact chat screen`
> Call from `calls` tab
> Call from `call back` button from notification

- Outgoing video -> calling -> abort
- Outgoing video -> calling -> ringing -> abort 
- Outgoing video -> calling -> ringing -> accepted -> connected -> ended
- Outgoing video -> calling -> ringing -> accepted -> connecting -> connected -> reconnecting -> reconnected -> ended
- Outgoing video -> calling -> ringing -> rejected
- Outgoing video -> calling -> ringing -> not answered -> aborted
- Outgoing video -> calling -> ringing -> not answered -> calling -> aborted
- Outgoing video -> calling -> ringing -> not answered -> calling -> ringing -> aborted
- Outgoing video -> calling -> ringing -> not answered -> calling -> ringing -> accepted -> connected -> ended
- Outgoing video -> calling -> ringing -> not answered -> calling -> ringing -> accepted -> connected -> reconnecting -> reconnected -> ended
- Outgoing video -> calling -> ringing -> not answered -> calling -> ringing -> rejected
- Outgoing video -> calling -> ringing -> busy -> aborted

#### `Incoming` video calls:

> Receive call when you are using whatsapp (as notification popup)
> Receive call when you are in home screen
> Receive call when you are in notification tray

- Incoming video -> decline (from notification popup)
- Incoming video -> decline (from call screen)
- Incoming video -> decline (from notification tray)
- Incoming video -> missed
- Incoming video -> accepted -> connecting -> connected -> ended
- Incoming video -> accepted -> connecting -> connected -> reconnecting -> reconnected -> ended

## Scenarios not handled

- Group calling feature
- Switch to video / audio in between the active call
- Add people to the call in between the active call
- Change of whatsapp call screen to background process by pressing home button in between the call
- Incoming video -> accepted -> connecting -> connected -> reconnecting -> ended
- Outgoing video -> calling -> ringing -> accepted -> connecting -> connected -> reconnecting -> ended
- Outgoing video -> calling -> ringing -> not answered -> calling -> ringing -> accepted -> connecting -> connected -> reconnecting -> ended
