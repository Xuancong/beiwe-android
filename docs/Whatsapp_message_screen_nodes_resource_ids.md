# Whatsapp `Message Screen` Resource Ids

> Important node infos required for screen scraping
> Format: resource_id - class name - text value (if any)

## Header Contact Info Area

- action_bar_root - android.widget.FrameLayout
- content - android.widget.FrameLayout
- toolbar - android.view.ViewGroup
- custom_view - android.widget.RelativeLayout
- back - android.widget.LinearLayout
- transition_start - android.view.View
- conversation_contact_photo - android.widget.ImageView
- conversation_contact - android.widget.LinearLayout
- conversation_contact_name - android.widget.TextView
- conversation_layout - android.widget.RelativeLayout
- transition_clipper_top - android.view.View
- attach_anchor - android.view.View
- conversation_background - android.widget.ImageView
- list - android.widget.ListView

## Message List Elements

> Message Type - Type

### Normal - Text Message 

- main_layout - android.widget.LinearLayout
- message_text - android.widget.TextView
- date_wrapper - android.widget.LinearLayout
- date - android.widget.TextView
- status - android.widget.ImageView

### Attachment - Video Message

- forward - android.widget.ImageView
- media_container - android.widget.FrameLayout
- thumb - android.widget.ImageView
- play_frame - android.widget.FrameLayout
- play_button - android.widget.ImageView
- info - android.widget.TextView
- text_and_date - android.widget.FrameLayout
- date_wrapper - android.widget.LinearLayout
- date - android.widget.TextView
- status - android.widget.ImageView

### Attachment - Contact Message

- forward - android.widget.ImageView
- contact_card - android.widget.LinearLayout
- picture - android.widget.ImageView
- vcard_text - android.widget.TextView
- button_div - android.view.View
- msg_contact_btn - android.widget.TextView
- date_wrapper - android.widget.LinearLayout
- date - android.widget.TextView
- status - android.widget.ImageView

### Attachment - Audio Messsage

- forward - android.widget.ImageView
- conversation_row_root - android.widget.LinearLayout
- picture_frame - android.widget.FrameLayout
- picture - android.widget.ImageView
- visualizer_frame - android.widget.FrameLayout
- duration - android.widget.TextView
- icon - android.widget.ImageView
- control_btn_holder - android.widget.FrameLayout
- control_btn - android.widget.ImageButton
- controls - android.widget.FrameLayout
- audio_seekbar - android.widget.SeekBar
- description - android.widget.TextView
- date_wrapper - android.widget.LinearLayout
- date - android.widget.TextView
- status - android.widget.ImageView

### Attachment - File Message

- forward - android.widget.ImageView
- content - android.widget.LinearLayout
- icon - android.widget.ImageView
- title - android.widget.TextView
- file_type - android.widget.TextView
- date_wrapper - android.widget.LinearLayout
- date - android.widget.TextView
- status - android.widget.ImageView

### Attachment - Live Location Message

- content - android.widget.LinearLayout
- map_frame - android.widget.FrameLayout
- thumb_button - android.widget.Button
- thumb - android.widget.ImageView
- balloon_incoming_frame - android.view.View
- map_holder - android.widget.FrameLayout
- contact_thumbnail - android.widget.ImageView
- live_location_info_holder - android.widget.FrameLayout
- live_location_label_holder - android.widget.LinearLayout
- live_location_icon_2 - android.widget.ImageView
- live_location_icon_1 - android.widget.ImageView
- live_location_icon_3 - android.widget.ImageView
- live_location_label - android.widget.TextView
- text_and_date - android.widget.FrameLayout
- live_location_caption - android.widget.TextView
- date_wrapper - android.widget.LinearLayout
- date - android.widget.TextView
- status - android.widget.ImageView
- stop_share_btn - android.widget.TextView

### Attachment - GIF Message

- forward - android.widget.ImageView
- media_container - android.widget.FrameLayout
- thumb - android.widget.ImageView
- control_frame - android.widget.FrameLayout
- button_image - android.widget.ImageView
- text_and_date - android.widget.FrameLayout
- date_wrapper - android.widget.LinearLayout
- date - android.widget.TextView
- status - android.widget.ImageView

### Attachment - Location Message

- forward - android.widget.ImageView
- content - android.widget.LinearLayout
- map_frame - android.widget.FrameLayout
- thumb - android.widget.ImageView
- thumb_button - android.widget.Button
- map_holder - android.widget.FrameLayout
- message_info_holder - android.widget.LinearLayout
- date_wrapper - android.widget.LinearLayout
- date - android.widget.TextView
- status - android.widget.ImageView

### Attachment - Photo Message

- forward - android.widget.ImageView
- media_container - android.widget.FrameLayout
- image - android.widget.ImageView
- text_and_date - android.widget.FrameLayout
- date_wrapper - android.widget.LinearLayout
- date - android.widget.TextView
- status - android.widget.ImageView

## Footer Message Type Area

- transition_clipper_bottom - android.view.View
- footer - android.widget.FrameLayout
- emoji_popup_anchor - android.widget.LinearLayout
- edit_layout - android.widget.RelativeLayout
- text_entry_layout - android.widget.FrameLayout
- voice_note_stub - android.widget.FrameLayout
- input_layout - android.widget.LinearLayout
- input_layout_content - android.widget.LinearLayout
- emoji_picker_btn - android.widget.ImageButton
- entry - android.widget.EditText
- input_attach_button - android.widget.ImageButton
- camera_btn - android.widget.ImageButton
- buttons - android.widget.FrameLayout
- voice_note_btn - android.widget.ImageButton
- send - android.widget.ImageButton
- footer_size - android.view.View
- mention_attach - android.widget.FrameLayout
- statusBarBackground - android.view.View
- navigationBarBackground - android.view.View

# Whatsapp `Popup Window Screen` Resource Ids

## Header Outside Popup

- action_bar_root - android.widget.FrameLayout
- content - android.widget.FrameLayout
- root_layout - android.widget.RelativeLayout

## Normal Message

- popup_thumb - android.widget.ImageView
- popup_title - android.widget.TextView

## Multiple Message Extra Resources

- navigation_divider - android.view.View
- navigation_holder - android.widget.FrameLayout
- prev_btn - android.widget.ImageView
- next_btn - android.widget.ImageView
- popup_count - android.widget.TextView

## Voice Note Message

- voice_note_info - android.widget.TextView
- voice_note_slide_to_cancel - android.widget.TextView

## Common Nodes (For all types of messages)
- conversation_contact_status - android.widget.TextView
- conversation_entry_holder - android.widget.FrameLayout
- message_view_pager - androidx.viewpager.widget.ViewPager
- "" - android.widget.TextView (No resourceId but holds text value)
- footer - android.widget.FrameLayout
- emoji_popup_anchor - android.widget.LinearLayout
- edit_layout - android.widget.RelativeLayout
- text_entry_layout - android.widget.FrameLayout
- input_layout - android.widget.LinearLayout
- input_layout_content - android.widget.LinearLayout
- emoji_picker_btn - android.widget.ImageButton
- entry - android.widget.EditText
- voice_note_stub - android.widget.FrameLayout
- voice_note_clipping_layout - android.widget.FrameLayout
- voice_note_cancel_layout - android.widget.LinearLayout
- buttons - android.widget.FrameLayout
- voice_note_btn - android.widget.ImageButton
- popup_ok_btn - android.widget.Button
- popup_action_btn - android.widget.Button

## Footer

- navigationBarBackground - android.view.View
