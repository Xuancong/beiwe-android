# Whatsapp `Messages` manual test cases

## Whatsapp `Incoming` messages

### Scenarios handled in our HOPES application

- Receive normal `notification` with `single text` message
- Receive normal `notification` with `single attachment`
- Receive `popup` window notification with `single text` message
- Receive `popup` window notifcation with `single attachment`
- Receive `conversation list screen` (screen which lists contacts with message count) notification with `single text` message
- Receive `onversation list screen` notification with `single attachment`
- `Contact chat screen` (screen where to type and send messages) receives `single text` message `without` keyboard layout
- `Contact chat screen` receives `single text` message `with` keyboard layout
- `Contact chat screen` receives `single attachment` `without` keyboard layout
- `Contact chat screen` receives `single attachment` `with` keyboard layout
- `Clear all` notification then `receive new` notification

### Scenarios not handled:

- Go to `contact chat screen` then `scroll up` and then `receive` message
- `Burst` notifications or `Multiple` notifications at same time
- Screen full of `same message` (Normal/Attachment) and receive `same message` again on `same minute`
	> More frequently happens with Image attachment. Screen is full of images and if you receive a image again there is no diff so can not capture

## Whatsapp `Outgoing` messages:

### Scenarios handled in our HOPES application

- Go to `contact chat screen` and then send `single text` message `with` keyboard layout open
- Go to `contact chat screen` and then send `single attachment` `with` keyboard layout open
- Go to `contact chat screen` and then send `single forward text` message `without` keyboard layout
- Go to `contact chat screen` and then send `single forward attachment` `without` keyboard layout
- From `popup` window with `single` notification send `single text` message
- From `popup` window with `single` notification send `single voice note`
- From `popup` window with `multiple` notification send `single text` message
- From `popup` window with `multiple` notification send `single voice note`
- From `popup` window with `multiple` notification send a `voice note` after sending a `text` message

### Scenarios not handled:

- Go to `contact chat screen` and then scroll up and now `send` a (Normal/attachment) message
- Go to `contact chat screen` and `forward group of attachments`
- Screen full of `same message` (Normal/Attachment) and send `same message` again on `same minute`
